/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.Placement;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public abstract class UIContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.UIContainer>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.basic.ModelElementConverter#onConvert(com.gs.gapp.language.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T uiContainer) {
		super.onConvert(element, uiContainer);

		// --- container title
		String title =
		    element.getOptionValueSettingsReader().getTextOptionValue(UiOptionEnum.TITLE.getName());
		if (title != null) {
			uiContainer.setTitle(title);
		}

		// --- action containers
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.TOOLBARS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	com.gs.gapp.metamodel.ui.container.UIActionContainer actionContainer =
    					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIActionContainer.class,
    							                       reference.getReferencedObject());
            	if (actionContainer != null) {
            		OptionEnumerationEntry toolbarPlacement = reference.getReferenceEnumerationValue();
            		Placement placement = null;
            		if (toolbarPlacement != null && toolbarPlacement.getEntry() != null) {
            			placement = Placement.valueOf(toolbarPlacement.getEntry().toUpperCase());
            		}
            		uiContainer.addActionContainer(actionContainer, placement);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to an action container, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
		
		// --- optional
		Boolean optional =
			    element.getOptionValueSettingsReader().getBooleanOptionValue(UiOptionEnum.OPTIONAL.getName());
		if (optional != null) {
			uiContainer.setOptional(optional);
		}
		
		// --- disablable
		Boolean disablable =
			    element.getOptionValueSettingsReader().getBooleanOptionValue(UiOptionEnum.DISABLABLE.getName());
		if (disablable != null) {
			uiContainer.setDisablable(disablable);
		}
		
		// --- consumed entities
		EList<OptionValueReference> optionValueConsumedEntitiesReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.CONSUMED_ENTITIES.getName());
		
		if (optionValueConsumedEntitiesReferences != null) {
			for (OptionValueReference optionValueReference : optionValueConsumedEntitiesReferences) {
				ModelElement referencedEntityElement = optionValueReference.getReferencedObject();
				Entity entity = this.convertWithOtherConverter(Entity.class, referencedEntityElement);
				if (entity != null) {
					uiContainer.addConsumedEntity(entity);
				}
			}
		}
		
		// --- configuration items
		OptionValueReference optionValueReference = element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.CONFIGURATION_ITEMS.getName());
		if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
			Enumeration enumeration = this.convertWithOtherConverter(Enumeration.class, optionValueReference.getReferencedObject());
			if (enumeration != null) {
			    uiContainer.setConfigurationItems(enumeration);
			} else {
				ComplexType enumerationItemsType = this.convertWithOtherConverter(ComplexType.class, optionValueReference.getReferencedObject());
				uiContainer.setConfigurationItems(enumerationItemsType);
			}
			
			if (uiContainer.getConfigurationItems() == null) {
				addError("Found a modeled configuration item element with name '" + optionValueReference.getReferencedObject().getName() + "' (option:" + UiOptionEnum.CONFIGURATION_ITEMS.getName()
						+ ") but could not make use of it since it is neither an enumeration nor a complex type."
						+ "A possible root cause for this is when you model a type that has no fields at all since such a type is interpreted as a primitive type only."
						+ "If this is the case, try to add a field to that type and generate again.");
			}
		}
		
		// --- init parameters
		OptionValueReference optionValueReferenceForInitParams = element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.INIT_PARAMETERS.getName());
		if (optionValueReferenceForInitParams != null && optionValueReferenceForInitParams.getReferencedObject() != null) {
		    ComplexType initParamsType = this.convertWithOtherConverter(ComplexType.class, optionValueReferenceForInitParams.getReferencedObject());
		    if (initParamsType != null) {
		        uiContainer.setInitParameters(initParamsType);
		    } else {
		    	addError("Found a modeled init parameters element with name '" + optionValueReferenceForInitParams.getReferencedObject().getName() + "' (option:" + UiOptionEnum.INIT_PARAMETERS.getName()
						+ ") but could not make use of it since it is not a complex type."
						+ "A possible root cause for this is when you model a type that has no fields at all since such a type is interpreted as a primitive type only."
						+ "If this is the case, try to add a field to that type and generate again.");
		    }
		}
		
		// --- layout variants
		OptionValueReference optionValueReferenceForLayoutVariants = element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.LAYOUT_VARIANTS.getName());
		if (optionValueReferenceForLayoutVariants != null && optionValueReferenceForLayoutVariants.getReferencedObject() != null) {
		    Enumeration layoutVariantsEnumeration = this.convertWithOtherConverter(Enumeration.class, optionValueReferenceForLayoutVariants.getReferencedObject());
		    if (layoutVariantsEnumeration == null) {
		    	addError("found a modeled option '" + UiOptionEnum.LAYOUT_VARIANTS.getName() + "' for ui container '" + uiContainer.getName() + "', but it seems as if it isn't an enumeration that is set for this option (" + element.toString() + ")");
		    }
		    uiContainer.setLayoutVariants(layoutVariantsEnumeration);
		}
		
		// --- test prelude parameters
		optionValueReference = element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.TEST_PRELUDE_PARAMETERS.getName());
		if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
			ComplexType testPreludeParametersType = this.convertWithOtherConverter(ComplexType.class, optionValueReference.getReferencedObject());
			if (testPreludeParametersType != null) {
			    uiContainer.setTestPreludeParameters(testPreludeParametersType);
			}
		}
		
		// --- test prelude views
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.TEST_PRELUDE_VIEWS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
                UIStructuralContainer structuralContainer =
                		this.convertWithOtherConverter(UIStructuralContainer.class, reference.getReferencedObject());
            	
            	if (structuralContainer != null) {
            		uiContainer.addTestPreludeView(structuralContainer);
            	} else {
            		throw new ModelConverterException("not able to create a UiStructuralContainer object for given referenced element '" + reference.getReferencedObject() + "'");
            	}
            }
		}
		
		// --- services
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.SERVICES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	ServiceInterface serviceInterface = this.convertWithOtherConverter(ServiceInterface.class, reference.getReferencedObject());
            	
            	if (serviceInterface != null) {
            		uiContainer.addConsumedServiceInterface(serviceInterface);
            	} else {
            		throw new ModelConverterException("not able to create a ServiceInterface object for given referenced element '" + reference.getReferencedObject() + "'");
            	}
            }
		}
		
		// --- dynamic structure
		Boolean dynamicStructure =
			    element.getOptionValueSettingsReader().getBooleanOptionValue(UiOptionEnum.DYNAMIC_STRUCTURE.getName());
		if (dynamicStructure != null) {
			uiContainer.setDynamicStructure(dynamicStructure);
		}
		
		// --- finally, convert all element members (here we handle fields only)
		for (ElementMember member : element.getElementMembers()) {
			@SuppressWarnings("unused")
			com.gs.gapp.metamodel.basic.typesystem.Field field =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Field.class,
							                       member, uiContainer);
		}
		
		// --- message keys
		List<String> messageKeys = element.getOptionValueSettingsReader().getTextOptionValues(UiOptionEnum.MESSAGES.getName());
		if (messageKeys != null) {
			for (String messageKey : messageKeys) {
				uiContainer.getMessageKeys().add(messageKey);
			}
		}
		List<String> warningMessageKeys = element.getOptionValueSettingsReader().getTextOptionValues(UiOptionEnum.WARNING_MESSAGES.getName());
		if (warningMessageKeys != null) {
			for (String messageKey : warningMessageKeys) {
				uiContainer.getMessageKeysForWarnings().add(messageKey);
			}
		}
		List<String> errorMessageKeys = element.getOptionValueSettingsReader().getTextOptionValues(UiOptionEnum.ERROR_MESSAGES.getName());
		if (errorMessageKeys != null) {
			for (String messageKey : errorMessageKeys) {
				uiContainer.getMessageKeysForErrors().add(messageKey);
			}
		}
	}

	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.CONTAINER;
	}
}
