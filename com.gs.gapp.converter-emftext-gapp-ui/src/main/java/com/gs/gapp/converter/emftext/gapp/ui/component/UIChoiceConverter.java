/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ChoiceTypeEnum;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.MultiplicityEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Multiplicity;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.component.UIChoice;

/**
 * @author mmt
 *
 */
public class UIChoiceConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIChoice>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIChoiceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T uiChoice) {
		super.onConvert(elementMember, uiChoice);

		// --- editable
		Boolean editable =
		    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(UiComponentsOptionEnum.EDITABLE.getName());
		if (editable != null) {
			uiChoice.setEditable(editable);
		}

		// --- preselected
		Boolean preselected =
		    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(UiComponentsOptionEnum.PRESELECTED.getName());
		if (preselected != null) {
			uiChoice.setPreselected(preselected);
		}

		// --- container
		OptionValueReference optionValueReference =
				elementMember.getOptionValueReferencesReader().getOptionValueReference(UiComponentsOptionEnum.CONTAINER_FOR_CHOICE.getName());
		if (optionValueReference != null) {
			com.gs.gapp.metamodel.ui.container.UIContainer container =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIContainer.class,
							                       optionValueReference.getReferencedObject());
			if (container != null) {
				uiChoice.setContainer(container);
			} else {
				throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}

		// --- multiplicity
		String multiplicity =
			elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.MULTIPLICITY.getName());
		if (multiplicity != null) {
			MultiplicityEnum multiplicityEnumEntry = MultiplicityEnum.getByName(multiplicity);
			if (multiplicityEnumEntry != null) {
				Multiplicity result = null;
                switch (multiplicityEnumEntry) {
				case MULTI_VALUED:
					result = Multiplicity.MULTI_VALUED;
					break;
				case SINGLE_VALUED:
					result = Multiplicity.SINGLE_VALUED;
					break;
				default:
					throw new ModelConverterException("found a multiplicity that was not handled though it should be", multiplicityEnumEntry);

                }
                uiChoice.setMultiplicity(result);
			} else {
				throw new ModelConverterException("invalid multiplicity found in model", multiplicity);
			}
		}
		
		// --- choice type
		String choiceTypeString =
			elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.CHOICE_TYPE.getName());
		if (choiceTypeString != null) {
			ChoiceTypeEnum choiceTypeEnumEntry = ChoiceTypeEnum.getByName(choiceTypeString);
			if (choiceTypeEnumEntry != null) {
				UIChoice.Type result = null;
                switch (choiceTypeEnumEntry) {
				case BUTTON:
					result = UIChoice.Type.BUTTON;
					break;
				case CHECK:
					result = UIChoice.Type.CHECK;
					break;
				case DROP_DOWN:
					result = UIChoice.Type.DROP_DOWN;
					break;
				case LIST:
					result = UIChoice.Type.LIST;
					break;
				case AUTO_COMPLETE:
					result = UIChoice.Type.AUTO_COMPLETE;
					break;
				case PICK_LIST:
					result = UIChoice.Type.PICK_LIST;
					if (uiChoice.getMultiplicity() == null) {
					    uiChoice.setMultiplicity(Multiplicity.MULTI_VALUED);  // It is assumed that there is no SINGLE_VALUED form of a pick list. (mmt 27-Jun-2019)
					} else if (uiChoice.getMultiplicity() == Multiplicity.SINGLE_VALUED) {
						addError("Choice component '" + uiChoice.getName() + "' in display '" + uiChoice.getOwner().getName() + "' is of type " + UIChoice.Type.PICK_LIST + " but has multiplicity set to SINGLE_VALUED. " +
								UIChoice.Type.PICK_LIST + " is has to be MULTI_VALUED. Please correct the model.");
					}
					break;
				default:
					throw new ModelConverterException("found a choice type that was not handled though it should be", choiceTypeEnumEntry);

                }
                uiChoice.setType(result);
			} else {
				throw new ModelConverterException("invalid choice type found in model", choiceTypeString);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T uiChoice = (T) new com.gs.gapp.metamodel.ui.component.UIChoice(elementMember.getName());
		uiChoice.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return uiChoice;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.CHOICE;
	}
}
