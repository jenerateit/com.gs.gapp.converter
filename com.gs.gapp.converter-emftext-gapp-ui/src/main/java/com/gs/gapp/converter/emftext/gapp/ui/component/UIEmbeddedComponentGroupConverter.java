/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.OrientationEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Orientation;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIEmbeddedComponentGroupConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIEmbeddedComponentGroup>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIEmbeddedComponentGroupConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T componentGroup) {
		super.onConvert(elementMember, componentGroup);
		
		// --- orientation
		String orientation =
			    elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.ORIENTATION.getName());

		if (orientation == null || orientation.length() == 0) {
			// no orientation set
		} else {
			OrientationEnum orientationEnumEntry = OrientationEnum.getByName(orientation);
			if (orientationEnumEntry != null) {
                switch (orientationEnumEntry) {
				case LEFT_TO_RIGHT:
					componentGroup.setOrientation(Orientation.LEFT_TO_RIGHT);
					break;
				case TOP_TO_BOTTOM:
					componentGroup.setOrientation(Orientation.TOP_TO_BOTTOM);
					break;
				default:
					throw new ModelConverterException("unhandled orientation", orientationEnumEntry);
                }
			} else {
				throw new ModelConverterException("invalid orientation found in model", orientation);
			}
		}

		// --- components in group
		OptionValueReference linkedDisplay = elementMember.getOptionValueReferencesReader().getOptionValueReference(UiComponentsOptionEnum.DISPLAY.getName());
		if (linkedDisplay != null) {
			Element displayElement = (Element) linkedDisplay.getReferencedObject();
			UIDataContainer dataContainer = convertWithOtherConverter(UIDataContainer.class, displayElement);
			componentGroup.setDataContainer(dataContainer);
		}
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.EMBEDDED_COMPONENT_GROUP;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T componentGroup =
				(T) new com.gs.gapp.metamodel.ui.component.UIEmbeddedComponentGroup(elementMember.getName());
		componentGroup.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return componentGroup;
	}
}
