/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIActionContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.UIActionContainer> extends
		UIContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIActionContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/**
	 * @param originalModelElement
	 * @param resultingModelElement
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- action components
		for (ElementMember member : originalModelElement.getElementMembers()) {
			com.gs.gapp.metamodel.ui.component.UIActionComponent actionComponent =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.component.UIActionComponent.class, member);
        	if (actionComponent != null) {
        		resultingModelElement.addActionComponent(actionComponent);
        	} else {
        		throw new ModelConverterException("not successfully converted a component to an action component, result was null", new GappModelElementWrapper(member));
        	}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.TOOLBAR;
	}

	/**
	 * @param originalModelElement
	 * @return
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.container.UIActionContainer(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
