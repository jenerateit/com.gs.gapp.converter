/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIRangeConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIRange>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIRangeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S modelElement, T range) {
		super.onConvert(modelElement, range);

		// TODO decide whether we need int or float type for the following values (mmt 28-Jun-2012)

		// --- lower
		Long lower =
	        modelElement.getOptionValueSettingsReader().getNumericOptionValue(UiComponentsOptionEnum.LOWER.getName());
		if (lower != null) {
			range.setLower((int)lower.longValue());
		}

		// --- upper
		Long upper =
	        modelElement.getOptionValueSettingsReader().getNumericOptionValue(UiComponentsOptionEnum.UPPER.getName());
		if (upper != null) {
			range.setUpper((int)upper.longValue());
		}

		// --- stepping
		Long stepping =
	        modelElement.getOptionValueSettingsReader().getNumericOptionValue(UiComponentsOptionEnum.STEPPING.getName());
		if (stepping != null) {
			range.setStepping((int)stepping.longValue());
		}
		
		// --- length
		Long length =
		        modelElement.getOptionValueSettingsReader().getNumericOptionValue(UiComponentsOptionEnum.LENGTH.getName());
		if (length != null) {
			range.setLength((int)length.longValue());
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result =
				(T) new com.gs.gapp.metamodel.ui.component.UIRange(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.RANGE;
	}

}
