/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container.data;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.DisplayContainerTypeEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIGridContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.data.UIGridContainer> extends
		UIDataContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIGridContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);

	}

	/**
	 * @param element
	 * @param gridContainer
	 */
    @Override
	protected void onConvert(S element, T gridContainer) {
		super.onConvert(element, gridContainer);
		
		// --- number of rows per page
        Long numberOfRowsPerPage = element.getOptionValueSettingsReader().getNumericOptionValue(UiOptionEnum.NUMBER_OF_ROWS_PER_PAGE.getName());
        if (numberOfRowsPerPage != null) {
        	gridContainer.setNumberOfRowsPerPage(numberOfRowsPerPage.intValue());
        }
	}

    /* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
            @SuppressWarnings("unchecked")
			S element = (S) originalModelElement;
            String displayType =
            		element.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.TYPE_FOR_DISPLAY.getName());
            if (displayType != null && displayType.length() > 0) {
            	DisplayContainerTypeEnum displayTypeEnumEntry = DisplayContainerTypeEnum.getByName(displayType);
            	if (displayTypeEnumEntry != null) {
            		if (displayTypeEnumEntry != DisplayContainerTypeEnum.GRID) {
            			// only if the type is LIST we create a list data container
            			result = false;
            	    }
            	} else {
            		throw new ModelConverterException("invalid display type modeled", displayType);
            	}
            } else {
            	result = false;
            }
		}

		return result;
	}

	/**
	 * @param element
	 * @return
	 */
    @Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T gridContainer = (T) new com.gs.gapp.metamodel.ui.container.data.UIGridContainer(element.getName());
		gridContainer.setOriginatingElement(new GappModelElementWrapper(element));
		return gridContainer;
	}
}
