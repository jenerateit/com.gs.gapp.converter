/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.LayoutContainerTypeEnum;
import com.gs.gapp.dsl.ui.OrientationEnum;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Orientation;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.container.StructuralContainerType;

/**
 * @author mmt
 *
 */
public class UIStructuralContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.UIStructuralContainer> extends
		UIContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIStructuralContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/**
	 * @param originalModelElement
	 * @param uiStructuralContainer
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T uiStructuralContainer) {

		super.onConvert(originalModelElement, uiStructuralContainer);

		// --- orientation
		String orientation =
		    originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.ORIENTATION.getName());

		if (orientation == null || orientation.length() == 0) {
			// no orientation set
		} else {
			OrientationEnum orientationEnumEntry = OrientationEnum.getByName(orientation);
			if (orientationEnumEntry != null) {
                switch (orientationEnumEntry) {
				case LEFT_TO_RIGHT:
					uiStructuralContainer.setOrientation(Orientation.LEFT_TO_RIGHT);
					break;
				case TOP_TO_BOTTOM:
					uiStructuralContainer.setOrientation(Orientation.TOP_TO_BOTTOM);
					break;
				default:
					throw new ModelConverterException("unhandled orientation", orientationEnumEntry);
                }
			} else {
				throw new ModelConverterException("invalid orientation found in model", orientation);
			}
		}
		
		// --- custom title
		Boolean customTitle =
			    originalModelElement.getOptionValueSettingsReader().getBooleanOptionValue(UiOptionEnum.CUSTOM_TITLE.getName());
		if (customTitle != null) {
			uiStructuralContainer.setCustomTitle(customTitle);
		}

		// --- type
		String type =
			    originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.TYPE_FOR_LAYOUT.getName());

		if (type == null || type.length() == 0) {
			// no orientation set
		} else {
			LayoutContainerTypeEnum layoutTypeEnumEntry = LayoutContainerTypeEnum.getByName(type);
			if (layoutTypeEnumEntry != null) {
                switch (layoutTypeEnumEntry) {
				case SPLIT:
					uiStructuralContainer.setType(StructuralContainerType.SPLIT);
					break;
				case TAB:
					uiStructuralContainer.setType(StructuralContainerType.TAB);
					break;
				case WIZARD:
					uiStructuralContainer.setType(StructuralContainerType.WIZARD);
					break;
				default:
					throw new ModelConverterException("unhandled layout container type", layoutTypeEnumEntry);
                }
			} else {
				throw new ModelConverterException("invalid layout container type found in model", type);
			}
		}

		// --- children
		EList<OptionValueReference> optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.CHILDREN.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	com.gs.gapp.metamodel.ui.container.UIContainer container =
    					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIContainer.class,
    							                       reference.getReferencedObject());
            	if (container != null) {
            		uiStructuralContainer.addChildContainer(container);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.LAYOUT;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.ui.container.UIContainerConverter#onCreateModelElement(com.gs.gapp.language.ui.container.UIContainer)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.container.UIStructuralContainer(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
