/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container.data;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.DisplayContainerTypeEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UICustomContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.data.UICustomContainer> extends
		UIDataContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UICustomContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);

	}


	/**
	 * @param originalModelElement
	 * @param resultingModelElement
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
            @SuppressWarnings("unchecked")
			S element = (S) originalModelElement;
            String displayType =
            		element.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.TYPE_FOR_DISPLAY.getName());
            if (displayType != null && displayType.length() > 0) {
            	DisplayContainerTypeEnum displayTypeEnumEntry = DisplayContainerTypeEnum.getByName(displayType);
            	if (displayTypeEnumEntry != null) {
            		if (displayTypeEnumEntry != DisplayContainerTypeEnum.CUSTOM) {
            			// only if the type is CUSTOM we are going to convert to a custom data container
            			result = false;
            	    }
            	} else {
            		throw new ModelConverterException("invalid display type modeled", displayType);
            	}
            } else {
            	result = false;
            }
		}

		return result;
	}


	/**
	 * @param originalModelElement
	 * @return
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.container.data.UICustomContainer(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
