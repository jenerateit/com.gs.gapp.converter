/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
@Deprecated
public class UIActionGroupConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIActionGroup>
    extends UIActionComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIActionGroupConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- an action group's action components
		/* TODO check whether an action group component would make sense, for the time being it is disabled (mmt 07-May-2013)
		for (UIActionComponent languageActionComponent : originalModelElement.getActionComponents()) {
			com.gs.gapp.metamodel.ui.component.UIActionComponent actionComponent =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.component.UIActionComponent.class,
							                       languageActionComponent, new Class<?>[0]);
		    resultingModelElement.addActionComponent(actionComponent);
		    actionComponent.addGroup(resultingModelElement);
		}
		*/
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.component.UIActionGroup(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return (T) result;
	}
}
