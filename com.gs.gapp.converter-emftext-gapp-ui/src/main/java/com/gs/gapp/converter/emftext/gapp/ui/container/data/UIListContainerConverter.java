/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container.data;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.DisplayContainerTypeEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.container.SelectionModeEnum;

/**
 * @author mmt
 *
 */
public class UIListContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.data.UIListContainer> extends
		UIDataContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIListContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);

	}

	/**
	 * @param element
	 * @param listContainer
	 */
    @Override
	protected void onConvert(S element, T listContainer) {
		super.onConvert(element, listContainer);
		
		String selectionMode =
        		element.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.SELECTION_MODE.getName());
		if (selectionMode != null && selectionMode.length() > 0) {
        	SelectionModeEnum selectionModeEnumEntry = SelectionModeEnum.fromString(selectionMode);
        	listContainer.setSelectionMode(selectionModeEnumEntry);
        } else {
        	listContainer.setSelectionMode(SelectionModeEnum.NONE);  // that's the default setting, if nothing is specified
        }
		
		// --- number of rows per page
        Long numberOfRowsPerPage = element.getOptionValueSettingsReader().getNumericOptionValue(UiOptionEnum.NUMBER_OF_ROWS_PER_PAGE.getName());
        if (numberOfRowsPerPage != null) {
        	listContainer.setNumberOfRowsPerPage(numberOfRowsPerPage.intValue());
        }
	}

    /* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
            @SuppressWarnings("unchecked")
			S element = (S) originalModelElement;
            String displayType =
            		element.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.TYPE_FOR_DISPLAY.getName());
            if (displayType != null && displayType.length() > 0) {
            	DisplayContainerTypeEnum displayTypeEnumEntry = DisplayContainerTypeEnum.getByName(displayType);
            	if (displayTypeEnumEntry != null) {
            		if (displayTypeEnumEntry != DisplayContainerTypeEnum.LIST) {
            			// only if the type is LIST we create a list data container
            			result = false;
            	    }
            	} else {
            		throw new ModelConverterException("invalid display type modeled", displayType);
            	}
            } else {
            	result = false;
            }
		}

		return result;
	}

	/**
	 * @param element
	 * @return
	 */
    @Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T listContainer = (T) new com.gs.gapp.metamodel.ui.container.data.UIListContainer(element.getName());
		listContainer.setOriginatingElement(new GappModelElementWrapper(element));
		return listContainer;
	}
}
