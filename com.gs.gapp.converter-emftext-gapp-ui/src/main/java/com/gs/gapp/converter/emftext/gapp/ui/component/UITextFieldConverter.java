/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UITextFieldConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UITextField>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UITextFieldConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- password-field, or more generally: secret content
		Boolean secretContent =
		    originalModelElement.getOptionValueSettingsReader().getBooleanOptionValue(UiComponentsOptionEnum.PASSWORD.getName());
		if (secretContent != null) {
			resultingModelElement.setSecretContent(secretContent);
		}
		
		// --- input mask
		String inputMask =
			originalModelElement.getOptionValueSettingsReader().getTextOptionValue(UiComponentsOptionEnum.INPUT_MASK.getName());
		if (inputMask != null) {
			resultingModelElement.setInputMask(inputMask);
		}
		
		// --- length
		Long length =
		        originalModelElement.getOptionValueSettingsReader().getNumericOptionValue(UiComponentsOptionEnum.LENGTH.getName());
		if (length != null) {
			resultingModelElement.setLength((int)length.longValue());
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.ui.component.UIComponentConverter#getComponentType()
	 */
	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.TEXT_FIELD;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result =
				(T) new com.gs.gapp.metamodel.ui.component.UITextField(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}

}
