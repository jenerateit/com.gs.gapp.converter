/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.ActionPurpose;

/**
 * @author mmt
 *
 */
public class UIActionComponentConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIActionComponent>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIActionComponentConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T actionComponent) {

		super.onConvert(element, actionComponent);

		// --- has icon
		Boolean hasIcon =
		    element.getOptionValueSettingsReader().getBooleanOptionValue(UiComponentsOptionEnum.HAS_ICON.getName());
		if (hasIcon != null) {
			actionComponent.setHasIcon(hasIcon);
		}

		// --- container flows are handled in the container flow converter

		// --- action groups are handled in the action group converter
		
		// --- long lasting
		Boolean longLasting =
		    element.getOptionValueSettingsReader().getBooleanOptionValue(UiComponentsOptionEnum.LONG_LASTING.getName());
		if (longLasting != null) {
			actionComponent.setLongLasting(longLasting);
		}
		
		// --- action purpose
		String actionPurpose =
			element.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.ACTION_PURPOSE.getName());
		if (actionPurpose != null && actionPurpose.length() > 0) {
			ActionPurpose actionPurposeEnumEntry = ActionPurpose.valueOf(actionPurpose.toUpperCase());
			if (actionPurposeEnumEntry != null) {
                actionComponent.setActionPurpose(actionPurposeEnumEntry);
			} else {
				throw new ModelConverterException("invalid action purpose found in model (for action component '" + actionComponent.getName() + "')", actionPurpose);
			}
		}
		
		// --- default action
		Boolean defaultAction =
		    element.getOptionValueSettingsReader().getBooleanOptionValue(UiComponentsOptionEnum.DEFAULT_ACTION.getName());
		if (defaultAction != null) {
			actionComponent.setDefaultAction(defaultAction);
		}
		
		// --- keyboard control
		String keyboardControl =
		    element.getOptionValueSettingsReader().getTextOptionValue(UiComponentsOptionEnum.KEYBOARD_CONTROL.getName());
		if (keyboardControl != null) {
			actionComponent.setKeyboardControl(keyboardControl);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.component.UIActionComponent(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.ACTION;
	}
}
