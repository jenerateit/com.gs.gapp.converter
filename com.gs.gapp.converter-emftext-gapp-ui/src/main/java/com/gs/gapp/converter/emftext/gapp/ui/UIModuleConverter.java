/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui;

import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.Namespace;



/**
 * @author mmt
 *
 */
public class UIModuleConverter<S extends Module, T extends com.gs.gapp.metamodel.ui.UIModule>
    extends ModuleConverter<S, T> {

	public UIModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T uiModule) {

		super.onConvert(module, uiModule);


		// --- namespace
		Namespace namespace = new Namespace(module.getNamespace().getName());
		uiModule.setNamespace(namespace);
		uiModule.addElement(namespace);

		// TODO no imports, no persistence modules, no action components and no container groups to be converted here - this was needed in a previous version of the EMFText modeling tool (mmt 07-May-2013)
		// --- imports, ui modules
        // --- persistence modules
		// --- action components
		// --- container groups

		for (Element moduleElement : module.getElements()) {

			// --- action containers
			com.gs.gapp.metamodel.ui.container.UIActionContainer actionContainer =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIActionContainer.class, moduleElement);
			if (actionContainer != null) {
				uiModule.addElement(actionContainer);
	            namespace.addContainer(actionContainer);
	            continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}

			// --- data containers
			com.gs.gapp.metamodel.ui.container.data.UIDataContainer dataContainer =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.data.UIDataContainer.class, moduleElement);
			if (dataContainer != null) {
				uiModule.addElement(dataContainer);
	            namespace.addContainer(dataContainer);
	            continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}

			// --- structural containers
			com.gs.gapp.metamodel.ui.container.UIStructuralContainer structuralContainer =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIStructuralContainer.class, moduleElement);
			if (structuralContainer != null) {
				uiModule.addElement(structuralContainer);
				namespace.addContainer(structuralContainer);
				continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}

			// --- container flows
			com.gs.gapp.metamodel.ui.ContainerFlow containerFlow =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.ContainerFlow.class, moduleElement);
			if (containerFlow != null) {
    			uiModule.addElement(containerFlow);
    			continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}

			// --- storages
			com.gs.gapp.metamodel.ui.databinding.LocalStorage localStorage =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.databinding.LocalStorage.class, moduleElement);
			if (localStorage != null) {
    			uiModule.addElement(localStorage);
    			continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}

			com.gs.gapp.metamodel.ui.databinding.RemoteStorage remoteStorage =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.databinding.RemoteStorage.class, moduleElement);
			if (remoteStorage != null) {
    			uiModule.addElement(remoteStorage);
    			continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.hasElementsOfTypes( new BasicEList<>(UiElementEnum.getElementTypeNames()) ) == false) {
					// only if the module has at least one ui element type, it is recognized as a ui module
					result = false;
				}
            }
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.UIModule(module.getName());
		result.setOriginatingElement(new GappModelElementWrapper(module));
		return result;
	}
}
