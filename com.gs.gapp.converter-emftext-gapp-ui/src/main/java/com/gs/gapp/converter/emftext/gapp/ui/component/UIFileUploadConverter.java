/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIFileUploadConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIFileUpload>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIFileUploadConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T fileUpload) {
		super.onConvert(elementMember, fileUpload);

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.ui.component.UIComponentConverter#getComponentType()
	 */
	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.FILE_UPLOAD;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T fileUpload =
				(T) new com.gs.gapp.metamodel.ui.component.UIFileUpload(elementMember.getName());
		fileUpload.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return fileUpload;
	}

}
