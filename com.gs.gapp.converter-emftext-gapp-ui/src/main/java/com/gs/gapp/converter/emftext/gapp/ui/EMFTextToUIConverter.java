/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui;

import java.util.List;

import com.gs.gapp.converter.emftext.gapp.function.EMFTextToFunctionConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIActionComponentConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIBooleanChoiceConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIButtonComponentConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIChartConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIChoiceConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIDateSelectorConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIEmbeddedComponentGroupConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIEmbeddedContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIFileUploadConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIHTMLConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIImageConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UILabelConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UILinkComponentConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIMapConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIRangeConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UITextAreaConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UITextFieldConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UITimelineConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UITreeConverter;
import com.gs.gapp.converter.emftext.gapp.ui.component.UIVideoConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.UIActionContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.UIMenuContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.UIStructuralContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.UIViewContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.data.UICustomContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.data.UIDataContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.data.UIGridContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.data.UIListContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.container.data.UITreeTableContainerConverter;
import com.gs.gapp.converter.emftext.gapp.ui.databinding.ContainerGroupConverter;
import com.gs.gapp.converter.emftext.gapp.ui.databinding.FunctionUsageConverter;
import com.gs.gapp.converter.emftext.gapp.ui.databinding.LocalStorageConverter;
import com.gs.gapp.converter.emftext.gapp.ui.databinding.RemoteStorageConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class EMFTextToUIConverter extends EMFTextToFunctionConverter {

	/**
	 * 
	 */
	public EMFTextToUIConverter() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- ui
		result.add(new ContainerFlowConverter<>(this));
		result.add(new UIModuleConverter<>(this));

		// --- containers
		result.add(new UIActionContainerConverter<>(this));
		result.add(new UIStructuralContainerConverter<>(this));
		result.add(new UIMenuContainerConverter<>(this));
		result.add(new UIViewContainerConverter<>(this));

		// --- data containers
		result.add(new UICustomContainerConverter<>(this));
		result.add(new UIDataContainerConverter<>(this));
		result.add(new UIListContainerConverter<>(this));
		result.add(new UIGridContainerConverter<>(this));
		result.add(new UITreeTableContainerConverter<>(this));

		// --- components
//		result.add(new UIComponentGroupConverter<com.gs.gapp.language.ui.component.UIComponentGroup, UIComponentGroup>(this));
//		result.add(new UIActionGroupConverter<com.gs.gapp.language.ui.component.UIActionGroup, UIActionGroup>(this));
//		result.add(new UICustomComponentConverter<com.gs.gapp.language.gapp.ElementMember, UICustomComponent>(this));

		result.add(new UIActionComponentConverter<>(this));
		result.add(new UIButtonComponentConverter<>(this));
		result.add(new UILinkComponentConverter<>(this));
		result.add(new UIBooleanChoiceConverter<>(this));
		result.add(new UIChoiceConverter<>(this));

		result.add(new UIDateSelectorConverter<>(this));
		result.add(new UIEmbeddedContainerConverter<>(this));
		result.add(new UIHTMLConverter<>(this));
		result.add(new UILabelConverter<>(this));
		result.add(new UIMapConverter<>(this));
		result.add(new UIRangeConverter<>(this));
		result.add(new UITextAreaConverter<>(this));
		result.add(new UITextFieldConverter<>(this));
		
		result.add(new UIImageConverter<>(this));
		result.add(new UIVideoConverter<>(this));
		result.add(new UIFileUploadConverter<>(this));
		result.add(new UIEmbeddedComponentGroupConverter<>(this));
		
		result.add(new UITimelineConverter<>(this));
		result.add(new UITreeConverter<>(this));
		result.add(new UIChartConverter<>(this));

		// --- databinding
		result.add(new ContainerGroupConverter<>(this));
		result.add(new LocalStorageConverter<>(this));
		result.add(new RemoteStorageConverter<>(this));
		result.add(new FunctionUsageConverter<>(this));
		
		return result;
	}
}
