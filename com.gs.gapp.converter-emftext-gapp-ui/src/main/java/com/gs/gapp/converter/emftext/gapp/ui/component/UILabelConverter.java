/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.component.UILabel;
import com.gs.gapp.metamodel.ui.component.UILabel.Format;

/**
 * @author mmt
 *
 */
public class UILabelConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UILabel>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UILabelConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T label) {
		super.onConvert(elementMember, label);

		// --- format
		String formatString =
				elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.FORMAT.getName());
		if (formatString != null) {
			Format formatEnumEntry = UILabel.Format.get(formatString);
			if (formatEnumEntry != null) {
				label.setFormat(formatEnumEntry);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result =
				(T) new com.gs.gapp.metamodel.ui.component.UILabel(elementMember.getName());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.LABEL;
	}

}
