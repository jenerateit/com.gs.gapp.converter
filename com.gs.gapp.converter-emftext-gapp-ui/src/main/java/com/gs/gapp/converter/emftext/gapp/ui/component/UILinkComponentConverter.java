package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

public class UILinkComponentConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UILinkComponent>
    extends UIActionComponentConverter<S, T> {

	public UILinkComponentConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T actionComponent) {
		super.onConvert(element, actionComponent);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.component.UILinkComponent(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return (T) result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.LINK;
	}
}
