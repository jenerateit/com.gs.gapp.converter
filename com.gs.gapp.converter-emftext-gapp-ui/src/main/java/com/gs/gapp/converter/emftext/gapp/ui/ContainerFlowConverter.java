/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.FlowTypeEnum;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;

/**
 * @author mmt
 *
 */
public class ContainerFlowConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.ContainerFlow>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ContainerFlowConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- flow type
		String flowType =
		    originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.TYPE_FOR_FLOW.getName());

		if (flowType == null || flowType.length() == 0) {
			// no flow type set
		} else {
			FlowTypeEnum flowTypeEnumEntry = FlowTypeEnum.getByName(flowType);
			if (flowTypeEnumEntry != null) {
				switch (flowTypeEnumEntry) {
				case OPEN:
					resultingModelElement.setType(com.gs.gapp.metamodel.ui.ContainerFlowType.OPEN);
					break;
				case PUSH:
					resultingModelElement.setType(com.gs.gapp.metamodel.ui.ContainerFlowType.PUSH);
					break;
				case REPLACE:
					resultingModelElement.setType(com.gs.gapp.metamodel.ui.ContainerFlowType.REPLACE);
					break;
				default:
					throw new ModelConverterException("unhandled flow type found", flowTypeEnumEntry);
				}
			}
		}

		// --- source container
		OptionValueReference optionValueReference =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.FROM.getName());
		if (optionValueReference != null) {
			com.gs.gapp.metamodel.ui.container.UIContainer container =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIContainer.class,
							                       optionValueReference.getReferencedObject());
			if (container != null) {
				resultingModelElement.setSource(container);
			} else {
				throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}

		// --- target container
		optionValueReference =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.TO.getName());
		if (optionValueReference != null) {
			com.gs.gapp.metamodel.ui.container.UIContainer container =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIContainer.class,
							                       optionValueReference.getReferencedObject());
			if (container != null) {
				resultingModelElement.setTarget(container);
			} else {
				throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}

		// --- action components
		EList<OptionValueReference> optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.COMPONENTS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	com.gs.gapp.metamodel.ui.component.UIComponent component =
    					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.component.UIComponent.class,
    							                       reference.getReferencedObject());
            	if (component != null) {
                    if (component instanceof UIActionComponent) {
                    	UIActionComponent uiActionComponent = (UIActionComponent) component;
						resultingModelElement.addActionComponent(uiActionComponent);
            			uiActionComponent.addFlow(resultingModelElement);
                    } else {
                    	// TODO how should we handle non-action-components? (mmt 07-May-2013)
                    }
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a ui component, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.FLOW;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.ContainerFlow(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
