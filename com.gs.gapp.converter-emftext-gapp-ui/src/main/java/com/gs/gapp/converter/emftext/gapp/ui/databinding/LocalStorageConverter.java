/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.databinding;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.StorageTypeEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class LocalStorageConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.databinding.LocalStorage>
    extends StorageConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public LocalStorageConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
            @SuppressWarnings("unchecked")
			S element = (S) originalModelElement;
            String storageType =
            		element.getOptionValueSettingsReader().getEnumeratedOptionValue(UiOptionEnum.TYPE_FOR_STORAGE.getName());
            if (storageType != null && storageType.length() > 0) {
            	StorageTypeEnum storageTypeEnumEntry = StorageTypeEnum.getByName(storageType);
            	if (storageTypeEnumEntry != null) {
            		if (storageTypeEnumEntry != StorageTypeEnum.LOCAL) {
            			// if the type is given, it has to be LOCAL to be converted with this converter
            			result = false;
            	    }
            	} else {
            		throw new ModelConverterException("invalid storage type modeled", storageType);
            	}
            } else {
            	// if nothing is specified, we are assuming _local_ storage, this is not an error
            }
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.databinding.LocalStorage(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return (T) result;
	}

}
