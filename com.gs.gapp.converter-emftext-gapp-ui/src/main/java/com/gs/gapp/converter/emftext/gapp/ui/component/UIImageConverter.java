/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.ImageTypeEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.component.UIImage.ImageType;

/**
 * @author mmt
 *
 */
public class UIImageConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIImage>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIImageConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T image) {
		super.onConvert(elementMember, image);
		
		// --- image type
		ImageType imageType = null;
		String imageTypeString = elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.IMAGE_TYPE.getName());
		if (imageTypeString != null) {
			ImageTypeEnum imageTypeEnum = ImageTypeEnum.getByName(imageTypeString);
			if (imageTypeEnum != null) {
				imageType = ImageType.valueOf(imageTypeEnum.name());
			}
			if (imageType == null) {
				throw new ModelConverterException("could not determine the image type for the option value '" + imageTypeString + "'");
			}
		} else {
			imageType = ImageType.ANY;
		}
		
		image.setImageType(imageType);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.ui.component.UIComponentConverter#getComponentType()
	 */
	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.IMAGE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T image =
				(T) new com.gs.gapp.metamodel.ui.component.UIImage(elementMember.getName());
		image.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return image;
	}

}
