/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container.data;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.ui.container.UIContainerConverter;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.FilterModeEnum;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIDataContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.data.UIDataContainer> extends
		UIContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIDataContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);

	}

	/**
	 * @param element
	 * @param uiDataContainer
	 */
	@Override
	protected void onConvert(
			S element,
			T uiDataContainer) {

		super.onConvert(element, uiDataContainer);

		// --- entity
		OptionValueReference optionValueReference =
		    element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.ENTITY_FOR_DISPLAY.getName());
		if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
			Entity entity =
					this.convertWithOtherConverter(Entity.class,
							                       optionValueReference.getReferencedObject());
			if (entity != null) {
				uiDataContainer.setEntity(entity);
			} else {
				throw new ModelConverterException("not successfully converted an entity for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}
		
		// --- function
		optionValueReference =
		    element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.FUNCTION_FOR_DISPLAY.getName());
		if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
			Function function=
					this.convertWithOtherConverter(Function.class,
							                       optionValueReference.getReferencedObject());
			if (function != null) {
				uiDataContainer.setFunction(function);
			} else {
				throw new ModelConverterException("not successfully converted a function for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}

		// --- components
		for (ElementMember member : element.getElementMembers()) {
			com.gs.gapp.metamodel.ui.component.UIComponent component =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.component.UIComponent.class, member);
        	if (component != null) {
        		uiDataContainer.addComponent(component);
        	} else {
        		// since we do not only model components for containers, we cannot handle this case as an error (mmt 17-Aug-2018)
        	}
		}

		// --- interactive  TODO interactive flag is not available in modeling (mmt 07-May-2013)
//		resultingModelElement.setInteractive(?);
		
		// --- sort by
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.SORT_BY.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	UIComponent componentForSortBy =
    					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.component.UIComponent.class,
    							                       reference.getReferencedObject());
    			if (componentForSortBy != null) {
    				uiDataContainer.addSortBy(componentForSortBy);
    			} else {
    				throw new ModelConverterException("not successfully converted a component for the setting of 'sortBy', result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
    			}
            }
		}
		
		// --- filter by
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.FILTER_BY.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	UIDataContainer.FilterMode filterMode = UIDataContainer.FilterMode.CONTAINS;
            	OptionEnumerationEntry filterModeOptionEnum = reference.getReferenceEnumerationValue();
            	if (filterModeOptionEnum != null) {
            		FilterModeEnum filterModeEnum = FilterModeEnum.getByName(filterModeOptionEnum.getEntry());
            		filterMode = UIDataContainer.FilterMode.valueOf(filterModeEnum.name());  // this can be done since the enum entries have identical names
            	}
            	
            	UIComponent componentForFilterBy =
    					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.component.UIComponent.class,
    							                       reference.getReferencedObject());
    			if (componentForFilterBy != null && filterMode != null) {
    				uiDataContainer.addFilterBy(componentForFilterBy, filterMode);
    			} else {
    				if (componentForFilterBy != null) {
    				    throw new ModelConverterException("not successfully converted a component for the setting of 'filterBy', result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
    				} else if (filterMode != null) {
    					throw new ModelConverterException("no valid filter mode identified, filter mode is null", new GappModelElementWrapper(filterModeOptionEnum));
    				}
    			}
            }
		}
		
		// --- COLLAPSIBLE, when not set, the default value is "false"
		Boolean collapsible =
			    element.getOptionValueSettingsReader().getBooleanOptionValue(UiOptionEnum.COLLAPSIBLE.getName());
		if (collapsible != null) {
			uiDataContainer.setCollapsible(collapsible.booleanValue());
		}
	}

	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.DISPLAY;
	}

	/**
	 * @param element
	 * @return
	 */
	@Override
	protected T onCreateModelElement(
			S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.container.data.UIDataContainer(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
