/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIEmbeddedContainerConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIEmbeddedContainer>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIEmbeddedContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- container
		OptionValueReference optionValueReference =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReference(UiComponentsOptionEnum.CONTAINER_FOR_EMBEDDED_CONTAINER.getName());
		if (optionValueReference != null) {
			com.gs.gapp.metamodel.ui.container.UIContainer container =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIContainer.class,
							                       optionValueReference.getReferencedObject());
			if (container != null) {
				resultingModelElement.setContainer(container);
			} else {
				throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result =
				(T) new com.gs.gapp.metamodel.ui.component.UIEmbeddedContainer(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.EMBEDDED_CONTAINER;
	}
}
