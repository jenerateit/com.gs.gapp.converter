/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.databinding;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.ui.UiUtil;

/**
 * @author mmt
 *
 */
public class FunctionUsageConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.databinding.FunctionUsage>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public FunctionUsageConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- function modules
		EList<OptionValueReference> optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.MODULES_FOR_INTERFACES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	FunctionModule functionModule = this.convertWithOtherConverter(FunctionModule.class, reference.getReferencedObject());
            	if (functionModule != null) {
            		resultingModelElement.addUsedFunctionModule(functionModule);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a function module, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}


		// --- functions
		Set<com.gs.gapp.metamodel.function.Function> functions =
				new LinkedHashSet<>();

		optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.FUNCTIONS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Function function = this.convertWithOtherConverter(Function.class, reference.getReferencedObject());
            	if (function != null) {
            		functions.add(function);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a function, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}

		// --- function groups  -> meanwhile function group and function module is the same thing (mmt 07-May-2013)



		// normalize the selectively modeled functions by creating new function module for them
		for (com.gs.gapp.metamodel.function.FunctionModule newFunctionModule : UiUtil.createNewFunctionModules(null, functions)) {
			resultingModelElement.addUsedFunctionModule(newFunctionModule);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.INTERFACES;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.databinding.FunctionUsage(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
