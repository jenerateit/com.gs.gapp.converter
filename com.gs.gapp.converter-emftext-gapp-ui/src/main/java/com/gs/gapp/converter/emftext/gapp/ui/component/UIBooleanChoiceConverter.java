/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.BooleanChoiceTypeEnum;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.component.UIBooleanChoice;

/**
 * @author mmt
 *
 */
public class UIBooleanChoiceConverter<S extends ElementMember, T extends UIBooleanChoice>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIBooleanChoiceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- boolean choice type
		String booleanChoiceTypeString =
			originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.BOOLEAN_CHOICE_TYPE.getName());
		if (booleanChoiceTypeString != null) {
			BooleanChoiceTypeEnum booleanChoiceTypeEnumEntry = BooleanChoiceTypeEnum.getByName(booleanChoiceTypeString);
			if (booleanChoiceTypeEnumEntry != null) {
				UIBooleanChoice.Type result = null;
                switch (booleanChoiceTypeEnumEntry) {
				case BUTTON:
					result = UIBooleanChoice.Type.BUTTON;
					break;
				case CHECK:
					result = UIBooleanChoice.Type.CHECK;
					break;
				case SWITCH:
					result = UIBooleanChoice.Type.SWITCH;
					break;
				default:
					throw new ModelConverterException("found a choice type that was not handled though it should be", booleanChoiceTypeEnumEntry);

                }
                resultingModelElement.setType(result);
			} else {
				throw new ModelConverterException("invalid choice type found in model", booleanChoiceTypeString);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.component.UIBooleanChoice(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.BOOLEAN_CHOICE;
	}
}
