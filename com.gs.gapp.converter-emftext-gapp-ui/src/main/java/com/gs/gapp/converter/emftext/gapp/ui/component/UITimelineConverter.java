/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UITimelineConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UITimeline>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UITimelineConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember,
			T timeline) {

		super.onConvert(elementMember, timeline);

		// --- nothing special to convert here
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result =
				(T) new com.gs.gapp.metamodel.ui.component.UITimeline(elementMember.getName());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.TIMELINE;
	}

}
