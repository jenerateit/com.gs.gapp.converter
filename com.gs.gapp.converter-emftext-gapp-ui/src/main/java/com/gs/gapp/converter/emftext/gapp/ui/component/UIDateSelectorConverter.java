/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.TemporalTypeEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIDateSelectorConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIDateSelector>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIDateSelectorConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S modelElement, T dateSelector) {
		super.onConvert(modelElement, dateSelector);

		// --- temporal type
		String temporalType =
		    modelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.TYPE.getName());
		if (temporalType != null) {
			TemporalTypeEnum temporalTypeEnumEntry = TemporalTypeEnum.getByName(temporalType);
			if (temporalTypeEnumEntry != null) {
				com.gs.gapp.metamodel.basic.TemporalType result = null;
                switch (temporalTypeEnumEntry) {
				case DATE:
					result = com.gs.gapp.metamodel.basic.TemporalType.DATE;
					break;
				case DATETIME:
					result = com.gs.gapp.metamodel.basic.TemporalType.DATETIME;
					break;
				case TIME:
					result = com.gs.gapp.metamodel.basic.TemporalType.TIME;
					break;
				case TIMESTAMP:
					result = com.gs.gapp.metamodel.basic.TemporalType.DATETIME;  // TODO how shall we handle timestamp? (mmt 07-May-2013)
					break;
				default:
					throw new ModelConverterException("found a temporal type that was not handled though it should", temporalTypeEnumEntry);

                }
                dateSelector.setType(result);
			} else {
				throw new ModelConverterException("invalid temporal type found in model", temporalType);
			}
		}
		
		// --- length
		Long length =
		        modelElement.getOptionValueSettingsReader().getNumericOptionValue(UiComponentsOptionEnum.LENGTH.getName());
		if (length != null) {
			dateSelector.setLength((int)length.longValue());
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.component.UIDateSelector(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.DATE_SELECTOR;
	}
}
