/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UIMenuContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.UIMenuContainer> extends
		UIActionContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIMenuContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/**
	 * @param element
	 * @param menuContainer
	 */
	@Override
	protected void onConvert(S element, T menuContainer) {
		super.onConvert(element, menuContainer);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.MENU;
	}

	/**
	 * @param element
	 * @return
	 */
	@Override
	protected T onCreateModelElement(
			S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.container.UIMenuContainer(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
