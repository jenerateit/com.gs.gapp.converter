/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.MultiplicityEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Multiplicity;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class UITreeConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UITree>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UITreeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T tree) {

		super.onConvert(elementMember, tree);

		// --- multiplicity
		String multiplicity =
			elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.MULTIPLICITY.getName());
		if (multiplicity != null) {
			MultiplicityEnum multiplicityEnumEntry = MultiplicityEnum.getByName(multiplicity);
			if (multiplicityEnumEntry != null) {
				Multiplicity result = null;
                switch (multiplicityEnumEntry) {
				case MULTI_VALUED:
					result = Multiplicity.MULTI_VALUED;
					break;
				case SINGLE_VALUED:
					result = Multiplicity.SINGLE_VALUED;
					break;
				default:
					throw new ModelConverterException("found a multiplicity that was not handled though it should be", multiplicityEnumEntry);

                }
                tree.setMultiplicity(result);
			} else {
				throw new ModelConverterException("invalid multiplicity found in model", multiplicity);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result =
				(T) new com.gs.gapp.metamodel.ui.component.UITree(elementMember.getName());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.TREE;
	}

}
