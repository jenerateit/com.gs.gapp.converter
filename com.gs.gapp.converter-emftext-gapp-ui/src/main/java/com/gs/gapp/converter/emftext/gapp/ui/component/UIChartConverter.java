/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.ui.ChartTypeEnum;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.component.UIChart;

/**
 * @author mmt
 *
 */
public class UIChartConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIChart>
    extends UIComponentConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIChartConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T uiChart) {
		super.onConvert(elementMember, uiChart);

		// --- chart type
		String chartTypeString =
			elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(UiComponentsOptionEnum.CHART_TYPE.getName());
		if (chartTypeString != null) {
			ChartTypeEnum chartTypeEnumEntry = ChartTypeEnum.getByName(chartTypeString);
			if (chartTypeEnumEntry != null) {
				UIChart.Type result = null;
				switch (chartTypeEnumEntry) {
				case BAR:
				    result = UIChart.Type.BAR;
					break;
				case BUBBLE:
					result = UIChart.Type.BUBBLE;
					break;
				case DONUT:
					result = UIChart.Type.DONUT;
					break;
				case LINE:
					result = UIChart.Type.LINE;
					break;
				case PIE:
					result = UIChart.Type.PIE;
					break;
				case POLAR_AREA:
					result = UIChart.Type.POLAR_AREA;
					break;
				case RADAR:
					result = UIChart.Type.RADAR;
					break;
				case SCATTER:
					result = UIChart.Type.SCATTER;
					break;
				default:
					throw new ModelConverterException("found a chart type that was not handled though it should be", chartTypeEnumEntry);
				}
				uiChart.setType(result);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T uiChart = (T) new com.gs.gapp.metamodel.ui.component.UIChart(elementMember.getName());
		uiChart.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return uiChart;
	}

	@Override
	ComponentsEnum getComponentType() {
		return ComponentsEnum.CHART;
	}
}
