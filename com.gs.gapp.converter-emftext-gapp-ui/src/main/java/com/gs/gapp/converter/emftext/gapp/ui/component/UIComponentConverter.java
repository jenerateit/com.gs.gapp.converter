/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.component;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.ComponentsEnum;
import com.gs.gapp.dsl.ui.FlowTypeEnum;
import com.gs.gapp.dsl.ui.UiComponentsOptionEnum;
import com.gs.gapp.dsl.ui.UiMemberEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.ui.CustomMessageType;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public abstract class UIComponentConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.ui.component.UIComponent>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIComponentConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T uiComponent) {
		super.onConvert(elementMember, uiComponent);

		convertOptions(elementMember, uiComponent);
		
		// --- entity fields for display
		EList<OptionValueReference> optionValueReferences = elementMember.getOptionValueReferencesReader().getOptionValueReferences(UiComponentsOptionEnum.ENTITY_FIELD_FOR_DISPLAY.getName());
	
		if (optionValueReferences != null && optionValueReferences.size() > 0) {
			for (OptionValueReference anOptionValueReference : optionValueReferences) {
				final ElementMember rawEntityField = ElementMember.class.cast(anOptionValueReference.getReferencedObject());
				final Element rawEntity = Element.class.cast(rawEntityField.eContainer().eContainer());
				final Entity entity = this.convertWithOtherConverter(Entity.class, rawEntity);
				final EntityField entityField = entity.getField(rawEntityField.getName());
	
				if (entityField == null) {
					throw new ModelConverterException("not successfully converted an entity field for displaying a value in the user interface, result was null", new GappModelElementWrapper(anOptionValueReference.getReferencedObject()));
	
				} else {
					uiComponent.addEntityFieldForDisplay(entityField);
				}
			}
		}
		
		// --- message keys
		List<String> messageKeys = elementMember.getOptionValueSettingsReader().getTextOptionValues(UiOptionEnum.MESSAGES.getName());
		if (messageKeys != null) {
			for (String messageKey : messageKeys) {
				uiComponent.getMessageKeys().add(messageKey);
			}
		}
		List<String> warningMessageKeys = elementMember.getOptionValueSettingsReader().getTextOptionValues(UiOptionEnum.WARNING_MESSAGES.getName());
		if (warningMessageKeys != null) {
			for (String messageKey : warningMessageKeys) {
				uiComponent.getMessageKeysForWarnings().add(messageKey);
			}
		}
		List<String> errorMessageKeys = elementMember.getOptionValueSettingsReader().getTextOptionValues(UiOptionEnum.ERROR_MESSAGES.getName());
		if (errorMessageKeys != null) {
			for (String messageKey : errorMessageKeys) {
				uiComponent.getMessageKeysForErrors().add(messageKey);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (!isOfType(((S)originalModelElement), getComponentType())) {
				generalResponsibility = false;
			}
		}

		return generalResponsibility;
	}

	protected boolean isOfType(ElementMember member, ComponentsEnum componentEnum) {
		if (member == null) throw new NullPointerException("parameter '" + member + "' must not be null");
		
		boolean result = false;
		if (member.getMemberType() != null) {
		    result = componentEnum == null ? false : member.getMemberType().getName().equalsIgnoreCase(componentEnum.getName());
		} else {
			// it is a member that does not have a component type set => try to make a good guess
			
			EntityField mappedEntityField = null;
			for (UiComponentsOptionEnum option : UiComponentsOptionEnum.getMemberOptions(UiMemberEnum.COMPONENT)) {
				OptionValueReference optionValueReference = null;
				switch (option) {
				case ENTITY_FIELD:
					optionValueReference =
					    member.getOptionValueReferencesReader().getOptionValueReference(option.getName());
					if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
						final ElementMember rawEntityField = ElementMember.class.cast(optionValueReference.getReferencedObject());
						final Element rawEntity = Element.class.cast(rawEntityField.eContainer().eContainer());
						final Entity entity = this.convertWithOtherConverter(Entity.class, rawEntity);
						mappedEntityField = entity.getField(rawEntityField.getName());

						if (mappedEntityField == null) {
							throw new ModelConverterException("not successfully converted an entity field for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
						} else {
							Type typeOfEntityField = mappedEntityField.getType();
							if (typeOfEntityField != null) {
								switch (componentEnum) {
								case ACTION:
									break;
								case ACTION_COMPONENT:
									break;
								case BOOLEAN_CHOICE:
									if (typeOfEntityField == PrimitiveTypeEnum.UINT1.getPrimitiveType()) {
										// default component for a boolean field is a boolean choice component
										result = true;
									}
									break;
								case BUTTON:
									break;
								case CHOICE:
									if (typeOfEntityField instanceof Entity) {
										// default component for an entity field with an entity type is a choice component, to be able to select an entity instance
										result = true;
									}
									break;
								case COMPONENT:
									break;
								case DATE_SELECTOR:
									if (typeOfEntityField == PrimitiveTypeEnum.DATE.getPrimitiveType() ||
									    typeOfEntityField == PrimitiveTypeEnum.DATETIME.getPrimitiveType() ||
									    typeOfEntityField == PrimitiveTypeEnum.TIME.getPrimitiveType()) {
										// default component for a Date or Datetime field is a date selector component
										result = true;
									}
									break;
								case EMBEDDED_COMPONENT_GROUP:
									break;
								case EMBEDDED_CONTAINER:
									break;
								case FILE_UPLOAD:
									break;
								case HTML:
									break;
								case IMAGE:
									break;
								case LABEL:
									break;
								case LINK:
									break;
								case MAP:
									break;
								case RANGE:
									break;
								case TEXT_AREA:
									break;
								case TEXT_FIELD:
									if (typeOfEntityField == PrimitiveTypeEnum.STRING.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.SINT8.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.SINT16.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.SINT32.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.UINT8.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.UINT16.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.UINT32.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.FLOAT16.getPrimitiveType() ||
										typeOfEntityField == PrimitiveTypeEnum.FLOAT32.getPrimitiveType()) {
										// default component for a string or number field (integer, long, float, double) is a text field component
										result = true;
									}
									break;
								case VIDEO:
								case CHART:
								case TIMELINE:
								case TREE:
									break;
								default:
									throw new ModelConverterException("unhandled enumeration entry " + componentEnum);
								}
							}
						}
					}
					break;
				default:
					// ignore everything else
					break;
				}
			}  // for all options of the modeled ui component
			
			if (result == false && mappedEntityField == null && componentEnum == ComponentsEnum.TEXT_FIELD) {
				result = true;  // no component type is modeled and there is no entity field mapped => choose a text field as the default ui component 
			}
		}
		
		return result;
	}

	/**
	 * @return
	 */
	abstract ComponentsEnum getComponentType();


	protected void convertOptions(S elementMember, T uiComponent) {

		// --- member options
		for (UiComponentsOptionEnum option : UiComponentsOptionEnum.getMemberOptions(UiMemberEnum.COMPONENT)) {
			OptionValueReference optionValueReference = null;
			switch (option) {
			case ENTITY_FIELD:
				optionValueReference =
				    elementMember.getOptionValueReferencesReader().getOptionValueReference(option.getName());
				if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
					final ElementMember rawEntityField = ElementMember.class.cast(optionValueReference.getReferencedObject());
					final Element rawEntity = Element.class.cast(rawEntityField.eContainer().eContainer());
					final Entity entity = this.convertWithOtherConverter(Entity.class, rawEntity);
					final EntityField entityField = entity.getField(rawEntityField.getName());

					if (entityField == null) {
						throw new ModelConverterException("not successfully converted an entity field for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));

					} else {
						uiComponent.setEntityField(entityField);
						if (!entityField.isNullable()) {
							uiComponent.setDataEntryRequired(true);
						}
					}
				}
				break;
			case LABEL:
				String label =
				    elementMember.getOptionValueSettingsReader().getTextOptionValue(option.getName());
				if (label != null) {
					uiComponent.setLabel(label);
				}
				break;
				
			case OPTIONAL:
				Boolean optional =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (optional != null) {
					uiComponent.setOptional(optional);
				}
				break;
				
			case DISABLABLE:
				Boolean disablable =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (disablable != null) {
					uiComponent.setDisablable(disablable);
				}
				break;
				
			case READ_ONLY:
				Boolean readOnly =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (readOnly != null) {
					uiComponent.setReadOnly(readOnly);
				}
				break;
				
			case HANDLE_SELECTION_EVENT:
				Boolean handleSelectionEvent =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (handleSelectionEvent != null) {
					uiComponent.setHandleSelectionEvent(handleSelectionEvent);
				}
				break;
				
			case WITHOUT_LABEL:
				Boolean withoutLabel =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (withoutLabel != null) {
					uiComponent.setWithoutLabel(withoutLabel);
				}
				break;

			case DATA_ENTRY_REQUIRED:
				Boolean dataEntryRequired =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (dataEntryRequired != null) {
					uiComponent.setDataEntryRequired(dataEntryRequired);
				}
				break;
				
			case DATA_TYPE:
				optionValueReference =
			    elementMember.getOptionValueReferencesReader().getOptionValueReference(option.getName());
				if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
					final Element rawType = Element.class.cast(optionValueReference.getReferencedObject());
					Type type = convertWithOtherConverter(Type.class, rawType);

					if (type == null) {
						throw new ModelConverterException("not successfully converted a type for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
					} else {
						uiComponent.setDataType(type);
					}
				}
				break;
				
			case CUSTOM_MESSAGE:
				List<String> customMessageTypeStrings = elementMember.getOptionValueSettingsReader().getEnumeratedOptionValues(UiComponentsOptionEnum.CUSTOM_MESSAGE.getName());
			    if (customMessageTypeStrings != null) {
			    	for (String customMessageTypeString : customMessageTypeStrings) {
			    		CustomMessageType customMessageType = CustomMessageType.valueOf(customMessageTypeString.toUpperCase());
			    		if (customMessageType != null) {
			    			uiComponent.addCustomMessageType(customMessageType);
			    		}
			    	}
			    }
				break;
				
			case NAVIGATION_TARGET:
				optionValueReference =
				    elementMember.getOptionValueReferencesReader().getOptionValueReference(option.getName());
				if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
					final Element targetView = Element.class.cast(optionValueReference.getReferencedObject());
					final UIStructuralContainer viewContainer = this.convertWithOtherConverter(UIStructuralContainer.class, targetView);
//					final UIViewContainer viewContainer = this.convertWithOtherConverter(UIViewContainer.class, targetView);
					OptionEnumerationEntry navigationTargetOption = optionValueReference.getReferenceEnumerationValue();
	                if (navigationTargetOption != null) {
	                	FlowTypeEnum flowTypeEnumEntry = FlowTypeEnum.getByName(navigationTargetOption.getEntry());
	        			if (flowTypeEnumEntry != null) {
	        				switch (flowTypeEnumEntry) {
	        				case OPEN:
	        					uiComponent.setContainerFlowType(com.gs.gapp.metamodel.ui.ContainerFlowType.OPEN);
	        					break;
	        				case PUSH:
	        					uiComponent.setContainerFlowType(com.gs.gapp.metamodel.ui.ContainerFlowType.PUSH);
	        					break;
	        				case REPLACE:
	        					uiComponent.setContainerFlowType(com.gs.gapp.metamodel.ui.ContainerFlowType.REPLACE);
	        					break;
	        				default:
	        					throw new ModelConverterException("unhandled flow type found", flowTypeEnumEntry);
	        				}
	        			}
	                }
		                
					if (viewContainer == null) {
						throw new ModelConverterException("not successfully converted a target view, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
					} else {
						uiComponent.setNavigationTarget(viewContainer);
					}
				}
	
				break;

		    case ENTITY_FIELD_FOR_DISPLAY:
			case CONTAINER_FOR_CHOICE:
			case CONTAINER_FOR_EMBEDDED_CONTAINER:
			case EDITABLE:
			case HAS_ICON:
			case LOWER:
			case MULTIPLICITY:
			case PASSWORD:
			case STEPPING:
			case TYPE:
			case UPPER:
			case LONG_LASTING:
			case ACTION_PURPOSE:
			case BOOLEAN_CHOICE_TYPE:
			case CHOICE_TYPE:
			case DISPLAY:
			case INPUT_MASK:
			case PRESELECTED:
			case DEFAULT_ACTION:
			case LENGTH:
			case CHART_TYPE:
			case FORMAT:
			case IMAGE_TYPE:
			case KEYBOARD_CONTROL:
				// those options are handled in other element converters
				break;
			
			
			default:
				break;
			}
		}
	}

	@Override
	public IMetatype getMetatype() {
		return UiMemberEnum.COMPONENT;
	}
}
