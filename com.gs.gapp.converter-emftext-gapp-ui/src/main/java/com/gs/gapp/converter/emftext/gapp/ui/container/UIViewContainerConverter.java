/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.container;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.ui.container.UIMenuContainer;

/**
 * @author mmt
 *
 */
public class UIViewContainerConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.container.UIViewContainer> extends
		UIStructuralContainerConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIViewContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/**
	 * @param element
	 * @param uiViewContainer
	 */
	@Override
	protected void onConvert(S element, T uiViewContainer) {
		super.onConvert(element, uiViewContainer);
		
		// --- view menus
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.MENUS.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference reference : optionValueReferences) {
				UIMenuContainer menuContainer = this.convertWithOtherConverter(UIMenuContainer.class,
            			                                                         reference.getReferencedObject());
            	if (menuContainer != null) {
            		uiViewContainer.addViewMenu(menuContainer);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a menu, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.VIEW;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.ui.container.UIContainerConverter#onCreateModelElement(com.gs.gapp.language.ui.container.UIContainer)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.container.UIViewContainer(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
