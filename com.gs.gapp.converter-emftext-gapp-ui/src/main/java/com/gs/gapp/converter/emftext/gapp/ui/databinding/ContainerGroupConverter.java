/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.databinding;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.ui.databinding.ContainerGroup;

/**
 * @author mmt
 *
 */
public class ContainerGroupConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.databinding.ContainerGroup>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ContainerGroupConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T containerGroup) {

		super.onConvert(element, containerGroup);

		// --- entity
		OptionValueReference optionValueReference =
		    element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.ENTITY_FOR_DATABINDING.getName());
		if (optionValueReference.getReferencedObject() != null) {
			Entity entity =
					this.convertWithOtherConverter(Entity.class,
							                       optionValueReference.getReferencedObject());
			if (entity != null) {
				containerGroup.setEntity(entity);
			} else {
				throw new ModelConverterException("not successfully converted an entity for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}
		
		// --- function
		optionValueReference =
		    element.getOptionValueReferencesReader().getOptionValueReference(UiOptionEnum.FUNCTION_FOR_DATABINDING.getName());
		if (optionValueReference.getReferencedObject() != null) {
			Function function =
					this.convertWithOtherConverter(Function.class,
							                       optionValueReference.getReferencedObject());
			if (function != null) {
				containerGroup.setFunction(function);
			} else {
				throw new ModelConverterException("not successfully converted a function for databinding, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}


		// --- data containers
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.CONTAINERS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	com.gs.gapp.metamodel.ui.container.data.UIDataContainer container =
    					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.data.UIDataContainer.class,
    							                       reference.getReferencedObject());
            	if (container != null) {
            		containerGroup.addDataContainer(container);
            		container.addGroup(containerGroup);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
		
		// --- dependent databindings
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.DEPENDENT_DATABINDINGS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	ContainerGroup dependentContainerGroup =
    					this.convertWithOtherConverter(ContainerGroup.class, reference.getReferencedObject());
            	if (dependentContainerGroup != null) {
            		containerGroup.addDependentContainerGroup(dependentContainerGroup);
            		dependentContainerGroup.addContainerGroupDependingOn(containerGroup);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a container group, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.DATABINDING;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.ui.databinding.ContainerGroup(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
