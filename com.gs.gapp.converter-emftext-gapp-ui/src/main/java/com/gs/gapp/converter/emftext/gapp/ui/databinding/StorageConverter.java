/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.ui.databinding;

import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.ui.UiElementEnum;
import com.gs.gapp.dsl.ui.UiOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.ui.UiUtil;

/**
 * @author mmt
 *
 */
public abstract class StorageConverter<S extends Element, T extends com.gs.gapp.metamodel.ui.databinding.Storage>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public StorageConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- persistence modules
		EList<OptionValueReference> optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.MODULES_FOR_INTERFACES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	PersistenceModule persistenceModule = this.convertWithOtherConverter(PersistenceModule.class, reference.getReferencedObject());
            	if (persistenceModule != null) {
            		resultingModelElement.addPersistenceModule(persistenceModule);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a persistence module, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}

		Set<com.gs.gapp.metamodel.persistence.Entity> entities =
				new LinkedHashSet<>();
		Set<com.gs.gapp.metamodel.persistence.EntityField> entityFields =
				new LinkedHashSet<>();
		Set<com.gs.gapp.metamodel.ui.component.UIComponent> uiComponents =
				new LinkedHashSet<>();



		// --- entities
		optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.ENTITIES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Entity entity = this.convertWithOtherConverter(Entity.class, reference.getReferencedObject());
            	if (entity != null) {
            		entities.add(entity);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to an entity, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}

		// --- entity fields
		optionValueReferences =
				originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(UiOptionEnum.ENTITY_FIELDS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	EntityField entityField = this.convertWithOtherConverter(EntityField.class, reference.getReferencedObject());
            	if (entityField != null) {
            		entityFields.add(entityField);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to an entity field, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}

		// --- ui components  TODO not supported yet, maybe support at a later time

		// normalize the selectively modeled persistent items by creating new persistence modules for them
		for (com.gs.gapp.metamodel.persistence.PersistenceModule newPersistenceModule : UiUtil.createNewPersistenceModules(entities, entityFields, uiComponents)) {
			resultingModelElement.addPersistenceModule(newPersistenceModule);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return UiElementEnum.STORAGE;
	}
}
