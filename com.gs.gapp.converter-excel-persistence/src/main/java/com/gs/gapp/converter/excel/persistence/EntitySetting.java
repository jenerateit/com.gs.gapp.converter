/**
 * 
 */
package com.gs.gapp.converter.excel.persistence;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum EntitySetting {
    MAPPED_SUPERCLASS ("mappedsuperclass"),
    LIFECYCLE_LISTENER ("lifecyclelistener"),
    UTILITY_FIELDS ("utilityfields"),
    ;
    
    private static final Map<String, EntitySetting> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (EntitySetting entitySetting : values()) {
			stringToEnum.put(entitySetting.name, entitySetting);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private EntitySetting(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static EntitySetting fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
