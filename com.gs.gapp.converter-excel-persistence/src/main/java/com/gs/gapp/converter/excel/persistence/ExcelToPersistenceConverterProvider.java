package com.gs.gapp.converter.excel.persistence;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

public class ExcelToPersistenceConverterProvider implements ModelConverterProviderI {

	@Override
	public ModelConverterI getModelConverter() {
		return new ExcelToPersistenceConverter();
	}

}
