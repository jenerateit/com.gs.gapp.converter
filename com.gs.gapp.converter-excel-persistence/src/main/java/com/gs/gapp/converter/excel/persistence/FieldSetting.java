/**
 * 
 */
package com.gs.gapp.converter.excel.persistence;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum FieldSetting {
    PK ("pk"),
    VERSION ("version"),
    COLLECTION ("collection"),
    TRANSIENT ("transient"),
    ENUMTYPE ("enumtype"),
    LOB ("lob"),
    LENGTH ("length"),
    MIN_LENGTH ("minlength"),
    NULLABLE ("nullable"),
    UNIQUE ("unique"),
    INCLUDE_IN_EQUALS_COMPARISON ("includeinequalitycomparison"),
    ;
    
    private static final Map<String, FieldSetting> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (FieldSetting fieldSetting : values()) {
			stringToEnum.put(fieldSetting.name, fieldSetting);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private FieldSetting(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static FieldSetting fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
