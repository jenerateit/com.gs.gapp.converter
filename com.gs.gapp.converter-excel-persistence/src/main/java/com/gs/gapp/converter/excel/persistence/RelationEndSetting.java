/**
 * 
 */
package com.gs.gapp.converter.excel.persistence;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum RelationEndSetting {
    
    FETCHTYPE ("fetchtype"),
    REF ("ref"),
    CASCADE ("cascade"),
    ORDER_BY ("orderby"),
    ORDER_BY_CLAUSE ("orderbyclause"),
    ;
    
    private static final Map<String, RelationEndSetting> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (RelationEndSetting relationEndSetting : values()) {
			stringToEnum.put(relationEndSetting.name, relationEndSetting);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private RelationEndSetting(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static RelationEndSetting fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
