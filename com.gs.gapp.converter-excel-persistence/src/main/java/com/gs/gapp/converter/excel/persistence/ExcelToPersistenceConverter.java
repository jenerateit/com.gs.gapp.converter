package com.gs.gapp.converter.excel.persistence;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.EntityRelationEnd;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.Relation;
import com.gs.gapp.metamodel.persistence.enums.CascadeType;
import com.gs.gapp.metamodel.persistence.enums.EnumType;
import com.gs.gapp.metamodel.persistence.enums.FetchType;

public final class ExcelToPersistenceConverter extends AbstractConverter {

	private static final int MAX_EMPTY_LINES = 5;
	
	private static final String PERSISTENCE_SHEET_NAME = "Persistence";
	private static final String CONFIGURATION_SHEET_NAME = "Configuration";

	private PersistenceModule persistenceModule;
	private final Properties cfg = new Properties();
	private final Map<String, Namespace> namespaces = new LinkedHashMap<>();

	private Map<EntityRelationEnd,String> relationEndToOrderByMap = new LinkedHashMap<>();

	/**
	 *
	 */
	public ExcelToPersistenceConverter() {
		super(new ModelElementCache());
	}

	@Override
	public String getVersion() {
		return "0.9.0";
	}

	public String getName() {
		return "gApp Excel to Persistence Model Converter";
	}

	public String getDescription() {
		return "This model converter reads an Excel file that holds information about entities for the persistence layer.";
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected Set<Object> clientConvert(Collection<?> rawElements, ModelConverterOptions options) throws ModelConverterException {
		
		super.clientConvert(rawElements, options);
		
		namespaces.clear();

		Set<Object> result = new LinkedHashSet<>();
		
		if (rawElements != null && !rawElements.isEmpty()) {
			for (Object o : rawElements) {
				if (o != null && HSSFWorkbook.class.isInstance(o)) {
					HSSFWorkbook workbook = (HSSFWorkbook) o;
					
					// --- read configuration
					HSSFSheet cfgSheet = workbook.getSheet(CONFIGURATION_SHEET_NAME);
					if (cfgSheet != null) {
						readCfg(cfgSheet);
					} else {
						addWarning("It looks like the workbook '" + workbook + 
								"' is not a Persistence model since the Sheet '" + 
								CONFIGURATION_SHEET_NAME + "}' is not available");
					}
					
					// --- read entities
					HSSFSheet sheet = workbook.getSheet(PERSISTENCE_SHEET_NAME);
					if (sheet != null) {
						readEntities(sheet);
						for (Namespace namespace : this.namespaces.values()) {
							result.addAll(namespace.getEntities());
						}
						result.add(this.persistenceModule);
						result.addAll(this.namespaces.values());
						break;
					} else {
						addWarning("It looks like the workbook '" + workbook + 
								"' is not a gApp Persistence model since the Sheet '" + 
								PERSISTENCE_SHEET_NAME + "}' is not available");
					}
					
				} else {
					addWarning("Found an unknown raw model element '" + o.getClass() + "'");
				}
			}
		}
		return result;
	}

	/**
	 * @param sheet
	 */
	private void readCfg(HSSFSheet sheet) {
		int rowNum = 1;
		int columnIndex = 0;
		int emptyCounter = 0;
		HSSFCell cell = null;
		HSSFRow row = null;
		Object key, value;
		
		do {
			columnIndex = 0;
			
			row = sheet.getRow(rowNum++);
			if (row == null) {
				emptyCounter++;
				continue;
			}
			
			cell = row.getCell(columnIndex++);
			if (cell == null || cell.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
				emptyCounter++;
				continue;
			} else if (cell.getCellType() != HSSFCell.CELL_TYPE_STRING) {
				throw new ModelConverterException("The cell type in sheet '" + sheet.getSheetName() + "' at position (" + (rowNum + 1) + "/" + columnIndex + ") is not of type STRING");
			} else {
				key = cell.getStringCellValue();
			}
			
			cell = row.getCell(columnIndex++);
			if (cell.getCellType() != HSSFCell.CELL_TYPE_STRING) {
				throw new ModelConverterException("The cell type in sheet '" + sheet.getSheetName() + "' at position (" + (rowNum + 1) + "/" + columnIndex + ") is not of type STRING");
			} else {
				value = cell.getStringCellValue();
			}
			
			this.cfg.put(key, value);
			
		} while (emptyCounter < MAX_EMPTY_LINES);
	}
	

	/**
	 * @param cell
	 * @return
	 */
	private Properties getProperties(HSSFCell cell) {
		Properties result = null;
		if (cell != null) {
			String cellContent = getStringCellValue(cell);
			if (StringTools.isNotEmpty(cellContent)) {
				result = getProperties(cellContent);
			}
		}
		return result == null ? new Properties() : result;
	}
	
	/**
	 * @param cellContent
	 * @return
	 */
	private Properties getProperties(String cellContent) {
		Properties result = new Properties();

		if (StringTools.isNotEmpty(cellContent)) {
			String[] propertiesArray = cellContent.split("[\\r\\n]");
			for (String propertyString : propertiesArray) {
				String[] keyValuePair = propertyString.split("=");
				if (keyValuePair.length >= 2) {
				    result.put(keyValuePair[0], keyValuePair[1]);
				} else {
					throw new ModelConverterException("Cell content '" + cellContent + "' is not a valid property since it does not contain an '='");
				}
			}
		}
		return result;
	}
	

	/**
	 * 
	 * @param sheet
	 */
	private void readEntities(HSSFSheet sheet) {
		int rowNum = 2;
		int columnIndex = 0;
		int emptyCounter = 0;
		String cellContent = null;
		HSSFCell cell = null;
		HSSFRow row = null;
		Entity entity = null;
		Namespace namespace = null;
		
		Map<String,Entity> entityNameMap = new LinkedHashMap<>();
		Map<EntityRelationEnd,String> relationEndToEntityNameMap = new LinkedHashMap<>();
		Map<Entity,String> parentEntityNameMap = new LinkedHashMap<>();
		
		do {
			columnIndex = 0;
			
			row = sheet.getRow(rowNum++);
			if (row == null) {
				emptyCounter++;
				continue;
			}
			
			/*
			 * module name
			 */
			cell = row.getCell(columnIndex++);
			cellContent = getStringCellValue(cell);
			if (StringTools.isNotEmpty(cellContent)) {
				if (this.persistenceModule != null) {
					throw new ModelConverterException("More than one module name found (" + cellContent + "), but only one persistence module allowed in one sheet");
				}
				this.persistenceModule = new PersistenceModule(cellContent);
				getModel().addElement(this.persistenceModule);  // adding the persistence module to the project in order to be able to process more than one persistence module per target project
			}
			
			/*
			 * namespace
			 */
			cell = row.getCell(columnIndex++);
			cellContent = getStringCellValue(cell);
			if (StringTools.isNotEmpty(cellContent)) {
				if (this.namespaces.containsKey(cellContent)) {
					namespace = this.namespaces.get(cellContent);
				} else {
					namespace = new Namespace(cellContent);
					this.namespaces.put(cellContent, namespace);
					this.persistenceModule.addElement(namespace);
					this.persistenceModule.setNamespace(namespace);
				}
			}
			
			/*
			 * entity
			 */
			cell = row.getCell(columnIndex++);
			cellContent = getStringCellValue(cell);
			if (cellContent != null && cellContent.indexOf("=") > 0) {
				// one or more properties of an entity
			    Properties entitySettings = getProperties(cellContent);
			    applyEntitySettings(entity, entitySettings);
			} else {
				if (StringTools.isNotEmpty(cellContent)) {
					entity = new Entity(cellContent);
					entityNameMap.put(entity.getName(), entity);
					this.persistenceModule.addElement(entity);
					namespace.addEntity(entity);
					entity.setNamespace(namespace);
					String entityComment = null;
					if (cell != null && cell.getCellComment() != null && cell.getCellComment().getString() != null) {
					    entityComment = cell.getCellComment().getString().getString();
					    entity.setBody(entityComment);
					}
				}
			}
			
			
			
			/*
			 * entity parent
			 */
			cell = row.getCell(columnIndex++);
			cellContent = getStringCellValue(cell);
			if (StringTools.isNotEmpty(cellContent)) {
				parentEntityNameMap.put(entity, cellContent);
			}
			
			/*
			 * field + type
			 */
			com.gs.gapp.metamodel.basic.typesystem.Type type = null;
			EntityField entityField = null;
			EntityRelationEnd entityRelationEnd = null;
			cell = row.getCell(columnIndex++);
			String fieldName = getStringCellValue(cell);
			String fieldComment = null;
			if (cell != null && cell.getCellComment() != null && cell.getCellComment().getString() != null) {
			    fieldComment = cell.getCellComment().getString().getString();
			}
			cell = row.getCell(columnIndex++);
			String fieldType = getStringCellValue(cell);
			
			if (StringTools.isNotEmpty(fieldName)) {
				if (StringTools.isEmpty(fieldType)) {
					throw new ModelConverterException("entity field type must not be null for field '" + fieldName + "'");
				} else {
					PrimitiveTypeEnum primitiveTypeEnum = PrimitiveTypeEnum.fromStringCaseInsensitive(fieldType);
					if (primitiveTypeEnum != null) {
						if (primitiveTypeEnum.getPrimitiveType() == null) {
							throw new ModelConverterException("primitive type enum '" + primitiveTypeEnum + "' does not return a primitive type instance");
						}
					    type = primitiveTypeEnum.getPrimitiveType();
					} else {
						// primitive type from incoming model is not known, at least create a new technology-agnostic model element for it
						type = (com.gs.gapp.metamodel.basic.typesystem.PrimitiveType) this.
								    getModelElementCache().findModelElement(fieldType,
								                                            com.gs.gapp.metamodel.basic.typesystem.PrimitiveType.class);
						if (type == null) {
//							type = new com.gs.gapp.metamodel.basic.typesystem.PrimitiveType(fieldType);
//							this.getModelElementCache().add(type);
						}
					}
				}
				
				if (type != null) {
					entityField = new EntityField(fieldName, entity);
					entityField.setType(type);
					entityField.setBody(fieldComment);
				} else {
					// in this case we have a complex type
					entityRelationEnd = new EntityRelationEnd(fieldName, entity);
					relationEndToEntityNameMap.put(entityRelationEnd, fieldType);
					entityRelationEnd.setBody(fieldComment);
				}
			}
			
			/*
			 * field settings
			 */
			cell = row.getCell(columnIndex++);
			if (cell != null) {
				Properties fieldSettings = getProperties(cell);
				if (entityField != null) {
					applyFieldSettings(entityField, fieldSettings);
				} else {
					applyRelationEndSettings(entityRelationEnd, fieldSettings);
				}
				emptyCounter = 0;
			}
			
		} while (emptyCounter < MAX_EMPTY_LINES);
		
		// at the end, lookup the entities for relations and inheritance
		for (EntityRelationEnd relationEnd : relationEndToEntityNameMap.keySet()) {
			if (StringTools.isNotEmpty(relationEnd.getReferencedBy())) {
				String entityName = relationEndToEntityNameMap.get(relationEnd);
				Entity entityOnOtherSide = entityNameMap.get(entityName);
				if (entityOnOtherSide == null) {
					throw new ModelConverterException("Invalid entity name for relation (" + entityName + ")", relationEnd);
				}
				EntityRelationEnd fieldOnOtherSide = (EntityRelationEnd) entityOnOtherSide.getField(relationEnd.getReferencedBy());
				Relation relation = new Relation("relation-" + relationEnd.getName() + "<--->" + fieldOnOtherSide.getName());
				relation.setRoleA(relationEnd);
				relationEnd.setRelation(relation);
				relation.setRoleB(fieldOnOtherSide);
				fieldOnOtherSide.setOwnerOfBidirectionalRelationship(true);
				fieldOnOtherSide.setRelation(relation);
			}
			String entityName = relationEndToEntityNameMap.get(relationEnd);
			Entity entityType = entityNameMap.get(entityName);
			if (entityType == null) {
				throw new ModelConverterException("Invalid entity name for a relation end's type: '" + entityName + "'", relationEnd);
			}
			relationEnd.setType(entityType);
		}
		
		for (Entity entityWithParent : parentEntityNameMap.keySet()) {
			String nameOfParentEntity = parentEntityNameMap.get(entityWithParent);
			entityWithParent.setParent(entityNameMap.get(nameOfParentEntity));
		}
		
		// --- resolve order by settings
        for (EntityRelationEnd relationEnd : this.relationEndToOrderByMap.keySet()) {
        	String orderByFields = this.relationEndToOrderByMap.get(relationEnd);
        	
        	String[] fieldNames = orderByFields.split(",");
    		
    		for (String fieldName : fieldNames) {
    			String[] nameAndSortOrder = fieldName.split(" ");
    			String sortOrder = null;
    			if (nameAndSortOrder.length == 1) {
    				sortOrder = "ASC";	
    			} else if (nameAndSortOrder.length > 1) {
    				sortOrder = nameAndSortOrder[1].toUpperCase();
    			}
    			if (relationEnd.getOtherRelationEnd() != null) {
    				EntityField field = relationEnd.getOtherRelationEnd().getOwner().getField(nameAndSortOrder[0].trim());
    				if (field != null) {
    				    relationEnd.addOrderBy(field, sortOrder);
    				} else {
    					throw new ModelConverterException("Invalid relation end setting found for order by (invalid field name): '" + nameAndSortOrder[0] + "'", relationEnd);
    				}
    			}
    		}
        }
	}

	
	/**
	 * @param entity
	 * @param entitySettings
	 */
	private void applyEntitySettings(Entity entity, Properties entitySettings) {
		if (entity == null) return;
		
		for (Object key : entitySettings.keySet()) {
			EntitySetting entitySetting = EntitySetting.fromString(key.toString().toLowerCase());
			if (entitySetting == null) {
				throw new ModelConverterException("Invalid entity setting found: '" + key + "'", entity);
			} else {
				switch (entitySetting) {
				case LIFECYCLE_LISTENER:
					// This no longer can be modeled. Instead, there is a transformation step option available to switch this completely on and off
//					if ("true".equalsIgnoreCase(entitySettings.getProperty(key.toString()))) {
//						entity.setEntityLifecycleListener(true);
//					} else {
//						entity.setEntityLifecycleListener(false);
//					}
					break;
				case MAPPED_SUPERCLASS:
					if ("true".equalsIgnoreCase(entitySettings.getProperty(key.toString()))) {
						entity.setMappedSuperclass(true);
					} else {
						entity.setMappedSuperclass(false);
					}
					break;
				case UTILITY_FIELDS:
					if ("true".equalsIgnoreCase(entitySettings.getProperty(key.toString()))) {
						entity.setUtilityFields(true);
					} else {
						entity.setUtilityFields(false);
					}
					break;
				default:
					throw new ModelConverterException("Unhandled entity setting found: '" + entitySetting + "'", entity);
				}
			}
		}
	}

	/**
	 * @param entityRelationEnd
	 * @param relationEndSettings
	 */
	private void applyRelationEndSettings(EntityRelationEnd entityRelationEnd,
			Properties relationEndSettings) {
		if (entityRelationEnd == null) return;
		
		
        Set<String> keysToRemove = new LinkedHashSet<>();
        
		for (Object key : relationEndSettings.keySet()) {
			RelationEndSetting relationEndSetting = RelationEndSetting.fromString(key.toString().toLowerCase());
			if (relationEndSetting == null) {
				FieldSetting fieldSetting = FieldSetting.fromString(key.toString().toLowerCase());
				if (fieldSetting == null) {
				    throw new ModelConverterException("Invalid relation end setting found: '" + key + "'", entityRelationEnd);
				}
			} else {
				switch (relationEndSetting) {
				case CASCADE:
					String cascadeOptions = relationEndSettings.getProperty(key.toString());
					String[] cascadeOptionsArr = cascadeOptions.split("[,]");
					for (String cascadeOption : cascadeOptionsArr) {
						CascadeType cascadeType = CascadeType.fromString(cascadeOption.trim());
						if (cascadeType != null) {
							entityRelationEnd.addCascadeOption(cascadeType);
						} else {
							throw new ModelConverterException("Invalid relation end setting found for cascade: '" + cascadeOption + "'", entityRelationEnd);							
						}
					}
					break;
				case FETCHTYPE:
					String fetchTypeName = relationEndSettings.getProperty(key.toString());
					try {
						FetchType fetchType = FetchType.valueOf(fetchTypeName.trim().toUpperCase());
						if (fetchType != null) {
							entityRelationEnd.setFetchType(fetchType);
						} else {
							throw new ModelConverterException("Invalid relation end setting found for fetchtype: '" + fetchTypeName + "'", entityRelationEnd);
						}
					} catch (IllegalArgumentException ex) {
						throw new ModelConverterException("no FetchType instance for value '" + fetchTypeName.toUpperCase() + "'", ex);
					}
					break;
				case ORDER_BY:
					String orderByFields = relationEndSettings.getProperty(key.toString());
					if (StringTools.isNotEmpty(orderByFields)) {
						relationEndToOrderByMap.put(entityRelationEnd, orderByFields);  // keep this information for later, to resolve it then
					}
					break;
				case ORDER_BY_CLAUSE:
					String orderByClause = relationEndSettings.getProperty(key.toString());
					if (StringTools.isNotEmpty(orderByClause)) {
						entityRelationEnd.setOrderByClause(orderByClause);
					}
					break;
				case REF:
					entityRelationEnd.setReferencedBy(relationEndSettings.getProperty(key.toString()));
					break;
				default:
					throw new ModelConverterException("Unhandled entity setting found: '" + relationEndSetting + "'", entityRelationEnd);
				}
				
				keysToRemove.add(key.toString());
			}
		}
		
		for (String keyToRemove : keysToRemove) {
			relationEndSettings.remove(keyToRemove);
		}
		
		applyFieldSettings(entityRelationEnd, relationEndSettings);  // relationEndSettings include field settings
		
	}

	/**
	 * @param entityField
	 * @param fieldSettings
	 */
	private void applyFieldSettings(EntityField entityField,
			Properties fieldSettings) {
		if (entityField == null) return;
		
		for (Object key : fieldSettings.keySet()) {
			FieldSetting fieldSetting = FieldSetting.fromString(key.toString().toLowerCase());
			if (fieldSetting == null) {
				throw new ModelConverterException("Invalid entity field setting found: '" + key + "'", entityField);
			} else {
				switch (fieldSetting) {
				case COLLECTION:
					String collectionTypeName = fieldSettings.getProperty(key.toString());
					CollectionType collectionType = CollectionType.fromString(collectionTypeName);
					if (collectionType != null) {
						entityField.setCollectionType(collectionType);
					} else {
						throw new ModelConverterException("incorrect collection type found: '" + collectionTypeName + "'", entityField);
					}
					break;
				case ENUMTYPE:
					String enumTypeName = fieldSettings.getProperty(key.toString()).trim();
					try {
						EnumType enumType = EnumType.valueOf(enumTypeName.toUpperCase());
						if (enumType != null) {
							entityField.setEnumType(enumType);
						} else {
							throw new ModelConverterException("incorrect enum type found: '" + enumTypeName + "'", entityField);
						}
					} catch (IllegalArgumentException ex) {
						throw new ModelConverterException("no EnumType instance for value '" + enumTypeName.toUpperCase() + "'", ex);
					}
					break;
				case LOB:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setLob(true);
					} else {
						entityField.setLob(false);
					}
					break;
				case PK:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setPartOfId(true);
					} else {
						entityField.setPartOfId(false);
					}
					break;
				case TRANSIENT:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setTransient(true);
					} else {
						entityField.setTransient(false);
					}
					break;
				case VERSION:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setVersion(true);
					} else {
						entityField.setVersion(false);
					}
					break;
				case LENGTH:
					String lengthString = fieldSettings.getProperty(key.toString());
					try {
					    int length = new Integer(lengthString.trim());
					    entityField.setLength(length);
					} catch (NumberFormatException ex) {
						throw new ModelConverterException("invalid value given for setting 'length':" + lengthString, ex, entityField);
					}
					break;
				case MIN_LENGTH:
					String minLengthString = fieldSettings.getProperty(key.toString());
					try {
					    int minLength = new Integer(minLengthString.trim());
					    entityField.setMinLength(minLength);
					} catch (NumberFormatException ex) {
						throw new ModelConverterException("invalid value given for setting 'length':" + minLengthString, ex, entityField);
					}
					break;
				case NULLABLE:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setNullable(true);
					} else {
						entityField.setNullable(false);
					}
					break;
				case UNIQUE:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setUnique(true);
					} else {
						entityField.setUnique(false);
					}
				    break;
				case INCLUDE_IN_EQUALS_COMPARISON:
					if ("true".equalsIgnoreCase(fieldSettings.getProperty(key.toString()).trim())) {
						entityField.setRelevantForEquality(true);
					} else {
						entityField.setRelevantForEquality(false);
					}
					break;
				default:
					throw new ModelConverterException("Unhandled entity field setting found: '" + fieldSetting + "'", entityField);
				}
			}
		}
	}

	/**
	 * @param cell
	 * @return
	 */
	private String getStringCellValue(HSSFCell cell) {
		String result = null;
		if (cell != null && cell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
			result = cell.getStringCellValue();
		}
		return result;
	}

	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		return Collections.emptyList();
	}
}
