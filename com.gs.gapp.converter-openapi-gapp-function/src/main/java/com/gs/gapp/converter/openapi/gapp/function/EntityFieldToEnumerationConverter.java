/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.openapi.PropertyWrapper;
import com.gs.gapp.metamodel.persistence.EntityField;

import io.swagger.models.properties.AbstractProperty;
import io.swagger.models.properties.DoubleProperty;
import io.swagger.models.properties.FloatProperty;
import io.swagger.models.properties.IntegerProperty;
import io.swagger.models.properties.LongProperty;
import io.swagger.models.properties.StringProperty;

/**
 * @author mmt
 *
 */
public class EntityFieldToEnumerationConverter<S extends EntityField, T extends Enumeration> extends AbstractM2MModelElementConverter<S, T> {

	public EntityFieldToEnumerationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		
		if (generalResponsibility) {
			EntityField entityField = (EntityField) originalModelElement;
			return isOpenApiPropertyUsingEnum(entityField);
		}
		
		return false;
	}
	
	
	private boolean isOpenApiPropertyUsingEnum(EntityField entityField) {
		boolean result = false;
		if (entityField.getType() != null && entityField.getOriginatingElement(PropertyWrapper.class) != null) {
			PropertyWrapper propertyWrapper = entityField.getOriginatingElement(PropertyWrapper.class);
			AbstractProperty abstractProperty = propertyWrapper.getProperty();
			if (abstractProperty instanceof StringProperty) {
				StringProperty stringProperty = (StringProperty) abstractProperty;
				result = stringProperty.getEnum() != null && stringProperty.getEnum().size() > 0;
			} else if (abstractProperty instanceof IntegerProperty) {
				@SuppressWarnings("unused")
				IntegerProperty integerProperty = (IntegerProperty) abstractProperty;
				// TODO continue for this case
			} else if (abstractProperty instanceof LongProperty) {
				@SuppressWarnings("unused")
				LongProperty longProperty = (LongProperty) abstractProperty;
				// TODO continue for this case
			} else if (abstractProperty instanceof FloatProperty) {
				@SuppressWarnings("unused")
				FloatProperty floatProperty = (FloatProperty) abstractProperty;
				// TODO continue for this case
			} else if (abstractProperty instanceof DoubleProperty) {
				@SuppressWarnings("unused")
				DoubleProperty doubleProperty = (DoubleProperty) abstractProperty;
				// TODO continue for this case
			}
		}
		
		return result;
	}



	@Override
	protected T onCreateModelElement(S entityField, ModelElementI previousResultingModelElement) {

		@SuppressWarnings("unchecked")
		T enumeration = (T) new Enumeration(entityField.getOwner().getName() + StringTools.firstUpperCase(entityField.getName()) + "Enum");
		
		// TODO is this right?
		// fix NullPointerException in basic EnumerationToJavaEnumConverter
		// enumeration has no module and hence no namespace
		enumeration.setModule(entityField.getOwner().getModule());
		
		entityField.getOwner().getPersistenceModule().addElement(enumeration);
		entityField.getOwner().getPersistenceModule().getNamespace().addElement(enumeration);
		enumeration.setModule(entityField.getOwner().getPersistenceModule());
		
		return enumeration;
	}
	
	@Override
	protected void onConvert(S entityField, T enumeration) {
		super.onConvert(entityField, enumeration);
		
		if (entityField.getType() != null && entityField.getOriginatingElement(PropertyWrapper.class) != null) {
			PropertyWrapper propertyWrapper = entityField.getOriginatingElement(PropertyWrapper.class);
			AbstractProperty abstractProperty = propertyWrapper.getProperty();
			if (abstractProperty instanceof StringProperty) {
				StringProperty stringProperty = (StringProperty) abstractProperty;
				for (String enumEntryString : stringProperty.getEnum()) {
					EnumerationEntry enumerationEntry = new EnumerationEntry(enumEntryString.toUpperCase());
					enumeration.addEnumerationEntry(enumerationEntry);
				}
			} else if (abstractProperty instanceof IntegerProperty) {
				IntegerProperty integerProperty = (IntegerProperty) abstractProperty;
				integerProperty.getEnum();
				// TODO continue for this case
			} else if (abstractProperty instanceof LongProperty) {
				@SuppressWarnings("unused")
				LongProperty longProperty = (LongProperty) abstractProperty;
				// TODO continue for this case
			} else if (abstractProperty instanceof FloatProperty) {
				@SuppressWarnings("unused")
				FloatProperty floatProperty = (FloatProperty) abstractProperty;
				// TODO continue for this case
			} else if (abstractProperty instanceof DoubleProperty) {
				@SuppressWarnings("unused")
				DoubleProperty doubleProperty = (DoubleProperty) abstractProperty;
				// TODO continue for this case
			}
		}
	}
}
