/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import java.util.ArrayList;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.jaxrs.JaxrsOptionEnum;
import com.gs.gapp.dsl.rest.MediaTypeEnum;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinition.OptionValue;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.Namespace;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.openapi.ComposedModelWrapper;
import com.gs.gapp.metamodel.openapi.ModelImplWrapper;
import com.gs.gapp.metamodel.openapi.OperationWrapper;
import com.gs.gapp.metamodel.openapi.PathWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

import io.swagger.models.ArrayModel;
import io.swagger.models.ComposedModel;
import io.swagger.models.HttpMethod;
import io.swagger.models.Model;
import io.swagger.models.ModelImpl;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.RefModel;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class SwaggerToFunctionModuleConverter<S extends Swagger, T extends FunctionModule> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public SwaggerToFunctionModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S swagger, T functionModule) {
		super.onConvert(swagger, functionModule);
		
		// --- get the persistence module that relates to the function module
		PersistenceModule persistenceModule = convertWithOtherConverter(PersistenceModule.class, functionModule);
		
		// --- convert types to ComplexTypes
		if (swagger.getDefinitions() != null) {
			for (String key : swagger.getDefinitions().keySet()) {
				Model model = swagger.getDefinitions().get(key);
				System.out.println("key:" + key + ", model class:" + model.getClass().getSimpleName());
				if (model instanceof RefModel) {
					@SuppressWarnings("unused")
					RefModel refModel = (RefModel) model;
					// it seems as if definitions cannot contain RefModel instances
				} else if (model instanceof ArrayModel) {
					@SuppressWarnings("unused")
					ArrayModel arrayModel = (ArrayModel) model;
					// it seems as if definitions cannot contain ArrayModel instances
				} else if (model instanceof ComposedModel) {
					ComposedModel composedModel = (ComposedModel) model;
					ComposedModelWrapper composedModelWrapper = new ComposedModelWrapper(key, composedModel, swagger);
					Entity entity = convertWithOtherConverter(Entity.class, composedModelWrapper, functionModule);
					if (entity != null) {
						persistenceModule.addElement(entity);
						persistenceModule.getNamespace().addEntity(entity);
					}
				} else if (model instanceof ModelImpl) {
					ModelImpl modelImpl = (ModelImpl) model;
					ModelImplWrapper modelImplWrapper = new ModelImplWrapper(key, modelImpl, swagger);
					Entity entity = convertWithOtherConverter(Entity.class, modelImplWrapper, functionModule);
					if (entity != null) {
					    persistenceModule.addElement(entity);
					    persistenceModule.getNamespace().addEntity(entity);
					}
				}
			}
		}
		
		// --- add consumes and produces option values
		if (swagger.getProduces() != null) {
			ArrayList<String> listOfMediaTypesForProduces = new ArrayList<>();
			for (String produces : swagger.getProduces()) {
				MediaTypeEnum mediaType = MediaTypeEnum.getInverse(produces);
				if (mediaType == null) throw new ModelConverterException("media type '" + produces + "' not supported");
				listOfMediaTypesForProduces.add(mediaType.getName());
			}
			if (listOfMediaTypesForProduces.size() > 0) {
			    OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(listOfMediaTypesForProduces);
			    functionModule.addOptions(producesOptionValue);
			}
		}
		
		if (swagger.getConsumes() != null) {
			ArrayList<String> listOfMediaTypesForConsumes = new ArrayList<>();
			for (String consumes : swagger.getConsumes()) {
				MediaTypeEnum mediaType = MediaTypeEnum.getInverse(consumes);
				if (mediaType == null) throw new ModelConverterException("media type '" + consumes + "' not supported");
				listOfMediaTypesForConsumes.add(mediaType.getName());
			}
			if (listOfMediaTypesForConsumes.size() > 0) {
			    OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(listOfMediaTypesForConsumes);
			    functionModule.addOptions(consumesOptionValue);
			}
		}
		
		// --- add REST-Path option
		OptionDefinition<String>.OptionValue pathOptionValue = RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(swagger.getBasePath());
		functionModule.addOptions(pathOptionValue);
		
		// --- create functions for OpenAPI operations
		for (String key : swagger.getPaths().keySet()) {
			Path path = swagger.getPaths().get(key);
			PathWrapper pathWrapper = new PathWrapper(key, path, swagger);
			for (HttpMethod httpMethod : path.getOperationMap().keySet()) {
				Operation operation = path.getOperationMap().get(httpMethod);
				OperationWrapper operationWrapper = new OperationWrapper(httpMethod, operation, swagger, pathWrapper);
				@SuppressWarnings("unused")
				Function function = convertWithOtherConverter(Function.class, operationWrapper, functionModule);
			}
		}
		
		// --- optionally, make the resource be a stateless session bean, TODO: make this configurable as a generator parameter (mmt 16-Mar-2017) 
		OptionDefinition<Boolean> optionDefinition = JaxrsOptionEnum.AS_SESSION_BEAN.getDefinition(Boolean.class);
		@SuppressWarnings("rawtypes")
		OptionValue optionValue = optionDefinition. new OptionValue(new Boolean(true));
		functionModule.addOptions(optionValue);
	}

	@Override
	protected T onCreateModelElement(S swagger, ModelElementI previousResultingModelElement) {

		ServiceInterface serviceInterface = (ServiceInterface) previousResultingModelElement;
		
		@SuppressWarnings("unchecked")
		T functionModule = (T) new FunctionModule(swagger.getInfo().getTitle().replaceAll("[^a-zA-Z0-9]", ""));
		serviceInterface.addFunctionModules(functionModule);
		serviceInterface.setModule(functionModule);
		
		String namespaceAsString = getModelConverter().getConverterOptions().getNamespace() + "." + swagger.getInfo().getTitle().toLowerCase().replaceAll("[^a-zA-Z0-9\\.]", "");
		com.gs.gapp.metamodel.function.Namespace namespace =
				(Namespace) getModelConverter().getModelElementCache().findModelElement(namespaceAsString, com.gs.gapp.metamodel.function.Namespace.class);
		if (namespace == null) {
			namespace = new com.gs.gapp.metamodel.function.Namespace(namespaceAsString);
			addModelElement(namespace);
		}
		functionModule.setNamespace(namespace);
		
		serviceInterface.setModule(functionModule);
		functionModule.getNamespace().addElement(serviceInterface);
		
		functionModule.setOriginatingElement(new GappOpenApiModelElementWrapper(swagger));
		return functionModule;
	}

}
