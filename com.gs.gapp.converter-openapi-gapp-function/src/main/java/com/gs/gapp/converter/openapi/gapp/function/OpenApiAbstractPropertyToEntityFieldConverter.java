/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.dsl.java.JavaOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.openapi.ObjectPropertyWrapper;
import com.gs.gapp.metamodel.openapi.PropertyWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;

import io.swagger.models.AbstractModel;
import io.swagger.models.Swagger;
import io.swagger.models.properties.AbstractProperty;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.refs.RefFormat;

/**
 * @author mmt
 *
 */
public class OpenApiAbstractPropertyToEntityFieldConverter<S extends PropertyWrapper, T extends EntityField> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public OpenApiAbstractPropertyToEntityFieldConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S propertyWrapper, T entityField) {
		
		AbstractProperty property = propertyWrapper.getProperty();
		
		entityField.setBody(propertyWrapper.getProperty().getDescription());
//		entityField.setNullable(!propertyWrapper.getProperty().getRequired()); TODO why is this commented? (mmt 13-May-2019)
		
		// --- set the type of the field
		System.out.println("property " + propertyWrapper.getName() + ", type:" + property.getType() + ", format:" + property.getFormat());
		PrimitiveType primitiveFieldType = getPrimitiveType(property);
		if (primitiveFieldType != null) {
			entityField.setType(primitiveFieldType);

			// by default we use the primitive type wrapper setting from the generator option "Java-UsePrimitiveWrapper"
			boolean useWrapper = getModelConverter().getConverterOptions().isJavaUsePrimitiveWrapper();
			OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(useWrapper);
			
			// check, whether we are going to generate a string type with format BYTE or BINARY, which has to end in a byte[] in Java (and _not_ Byte[])
			if (property.getType().equals(OpenApiDataType.STRING.getName()) &&
					property.getFormat() != null &&
					(property.getFormat().equals(OpenApiDataFormat.BYTE.getName()) || property.getFormat().equals(OpenApiDataFormat.BINARY.getName()))) {
				
				entityField.setCollectionType(CollectionType.ARRAY);
				primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(false);
			}
			
			
			// check, whether the OpenAPI model has an ARRAY of BYTE defined, which should end in a byte[] in Java (and _not_ Byte[])
			OpenApiPrimitiveType openApiPrimitiveType = OpenApiPrimitiveType.get(property);
			if (openApiPrimitiveType == OpenApiPrimitiveType.BYTE) {
				entityField.setCollectionType(CollectionType.ARRAY);
				primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(false);
			}
			
			// --- ensure that in case of a Java generation, the primitive type int, boolean, long, double, ... are not used but real classes Integer, Boolean, ... . With this, you can find out whether a mandatory field has a value or not.
			// TODO a more general solution would be to add a converter option that is going to be used in FieldToJavaFieldConverter (mmt 20-Apr-2018)
			entityField.addOptions(primitiveTypeWrapperOptionValue);
			
			// --- handle the special case of enumerations
			Enumeration enumForFieldValues = createEnumeration(entityField);
			if (enumForFieldValues != null) {
				String enumHint = "valid values are found in the enumeration '" + enumForFieldValues.getName() + "'";
				entityField.setBody(entityField.getBody() == null ?  enumHint : entityField.getBody() + System.lineSeparator() + enumHint);
			}
		} else {  // an array or something else non-primitive 
			if (property instanceof ArrayProperty) {
				ArrayProperty arrayProperty = (ArrayProperty) property;
				entityField.setCollectionType(CollectionType.LIST);
				
				PrimitiveType fieldTypeThroughArrayType = getPrimitiveType( arrayProperty.getItems() );
				if (fieldTypeThroughArrayType != null) {
					entityField.setType(fieldTypeThroughArrayType);
					OpenApiPrimitiveType openApiPrimitiveType = OpenApiPrimitiveType.get( arrayProperty.getItems() );
					if (openApiPrimitiveType == OpenApiPrimitiveType.BYTE) {
						entityField.setCollectionType(CollectionType.ARRAY);
					}
				} else {
					// the type for the array is not a primitive one
					if (arrayProperty.getItems() instanceof RefProperty) {
						RefProperty refProperty = (RefProperty) arrayProperty.getItems();
						System.out.println("ref property, ref:" + refProperty.get$ref() + ", simple ref:" + refProperty.getSimpleRef() + ", type:" + refProperty.getType() + ", ref format:" + refProperty.getRefFormat());
						
						if (refProperty.getRefFormat() == RefFormat.INTERNAL) {
							Entity entity = entityField.getOwner().getPersistenceModule().getEntity(refProperty.getSimpleRef());
							if (entity != null) {
								entityField.setType(entity);
							} else {
								throw new ModelConverterException("could not find entity with simple ref '" + refProperty.getSimpleRef() + "'");
							}
						}
					} else {
						throw new ModelConverterException("not able to handle inline type definitions for array '" + arrayProperty + "'");
					}
				}
				
				if (entityField.getCollectionType() != null && entityField.getCollectionType() != CollectionType.ARRAY) {
					// AJAX-116 In case of Java: we have to ensure that the type of the field that is going to be generated will not be a Collection<int>, which is not allowed in Java.
		 			// TODO a more general solution would be to add a converter option that is going to be used in FieldToJavaFieldConverter (mmt 12-Apr-2019)
					OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
		 			entityField.addOptions(primitiveTypeWrapperOptionValue);
				}
			} else if (property instanceof ObjectProperty) {
				ObjectProperty objectProperty = (ObjectProperty) property;
				String composedObjectPropertyName = entityField.getOwner().getName() + StringTools.firstUpperCase(propertyWrapper.getName()) + "Type";
				Entity entity = convertWithOtherConverter(Entity.class, new ObjectPropertyWrapper(composedObjectPropertyName, objectProperty, propertyWrapper.getOpenApiModel()));
				if (entity != null) {
					entityField.setType(entity);
				} else {
					throw new ModelConverterException("could not convert object property " + objectProperty + " to an entity");
				}
				
			} else if (property instanceof RefProperty) {
				RefProperty refProperty = (RefProperty) property;
				System.out.println("ref property, ref:" + refProperty.get$ref() + ", simple ref:" + refProperty.getSimpleRef() + ", type:" + refProperty.getType() + ", ref format:" + refProperty.getRefFormat());
				
				if (refProperty.getRefFormat() == RefFormat.INTERNAL) {
					Entity entity = entityField.getOwner().getPersistenceModule().getEntity(refProperty.getSimpleRef());
					if (entity != null) {
						entityField.setType(entity);
					} else {
						throw new ModelConverterException("could not find entity with simple ref '" + refProperty.getSimpleRef() + "'");
					}
				}
			} else {
				// TODO continue here with other property types
				
				throw new ModelConverterException("code generation for property of type '" + property.getClass() + "' not supported", property);
			}
		}
		
		if (entityField.getType() == null) {
			throw new ModelConverterException("not able to find an appropriate type for field '" + entityField.getName() + "'", property);
		}
	}
	
	private Enumeration createEnumeration(T entityField) {
		return convertWithOtherConverter(Enumeration.class, entityField);
	}

	protected Swagger getSwagger(EntityField entityField) {
		FunctionModule functionModule = entityField.getOwner().getPersistenceModule().getOriginatingElement(FunctionModule.class);
		return functionModule.getOriginatingElement(Swagger.class);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S propertyWrapper, ModelElementI previousResultingModelElement) {
		if (!(previousResultingModelElement instanceof Entity)) {
			throw new ModelConverterException("the previously resulting element has to be of type " + Entity.class.getName());
		}
		Entity entity = (Entity) previousResultingModelElement;
		
		@SuppressWarnings("unused")
		AbstractProperty property = propertyWrapper.getProperty();

		// TODO remove this once the PropertyWrapper works fine
//		AbstractModel model = getModel(previousResultingModelElement);
//		String name = getNameForModel(propertyWrapper, model);

        @SuppressWarnings("unchecked")
		T entityField = (T) new EntityField(propertyWrapper.getName(), entity);
        return entityField;
	}
	
	@Deprecated
	protected AbstractModel getModel(ModelElementI previousResultingModelElement) {
		AbstractModel result = null;
		
		if (previousResultingModelElement instanceof Entity) {
			
			Entity entity = (Entity) previousResultingModelElement;
			Object originatingElement = entity.getOriginatingElement();
			if (originatingElement instanceof GappOpenApiModelElementWrapper) {
				GappOpenApiModelElementWrapper gappOpenApiModelElementWrapper = (GappOpenApiModelElementWrapper) originatingElement;
				if (gappOpenApiModelElementWrapper.getWrappedElement() instanceof AbstractModel) {
					result = (AbstractModel) gappOpenApiModelElementWrapper.getWrappedElement();
				}
			}
		}
		
		if (result == null) {
			throw new ModelConverterException("not able to determine model object for given previously resulting model element", previousResultingModelElement);
		}
		
		return result;
	}
	
	/**
	 * @param model
	 * @param swagger
	 * @return
	 */
	@Deprecated
	protected String getNameForModel(AbstractProperty property, AbstractModel model) {
		if (model == null) throw new NullPointerException("parameter 'model' must not be null");
		if (model.getProperties() == null) {
			throw new NullPointerException("properties for parameter 'model' must not be null");
		}
		
		for (String key : model.getProperties().keySet()) {
			if (model.getProperties().get(key) == property) return key;
		}
		
		return null;
	}
}
