/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public class FunctionModuleToPersistenceModuleConverter<S extends FunctionModule, T extends PersistenceModule> extends AbstractM2MModelElementConverter<S, T> {

	public FunctionModuleToPersistenceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S functionModule, T persistenceModule) {
		super.onConvert(functionModule, persistenceModule);
		// --- nothing to be done so far
	}

	@Override
	protected T onCreateModelElement(S functionModule, ModelElementI previousResultingModelElement) {

		@SuppressWarnings("unchecked")
		T persistenceModule = (T) new PersistenceModule(functionModule.getName() + "Definitions");
		
		String namespaceAsString = functionModule.getNamespace().getName() + ".definition";
		com.gs.gapp.metamodel.persistence.Namespace namespace =
				(Namespace) getModelConverter().getModelElementCache().findModelElement(namespaceAsString, com.gs.gapp.metamodel.persistence.Namespace.class);
		if (namespace == null) {
			namespace = new com.gs.gapp.metamodel.persistence.Namespace(namespaceAsString);
			addModelElement(namespace);
		}
		persistenceModule.setNamespace(namespace);
		functionModule.addPersistenceModule(persistenceModule);
		
		return persistenceModule;
	}

}
