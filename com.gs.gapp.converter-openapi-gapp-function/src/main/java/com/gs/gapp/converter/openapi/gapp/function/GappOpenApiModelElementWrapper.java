package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementWrapper;

import io.swagger.models.Swagger;

public class GappOpenApiModelElementWrapper extends ModelElementWrapper {

	private static final long serialVersionUID = -6695510545219422458L;

	public GappOpenApiModelElementWrapper(Object openApiElement) {
		super(openApiElement);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getModuleName()
	 */
	@Override
	public String getModuleName() {
		String result = super.getModuleName();
		if (getWrappedElement() instanceof Swagger) {
			Swagger swagger = (Swagger) getWrappedElement();
			if (swagger.getInfo() != null && swagger.getInfo().getTitle() != null) {
			    result = swagger.getInfo().getTitle();
			}
		} else if (getWrappedElement() instanceof io.swagger.models.ComposedModel) {
			io.swagger.models.ComposedModel composedModel = (io.swagger.models.ComposedModel) getWrappedElement();
			if (composedModel.getTitle() != null) {
			    result = composedModel.getTitle();
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getId()
	 */
	@Override
	public String getId() {
		String result = getName().replace(" ", "_");
		if (getWrappedElement() instanceof Swagger) {

		}  // TODO continue
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#getName()
	 */
	@Override
	public String getName() {
		String result = getWrappedElement().toString();
		if (getWrappedElement() instanceof Swagger) {
			Swagger swagger = (Swagger) getWrappedElement();
			result = swagger.getInfo().getTitle();
		}  // TODO continue
		
		return result;
	}
}
