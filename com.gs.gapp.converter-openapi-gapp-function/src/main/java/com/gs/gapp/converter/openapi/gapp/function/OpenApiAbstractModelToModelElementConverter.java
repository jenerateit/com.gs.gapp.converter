/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.openapi.AbstractModelWrapper;
import com.gs.gapp.metamodel.persistence.Entity;

import io.swagger.models.AbstractModel;
import io.swagger.models.Swagger;

/**
 * An element converter that inherits from this element converter has to provide an object of type FunctionModule
 * as the 'previous element'.
 * 
 * @author mmt
 *
 */
public abstract class OpenApiAbstractModelToModelElementConverter<S extends AbstractModelWrapper, T extends ModelElement> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public OpenApiAbstractModelToModelElementConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	public OpenApiAbstractModelToModelElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S model, ModelElementI previousResultingModelElement) {
//		if (!(previousResultingModelElement instanceof FunctionModule)) {
//			throw new ModelConverterException("the previously resulting element has to be of type " + FunctionModule.class.getName());
//		}
//		
//		Swagger swagger = getSwagger(previousResultingModelElement);
//		String name = getNameForModel(model, swagger);
//		
//		if (name == null || name.length() == 0) throw new ModelConverterException("not able to find a name for an Entity object that should be created for a given model '" + model + "'");
//		
		return null;
	}
	
	protected Swagger getSwagger(Entity entity) {
		Swagger swagger = getSwagger(entity.getPersistenceModule().getOriginatingElement(FunctionModule.class));
		
		return swagger;
	}
	
	protected Swagger getSwagger(ModelElementI previousResultingModelElement) {
		Swagger result = null;
		
		if (previousResultingModelElement instanceof FunctionModule) {
			
			FunctionModule functionModule = (FunctionModule) previousResultingModelElement;
			Object originatingElement = functionModule.getOriginatingElement();
			if (originatingElement instanceof GappOpenApiModelElementWrapper) {
				GappOpenApiModelElementWrapper gappOpenApiModelElementWrapper = (GappOpenApiModelElementWrapper) originatingElement;
				if (gappOpenApiModelElementWrapper.getWrappedElement() instanceof Swagger) {
					result = (Swagger) gappOpenApiModelElementWrapper.getWrappedElement();
				}
			}
		}
		
		if (result == null) {
			throw new ModelConverterException("not able to determine swagger object for given previously resulting model element", previousResultingModelElement);
		}
		
		return result;
	}
	
	/**
	 * @param model
	 * @param swagger
	 * @return
	 */
	protected String getNameForModel(AbstractModel model, Swagger swagger) {
		for (String key : swagger.getDefinitions().keySet()) {
			if (swagger.getDefinitions().get(key) == model) return key;
		}
		
		return null;
	}
}
