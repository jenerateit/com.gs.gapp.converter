package com.gs.gapp.converter.openapi.gapp.function;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverterOptions;

public class OpenApiToFunctionConverterOptions extends PersistenceToBasicConverterOptions  {
	
	public static final String OPTION_NAMESPACE = "namespace";
	public static final String JAVA_PRIMITIVE_WRAPPER = "Java-UsePrimitiveWrapper";
	
	public OpenApiToFunctionConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public String getNamespace() {
		Serializable postfix = getOptions().get(OPTION_NAMESPACE);
		String result = (postfix == null || postfix.toString().length() == 0 ? "com.gs.vd.api" : postfix.toString().toLowerCase());
		return result;
	}

	public boolean isJavaUsePrimitiveWrapper() {
		String listBeanGenerationSupported = (String) getOptions().get(JAVA_PRIMITIVE_WRAPPER);
		validateBooleanOption(listBeanGenerationSupported, JAVA_PRIMITIVE_WRAPPER);
		return (listBeanGenerationSupported != null) && listBeanGenerationSupported.equalsIgnoreCase("true");
	}
}
