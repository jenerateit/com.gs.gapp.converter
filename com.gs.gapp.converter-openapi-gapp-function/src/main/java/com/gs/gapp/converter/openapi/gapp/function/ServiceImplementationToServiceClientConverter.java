/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;

/**
 * @author mmt
 *
 */
public class ServiceImplementationToServiceClientConverter<S extends ServiceImplementation, T extends ServiceClient> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public ServiceImplementationToServiceClientConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S serviceImplementation, T serviceClient) {
		super.onConvert(serviceImplementation, serviceClient);

	}

	@Override
	protected T onCreateModelElement(S serviceImplementation, ModelElementI previousResultingModelElement) {
		
		@SuppressWarnings("unchecked")
		T serviceClient= (T) new ServiceClient(serviceImplementation.getServiceInterface().getName() + "Client", serviceImplementation.getServiceInterface());
		serviceClient.addConsumedServiceImplementations(serviceImplementation);
		return serviceClient;
	}

}
