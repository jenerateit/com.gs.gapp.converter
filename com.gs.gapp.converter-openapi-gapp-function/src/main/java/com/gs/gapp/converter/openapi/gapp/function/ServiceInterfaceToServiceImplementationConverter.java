/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;

/**
 * @author mmt
 *
 */
public class ServiceInterfaceToServiceImplementationConverter<S extends ServiceInterface, T extends ServiceImplementation> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public ServiceInterfaceToServiceImplementationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S serviceInterface, T serviceImplementation) {
		super.onConvert(serviceInterface, serviceImplementation);
		
		@SuppressWarnings("unused")
		ServiceClient serviceClient = convertWithOtherConverter(ServiceClient.class, serviceImplementation);

	}

	@Override
	protected T onCreateModelElement(S serviceInterface, ModelElementI previousResultingModelElement) {
		
		@SuppressWarnings("unchecked")
		T serviceImplementation = (T) new ServiceImplementation(serviceInterface.getName() + "Impl", serviceInterface);
		serviceInterface.addServiceImplementations(serviceImplementation);
		return serviceImplementation;
	}

}
