/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.openapi.ModelImplWrapper;
import com.gs.gapp.metamodel.openapi.PropertyWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;

import io.swagger.models.ModelImpl;
import io.swagger.models.Swagger;
import io.swagger.models.properties.Property;

/**
 * @author mmt
 *
 */
public class OpenApiModelImplToEntityConverter<S extends ModelImplWrapper, T extends Entity> extends OpenApiAbstractModelToModelElementConverter<S, T> {

	public OpenApiModelImplToEntityConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	@Override
	protected void onConvert(S modelImplWrapper, T entity) {
		
		ModelImpl model = modelImplWrapper.getModelImpl();
		Swagger swagger = getSwagger(entity);

		entity.setBody(modelImplWrapper.getModelImpl().getDescription());
		
		System.out.println("*** Entity:" + entity.getName() + ", discriminator from ModelImpl:" + model.getDiscriminator());
		if (model.getProperties() != null) { //-- if the object has no properties this can be null
			for (String key : model.getProperties().keySet()) {
				Property property = model.getProperties().get(key);
				System.out.println("property, key:" + key + ", class:" + property.getClass().getSimpleName() + ", name:" + property.getName() + ", type:" + property.getType());
				PropertyWrapper propertyWrapper = new PropertyWrapper(key, property, swagger);
				@SuppressWarnings("unused")
				EntityField entityField = convertWithOtherConverter(EntityField.class, propertyWrapper, entity);
			}
		}
	}

	@Override
	protected T onCreateModelElement(S modelImplWrapper, ModelElementI previousResultingModelElement) {
//		super.onCreateModelElement(model, previousResultingModelElement);  // called for performing validation checks only
		
//		Swagger swagger = getSwagger(previousResultingModelElement);
//		String name = getNameForModel(model, swagger);

        @SuppressWarnings("unchecked")
		T entity = (T) new Entity(modelImplWrapper.getName());
//        entity.setOriginatingElement(new GappOpenApiModelElementWrapper(model));
        return entity;
	}
}
