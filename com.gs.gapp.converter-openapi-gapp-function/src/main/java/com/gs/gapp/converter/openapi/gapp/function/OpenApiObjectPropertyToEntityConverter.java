/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.openapi.ObjectPropertyWrapper;
import com.gs.gapp.metamodel.openapi.PropertyWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

import io.swagger.models.Swagger;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;

/**
 * @author mmt
 *
 */
public class OpenApiObjectPropertyToEntityConverter<S extends ObjectPropertyWrapper, T extends Entity> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public OpenApiObjectPropertyToEntityConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S objectPropertyWrapper, T entity) {
		
		ObjectProperty objectProperty = objectPropertyWrapper.getProperty();
		Swagger swagger = objectPropertyWrapper.getOpenApiModel();
		
		for (String key : objectProperty.getProperties().keySet()) {
			Property property = objectProperty.getProperties().get(key);
			PropertyWrapper propertyWrapper = new PropertyWrapper(key, property, swagger);
			EntityField entityField = convertWithOtherConverter(EntityField.class, propertyWrapper, entity);
			System.out.println("entity field for property '" + key + "':" + entityField);
		}
	}
	
	protected Swagger getSwagger(EntityField entityField) {
		FunctionModule functionModule = entityField.getOwner().getPersistenceModule().getOriginatingElement(FunctionModule.class);
		return functionModule.getOriginatingElement(Swagger.class);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S objectPropertyWrapper, ModelElementI previousResultingModelElement) {

		@SuppressWarnings("unused")
		ObjectProperty property = objectPropertyWrapper.getProperty();

        @SuppressWarnings("unchecked")
		T entity = (T) new Entity(StringTools.firstUpperCase(objectPropertyWrapper.getName()));
        
        Swagger swagger = objectPropertyWrapper.getOpenApiModel();
        ServiceInterface serviceInterface = convertWithOtherConverter(ServiceInterface.class, swagger);
        FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, swagger, serviceInterface);
        PersistenceModule persistenceModule = functionModule.getSingleExtensionElement(PersistenceModule.class);
        persistenceModule.addElement(entity);
        entity.setModule(persistenceModule);
        entity.setNamespace(persistenceModule.getNamespace());
        persistenceModule.getNamespace().addEntity(entity);
        
        return entity;
	}
}
