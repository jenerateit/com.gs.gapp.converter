/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.openapi.ModelImplWrapper;
import com.gs.gapp.metamodel.openapi.PropertyWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;

import io.swagger.models.ModelImpl;
import io.swagger.models.properties.AbstractProperty;
import io.swagger.models.properties.Property;

/**
 * @author mmt
 *
 */
public class OpenApiModelImplConverter<S extends ModelImplWrapper, T extends ModelImplWrapper> extends OpenApiAbstractModelToModelElementConverter<S, T> {

	public OpenApiModelImplConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}
	
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {}

	@Override
	protected void onConvert(S modelImplWrapper, T modelImplWrapper2, ModelElementI previousResultingModelElement) {
		super.onConvert(modelImplWrapper, modelImplWrapper2, previousResultingModelElement);
		
        Entity entity = (Entity) previousResultingModelElement;
		
		ModelImpl model = modelImplWrapper.getModelImpl();
		
		for (String key : model.getProperties().keySet()) {
			Property property = model.getProperties().get(key);
			if (property instanceof AbstractProperty) {
				@SuppressWarnings("unused")
				AbstractProperty abstractProperty = (AbstractProperty) property;
				PropertyWrapper propertyWrapper = new PropertyWrapper(key, property, modelImplWrapper.getOpenApiModel());
				@SuppressWarnings("unused")
				EntityField entityField = convertWithOtherConverter(EntityField.class, propertyWrapper, entity);
			}
		}
	}

	@Override
	protected T onCreateModelElement(S modelImplWrapper, ModelElementI previousResultingModelElement) {
		
		@SuppressWarnings("unchecked")
		T result = (T) modelImplWrapper;
		
        return result;
	}
}
