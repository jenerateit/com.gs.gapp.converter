package com.gs.gapp.converter.openapi.gapp.function;

import java.util.HashMap;
import java.util.Map;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

import io.swagger.models.parameters.AbstractSerializableParameter;
import io.swagger.models.properties.Property;

public abstract class AbstractOpenApiToFunctionModelElementConverter<S extends Object, T extends ModelElement> extends AbstractModelElementConverter<S, T> {

	public AbstractOpenApiToFunctionModelElementConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	public static PrimitiveType getPrimitiveType(Property property) {
		return getPrimitiveType(property.getType(), property.getFormat());
	}
	
	public static PrimitiveType getPrimitiveType(AbstractSerializableParameter<?> parameter) {
		return getPrimitiveType(parameter.getType(), parameter.getFormat());
	}
	
	public static PrimitiveType getPrimitiveType(String type, String format) {
		OpenApiPrimitiveType openApiPrimitiveType = OpenApiPrimitiveType.get(type, format);
        return openApiPrimitiveType == null ? null : openApiPrimitiveType.getGappPrimitiveType().getPrimitiveType(); 
	}
	
    @Override
	public OpenApiToFunctionConverter getModelConverter() {
    	return (OpenApiToFunctionConverter) super.getModelConverter();
    }
    
    @Override
	protected void onConvert(S object, T modelElement) {
    	// nothing to be done here so far
    }
	
	/**
	 * @author mmt
	 *
	 */
	public static enum OpenApiDataType {
		INTEGER ("integer"),
		NUMBER ("number"),
		STRING ("string"),
		BOOLEAN ("boolean"),
		;
		
		private static final Map<String, OpenApiDataType> stringToEnumCaseInsensitive =
				new HashMap<>();

		static {
			for (OpenApiDataType enumEntry : values()) {
				stringToEnumCaseInsensitive.put(enumEntry.getName().toLowerCase(), enumEntry);
			}
		}
		
		private final String name;
		
		private OpenApiDataType(String name) {
			this.name= name;
		}

		public String getName() {
			return name;
		}
		
		public static OpenApiDataType get(String name) {
			return name == null ? null : stringToEnumCaseInsensitive.get(name.toLowerCase());
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static enum OpenApiDataFormat {
		INT_32 ("int32"),
		INT_64 ("int64"),
		FLOAT ("float"),
		DOUBLE ("double"),
		BYTE ("byte"),
		BINARY ("binary"),
		DATE ("date"),
		DATE_TIME ("date-time"),
		PASSWORD ("password"),
		;
		
		private static final Map<String, OpenApiDataFormat> stringToEnumCaseInsensitive =
				new HashMap<>();

		static {
			for (OpenApiDataFormat enumEntry : values()) {
				stringToEnumCaseInsensitive.put(enumEntry.getName().toLowerCase(), enumEntry);
			}
		}
		
		private final String name;
		
		private OpenApiDataFormat(String name) {
			this.name= name;
		}

		public String getName() {
			return name;
		}
		
		public static OpenApiDataFormat get(String name) {
			return name == null ? null : stringToEnumCaseInsensitive.get(name.toLowerCase());
		}
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum OpenApiPrimitiveType {
		INTEGER  ("integer",  OpenApiDataType.INTEGER, OpenApiDataFormat.INT_32,    PrimitiveTypeEnum.SINT32),
		LONG     ("long",     OpenApiDataType.INTEGER, OpenApiDataFormat.INT_64,    PrimitiveTypeEnum.SINT64),
		/**
		 * According to this table:https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#dataTypeFormat
		 * this entry is not allowed. The OpenAPI parser nonetheless does not complain.
		 */
		NUMBER_INTEGER  ("integer",  OpenApiDataType.NUMBER, OpenApiDataFormat.INT_32,    PrimitiveTypeEnum.SINT32),
		/**
		 * According to this table:https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md#dataTypeFormat
		 * this entry is not allowed. The OpenAPI parser nonetheless does not complain.
		 */
		NUMBER_LONG     ("long",     OpenApiDataType.NUMBER, OpenApiDataFormat.INT_64,    PrimitiveTypeEnum.SINT64),
		FLOAT    ("float",    OpenApiDataType.NUMBER,  OpenApiDataFormat.FLOAT,     PrimitiveTypeEnum.FLOAT32),
		DOUBLE   ("double",   OpenApiDataType.NUMBER,  OpenApiDataFormat.DOUBLE,    PrimitiveTypeEnum.FLOAT64),
		STRING   ("string",   OpenApiDataType.STRING,  null,                        PrimitiveTypeEnum.STRING),
		BYTE     ("byte",     OpenApiDataType.STRING,  OpenApiDataFormat.BYTE,      PrimitiveTypeEnum.SINT8),
		BINARY   ("binary",   OpenApiDataType.STRING,  OpenApiDataFormat.BINARY,    PrimitiveTypeEnum.SINT8),
		BOOLEAN  ("boolean",  OpenApiDataType.BOOLEAN, null,                        PrimitiveTypeEnum.UINT1),
		DATE     ("date",     OpenApiDataType.STRING,  OpenApiDataFormat.DATE,      PrimitiveTypeEnum.DATE),
		DATETIME ("dateTime", OpenApiDataType.STRING,  OpenApiDataFormat.DATE_TIME, PrimitiveTypeEnum.DATETIME),
		PASSWORD ("password", OpenApiDataType.STRING,  OpenApiDataFormat.PASSWORD,  PrimitiveTypeEnum.STRING),
		;
		
        private final String name;
        private final OpenApiDataType type;
        private final OpenApiDataFormat format;
        private final PrimitiveTypeEnum gappPrimitiveType;
		
		private OpenApiPrimitiveType(String name, OpenApiDataType type, OpenApiDataFormat format, PrimitiveTypeEnum gappPrimitiveType) {
			this.name= name;
			this.type = type;
			this.format = format;
			this.gappPrimitiveType = gappPrimitiveType;
		}

		public String getName() {
			return name;
		}

		public OpenApiDataType getType() {
			return type;
		}

		public OpenApiDataFormat getFormat() {
			return format;
		}

		public PrimitiveTypeEnum getGappPrimitiveType() {
			return gappPrimitiveType;
		}
		
		public static OpenApiPrimitiveType get(String type, String format) {
			return get(OpenApiDataType.get(type), OpenApiDataFormat.get(format));
		}
		
        public static OpenApiPrimitiveType get(OpenApiDataType dataType, OpenApiDataFormat dataFormat) {
        	
        	for (OpenApiPrimitiveType openApiPrmitiveType : OpenApiPrimitiveType.values()) {
        		if (openApiPrmitiveType.getType() == dataType) {
        			if (dataFormat == null || dataFormat == openApiPrmitiveType.getFormat()) {
        				return openApiPrmitiveType;
        			}
        		}
        	}
        	
        	return null;
		}
        
        public static OpenApiPrimitiveType get(Property property) {
    		return get(property.getType(), property.getFormat());
    	}
    	
    	public static OpenApiPrimitiveType get(AbstractSerializableParameter<?> parameter) {
    		return get(parameter.getType(), parameter.getFormat());
    	}
	}
}
