/**
 *
 */
package com.gs.gapp.converter.openapi.gapp.function;

import java.util.List;

import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class OpenApiToFunctionConverter extends PersistenceToBasicConverter {
	
	private OpenApiToFunctionConverterOptions converterOptions;

	public OpenApiToFunctionConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		result.add( new SwaggerToServiceInterfaceConverter<>(this) );
		
		// --- slave element converters
		result.add( new SwaggerToFunctionModuleConverter<>(this) );
		result.add( new FunctionModuleToPersistenceModuleConverter<>(this) );
		result.add( new ServiceInterfaceToServiceImplementationConverter<>(this) );
		result.add( new ServiceImplementationToServiceClientConverter<>(this) );
		
		result.add( new OpenApiModelImplToEntityConverter<>(this) );
		result.add( new OpenApiComposedModelToEntityConverter<>(this) );
		
		result.add( new OpenApiModelImplConverter<>(this) );
		
		result.add( new OpenApiAbstractPropertyToEntityFieldConverter<>(this) );
		
		result.add( new OpenApiOperationWrapperToFunctionConverter<>(this) );
		
		result.add( new OpenApiObjectPropertyToEntityConverter<>(this) );
		
		result.add( new EntityFieldToEnumerationConverter<>(this) );
		
		
		return result;
	}

	/* (non-Javadoc)
     * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#getConverterOptions()
     */
    @Override
	public OpenApiToFunctionConverterOptions getConverterOptions() {
    	if (this.converterOptions == null) {
    		this.converterOptions = new OpenApiToFunctionConverterOptions(getOptions());
    	}
		return converterOptions;
	}
}
