/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.persistence.Entity;

import io.swagger.models.ComposedModel;

/**
 * @author mmt
 *
 */
public class ComposedModelConverter<S extends ComposedModel, T extends GappOpenApiModelElementWrapper> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public ComposedModelConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	protected T onCreateModelElement(S composedModel, ModelElementI previousResultingModelElement) {

		@SuppressWarnings("unused")
		Entity entity = (Entity) previousResultingModelElement;
		
		@SuppressWarnings("unchecked")
		T result = (T) new GappOpenApiModelElementWrapper(composedModel);

		
		

		return result;
	}

	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {}
}
