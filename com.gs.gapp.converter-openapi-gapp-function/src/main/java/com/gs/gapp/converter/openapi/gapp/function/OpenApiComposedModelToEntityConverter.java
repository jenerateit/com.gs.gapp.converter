/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.openapi.ComposedModelWrapper;
import com.gs.gapp.metamodel.openapi.ModelImplWrapper;
import com.gs.gapp.metamodel.openapi.PropertyWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;

import io.swagger.models.ArrayModel;
import io.swagger.models.ComposedModel;
import io.swagger.models.Model;
import io.swagger.models.ModelImpl;
import io.swagger.models.RefModel;
import io.swagger.models.Swagger;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.Property;

/**
 * @author mmt
 *
 */
public class OpenApiComposedModelToEntityConverter<S extends ComposedModelWrapper, T extends Entity> extends OpenApiAbstractModelToModelElementConverter<S, T> {

	public OpenApiComposedModelToEntityConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	@Override
	protected void onConvert(S composedModelWrapper, T entity) {
		computeEntityForComposedModel(composedModelWrapper.getComposedModel(), entity, composedModelWrapper.getOpenApiModel());
	}
	
	private void computeEntityForComposedModel(ComposedModel composedModel, Entity entity, Swagger openApiModel) {
		StringBuilder additionalDocumentation = new StringBuilder();
		
		System.out.println("*** Entity:" + entity.getName() + ", parent from composed model:" + composedModel.getParent());
		int ii = 0;
		for (Model modelFromAllOf : composedModel.getAllOf()) {
            if (modelFromAllOf instanceof RefModel) {
				RefModel refModel = (RefModel) modelFromAllOf;
				
				Model model = openApiModel.getDefinitions().get(refModel.getSimpleRef());
                System.out.println("ref model with ref " + refModel.get$ref() + ", model for simple ref:" + model.getClass().getSimpleName());

                
                if (model instanceof ModelImpl) {
					ModelImpl modelImpl = (ModelImpl) model;
//					if (modelImpl.getEnum() != null && modelImpl.getEnum().size() > 0) {
//					}
					
					if (modelImpl.getDiscriminator() != null && modelImpl.getDiscriminator().length() > 0) {
						// TODO if we want to reflect the JSON object inheritance in the Entity object, we have to set the parent relationship here instead of simply adding the parent's properties (mmt 08-Mar-2017)
						Swagger swagger = getSwagger(entity);
						for (String key : swagger.getDefinitions().keySet()) {
							Model definition = swagger.getDefinitions().get(key);
							if (definition == modelImpl) {
								Entity parentEntity = entity.getPersistenceModule().getEntity(key);
								if (parentEntity != null) {
								    additionalDocumentation.append(System.lineSeparator()).append("this type has properties that stem from '").append(parentEntity.getName()).append("' (through composition)");
								}
								break;
							}
						}
					}
                }
                if (model instanceof ComposedModel) {
    				ComposedModel composedModelFromAllOf = (ComposedModel) model;
    				computeEntityForComposedModel(composedModelFromAllOf, entity, openApiModel);
                } else {
	                for (String key : model.getProperties().keySet()) {
	        			Property property = model.getProperties().get(key);
	        			System.out.println("property, key:" + key + ", class:" + property.getClass().getSimpleName() + ", name:" + property.getName());
	        			PropertyWrapper propertyWrapper = new PropertyWrapper(key, property, openApiModel);
	        			@SuppressWarnings("unused")
	        			EntityField entityField = convertWithOtherConverter(EntityField.class, propertyWrapper, entity);
	        		}
                }
			} else if (modelFromAllOf instanceof ArrayModel) {
				ArrayModel arrayModel = (ArrayModel) modelFromAllOf;
				System.out.println("array model, title:" + arrayModel.getTitle() + ", type:" + arrayModel.getType());
				Property propertyForItemsOfArray = arrayModel.getItems();
				if (propertyForItemsOfArray instanceof ArrayProperty) {
					@SuppressWarnings("unused")
					ArrayProperty arrayProperty = (ArrayProperty) propertyForItemsOfArray;
				}
				
			} else if (modelFromAllOf instanceof ComposedModel) {
				// TODO this code here is just a first idea so far, nothing else
				ComposedModel composedModelFromAllOf = (ComposedModel) modelFromAllOf;
				@SuppressWarnings("unused")
				GappOpenApiModelElementWrapper elementWrapper =
						convertWithOtherConverter(GappOpenApiModelElementWrapper.class, composedModelFromAllOf, entity);
//				if (entity != null) {
//					persistenceModule.addElement(entity);
//					persistenceModule.getNamespace().addEntity(entity);
//				}
			} else if (modelFromAllOf instanceof ModelImpl) {
				ModelImpl modelImplFromAllOf = (ModelImpl) modelFromAllOf;
				ModelImplWrapper modelImplWrapper = new ModelImplWrapper(entity.getName() + "-" + ii, modelImplFromAllOf, openApiModel);
				convertWithOtherConverter(ModelImplWrapper.class, modelImplWrapper, entity);
//				Entity otherEntity = convertWithOtherConverter(Entity.class, modelImplFromAllOf, entity.getPersistenceModule().getOriginatingElement(FunctionModule.class));
//				if (otherEntity != null) {
//					entity.getPersistenceModule().addElement(entity);
//					entity.getPersistenceModule().getNamespace().addEntity(entity);
//				}
			}
            
            ii++;
		}
		
		if (additionalDocumentation.length() > 0) {
			entity.setBody((entity.getBody() == null ? "" : entity.getBody()) + additionalDocumentation.toString());
		}
	}

	@Override
	protected T onCreateModelElement(S composedModelWrapper, ModelElementI previousResultingModelElement) {
//		super.onCreateModelElement(composedModelWrapper, previousResultingModelElement);  // called for performing validation checks only
		
//		Swagger swagger = getSwagger(previousResultingModelElement);
//		String name = getNameForModel(model, swagger);

        @SuppressWarnings("unchecked")
		T entity = (T) new Entity(composedModelWrapper.getName());
//        entity.setOriginatingElement(new GappOpenApiModelElementWrapper(composedModelWrapper));
        return entity;
	}
}
