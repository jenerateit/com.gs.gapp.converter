/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;

import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class SwaggerToServiceInterfaceConverter<S extends Swagger, T extends ServiceInterface> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public SwaggerToServiceInterfaceConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	@Override
	protected void onConvert(S swagger, T serviceInterface) {
		super.onConvert(swagger, serviceInterface);
		
		@SuppressWarnings("unused")
		ServiceImplementation serviceImplementation = convertWithOtherConverter(ServiceImplementation.class, serviceInterface);
		
		serviceInterface.setOriginatingElement(new GappOpenApiModelElementWrapper(swagger));
		
	}

	@Override
	protected T onCreateModelElement(S swagger, ModelElementI previousResultingModelElement) {
		
		@SuppressWarnings("unchecked")
		T serviceInterface = (T) new ServiceInterface(swagger.getInfo().getTitle().replaceAll("[^a-zA-Z0-9]", ""));
		serviceInterface.setOriginatingElement(new GappOpenApiModelElementWrapper(swagger));
		
		// --- convert swagger to function module
		@SuppressWarnings("unused")
		FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, swagger, serviceInterface);
		
		return serviceInterface;
	}
}
