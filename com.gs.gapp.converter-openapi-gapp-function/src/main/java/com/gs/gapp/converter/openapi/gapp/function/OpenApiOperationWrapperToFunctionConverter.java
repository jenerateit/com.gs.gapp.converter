/**
 * 
 */
package com.gs.gapp.converter.openapi.gapp.function;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.dsl.rest.TransportTypeEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.openapi.HttpStatusCode;
import com.gs.gapp.metamodel.openapi.ObjectPropertyWrapper;
import com.gs.gapp.metamodel.openapi.OperationWrapper;
import com.gs.gapp.metamodel.openapi.PathWrapper;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

import io.swagger.models.ArrayModel;
import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.RefModel;
import io.swagger.models.Response;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.CookieParameter;
import io.swagger.models.parameters.FormParameter;
import io.swagger.models.parameters.HeaderParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.models.parameters.RefParameter;
import io.swagger.models.parameters.SerializableParameter;
import io.swagger.models.properties.AbstractProperty;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.ObjectProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.refs.RefFormat;

/**
 * @author mmt
 *
 */
public class OpenApiOperationWrapperToFunctionConverter<S extends OperationWrapper, T extends Function> extends AbstractOpenApiToFunctionModelElementConverter<S, T> {

	public OpenApiOperationWrapperToFunctionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S operationWrapper, T function) {
		
		FunctionModule functionModule = (FunctionModule) function.getModule();
		PersistenceModule persistenceModule = functionModule.getPersistenceModules().iterator().next();
		
		Operation operation = operationWrapper.getOperation();
		@SuppressWarnings("unused")
		HttpMethod httpMethod = operationWrapper.getHttpMethod();
		@SuppressWarnings("unused")
		PathWrapper pathWrapper = operationWrapper.getPathWrapper();
		
		StringBuilder documentation = new StringBuilder();
		if (operation.getSummary() != null) {
			documentation.append(operation.getSummary());
			if(operation.getDescription() != null) {
				documentation.append(System.lineSeparator());
			}
		}
		if (operation.getDescription() != null) {
			documentation.append(operation.getDescription());
		}
		
		function.setBody(documentation.toString());
		
		// --- add REST-Path option
		OptionDefinition<String>.OptionValue pathOptionValue = RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(operationWrapper.getPathWrapper().getName());
		function.addOptions(pathOptionValue);
		
		// --- add REST-Operation option (= http verb)
		OptionDefinition<String>.OptionValue operationOptionValue = RestOptionEnum.OPTION_DEFINITION_OPERATION. new OptionValue(operationWrapper.getHttpMethod().name());
		function.addOptions(operationOptionValue);
		
		// --- add consumes and produces option values
		if (operation.getProduces() != null) {
			ArrayList<String> listOfMediaTypesForProduces = new ArrayList<>();
			for (String produces : operation.getProduces()) {
				//MediaTypeEnum mediaType = MediaTypeEnum.getInverse(produces);
				listOfMediaTypesForProduces.add(produces /* mediaType.getName()*/);
			}
			if (listOfMediaTypesForProduces.size() > 0) {
			    OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(listOfMediaTypesForProduces);
			    function.addOptions(producesOptionValue);
			}
		}
		
		if (operation.getConsumes() != null) {
			ArrayList<String> listOfMediaTypesForConsumes = new ArrayList<>();
			for (String consumes : operation.getConsumes()) {
				//MediaTypeEnum mediaType = MediaTypeEnum.getInverse(consumes);
				listOfMediaTypesForConsumes.add(consumes /*mediaType.getName()*/);
			}
			if (listOfMediaTypesForConsumes.size() > 0) {
			    OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(listOfMediaTypesForConsumes);
			    function.addOptions(consumesOptionValue);
			}
		}
		
		// --- process parameters (header, query, path, cookie, formData, body)
		for (Parameter parameter : operation.getParameters()) {
			System.out.println("*** parameter, class:" + parameter.getClass().getSimpleName() + ", in:" + parameter.getIn() + ", access:" + parameter.getAccess() + ", pattern:" + parameter.getPattern());
			if (parameter instanceof SerializableParameter) {
				SerializableParameter serializableParameter = (SerializableParameter) parameter;
				System.out.println("serializable param, type:" + serializableParameter.getType() +
						", format:" + serializableParameter.getFormat() +
						", collection format:" + serializableParameter.getCollectionFormat() +
						", pattern:" + serializableParameter.getPattern() +
						", items:" + (serializableParameter.getItems() == null ? "-noitem-" : serializableParameter.getItems().getName()));
				
				@SuppressWarnings("unused")
				String typeOfParameterValue = serializableParameter.getType();  // TODO how to evaluate this?
				
				FunctionParameter functionParameter = new FunctionParameter(parameter.getName());
				functionParameter.setBody(parameter.getDescription());
				function.addFunctionInParameter(functionParameter);
				PrimitiveType primitiveType = getPrimitiveType(serializableParameter.getType(), serializableParameter.getFormat());
				OptionDefinition<String>.OptionValue paramTypeOptionValue = null;
						
				if (primitiveType != null) {
					functionParameter.setType(primitiveType);
				} else {
					// TODO what to do in this case?
				}
				
				if (serializableParameter.getType().equalsIgnoreCase("array")) {
					functionParameter.setCollectionType(CollectionType.LIST);
				}
				
				if (parameter instanceof CookieParameter) {
					@SuppressWarnings("unused")
					CookieParameter cookieParameter = (CookieParameter) parameter;
					System.out.println("cookie param,");
					
					paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.COOKIE.getName());
				} else if (parameter instanceof FormParameter) {
					@SuppressWarnings("unused")
					FormParameter formParameter = (FormParameter) parameter;
					System.out.println("form param,");
					
					paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.FIELD.getName());
				} else if (parameter instanceof HeaderParameter) {
					@SuppressWarnings("unused")
					HeaderParameter headerParameter = (HeaderParameter) parameter;
					System.out.println("header param,");
				
					paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.HEADER.getName());
				} else if (parameter instanceof PathParameter) {
					@SuppressWarnings("unused")
					PathParameter pathParameter = (PathParameter) parameter;
					System.out.println("path param,");
					
					paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.PATH.getName());
				} else if (parameter instanceof QueryParameter) {
					QueryParameter queryParameter = (QueryParameter) parameter;
					System.out.println("query param, in:" + queryParameter.getIn());
					
					paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName());
				} else if (parameter instanceof BodyParameter) {
					BodyParameter bodyParameter = (BodyParameter) parameter;
					System.out.println("body param, in:" + bodyParameter.getIn());
					// TODO we need to have a better concept for method parameter annotations first, before we can properly handle an instance of BodyParameter (mmt 29-Mar-2017)
					paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.BODY.getName());

				}
			
				if (functionParameter.getType() == null) {
					functionParameter.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());  // TODO this is just a quick hack to avoid NPEs during development
				}
				
			    if (paramTypeOptionValue != null) {
			    	functionParameter.addOptions(paramTypeOptionValue);
			    }
			} else if (parameter instanceof BodyParameter) {
				
				OptionDefinition<String>.OptionValue paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.BODY.getName());
				FunctionParameter functionParameter = null;
				
				BodyParameter bodyParameter = (BodyParameter) parameter;
				System.out.println("body param, schema:" + bodyParameter.getSchema());
				if (bodyParameter.getSchema() != null) {
					if (bodyParameter.getSchema() instanceof RefModel) {
						RefModel refModel = (RefModel) bodyParameter.getSchema();
						Entity entity = persistenceModule.getEntity(refModel.getSimpleRef());
						ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
						
						functionParameter = new FunctionParameter(parameter.getName());
						functionParameter.setBody(parameter.getDescription());

						OptionDefinition<String>.OptionValue option = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.BODY.getName());
						functionParameter.addOptions(option);
						function.addFunctionInParameter(functionParameter);

						if (complexType != null) {
							functionParameter.setType(complexType);
			            } else {
			            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
			            }
					} else if (bodyParameter.getSchema() instanceof ArrayModel) {
						ArrayModel arrayModel = (ArrayModel) bodyParameter.getSchema();
						Property propertyForItemsOfArray = arrayModel.getItems();
						if (propertyForItemsOfArray instanceof RefProperty) {
							RefProperty refProperty = (RefProperty) propertyForItemsOfArray;
							System.out.println("ref property, ref:" + refProperty.get$ref() + ", simple ref:" + refProperty.getSimpleRef() + ", type:" + refProperty.getType() + ", ref format:" + refProperty.getRefFormat());
							
							functionParameter = new FunctionParameter(parameter.getName());
							functionParameter.setBody(parameter.getDescription());
							functionParameter.setCollectionType(CollectionType.ARRAY);
							
							if (refProperty.getRefFormat() == RefFormat.INTERNAL) {
								Entity entity = persistenceModule.getEntity(refProperty.getSimpleRef());
								if (entity != null) {
									ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
									if (complexType != null) {
										functionParameter.setType(complexType);
						            } else {
						            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
						            }
								} else {
									throw new ModelConverterException("could not get entity for simple ref '" + refProperty.getSimpleRef() + "'");
								}
							}
							
						} else if (propertyForItemsOfArray instanceof ArrayProperty) {
							ArrayProperty arrayProperty = (ArrayProperty) propertyForItemsOfArray;
							
							functionParameter = new FunctionParameter(parameter.getName());
							functionParameter.setBody(parameter.getDescription());
							functionParameter.setCollectionType(CollectionType.ARRAY);
							
							PrimitiveType fieldTypeThroughArrayType = getPrimitiveType( arrayProperty.getItems() );
							if (fieldTypeThroughArrayType != null) {
								functionParameter.setType(fieldTypeThroughArrayType);
							} else {
								// the type for the array is not a primitive one
								if (arrayProperty.getItems() instanceof RefProperty) {
									RefProperty refProperty = (RefProperty) arrayProperty.getItems();
									System.out.println("ref property, ref:" + refProperty.get$ref() + ", simple ref:" + refProperty.getSimpleRef() + ", type:" + refProperty.getType() + ", ref format:" + refProperty.getRefFormat());
									
									
									if (refProperty.getRefFormat() == RefFormat.INTERNAL) {
										Entity entity = persistenceModule.getEntity(refProperty.getSimpleRef());
										if (entity != null) {
											ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
											if (complexType != null) {
												functionParameter.setType(complexType);
								            } else {
								            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
								            }
											functionParameter.setType(entity);
										} else {
											throw new ModelConverterException("could not find entity with simple ref '" + refProperty.getSimpleRef() + "'");
										}
									}
								} else {
									throw new ModelConverterException("unhandled property type for array model:" + propertyForItemsOfArray.getClass());
								}
							}
							
						} else {
							throw new ModelConverterException("unhandled property type for array model:" + propertyForItemsOfArray.getClass());
						}
						
					} else {
						throw new ModelConverterException("unhandled schema type for a body parameter:" + bodyParameter.getSchema().getClass());
					}
				}  // if bodyParameter.getSchema() != null
				
				if (functionParameter != null && functionParameter.getType() != null) {
					if (paramTypeOptionValue != null) {
				    	functionParameter.addOptions(paramTypeOptionValue);
				    }
				    function.addFunctionInParameter(functionParameter);
				} else {
					throw new ModelConverterException("could not create a FunctionParameter for given BodyParameter '" + parameter + "'");
				}
				
			} else if (parameter instanceof RefParameter) {
				RefParameter refParameter = (RefParameter) parameter;
				System.out.println("ref param, simple ref:" + refParameter.getSimpleRef());

				Entity entity = persistenceModule.getEntity(refParameter.getSimpleRef());
				ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
				
				FunctionParameter functionParameter = new FunctionParameter(parameter.getName());
				functionParameter.setBody(parameter.getDescription());
				function.addFunctionInParameter(functionParameter);
				if (complexType != null) {
					functionParameter.setType(complexType);
	            } else {
	            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
	            }
				
			}
		}
		
		// --- process responses
		@SuppressWarnings("unused")
		String httpCodeForSuccess = null;
		Response responseForSuccess = null;
		HttpStatusCode httpStatusCodeForSuccess = null;
		Map<String, Property> successResponseHeaders = new LinkedHashMap<>();
		for (String httpCode : operation.getResponses().keySet()) {
			if ("default".equalsIgnoreCase(httpCode)) continue;  // this has no effect on the determination of the responses entry that relates to success
			Response response = operation.getResponses().get(httpCode);
			HttpStatusCode httpStatusCode = HttpStatusCode.get(httpCode);
			if (httpStatusCodeForSuccess == null) httpStatusCodeForSuccess = httpStatusCode;  // make sure that there is a value set, which is needed in case there is only one defined (which is sufficient, according to OpenAPI spec)
			if (httpStatusCode.isSuccess()) {
				if (response.getHeaders() != null) {
					for (String headerName : response.getHeaders().keySet()) {
						successResponseHeaders.put(headerName, response.getHeaders().get(headerName));
					}
				}
				if (responseForSuccess == null) {
					responseForSuccess = response;
					httpCodeForSuccess = httpCode;
					httpStatusCodeForSuccess = httpStatusCode;
				} else {
					if (httpStatusCodeForSuccess.isSuccess() == false || httpStatusCode == HttpStatusCode.SC_OK) {
						// in case the currently set status code is not for success or in case several 2xx codes are specified, use the 200 code in case there is one, otherwise use the first one that has been found
						responseForSuccess = response;
						httpCodeForSuccess = httpCode;
						httpStatusCodeForSuccess = httpStatusCode;
					}
				}
			}
		}
		
		if (successResponseHeaders.size() > 0) {
			// --- add additional out parameters that are marked as response header (a REST option)
			for (String headerName : successResponseHeaders.keySet()) {
				Property responseHeaderProperty = successResponseHeaders.get(headerName);
				ArrayProperty arrayProperty = null;
			    if (responseHeaderProperty instanceof ArrayProperty) {
				    arrayProperty = (ArrayProperty) responseHeaderProperty;
				    responseHeaderProperty = arrayProperty.getItems();
			    }
			    
			    OpenApiPrimitiveType openApiPrimitiveType = OpenApiPrimitiveType.get(responseHeaderProperty);
			    if (openApiPrimitiveType != null) {
				    PrimitiveTypeEnum primitiveTypeEnumEntry = openApiPrimitiveType.getGappPrimitiveType();
					PrimitiveType primitiveType = primitiveTypeEnumEntry.getPrimitiveType();
				    FunctionParameter functionOutParameter = new FunctionParameter(headerName);
					function.addFunctionOutParameter(functionOutParameter);
					functionOutParameter.setParameterType(ParameterType.OUT);
					functionOutParameter.setType(primitiveType);
					OptionDefinition<String>.OptionValue optionValueTransportType = RestOptionEnum.OPTION_DEFINITION_TRANSPORT_TYPE. new OptionValue(TransportTypeEnum.HEADER.getName());
					functionOutParameter.addOptions(optionValueTransportType);
					if (arrayProperty != null) {
						functionOutParameter.setCollectionType(CollectionType.LIST);
					} else if (openApiPrimitiveType == OpenApiPrimitiveType.BYTE) {
						functionOutParameter.setCollectionType(CollectionType.ARRAY);
					}
			    }
				
//				if (responseHeaderProperty instanceof StringProperty) {
//					StringProperty stringProperty = (StringProperty) responseHeaderProperty;
//				} else if (responseHeaderProperty instanceof IntegerProperty) {
//					IntegerProperty integerProperty = (IntegerProperty) responseHeaderProperty;
//				} else if (responseHeaderProperty instanceof LongProperty) {
//					LongProperty longProperty = (LongProperty) responseHeaderProperty;
//					// TODO continue for this case
//				} else if (responseHeaderProperty instanceof FloatProperty) {
//					FloatProperty floatProperty = (FloatProperty) responseHeaderProperty;
//					// TODO continue for this case
//				} else if (responseHeaderProperty instanceof DoubleProperty) {
//					DoubleProperty doubleProperty = (DoubleProperty) responseHeaderProperty;
//					// TODO continue for this case
//				} else if (responseHeaderProperty instanceof BooleanProperty) {
//					BooleanProperty booleanProperty = (BooleanProperty) responseHeaderProperty;
//				}
			}
		}
		
		if (httpStatusCodeForSuccess == null) {
			throw new ModelConverterException("could not identify a response that represents the SUCCESS case", operationWrapper);
		}
		function.addOptions(RestOptionEnum.OPTION_DEFINITION_STATUS_CODE. new OptionValue(httpStatusCodeForSuccess.getCode().longValue()));
		
		// --- TODO maybe it makes sense to invent a ResponseToExceptionConverter or a ResponseWrapperToExceptionConverter (mmt 07-Mar-2017)
		for (String httpCode : operation.getResponses().keySet()) {
			Response response = operation.getResponses().get(httpCode);
			HttpStatusCode httpStatusCode = HttpStatusCode.get(httpCode);
			
			// --- create business and technical exceptions for the given response codes
			OptionDefinition<Long>.OptionValue statusCodeOptionValue = null;
			if (httpStatusCode != null) {
				statusCodeOptionValue = RestOptionEnum.OPTION_DEFINITION_STATUS_CODE. new OptionValue(httpStatusCode.getCode().longValue());
			}
			
			ExceptionType exceptionType = null;
			if (httpStatusCode == null) {
				// this is the case for the default exception - it has no status code defined
				TechnicalException technicalException = new TechnicalException("TechnicalException" + StringTools.firstUpperCase(function.getName()) + StringTools.firstUpperCase(httpCode));
				technicalException.setOriginatingElement(new GappOpenApiModelElementWrapper(httpCode));
				function.addTechnicalException(technicalException);
				addModelElement(technicalException);
				functionModule.getNamespace().addElement(technicalException);
				technicalException.setModule(functionModule);
				exceptionType = technicalException;
			} else if (httpStatusCode.isServerError() || httpStatusCode.isRedirection()) {
				TechnicalException technicalException = new TechnicalException("TechnicalException" + StringTools.firstUpperCase(function.getName()) + StringTools.firstUpperCase(httpCode));
				technicalException.setOriginatingElement(new GappOpenApiModelElementWrapper(httpCode));
				function.addTechnicalException(technicalException);
				technicalException.addOptions(statusCodeOptionValue);  // with this we later know the HTTP status code to be used in a writer
				addModelElement(technicalException);
				functionModule.getNamespace().addElement(technicalException);
				technicalException.setModule(functionModule);
				exceptionType = technicalException;
			} else if (httpStatusCode.isClientError()) {
				BusinessException businessException = new BusinessException("BusinessException" + StringTools.firstUpperCase(function.getName()) + StringTools.firstUpperCase(httpCode));
				businessException.setOriginatingElement(new GappOpenApiModelElementWrapper(httpCode));
				function.addBusinessException(businessException);
				businessException.addOptions(statusCodeOptionValue);  // with this we later know the HTTP status code to be used in a writer
				addModelElement(businessException);
				functionModule.getNamespace().addElement(businessException);
				businessException.setModule(functionModule);
				exceptionType = businessException;
			}
			
			if (exceptionType != null) {
				StringBuilder exceptionDoc = new StringBuilder();
				if (response.getDescription() != null) {
					exceptionDoc.append(response.getDescription()).append(System.lineSeparator());
					exceptionType.setDefaultExceptionMessage(response.getDescription());
				}
				if (httpStatusCode != null) {
					exceptionDoc.append("exception type to be used for the HTTP stauts code ")
							.append(httpStatusCode.getCode()).append("(")
							.append(httpStatusCode.getDescription()).append(")");
				} else {
					exceptionDoc.append("exception type to be used as default, for cases where there is not an explicit exception type available");
				}
				exceptionType.setBody(exceptionDoc.toString());
			}
			
			Property responseSchema = response.getSchema();
			FunctionParameter functionOutParameter = null;
			
			if (httpStatusCodeForSuccess == httpStatusCode && responseSchema != null) {
				// --- here is the place, where the return parameter is created
				functionOutParameter = new FunctionParameter("result");
				function.addFunctionOutParameter(functionOutParameter);
				functionOutParameter.setParameterType(ParameterType.OUT);
			}
			
			// --- now determine the type for the function return parameter
			if (responseSchema == null) {
				// no return type modeled => nothing needs to be returned from the function (in terms of REST-APIs its still possible to add HTTP headers to the HTTP response, though)
			} else {
				AbstractProperty property = (AbstractProperty) responseSchema;
				@SuppressWarnings("unused")
				String type = property.getType();  // TODO how to handle this?
				Type functionParameterType = null;
				CollectionType collectionType = null;
				
				PrimitiveType primitiveType = getPrimitiveType(property);
				if (primitiveType != null) {
					functionParameterType = primitiveType;
					OpenApiPrimitiveType openApiPrimitiveType = OpenApiPrimitiveType.get(property);
					if (openApiPrimitiveType == OpenApiPrimitiveType.BYTE) {
						collectionType = CollectionType.ARRAY;
					}
				} else {  // an array or something else non-primitive 
					if (property instanceof ArrayProperty) {
						ArrayProperty arrayProperty = (ArrayProperty) property;
						collectionType = CollectionType.LIST;
						
						PrimitiveType typeThroughArrayType = getPrimitiveType( arrayProperty.getItems() );
						if (typeThroughArrayType != null) {
							functionParameterType = typeThroughArrayType;
							OpenApiPrimitiveType openApiPrimitiveType = OpenApiPrimitiveType.get( arrayProperty.getItems() );
							if (openApiPrimitiveType == OpenApiPrimitiveType.BYTE) {
								collectionType = CollectionType.ARRAY;
							}
						} else {
							// the type for the array is not a primitive one
							if (arrayProperty.getItems() instanceof RefProperty) {
								RefProperty refProperty = (RefProperty) arrayProperty.getItems();
								System.out.println("ref property, ref:" + refProperty.get$ref() + ", simple ref:" + refProperty.getSimpleRef() + ", type:" + refProperty.getType() + ", ref format:" + refProperty.getRefFormat());
								
								if (refProperty.getRefFormat() == RefFormat.INTERNAL) {
									Entity entity = persistenceModule.getEntity(refProperty.getSimpleRef());
									if (entity != null) {
										ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
										if (complexType != null) {
											functionParameterType = complexType;
							            } else {
							            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
							            }
									} else {
										throw new ModelConverterException("could not get entity for simple ref '" + refProperty.getSimpleRef() + "'");
									}
								}
							}
						}
					} else if (property instanceof ObjectProperty) {
						ObjectProperty objectProperty = (ObjectProperty) property;
						System.out.println("object property, name:" + objectProperty.getName());
						boolean operationIdAvailable = operationWrapper.getOperation().getOperationId() != null && operationWrapper.getOperation().getOperationId().length() > 0;
						
						String composedObjectPropertyName = operationIdAvailable ? "ResponseType" + StringTools.firstUpperCase(operationWrapper.getOperation().getOperationId()) :
							"ResponseType" + operationWrapper.getName() + httpCode;
						Entity entity = convertWithOtherConverter(Entity.class, new ObjectPropertyWrapper(composedObjectPropertyName, objectProperty, operationWrapper.getOpenApiModel()));
						if (entity != null) {
							ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
							if (complexType != null) {
								functionParameterType = complexType;
				            } else {
				            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
				            }
						} else {
							throw new ModelConverterException("could not convert object property " + objectProperty + " to an entity");
						}

					} else if (property instanceof RefProperty) {
						RefProperty refProperty = (RefProperty) property;
						System.out.println("ref property, ref:" + refProperty.get$ref() + ", simple ref:" + refProperty.getSimpleRef() + ", type:" + refProperty.getType() + ", ref format:" + refProperty.getRefFormat());
						
						if (refProperty.getRefFormat() == RefFormat.INTERNAL) {
							Entity entity = persistenceModule.getEntity(refProperty.getSimpleRef());
							if (entity != null) {
								ComplexType complexType = this.convertWithOtherConverter(ComplexType.class, entity);
								if (complexType != null) {
									functionParameterType = complexType;
					            } else {
					            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
					            }
							} else {
								throw new ModelConverterException("could not get entity for simple ref '" + refProperty.getSimpleRef() + "'");
							}
						}
					} else {
						// TODO continue here with other property types
						throw new ModelConverterException("code generation for function parameter of type '" + property.getClass() + "' not supported", property);
					}
				}
				
				if (functionParameterType != null) {
					if (functionOutParameter != null) {
						functionOutParameter.setCollectionType(collectionType);
						functionOutParameter.setType(functionParameterType);
					} else if (exceptionType != null) {
						// here we are not handling the function's return type but we are creating a field in an exception type to hold error information
						Field field = new Field("responseCode" + StringTools.firstUpperCase(httpCode), exceptionType);
						field.setType(functionParameterType);
						field.setCollectionType(collectionType);
						
						// TODO the following field could be added and initialized with the HTTP status code
//							if (httpStatusCode != null) {
//								Field statusCodeField = new Field("statusCode", exceptionType);
//								statusCodeField.setType(PrimitiveTypeEnum.SINT32.getPrimitiveType());
//								statusCodeField.setCollectionType(collectionType);
//							}
					}
				} else {
					throw new ModelConverterException("could not identify return type parameter for function '" + function.getName() + "' and http code '" + httpCode + "'");
				}
			}  // if there is a response schema
		}  // for all responses
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S operationWrapper, ModelElementI previousResultingModelElement) {
		if (!(previousResultingModelElement instanceof FunctionModule)) {
			throw new ModelConverterException("the previously resulting element has to be of type " + FunctionModule.class.getName());
		}
		FunctionModule functionModule = (FunctionModule) previousResultingModelElement;
		
		Operation operation = operationWrapper.getOperation();
		HttpMethod httpMethod = operationWrapper.getHttpMethod();
		PathWrapper pathWrapper = operationWrapper.getPathWrapper();
		String pathName = pathWrapper.getName();
		
		String name = httpMethod.name().toLowerCase();

		if (operation.getOperationId() != null && operation.getOperationId().length() > 0) {
			name = operation.getOperationId().replaceAll("[^a-zA-Z0-9]", "");
		} else if (pathName.indexOf("{") < 0 && pathName.indexOf("}") < 0 && pathName.indexOf("/") >= 0) {
			String[] splittedPathName = pathName.split("/");
			name = name + StringTools.firstUpperCase(splittedPathName[splittedPathName.length-1]);
		} else if (operation.getTags() != null && operation.getTags().size() > 0) {
			name = name + operation.getTags().get(0);
		} else if (operation.getSummary() != null) {
			name = name + operation.getSummary().replaceAll("[^a-zA-Z0-9]", "");
		}

        @SuppressWarnings("unchecked")
		T function = (T) new Function(name, functionModule);
        return function;
	}
}
