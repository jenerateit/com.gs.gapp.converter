/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.openapi.gapp.function;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author mmt
 *
 */
public class OpenApiToFunctionConverterProvider implements ModelConverterProviderI {

	/**
	 * 
	 */
	public OpenApiToFunctionConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new OpenApiToFunctionConverter();
	}
}
