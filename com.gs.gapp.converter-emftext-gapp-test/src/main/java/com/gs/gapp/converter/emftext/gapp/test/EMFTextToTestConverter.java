/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.test;

import java.util.List;
import com.gs.gapp.converter.emftext.gapp.product.EMFTextToProductConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class EMFTextToTestConverter extends EMFTextToProductConverter {

	/**
	 * 
	 */
	public EMFTextToTestConverter() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new TestModuleConverter<>(this));
		result.add(new TestConverter<>(this));
		result.add(new StepConverter<>(this));

		return result;
	}

}
