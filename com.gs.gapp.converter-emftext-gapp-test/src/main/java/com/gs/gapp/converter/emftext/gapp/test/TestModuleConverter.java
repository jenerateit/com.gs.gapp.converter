/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.test;


import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.test.TestElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.test.Namespace;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.test.TestModule;

/**
 * @author mmt
 *
 */
public class TestModuleConverter<S extends Module, T extends TestModule>
    extends ModuleConverter<S, T> {

	public TestModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T testModule) {

		super.onConvert(module, testModule);

		Namespace namespace = new Namespace(module.getNamespace().getName());
		testModule.addElement(namespace);
		testModule.setNamespace(namespace);

		for (Element moduleElement : module.getElements()) {

			Test test = this.convertWithOtherConverter(Test.class, moduleElement);
			if (test != null) {
				testModule.addElement(test);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.hasElementsOfTypes( new BasicEList<>(TestElementEnum.getElementTypeNames()) ) == false) {
					// only if the module has at least one test element type, it is recognized as a test module
					generalResponsibility = false;
				}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		TestModule testModule = new TestModule(module.getName());
		testModule.setOriginatingElement(new GappModelElementWrapper(module));
		return (T) testModule;
	}
}
