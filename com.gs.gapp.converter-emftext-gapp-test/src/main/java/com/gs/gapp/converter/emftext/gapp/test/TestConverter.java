/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.test;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.ComplexTypeConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.test.TestElementEnum;
import com.gs.gapp.dsl.test.TestOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public class TestConverter<S extends Element, T extends Test>
    extends ComplexTypeConverter<S, T> {

	public TestConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T test) {
		super.onConvert(element, test);
		
		// --- parent test
		ComplexType parentTest = null;
		if (element.getParent() != null) {
			parentTest = this.convertWithOtherConverter(ComplexType.class, element.getParent(), new Class<?>[0]);
			if (parentTest != null) {
				test.setParent(parentTest);
			} else {
				throw new ModelConverterException("was not able to convert a parent test: '" + element.getParent() + "'");
			}
		}

		// --- test prelude parameters
		OptionValueReference optionValueReference = element.getOptionValueReferencesReader().getOptionValueReference(TestOptionEnum.PRELUDE_PARAMETERS.getName());
		if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
			ComplexType testPreludeParametersType = this.convertWithOtherConverter(ComplexType.class, optionValueReference.getReferencedObject());
			if (testPreludeParametersType != null) {
			    test.setPreludeParameters(testPreludeParametersType);
			}
		}
		
		// --- test prelude views
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(TestOptionEnum.PRELUDE_VIEWS.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
                UIStructuralContainer structuralContainer =
                		this.convertWithOtherConverter(UIStructuralContainer.class, reference.getReferencedObject());
            	
            	if (structuralContainer != null) {
            		test.addPreludeView(structuralContainer);
            	} else {
            		throw new ModelConverterException("not able to create a UiStructuralContainer object for given referenced element '" + reference.getReferencedObject() + "'");
            	}
            }
		}
		
		// --- create steps
		for (ElementMember elementMember : element.getElementMembers()) {
			@SuppressWarnings("unused")
			Step step = convertWithOtherConverter(Step.class, elementMember, test);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return TestElementEnum.TEST;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.isTypeof(TestElementEnum.TEST.getName())) {
                    // yes, it is an entity, we are going to convert it
            	} else {
            		generalResponsibility = false;
            	}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T test = (T) new Test(element.getName());
		test.setOriginatingElement(new GappModelElementWrapper(element));

		return test;
	}
}
