/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.test;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.test.TestMemberEnum;
import com.gs.gapp.dsl.test.TestOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.ActionTypeI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.test.Step.Action;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.ui.ActionType;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public class StepConverter<S extends ElementMember, T extends Step>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public StepConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.FieldConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (previousResultingModelElement == null) {
			return false;
		} else {
			if (result) {
				if (originalModelElement instanceof ElementMember) {
					ElementMember elementMember = (ElementMember) originalModelElement;
	            	if (elementMember.isTypeof(TestMemberEnum.STEP.getName())) {
	                    // yes, it is a step, we are going to convert it
	            	} else {
	            		result = false;
	            	}
	            }
			}
		}

		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T step) {
		super.onConvert(elementMember, step);
		
		// --- step type
		final Element memberType = elementMember.getMemberType();
		if (memberType != null) {
			final UIStructuralContainer container = convertWithOtherConverter(UIStructuralContainer.class, memberType);
			if (container != null) {
			    step.setPageContainer(container);
			} else {
				throw new ModelConverterException("was not able to convert the type '" + memberType + "' to a UIStructuralContainer", elementMember);
			}
		} else {
			// there was no member-type modeled, which could have been made on purpose (e.g. for automated derivation of the type according to option settings)
			// TODO what should we do in this case?
		}

		
		// --- actions
		EList<OptionValueReference> optionValueReferences = elementMember.getOptionValueReferencesReader().getOptionValueReferences(TestOptionEnum.ACTIONS.getName());
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				ModelElement referencedObject = optionValueReference.getReferencedObject();
				UIContainer uiContainer = null;
				UIComponent uiComponent = convertWithOtherConverter(UIComponent.class, referencedObject);
				if (uiComponent == null) {
					// --- the referenced element is not a ui component, try to convert it to a ui container
					uiContainer = convertWithOtherConverter(UIContainer.class, referencedObject);	
				}
				
				if (uiComponent != null || uiContainer != null) {
	                OptionEnumerationEntry actionTypeEnumEntry = optionValueReference.getReferenceEnumerationValue();
	                ActionTypeI actionType = ActionType.valueOf(actionTypeEnumEntry.getEntry());
	                actionType = actionType != null ? actionType : ActionType.CUSTOM;
	                Action action = new Action(actionType, uiComponent != null ? uiComponent : uiContainer);
	                step.addAction(action);
				} else {
					throw new ModelConverterException("could not convert referenced object '" + referencedObject + "' to a ui component or a ui container");
				}
			}
		}
		
		// --- open-the-step-view flag
		Boolean openTheStepView =
			    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(TestOptionEnum.OPEN_THE_STEP_VIEW.getName());
		if (openTheStepView != null) {
			step.setOpenThePageContainer(openTheStepView);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previousResultingElement) {
		Test stepOwner = null;
		if (previousResultingElement != null && previousResultingElement instanceof Test) {
			stepOwner = (Test) previousResultingElement;
		} else {
			throw new ModelConverterException("no preivous element of type 'Test' found, cannot continue member conversion", elementMember);
		}

		T step = (T) new Step(elementMember.getName(), stepOwner);
		
		step.setOriginatingElement(new GappModelElementWrapper(elementMember));
 
		return step;
	}
	
	@Override
	public IMetatype getMetatype() {
		return TestMemberEnum.STEP;
	}
}
