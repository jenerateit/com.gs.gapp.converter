package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.Property;
import org.eclipse.vorto.core.api.model.functionblock.Fault;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.device.Sensor;

public class FaultToExceptionTypeConverter<S extends Fault, T extends ExceptionType> extends AbstractModelElementConverter<S, T> {

	public FaultToExceptionTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S fault, T exceptionType) {
		for (Property property : fault.getProperties())  {
			convertWithOtherConverter(Field.class, property, exceptionType);
		}
		
		//System.out.println("create ExceptionType (from Fault) " + exceptionType.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S fault, ModelElementI previouslyResultingElement) {
		String name = ((Sensor)previouslyResultingElement).getName() + "Fault";
		//System.out.println("create ExceptionType (from Fault) " + name + "...");
		
		@SuppressWarnings("unchecked")
		T result = (T) new ExceptionType(name);
		
		return result;
	}
	
	
	

}
