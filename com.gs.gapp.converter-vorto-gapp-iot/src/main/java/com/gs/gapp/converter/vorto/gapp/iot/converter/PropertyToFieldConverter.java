package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.Entity;
import org.eclipse.vorto.core.api.model.datatype.Enum;
import org.eclipse.vorto.core.api.model.datatype.ObjectPropertyType;
import org.eclipse.vorto.core.api.model.datatype.PrimitivePropertyType;
import org.eclipse.vorto.core.api.model.datatype.PrimitiveType;
import org.eclipse.vorto.core.api.model.datatype.Property;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

public class PropertyToFieldConverter<S extends Property, T extends Field> extends AbstractModelElementConverter<S, T> {

	public PropertyToFieldConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S property, T field) {
		if (property.getType() instanceof ObjectPropertyType) {
			ObjectPropertyType objectPropertyType = (ObjectPropertyType) property.getType();
			if(objectPropertyType.getType() instanceof Entity) {
				Entity entity = (Entity) objectPropertyType.getType();
				ComplexType complexTypeForEntity = convertWithOtherConverter(ComplexType.class, entity);
				field.setType(complexTypeForEntity);
			}
			else if(objectPropertyType.getType() instanceof Enum) {
				Enum vortoEnumType = (Enum) objectPropertyType.getType();
				ComplexType complexTypeForEntity = convertWithOtherConverter(ComplexType.class, vortoEnumType);
				field.setType(complexTypeForEntity);
			}
		} else if (property.getType() instanceof PrimitivePropertyType) {
			PrimitivePropertyType primitivePropertyType = (PrimitivePropertyType) property.getType();
			PrimitiveType primitiveType = primitivePropertyType.getType();
			com.gs.gapp.metamodel.basic.typesystem.PrimitiveType primitiveTypeForPrimitiveType =
					convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.PrimitiveType.class, primitiveType);
			field.setType(primitiveTypeForPrimitiveType);
			
		} else {
			throw new RuntimeException("setType for field failed");
		}
		if(property.isMultiplicity()) {
			field.setCollectionType(CollectionType.LIST);
		}
		//System.out.println("create Field (from Property) " + property.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S property, ModelElementI previouslyResultingElement) {
		//System.out.println("create Field (from Property) " + property.getName() + "...");
		
		@SuppressWarnings("unchecked")
		T result = (T) new Field(property.getName(), (ComplexType) previouslyResultingElement);
		
		return result;
	}
}
