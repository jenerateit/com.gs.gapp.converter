package com.gs.gapp.converter.vorto.gapp.iot.converter;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Namespace;
import com.gs.gapp.metamodel.iot.device.DeviceModule;

public class StringToDeviceModuleConverter<S extends String, T extends DeviceModule> extends AbstractModelElementConverter<S, T> {

	public StringToDeviceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S informationModel, T deviceModule) {

		//System.out.println("create DeviceModule " + deviceModule.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S name, ModelElementI previouslyResultingElement) {
		//System.out.println("create DeviceModule " + name + "...");

		@SuppressWarnings("unchecked")
		T result = (T) new DeviceModule(name);
		result.setNamespace(new Namespace(name));
		
		return result;
	}

}
