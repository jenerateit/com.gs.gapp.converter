package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.Property;
import org.eclipse.vorto.core.api.model.functionblock.Status;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.device.Sensor;

public class StatusToComplexTypeConverter<S extends Status, T extends ComplexType> extends AbstractModelElementConverter<S, T> {

	public StatusToComplexTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S status, T complexType) {
		for (Property property : status.getProperties())  {
			convertWithOtherConverter(Field.class, property, complexType);

			// TODO set measurementunit with property.getPropertyAttributes()
		}
		
		//System.out.println("create ComplexType (from Status) " + complexType.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S status, ModelElementI previouslyResultingElement) {
		String name = ((Sensor)previouslyResultingElement).getName() + "Status";
		//System.out.println("create ComplexType (from Status) " + name + "...");
		
		@SuppressWarnings("unchecked")
		T result = (T) new ComplexType(name);
		
		return result;
	}
	
	
	

}
