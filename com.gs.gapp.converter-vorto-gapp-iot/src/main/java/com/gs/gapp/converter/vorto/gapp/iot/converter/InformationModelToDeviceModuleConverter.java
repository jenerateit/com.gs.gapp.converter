package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.informationmodel.InformationModel;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Namespace;
import com.gs.gapp.metamodel.iot.device.DeviceModule;

public class InformationModelToDeviceModuleConverter<S extends InformationModel, T extends DeviceModule> extends AbstractModelElementConverter<S, T> {

	public InformationModelToDeviceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}
	
	@Override
	protected void onConvert(S informationModel, T deviceModule) {
		Namespace namespace = new Namespace(informationModel.getNamespace());
		deviceModule.setNamespace(namespace);
		
		//System.out.println("create DeviceModule " + deviceModule.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S informationModel, ModelElementI previouslyResultingElement) {
		//System.out.println("create DeviceModule " + informationModel.getName() + "...");

		@SuppressWarnings("unchecked")
		T result = (T) new DeviceModule(informationModel.getName());
		
		return result;
	}

}
