package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.informationmodel.FunctionblockProperty;
import org.eclipse.vorto.core.api.model.informationmodel.InformationModel;

import com.gs.gapp.converter.vorto.gapp.iot.AbstractIotModelElementConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.device.DeviceModule;
import com.gs.gapp.metamodel.iot.device.SensorUsage;

public class InformationModelToDeviceConverter<S extends InformationModel, T extends Device> extends AbstractIotModelElementConverter<S, T> {

	public InformationModelToDeviceConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	@Override
	protected void onConvert(S informationModel, T device) {
		Application application = convertWithOtherConverter(Application.class, getModel());
		application.addDevice(device);
		device.setApplication(application);
		
		DeviceModule deviceModule = convertWithOtherConverter(DeviceModule.class, informationModel);
		deviceModule.addDevice(device);
		
		for(FunctionblockProperty property: informationModel.getProperties()) {
			SensorUsage sensorUsage = convertWithOtherConverter(SensorUsage.class, property, device);
			sensorUsage.setModule(deviceModule);
		}
		
		//System.out.println("create Device " + informationModel.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S informationModel, ModelElementI previouslyResultingElement) {
		//System.out.println("create Device " + informationModel.getName() + "...");

		@SuppressWarnings("unchecked")
		T device = (T) new Device(informationModel.getName());
		
		return device;
	}

}
