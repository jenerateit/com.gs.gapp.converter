package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.emf.common.util.EList;
import org.eclipse.vorto.core.api.model.functionblock.Operation;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.iot.device.Sensor;

public class OperationsToFunctionModuleConverter<S extends EList<Operation>, T extends FunctionModule> extends AbstractModelElementConverter<S, T> {

	public OperationsToFunctionModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S operations, T functionModule) {
		for(Operation op : operations) {
			Function fnc = new Function(op.getName(), functionModule);
			functionModule.addFunction(fnc);
		}
		
		//System.out.println("create FunctionModule " + functionModule.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S operations, ModelElementI previouslyResultingElement) {
		String name = ((Sensor) previouslyResultingElement).getName() + "Operations";
		//System.out.println("create FunctionModule " + name + "...");

		@SuppressWarnings("unchecked")
		T result = (T) new FunctionModule(name);
		
		return result;
	}
	
	
	

}
