package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.PrimitiveType;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

public class PrimitiveTypeToPrimitiveTypeConverter<S extends PrimitiveType, T extends com.gs.gapp.metamodel.basic.typesystem.PrimitiveType> extends AbstractModelElementConverter<S, T> {

	public PrimitiveTypeToPrimitiveTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S vortoPrimitive, T gappPrimitive) {

		//System.out.println("create PrimitiveType " + vortoPrimitive.getName() + " [DONE]");
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S vortoPrimitive, ModelElementI previouslyResultingElement) {
		//System.out.println("create PrimitiveType " + vortoPrimitive.getName() + "...");
		
		T result = null;
		switch(vortoPrimitive.getName().toLowerCase()) {
			case "string":
				result = (T) PrimitiveTypeEnum.STRING.getPrimitiveType();
				break;
			case "int":
				result = (T) PrimitiveTypeEnum.SINT32.getPrimitiveType();
				break;
			case "float":
				result = (T) PrimitiveTypeEnum.FLOAT32.getPrimitiveType();
				break;
			case "boolean":
				result = (T) PrimitiveTypeEnum.UINT1.getPrimitiveType();
				break;
			case "datetime":
				result = (T) PrimitiveTypeEnum.DATETIME.getPrimitiveType();
				break;
			case "double":
				result = (T) PrimitiveTypeEnum.FLOAT64.getPrimitiveType();
				break;
			case "long":
				result = (T) PrimitiveTypeEnum.SINT64.getPrimitiveType();
				break;
			case "short":
				result = (T) PrimitiveTypeEnum.SINT16.getPrimitiveType();
				break;
			case "base64binary":
				result = (T) PrimitiveTypeEnum.STRING.getPrimitiveType();
				break;
			case "byte":
				result = (T) PrimitiveTypeEnum.UINT8.getPrimitiveType();
				break;
			default:
				throw new ModelConverterException("unknown primitive type name found in vorto model: '" + vortoPrimitive.getName() + "'" , vortoPrimitive);
		}
		
		
		return result;
	}
}
