/**
 *
 */
package com.gs.gapp.converter.vorto.gapp.iot;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.vorto.core.api.model.functionblock.FunctionblockModel;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.AbstractIotModelElement;


public abstract class AbstractIotModelElementConverter<S extends EObject, T extends AbstractIotModelElement>
    extends AbstractModelElementConverter<S, T> {

	public AbstractIotModelElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	public AbstractIotModelElementConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}
	
	
	/**
	 * @param functionblockModel
	 * @return
	 */
	protected static boolean isSensorOnly(FunctionblockModel functionblockModel) {
		return false;  // TODO how to find out whether a function block is an actuator? (mmt 2017-Jan-25)
//		boolean isSensorOnly = true;
//		if (functionblockModel.getFunctionblock().getOperations().size() > 0) {
//			for(Operation op : functionblockModel.getFunctionblock().getOperations()) {
//				if(!op.getName().startsWith("get") && !op.getName().startsWith("is")) {
//					isSensorOnly = false;
//				}
//			}
//		} else {
//			
//		}
//		return true;
	}
}
