package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.Entity;
import org.eclipse.vorto.core.api.model.datatype.Property;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

public class EntityToComplexTypeConverter<S extends Entity, T extends ComplexType> extends AbstractModelElementConverter<S, T> {

	public EntityToComplexTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S entity, T complexType) {
		complexType.setModule(convertWithOtherConverter(Module.class, entity.getNamespace()));
		
		for(Property property : entity.getProperties()) {
			convertWithOtherConverter(Field.class, property, complexType);
			
		}

		//System.out.println("create ComplexType (from Entity) " + entity.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S entity, ModelElementI previouslyResultingElement) {
		//System.out.println("create ComplexType (from Entity) " + entity.getName() + "...");
		
		@SuppressWarnings("unchecked")
		T result = (T) new ComplexType(entity.getName());
		
		return result;
	}
}
