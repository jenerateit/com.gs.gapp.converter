package com.gs.gapp.converter.vorto.gapp.iot;

public class Constants {

	
	public static final Integer HISTORY_LENGTH = 10;
	public static final Long SAMPLING_INTERVAL = (long) 5000;
	public static final String DEFAULT_NAMESPACE = "com.gs.vd";
	public static final String GATEWAY_NAME = "VortoIoTGateway";
	
}
