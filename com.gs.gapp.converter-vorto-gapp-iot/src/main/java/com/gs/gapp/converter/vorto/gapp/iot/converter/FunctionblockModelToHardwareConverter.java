package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.functionblock.FunctionBlock;
import org.eclipse.vorto.core.api.model.functionblock.FunctionblockModel;

import com.gs.gapp.converter.vorto.gapp.iot.AbstractIotModelElementConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.iot.device.Actuator;
import com.gs.gapp.metamodel.iot.device.DeviceModule;
import com.gs.gapp.metamodel.iot.device.Sensor;

public class FunctionblockModelToHardwareConverter<S extends FunctionblockModel, T extends Sensor> extends AbstractIotModelElementConverter<S, T> {

	public FunctionblockModelToHardwareConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S functionblockModel, T sensor) {
		DeviceModule deviceModule = convertWithOtherConverter(DeviceModule.class, functionblockModel.getNamespace());
		
		if(sensor instanceof Actuator) {
			Actuator actuator = (Actuator) sensor;
			deviceModule.addActuator(actuator);
		}
		else {
			deviceModule.addSensor(sensor);
		}
		
		
		FunctionBlock block = functionblockModel.getFunctionblock();
		if(block.getConfiguration() != null) {
			ComplexType configurationDataStructure = convertWithOtherConverter(ComplexType.class, block.getConfiguration(), sensor);
			sensor.setConfigurationDataStructure(configurationDataStructure);
			configurationDataStructure.setModule(deviceModule);
		}
		//sensor.setConnectionDataStructure(connectionDataStructure);
		if(block.getFault() != null) {
			ExceptionType faultDataStructure = convertWithOtherConverter(ExceptionType.class, block.getFault(), sensor);
			sensor.setFaultDataStructure(faultDataStructure);
			faultDataStructure.setModule(deviceModule);
		}

		if(block.getStatus() != null) {
			ComplexType statusDataStructure = convertWithOtherConverter(ComplexType.class, block.getStatus(), sensor);
			sensor.setStatusDataStructure(statusDataStructure);
			statusDataStructure.setModule(deviceModule);
		}
		
		if(block.getOperations() != null && block.getOperations().size() > 0) {
			FunctionModule operations = convertWithOtherConverter(FunctionModule.class, block.getOperations(), sensor);
			sensor.setOperations(operations);
		}
		
		if(sensor instanceof Actuator) {
			//System.out.println("create Actuator " + functionblockModel.getName() + " [DONE]");
		}
		else {
			//System.out.println("create Sensor " + functionblockModel.getName() +  " [DONE]");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S functionblockModel, ModelElementI previouslyResultingElement) {
		T result = null;
		if(isSensorOnly(functionblockModel)) {
			//System.out.println("create Sensor " + functionblockModel.getName() + "...");
			result = (T) new Sensor(functionblockModel.getName());
		}
		else {
			//System.out.println("create Actuator " + functionblockModel.getName() + "...");
			result = (T) new Actuator(functionblockModel.getName());
		}
		
		return result;
	}

}
