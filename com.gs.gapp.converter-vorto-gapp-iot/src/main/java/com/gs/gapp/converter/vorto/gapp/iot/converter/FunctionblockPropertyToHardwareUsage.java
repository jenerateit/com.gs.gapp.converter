package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.informationmodel.FunctionblockProperty;

import com.gs.gapp.converter.vorto.gapp.iot.AbstractIotModelElementConverter;
import com.gs.gapp.converter.vorto.gapp.iot.Constants;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.businesslogic.BusinessLogic;
import com.gs.gapp.metamodel.iot.device.Actuator;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.device.Sensor;
import com.gs.gapp.metamodel.iot.device.SensorUsage;

public class FunctionblockPropertyToHardwareUsage<S extends FunctionblockProperty, T extends SensorUsage> extends AbstractIotModelElementConverter<S, T> {

	public FunctionblockPropertyToHardwareUsage(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	
	@Override
	protected void onConvert(S functionblockProperty, T sensorUsage) {
		String topicName = sensorUsage.getName();
		sensorUsage.setTopic(new Topic(topicName, topicName));
		sensorUsage.setHistoryLength(Constants.HISTORY_LENGTH);
		sensorUsage.setSamplingInterval(Constants.SAMPLING_INTERVAL);
		
		//--- add businesslogic
		String businessName = sensorUsage.getOwner().getName() + sensorUsage.getName();
		Application application = convertWithOtherConverter(Application.class, getModel());
		BusinessLogic logic = new BusinessLogic(businessName, application);
		if(sensorUsage instanceof ActuatorUsage) {
			ActuatorUsage actuatorUsage = (ActuatorUsage) sensorUsage;
			logic.addTriggerActuatorUsage(actuatorUsage);
		}
		else {
			logic.addTriggerSensorUsage(sensorUsage);
		}
		
		if(sensorUsage instanceof ActuatorUsage) {
			ActuatorUsage actuatorUsage = (ActuatorUsage) sensorUsage;
			Actuator actuator = (Actuator) convertWithOtherConverter(Sensor.class, functionblockProperty.getType());
			actuator.addActuatorUsage(actuatorUsage);
			actuatorUsage.setImplementation(actuator);
			//System.out.println("create ActuatorUsage " + functionblockProperty.getName() + " [DONE]");		
		}
		else {
			Sensor sensor = convertWithOtherConverter(Sensor.class, functionblockProperty.getType());
			sensor.addSensorUsage(sensorUsage);
			sensorUsage.setImplementation(sensor);
			//System.out.println("create SensorUsage " + functionblockProperty.getName() + " [DONE]");
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S functionblockProperty, ModelElementI previouslyResultingElement) {
		String name = Character.toUpperCase(functionblockProperty.getName().charAt(0)) + functionblockProperty.getName().substring(1);
		T result = null;
		if(isSensorOnly(functionblockProperty.getType())) {
			//System.out.println("create SensorUsage " + name + "...");
			result = (T) new SensorUsage(name, (Device) previouslyResultingElement);
		}
		else {
			//System.out.println("create ActuatorUsage " + name + "...");
			result = (T) new ActuatorUsage(name, (Device) previouslyResultingElement);
		}
		
		
		return result;
	}

}
