package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.Enum;
import org.eclipse.vorto.core.api.model.datatype.EnumLiteral;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

public class EnumToEnumerationConverter<S extends Enum, T extends Enumeration> extends AbstractModelElementConverter<S, T> {

	public EnumToEnumerationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S vortoEnum, T gappEnum) {
		gappEnum.setModule(convertWithOtherConverter(Module.class, vortoEnum.getNamespace()));
		for(EnumLiteral enumLiteral : vortoEnum.getEnums()) {
			EnumerationEntry enumerationEntry = new EnumerationEntry(enumLiteral.getName());
			gappEnum.addEnumerationEntry(enumerationEntry);
		}

		//System.out.println("create Enumeration " + vortoEnum.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S vortoEnum, ModelElementI previouslyResultingElement) {
		//System.out.println("create Enumeration " + vortoEnum.getName() + "...");
		
		@SuppressWarnings("unchecked")
		T result = (T) new Enumeration(vortoEnum.getName());
		
		return result;
	}
}
