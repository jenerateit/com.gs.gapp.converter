
package com.gs.gapp.converter.vorto.gapp.iot;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;


public class VortoToIotConverterProvider implements ModelConverterProviderI {

	/**
	 * 
	 */
	public VortoToIotConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new VortoToIotConverter();
	}
}
