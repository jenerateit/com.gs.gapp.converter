package com.gs.gapp.converter.vorto.gapp.iot.converter;

import com.gs.gapp.converter.vorto.gapp.iot.Constants;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.ApplicationTypeEnum;
import com.gs.gapp.metamodel.iot.topology.Broker;
import com.gs.gapp.metamodel.iot.topology.BrokerConnection;

public class ModelToApplicationConverter<S extends Model, T extends Application> extends AbstractModelElementConverter<S, T> {

	public ModelToApplicationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S model, T application) {
		application.setApplicationType(ApplicationTypeEnum.GATEWAY);
		BrokerConnection brokerConnection = new BrokerConnection("DataPublishing", application, new Broker("Mosquitto"));
		application.addBrokerConnection(brokerConnection);
		Module module = new Module("ApplicationModule");
		module.setNamespace(new Namespace(Constants.DEFAULT_NAMESPACE));  // TODO is there a way to use a namespace of one of the Vorto model files? (mmt 2017-Jan-25)
		application.setModule(module);
		
		//System.out.println("create Application " + application.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S model, ModelElementI previouslyResultingElement) {
		String name = Constants.GATEWAY_NAME;
		//System.out.println("create Application " + name + "...");

		@SuppressWarnings("unchecked")
		T result = (T) new Application(name);
		
		return result;
	}
	
	
	

}
