package com.gs.gapp.converter.vorto.gapp.iot.converter;

import org.eclipse.vorto.core.api.model.datatype.Property;
import org.eclipse.vorto.core.api.model.functionblock.Configuration;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.device.Sensor;

public class ConfigurationToComplexTypeConverter<S extends Configuration, T extends ComplexType> extends AbstractModelElementConverter<S, T> {

	public ConfigurationToComplexTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}

	@Override
	protected void onConvert(S configuration, T complexType) {
		for (Property property : configuration.getProperties())  {
			
			convertWithOtherConverter(Field.class, property, complexType);
		}
		

		//System.out.println("create ComplexType (from Configuration) " + complexType.getName() + " [DONE]");
	}

	@Override
	protected T onCreateModelElement(S configuration, ModelElementI previouslyResultingElement) {
		String name = ((Sensor)previouslyResultingElement).getName() + "Configuration";
		//System.out.println("create ComplexType (from Configuration) " + name + "...");
		
		@SuppressWarnings("unchecked")
		T result = (T) new ComplexType(name);
		
		return result;
	}
	
	
	

}
