/**
 *
 */
package com.gs.gapp.converter.vorto.gapp.iot;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.ConfigurationToComplexTypeConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.EntityToComplexTypeConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.EnumToEnumerationConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.FaultToExceptionTypeConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.FunctionblockModelToHardwareConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.FunctionblockPropertyToHardwareUsage;
import com.gs.gapp.converter.vorto.gapp.iot.converter.InformationModelToDeviceConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.InformationModelToDeviceModuleConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.ModelToApplicationConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.OperationsToFunctionModuleConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.PrimitiveTypeToPrimitiveTypeConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.PropertyToFieldConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.StatusToComplexTypeConverter;
import com.gs.gapp.converter.vorto.gapp.iot.converter.StringToDeviceModuleConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;


public class VortoToIotConverter extends AbstractAnalyticsConverter {


	public VortoToIotConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();


		result.add(new InformationModelToDeviceConverter<>(this));
		result.add(new InformationModelToDeviceModuleConverter<>(this));
		result.add(new ModelToApplicationConverter<>(this));
		
		result.add(new FunctionblockPropertyToHardwareUsage<>(this));

		result.add(new FunctionblockModelToHardwareConverter<>(this));
		
		result.add(new ConfigurationToComplexTypeConverter<>(this));
		result.add(new StatusToComplexTypeConverter<>(this));
		result.add(new FaultToExceptionTypeConverter<>(this));
		result.add(new OperationsToFunctionModuleConverter<>(this));
		
		result.add(new EntityToComplexTypeConverter<>(this));
		result.add(new EnumToEnumerationConverter<>(this));
		result.add(new PrimitiveTypeToPrimitiveTypeConverter<>(this));
		result.add(new PropertyToFieldConverter<>(this));
		result.add(new StringToDeviceModuleConverter<>(this));
		
		return result;
	}


	@Override
	protected Set<Object> clientConvert(Collection<?> rawElements, ModelConverterOptions options)
			throws ModelConverterException {

		return super.clientConvert(rawElements, options);
	}
}
