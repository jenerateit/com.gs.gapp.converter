/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.converter.emftext.gapp.iot.EMFTextToIotConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.product.Capability;
import com.gs.gapp.metamodel.product.ProductMetamodel;
import com.gs.gapp.metamodel.product.ValidatorApplications;

/**
 * @author mmt
 *
 */
public class EMFTextToProductConverter extends EMFTextToIotConverter {

	private EMFTextToProductConverterOptions converterOptions;
	
	/**
	 * 
	 */
	public EMFTextToProductConverter() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.basic.java.BasicToJavaConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		super.onInitOptions();
		this.converterOptions = new EMFTextToProductConverterOptions(getOptions());
	}
	
	@Override
	public EMFTextToProductConverterOptions getConverterOptions() {
		return converterOptions;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new ProductModuleConverter<>(this));
		result.add(new FeatureConverter<>(this));
		result.add(new CapabilityConverter<>(this));
		result.add(new OrganizationConverter<>(this));
		result.add(new ProductConverter<>(this));
		result.add(new ProductVariantConverter<>(this));
		result.add(new UiApplicationConverter<>(this));
		result.add(new ServiceApplicationConverter<>(this));
		result.add(new IotApplicationConverter<>(this));

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.function.EMFTextToFunctionConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
        Set<Object> intermediateResult = super.onPerformModelConsolidation(normalizedElements);
        List<Capability> capabilities = intermediateResult.stream().filter(obj -> obj instanceof Capability).map(obj -> (Capability) obj).collect(Collectors.toList());
        
        // --- autohealing functionality - whenever there is exactly one capability and one ui app xor/and one service app, we can make the assignment automatically 
        ProductMetamodel.INSTANCE.autolinkSingleCapabilityWithSingleApplication(intermediateResult, capabilities);
        
        // --- create link between capabilities in case there is more than one capability given in the generation input.
        ProductMetamodel.INSTANCE.completeCapabilityDependencies(getConverterOptions().getNameOfFilteredCapability(), capabilities);
        
		return intermediateResult;
	}

	@Override
	protected void onPerformModelValidation(Set<Object> result) {
		super.onPerformModelValidation(result);
		
		new ValidatorApplications()
		    .validate(result)
		    .forEach(message -> addMessage(message));
	}
}
