/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.product.Feature;
import com.gs.gapp.metamodel.product.Product;
import com.gs.gapp.metamodel.product.ProductVariant;

/**
 * @author mmt
 *
 */
public class ProductVariantConverter<S extends Element, T extends ProductVariant> extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ProductVariantConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T productVariant) {
		super.onConvert(element, productVariant);
		
		// --- product
		if (element.getOptionValueReferencesReader().getOptionValueReference(ProductOptionEnum.PRODUCT.getName()) != null) {
			Product product = convertWithOtherConverter(Product.class, element.getOptionValueReferencesReader().getOptionValueReference(ProductOptionEnum.PRODUCT.getName()).getReferencedObject());
			productVariant.setProduct(product);
		}
		
		// --- features
		EList<OptionValueReference> optionValueReferences = element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.FEATURES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Feature feature = this.convertWithOtherConverter(Feature.class, reference.getReferencedObject());
            	if (feature != null) productVariant.addFeature(feature);
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return ProductElementEnum.VARIANT;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T productVariant = (T) new ProductVariant(element.getName());
		productVariant.setOriginatingElement(new GappModelElementWrapper(element));
		return productVariant;
	}
}
