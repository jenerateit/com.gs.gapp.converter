/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.product.UiApplication;
import com.gs.gapp.metamodel.product.UiApplication.LayoutAreaEnum;
import com.gs.gapp.metamodel.ui.UIModule;
import com.gs.gapp.metamodel.ui.container.UIMenuContainer;
import com.gs.gapp.metamodel.ui.databinding.FunctionUsage;

/**
 * @author mmt
 *
 */
public class UiApplicationConverter<S extends Element, T extends UiApplication>
    extends AbstractApplicationConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UiApplicationConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T application) {

		super.onConvert(element, application);

		// --- root container
		OptionValueReference optionValueReference =
				element.getOptionValueReferencesReader().getOptionValueReference(ProductOptionEnum.ROOT.getName());
		if (optionValueReference != null) {
			com.gs.gapp.metamodel.ui.container.UIContainer container =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.ui.container.UIContainer.class,
							                       optionValueReference.getReferencedObject());
			if (container != null) {
				application.setRootContainer(container);
			} else {
				throw new ModelConverterException("not successfully converted a referenced object to a container, result was null", new GappModelElementWrapper(optionValueReference.getReferencedObject()));
			}
		}

		// --- application menus
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.MENUS.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference reference : optionValueReferences) {
				UIMenuContainer menuContainer = this.convertWithOtherConverter(UIMenuContainer.class,
            			                                                         reference.getReferencedObject());
            	if (menuContainer != null) {
            		application.addApplicationMenu(menuContainer);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a menu, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}

		// --- function usage
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.INTERFACES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	FunctionUsage functionUsage = this.convertWithOtherConverter(FunctionUsage.class, reference.getReferencedObject());
            	if (functionUsage != null) {
            		application.addFunctionUsage(functionUsage);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a function usage, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
		
		// --- rendered and customized layout areas
		List<String> renderedLayoutAreas = element.getOptionValueSettingsReader().getEnumeratedOptionValues(ProductOptionEnum.RENDERED_LAYOUT_AREAS.getName());
		for (String renderedLayoutArea : renderedLayoutAreas) {
			LayoutAreaEnum layoutAreaEnum = LayoutAreaEnum.fromString(renderedLayoutArea);
			if (layoutAreaEnum != null) application.addRenderedLayoutArea(layoutAreaEnum);
		}
		List<String> customizedLayoutAreas = element.getOptionValueSettingsReader().getEnumeratedOptionValues(ProductOptionEnum.CUSTOMIZED_LAYOUT_AREAS.getName());
		for (String customizedLayoutArea : customizedLayoutAreas) {
			LayoutAreaEnum layoutAreaEnum = LayoutAreaEnum.fromString(customizedLayoutArea);
			if (layoutAreaEnum != null) application.addCustomizedLayoutArea(layoutAreaEnum);
		}
		
		// --- ui modules being used in the given application
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.UI_MODULES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Set<ModelElementI> convertedModelElements = this.getModelConverter().getConvertedModelElements(reference.getReferencedObject(), UIModule.class);
            	if (convertedModelElements != null) {
            		for (ModelElementI convertedModelElement : convertedModelElements) {
            			if (convertedModelElement instanceof UIModule) {
            				application.addUiModule((UIModule) convertedModelElement);
            			}
            		}
            	}
            }
		}

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return ProductElementEnum.APPLICATION_UI;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T application = (T) new UiApplication(element.getName());
		application.setOriginatingElement(new GappModelElementWrapper(element));
		return application;
	}
}
