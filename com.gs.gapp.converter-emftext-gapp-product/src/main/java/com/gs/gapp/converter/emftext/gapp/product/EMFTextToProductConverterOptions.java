package com.gs.gapp.converter.emftext.gapp.product;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.emftext.gapp.function.EMFTextToFunctionConverterOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;

public class EMFTextToProductConverterOptions extends EMFTextToFunctionConverterOptions {
	
	public static final OptionDefinitionString OPTION_DEF_FILTER_KEEP_CAPABILITY =
			new OptionDefinitionString("filter.keep-capability", "this option nominates the name of the capability that is going to be generated application code for", false, false);


	public EMFTextToProductConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public String getNameOfFilteredCapability() {
		Serializable option = getOptions().get(OPTION_DEF_FILTER_KEEP_CAPABILITY.getKey());
		if (option != null) {
			return option.toString();
		}
		
		return null;
	}
}
