/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.product.ServiceApplication;

/**
 * @author mmt
 *
 */
public class ServiceApplicationConverter<S extends Element, T extends ServiceApplication>
    extends AbstractApplicationConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ServiceApplicationConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T application) {
		super.onConvert(element, application);
		
		// --- services
		EList<OptionValueReference>  optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.SERVICES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	ServiceImplementation serviceImplementation = this.convertWithOtherConverter(ServiceImplementation.class, reference.getReferencedObject());
            	if (serviceImplementation != null) {
            		application.addService(serviceImplementation);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a service implementation, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return ProductElementEnum.APPLICATION_SERVICE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T application = (T) new ServiceApplication(element.getName());
		application.setOriginatingElement(new GappModelElementWrapper(element));
		return application;
	}
}
