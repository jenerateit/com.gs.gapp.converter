/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.product.AbstractApplication;
import com.gs.gapp.metamodel.product.Namespace;



/**
 * @author mmt
 *
 */
public class ProductModuleConverter<S extends Module, T extends com.gs.gapp.metamodel.product.ProductModule>
    extends ModuleConverter<S, T> {

	public ProductModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T productModule) {
		super.onConvert(module, productModule);

		// --- namespace
		Namespace namespace = new Namespace(module.getNamespace().getName());
		productModule.setNamespace(namespace);
		productModule.addElement(namespace);
		
		for (Element moduleElement : module.getElements()) {
			
			// --- applications
			Collection<AbstractApplication> applications = this.convertWithOtherConverters(com.gs.gapp.metamodel.product.AbstractApplication.class, moduleElement);
			if (applications != null) {
				for (AbstractApplication application : applications) {
					namespace.addApplication(application);
					productModule.addElement(application);
				}
			}

			// --- organization
			com.gs.gapp.metamodel.product.Organization organization =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.product.Organization.class, moduleElement);
			if (organization != null) {
				namespace.addElement(organization);
				productModule.addElement(organization);
	            continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}
			
			// --- capability
			com.gs.gapp.metamodel.product.Capability capability =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.product.Capability.class, moduleElement);
			if (capability != null) {
				namespace.addElement(capability);
				productModule.addElement(capability);
	            continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}
			
			// --- product
			com.gs.gapp.metamodel.product.Product product =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.product.Product.class, moduleElement);
			if (product != null) {
				namespace.addElement(product);
				productModule.addElement(product);
	            continue;  // successfully converted, we can continue here, since we do not convert a single element into more than one new element
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.hasElementsOfTypes( new BasicEList<>(ProductElementEnum.getElementTypeNames()) ) == false) {
					// only if the module has at least one product element type, it is recognized as a product module
					result = false;
				}
            }
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.product.ProductModule(module.getName());
		result.setOriginatingElement(new GappModelElementWrapper(module));
		return result;
	}
}
