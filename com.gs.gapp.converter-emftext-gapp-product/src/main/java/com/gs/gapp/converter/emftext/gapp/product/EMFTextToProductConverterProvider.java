/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.emftext.gapp.product;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author hrr
 *
 */
public class EMFTextToProductConverterProvider implements ModelConverterProviderI {

	/**
	 *
	 */
	public EMFTextToProductConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new EMFTextToProductConverter();
	}
}
