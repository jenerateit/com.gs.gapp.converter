/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.product.Feature;

/**
 * @author mmt
 *
 */
public class FeatureConverter<S extends Element, T extends Feature> extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public FeatureConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T feature) {
		super.onConvert(element, feature);
		
		// --- active
		Boolean isActive =
			    element.getOptionValueSettingsReader().getBooleanOptionValue(ProductOptionEnum.ACTIVE.getName());
			if (isActive != null) {
				feature.setActive(isActive);
			}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return ProductElementEnum.FEATURE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")
		T feature = (T) new Feature(element.getName());
		feature.setOriginatingElement(new GappModelElementWrapper(element));
		return feature;
	}
}
