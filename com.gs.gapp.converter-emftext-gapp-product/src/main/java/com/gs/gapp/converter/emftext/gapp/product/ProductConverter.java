/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.product.Feature;
import com.gs.gapp.metamodel.product.Organization;
import com.gs.gapp.metamodel.product.Product;

/**
 * @author mmt
 *
 */
public class ProductConverter<S extends Element, T extends Product> extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ProductConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T product) {
		super.onConvert(element, product);
		
		// --- organization
		if (element.getOptionValueReferencesReader().getOptionValueReference(ProductOptionEnum.ORGANIZATION.getName()) != null) {
			Organization organization = convertWithOtherConverter(Organization.class, element.getOptionValueReferencesReader().getOptionValueReference(ProductOptionEnum.ORGANIZATION.getName()).getReferencedObject());
			product.setOrganization(organization);
		}
		
		// --- features
		EList<OptionValueReference> optionValueReferences = element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.FEATURES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Feature feature = this.convertWithOtherConverter(Feature.class, reference.getReferencedObject());
            	if (feature != null) product.addFeature(feature);
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return ProductElementEnum.PRODUCT;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")
		T product = (T) new Product(element.getName());
		product.setOriginatingElement(new GappModelElementWrapper(element));
		return product;
	}
}
