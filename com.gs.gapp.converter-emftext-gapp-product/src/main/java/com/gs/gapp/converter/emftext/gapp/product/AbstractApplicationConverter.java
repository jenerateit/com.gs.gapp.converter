/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.product.Capability;

/**
 * @author mmt
 *
 */
public abstract class AbstractApplicationConverter<S extends Element, T extends com.gs.gapp.metamodel.product.AbstractApplication>
    extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractApplicationConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T application) {

		super.onConvert(element, application);

		// --- default language
		String defaultLanguage =
		    element.getOptionValueSettingsReader().getTextOptionValue(ProductOptionEnum.DEFAULT_LANGUAGE.getName());
		if (defaultLanguage != null) {
			application.setDefaultLanguage(defaultLanguage);
		}
		
		// --- capabilities
		EList<OptionValueReference>  optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.CAPABILITIES.getName());

		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Capability capability = this.convertWithOtherConverter(Capability.class, reference.getReferencedObject());
            	if (capability != null) {
            		application.addCapability(capability);
            	} else {
            		throw new ModelConverterException("not successfully converted a referenced object to a capability, result was null", new GappModelElementWrapper(reference.getReferencedObject()));
            	}
            }
		}
	}
}
