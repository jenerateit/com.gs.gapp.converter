/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.product;

import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.product.ProductElementEnum;
import com.gs.gapp.dsl.product.ProductOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.BusinessLogic;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.Storage;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.product.Capability;
import com.gs.gapp.metamodel.product.Feature;
import com.gs.gapp.metamodel.product.ServiceApplication;
import com.gs.gapp.metamodel.product.UiApplication;
import com.gs.gapp.metamodel.ui.UIModule;

/**
 * @author mmt
 *
 */
public class CapabilityConverter<S extends Element, T extends Capability> extends EMFTextToBasicModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public CapabilityConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T capability) {
		super.onConvert(element, capability);
		
		// --- modules
		// TODO remove this conversion once we are sure that no model is using this anymore (mmt 28-Nov-2018)
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.MODULES.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Set<ModelElementI> convertedModelElements = this.getModelConverter().getConvertedModelElements(reference.getReferencedObject(), Module.class);
            	if (convertedModelElements != null) {
	            	if (reference.getReferenceEnumerationValue() == null || reference.getReferenceEnumerationValue().getEntry() == null ||
	            			reference.getReferenceEnumerationValue().getEntry().equalsIgnoreCase("INTERFACE")) {
	            		for (ModelElementI convertedModelElement : convertedModelElements) {
	            			if (convertedModelElement instanceof Module) {
	            				capability.addInterfaceModule((Module) convertedModelElement);
	            			}
	            		}
	            	} else if (reference.getReferenceEnumerationValue() != null && reference.getReferenceEnumerationValue().getEntry() != null &&
	            			   reference.getReferenceEnumerationValue().getEntry().equalsIgnoreCase("INTERNAL")) {
	            		for (ModelElementI convertedModelElement : convertedModelElements) {
	            			if (convertedModelElement instanceof Module) {
	            				capability.addInternalModule((Module) convertedModelElement);
	            			}
	            		}
	            	} else {
	            		throw new ModelConverterException("unknown enumeration entry for option 'Modules' found:" + reference.getReferenceEnumerationValue());
	            	}
            	}
            }
		}
		
		// --- features
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.FEATURES.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Feature feature = this.convertWithOtherConverter(Feature.class, reference.getReferencedObject());
            	if (feature == null) throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a 'Feature' object");
            	capability.addFeature(feature);
            }
		}
		
		// --- configuration items
		OptionValueReference optionValueReference = element.getOptionValueReferencesReader().getOptionValueReference(ProductOptionEnum.CONFIGURATION_ITEMS.getName());
		if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
			Enumeration enumeration = this.convertWithOtherConverter(Enumeration.class, optionValueReference.getReferencedObject());
			capability.setConfigurationItems(enumeration);
		}
		
		// --- user interface modules
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.USER_INTERFACES.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	UIModule uiModule = this.convertWithOtherConverter(UIModule.class, reference.getReferencedObject());
            	if (uiModule == null) throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a UIModule object");
            	capability.addUiModule(uiModule);
            }
		}
		
		// --- provided services
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.PROVIDED_SERVICES.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	ServiceImplementation serviceImplementation = this.convertWithOtherConverter(ServiceImplementation.class, reference.getReferencedObject());
            	if (serviceImplementation != null) {
            		capability.addServices(serviceImplementation);
            	} else {
           			throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a service implementation object");
            	}
            }
		}
		
		// --- consumed services (as function modules and as persistence modules)
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.CONSUMED_SERVICES.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	ServiceClient serviceClient = this.convertWithOtherConverter(ServiceClient.class, reference.getReferencedObject());
            	if (serviceClient != null) {
            		capability.addConsumedService(serviceClient);
            	} else {
           			throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a service client object");
            	}
            }
		}
		
		// --- storage (as function modules and as persistence modules)
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.STORAGE.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Storage storage = this.convertWithOtherConverter(Storage.class, reference.getReferencedObject());
            	if (storage != null) {
            		capability.addStorage(storage);
            	} else {
           			throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a Storage object");
            	}
            }
		}
		
		// --- function modules for business logic
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.BUSINESS_LOGIC.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	BusinessLogic businessLogic = this.convertWithOtherConverter(BusinessLogic.class, reference.getReferencedObject());
            	if (businessLogic == null) throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a BusinessLogic object");
            	capability.addBusinessLogic(businessLogic);
            }
		}
		
		// --- service applications
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.SERVICE_APPLICATIONS.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	ServiceApplication serviceApplication = this.convertWithOtherConverter(ServiceApplication.class, reference.getReferencedObject());
            	if (serviceApplication == null) throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a service application");
            	capability.addServiceApplication(serviceApplication);
            }
		}
		
		// --- ui applications
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.UI_APPLICATIONS.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	UiApplication uiApplication = this.convertWithOtherConverter(UiApplication.class, reference.getReferencedObject());
            	if (uiApplication == null) throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a ui application");
            	capability.addUiApplication(uiApplication);
            }
		}
		
		// --- default storage
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.DEFAULT_STORAGE.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	PersistenceModule persistenceModule = this.convertWithOtherConverter(PersistenceModule.class, reference.getReferencedObject());
            	if (persistenceModule != null) {
            		capability.getDefaultStorage().addPersistenceModules(persistenceModule);
            	} else {
            		FunctionModule functionModule = this.convertWithOtherConverter(FunctionModule.class, reference.getReferencedObject());
            		if (functionModule != null) {
            			capability.getDefaultStorage().addFunctionModules(functionModule);
            		}
            	}
            }
		}
		
		// --- extended capabilities
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(ProductOptionEnum.EXTENDS.getName());
		if (optionValueReferences != null) {
            for (OptionValueReference reference : optionValueReferences) {
            	Capability extendedCapability = this.convertWithOtherConverter(Capability.class, reference.getReferencedObject());
            	if (extendedCapability == null) throw new ModelConverterException("not able to convert the model element '" + reference.getReferencedObject() + "' to a 'Capability' object");
            	capability.addExtendedCapability(extendedCapability);
            }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return ProductElementEnum.CAPABILITY;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T capability = (T) new Capability(element.getName());
		capability.setOriginatingElement(new GappModelElementWrapper(element));
		
		// creating default elements, that do not need to have a corresponding element in the model
		capability.setDefaultStorage(new Storage(capability.getName() + "DefaultStorage"));
		capability.setDefaultBusinesslogic(new BusinessLogic(capability.getName() + "DefaultBusinesslogic"));
		capability.addStorage(capability.getDefaultStorage());
		capability.addBusinessLogic(capability.getDefaultBusinesslogic());
		
		return capability;
	}
}
