package com.gs.gapp.converter.vorto.gen.anyfile.converter;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.vorto.codegen.api.GenerationResultZip;
import org.eclipse.vorto.codegen.api.IGenerationResult;
import org.eclipse.vorto.codegen.api.IVortoCodeGenerator;
import org.eclipse.vorto.codegen.api.InvocationContext;
import org.eclipse.vorto.codegen.api.SingleGenerationResult;
//import org.eclipse.vorto.codegen.api.mapping.InvocationContext;
import org.eclipse.vorto.core.api.model.informationmodel.InformationModel;
import org.eclipse.vorto.core.api.model.mapping.MappingModel;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;
import org.osgi.service.log.LogService;

import com.gs.gapp.metamodel.anyfile.AnyFile;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

public class InformationModelToGenerationResultConverter<S extends InformationModel, T extends GenerationResultModelElement>
		extends AbstractModelElementConverter<S, T> {

	private static LogService logger;
	static {
		BundleContext context = FrameworkUtil.getBundle(InformationModelToGenerationResultConverter.class).getBundleContext();
		ServiceReference<LogService> ref = context.getServiceReference(LogService.class);
		logger = context.getService(ref);
	}
	
	public InformationModelToGenerationResultConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	@Override
	protected void onConvert(S informationModel, T generationResultModelElement) {

		IGenerationResult generationResult = generationResultModelElement.getGenerationResult();
		if (generationResult instanceof GenerationResultZip) {
			GenerationResultZip generationResultZip = (GenerationResultZip) generationResult;
			byte[] data = generationResultZip.getContent();
			try (ZipInputStream zipInputStream = new ZipInputStream(new ByteArrayInputStream(data))) {

				ZipEntry zipEntry = null;
				while ((zipEntry = zipInputStream.getNextEntry()) != null) {
					/*
					 * override the BufferedInputStream#close() method to NOT
					 * close the ZipInputStream since multiple entries will be
					 * read if available. Resource#load(InputStream) close the
					 * stream after end of file
					 */
					final InputStream inputNotClose = new BufferedInputStream(zipInputStream) {

						/**
						 * do nothing!
						 * 
						 * @see java.io.BufferedInputStream#close()
						 */
						@Override
						public void close() throws IOException {
						}
					};

					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int read;
					while ((read = inputNotClose.read(buffer, 0, buffer.length)) > 0) {
						byteArrayOutputStream.write(buffer, 0, read);
					}
					byteArrayOutputStream.flush();

					byte[] content = byteArrayOutputStream.toByteArray();
					byteArrayOutputStream.close();
					
					addAnyFile(zipEntry.getName(), content);
				}
			} catch (Exception e) {
				addError("could not read generated vorto: " + e.getMessage());
				throw new RuntimeException(e);
			}
		} else if (generationResult instanceof SingleGenerationResult) {
			SingleGenerationResult singleGenerationResult = (SingleGenerationResult) generationResult;
			
			addAnyFile(singleGenerationResult.getFileName(), singleGenerationResult.getContent());
		}
		else {
			throw new IllegalArgumentException("unknown generation result");
		}

	}
	
	private void addAnyFile(String fileName, byte[] content) {
		/* remove the root directory */
		ModelConverterOptions options = getModelConverter().getOptions();
		String removeRootDir = "vorto.generator.removerootdir";
		if (!options.containsKey(removeRootDir) || !options.get(removeRootDir).equals("false")) {
			fileName = fileName.replaceFirst("^(/*)[^/]+\\/", "");
		}
		
		File file = new File(fileName);
		String path = file.getParentFile() == null ? "" : file.getParentFile().getPath();
		AnyFile anyFile = new AnyFile(file.getName(), path, null);
		anyFile.setContent(content);
		addModelElement(anyFile);
	}

	@Override
	protected T onCreateModelElement(S informationModel, ModelElementI previouslyResultingElement) {
		BundleContext bundleContext = FrameworkUtil.getBundle(getClass()).getBundleContext();
		String bundleId = (String) getModelConverter().getOptions().get("vorto.generator.bundle");
		String clazzName = (String) getModelConverter().getOptions().get("vorto.generator.class");
		IVortoCodeGenerator generator = null;
		for (Bundle bundle : bundleContext.getBundles()) {
			if (bundle.getSymbolicName().equals(bundleId)) {
				try {
					logger.log(LogService.LOG_INFO, "loading '" + clazzName + "' from bundle '" + bundleId + "'");
					Class<?> clazz = bundle.loadClass(clazzName);
					generator = (IVortoCodeGenerator) clazz.newInstance();
					break;
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
					addError("vorto generator class could not be created: " + e.getMessage());
					throw new RuntimeException(e);
				}
			}
		}

		List<MappingModel> mappingModels = new ArrayList<>();
		
		// TODO simple replaced using old InvocationContext class by new InvocationContext class and set 2. param to null - is this enough??? (mmt 08-Aug-2016)
		// TODO another change in Vorto API has happened, now we need a third parameter (mmt 29-Aug-2016)
		InvocationContext invocationContext = new InvocationContext(mappingModels, null, new HashMap<String, String>());  

		// mappingContext.addMappingModel(new
		// InformationMappingModel(informationModel));

		IGenerationResult generationResult = null;
		try {/*
                IVortoCodeGenProgressMonitor monitor = new IVortoCodeGenProgressMonitor() {
				
				@Override
				public void monitorWarning(String arg0) {
					logger.log(LogService.LOG_WARNING, "vorto code gen: " + arg0);
				}
				
				@Override
				public void monitorInfo(String arg0) {
					logger.log(LogService.LOG_INFO, "vorto code gen: " + arg0);
					
				}
			};*/
			generationResult = generator.generate(informationModel, invocationContext/*, monitor*/);
		} catch (Exception ex) {
			throw new ModelConverterException("exception occurred when running the vorto code generator: " + ex.getMessage(), ex);
		}
		
		@SuppressWarnings("unchecked")
		T result = (T) new GenerationResultModelElement(informationModel.getName());
		result.setGenerationResult(generationResult);

		return result;
	}
}
