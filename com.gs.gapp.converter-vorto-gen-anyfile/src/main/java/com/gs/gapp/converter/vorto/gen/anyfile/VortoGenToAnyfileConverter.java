package com.gs.gapp.converter.vorto.gen.anyfile;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.converter.vorto.gen.anyfile.converter.InformationModelToGenerationResultConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

public class VortoGenToAnyfileConverter extends AbstractAnalyticsConverter {

	public VortoGenToAnyfileConverter() {
		super(new ModelElementCache());
	}
	
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new InformationModelToGenerationResultConverter<>(this));
		
		return result;
	}


	@Override
	protected Set<Object> clientConvert(Collection<?> rawElements, ModelConverterOptions options)
			throws ModelConverterException {

		return super.clientConvert(rawElements, options);
	}
}

