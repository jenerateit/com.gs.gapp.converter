package com.gs.gapp.converter.vorto.gen.anyfile.converter;

import org.eclipse.vorto.codegen.api.IGenerationResult;

import com.gs.gapp.metamodel.basic.ModelElement;

public class GenerationResultModelElement extends ModelElement {

	private static final long serialVersionUID = -79854464168707507L;

	private IGenerationResult result;

	public GenerationResultModelElement(String name) {
		super(name);
	}
	
	public void setGenerationResult(IGenerationResult result) {
		this.result = result;
	}
	
	public IGenerationResult getGenerationResult() {
		return result;
	}
	
}
