package com.gs.gapp.converter.vorto.gen.anyfile;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

public class VortoGenToAnyfileConverterProvider implements ModelConverterProviderI {

	public VortoGenToAnyfileConverterProvider() {
		super();
	}
	
	
	@Override
	public ModelConverterI getModelConverter() {
		return new VortoGenToAnyfileConverter();
	}

}
