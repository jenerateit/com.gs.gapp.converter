/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionModuleTypeEnum;
import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Namespace;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public class FunctionModuleConverter<S extends Module, T extends com.gs.gapp.metamodel.function.FunctionModule>
    extends ModuleConverter<S, T> {

	public FunctionModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.CREATE_AND_CONVERT_IN_ONE_GO);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T functionModule) {
		super.onConvert(module, functionModule);

		// --- namespace
		Namespace namespace = new Namespace(module.getNamespace().getName());
		functionModule.setNamespace(namespace);


		// --- module elements
		for (Element moduleElement : module.getElements()) {

			@SuppressWarnings("unused")
			com.gs.gapp.metamodel.function.Function function =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.function.Function.class,
						                       moduleElement, functionModule);
			
			com.gs.gapp.metamodel.function.ServiceInterface serviceInterface =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.function.ServiceInterface.class,
							                       moduleElement);
			if (serviceInterface != null) {
				functionModule.addElement(serviceInterface);
			}
			
			com.gs.gapp.metamodel.function.ServiceClient serviceClient =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.function.ServiceClient.class,
							                       moduleElement);
			if (serviceClient != null) {
				functionModule.addElement(serviceClient);
			}
			
			com.gs.gapp.metamodel.function.ServiceImplementation serviceImplementation =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.function.ServiceImplementation.class,
							                       moduleElement);
			if (serviceImplementation != null) {
				functionModule.addElement(serviceImplementation);
			}
		}
		
		// --- linked persistence modules
		EList<OptionValueReference> optionValueReferences =
				module.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.MODULE_DATA.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				ModelElement referencedModelElement = optionValueReference.getReferencedObject();
				PersistenceModule persistenceModule = this.convertWithOtherConverter(PersistenceModule.class, referencedModelElement);
				if (persistenceModule != null) {
					functionModule.addPersistenceModule(persistenceModule);
				}
			}
		}
		
		// --- consumed entities
		EList<OptionValueReference> optionValueConsumedEntitiesReferences =
				module.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.CONSUMED_ENTITIES.getName());
		
		if (optionValueConsumedEntitiesReferences != null) {
			for (OptionValueReference optionValueReference : optionValueConsumedEntitiesReferences) {
				ModelElement referencedEntityElement = optionValueReference.getReferencedObject();
				Entity entity = this.convertWithOtherConverter(Entity.class, referencedEntityElement);
				if (entity != null) {
					functionModule.addConsumedEntity(entity);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.getModuleTypes().size() == 0 || module.getModuleTypesReader().containsModuleType(FunctionModuleTypeEnum.FUNCTION.getName())) {
					if (module.hasElementsOfTypes( new BasicEList<>(FunctionElementEnum.getElementTypeNames()) ) == false) {
						// only if the module has at least one function element type, it is recognized as a function module
						generalResponsibility = false;
					}
				} else {
					generalResponsibility = false;
				}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.function.FunctionModule(module.getName());
		result.setOriginatingElement(new GappModelElementWrapper(module));
		
		return result;
	}

}
