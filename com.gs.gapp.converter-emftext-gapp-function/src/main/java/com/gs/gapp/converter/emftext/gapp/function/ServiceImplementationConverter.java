/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceImplementation.ServicePattern;
import com.gs.gapp.metamodel.function.ServiceInterface;

/**
 * @author mmt
 *
 */
public class ServiceImplementationConverter<S extends Element, T extends ServiceImplementation>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public ServiceImplementationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T serviceImplementation) {
		super.onConvert(element, serviceImplementation);

		// --- now handle used service clients
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.USED_SERVICE_CLIENTS.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				ServiceClient serviceClient = convertWithOtherConverter(ServiceClient.class, optionValueReference.getReferencedObject());
				if (serviceClient != null) serviceImplementation.addUsedServiceClients(serviceClient);
			}
		}
		
		// --- now handle used service interfaces
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.USED_SERVICE_INTERFACES.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				ServiceInterface serviceInterface = convertWithOtherConverter(ServiceInterface.class, optionValueReference.getReferencedObject());
				if (serviceInterface != null) serviceImplementation.addConsumedServiceInterface(serviceInterface);
			}
		}
		
		// --- service pattern
		String servicePatternString = element.getOptionValueSettingsReader().getEnumeratedOptionValue(FunctionOptionEnum.SERVICE_PATTERN.getName());
		if (servicePatternString != null) {
			ServicePattern servicePattern = ServicePattern.valueOf(servicePatternString.toUpperCase());
			if (servicePattern == null) {
				throw new ModelConverterException("no suitable enumeration entry found in ServicePattern enumeration, for string '" + servicePatternString + "'");
			}
			serviceImplementation.setServicePattern(servicePattern);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return FunctionElementEnum.IMPLEMENTATION;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		
		// --- get the service interface reference
		OptionValueReference optionValueReference =
				element.getOptionValueReferencesReader().getOptionValueReference(FunctionOptionEnum.INTERFACE_IMPLEMENTATION.getName());
				
		if (optionValueReference != null) {
			ServiceInterface serviceInterface = convertWithOtherConverter(ServiceInterface.class, optionValueReference.getReferencedObject());
			if (serviceInterface == null) {
                throw new ModelConverterException("found a referenced service interface (" + optionValueReference.getReferencedObject() + "), but could not convert it to a ServiceInterface object");
			} else {
				@SuppressWarnings("unchecked")
				T result = (T) new ServiceImplementation(element.getName(), serviceInterface);
				result.setOriginatingElement(new GappModelElementWrapper(element));
				return result;
			}
		} else {
			throw new ModelConverterException("no modeled reference to a service interface found");
		}
	}
}
