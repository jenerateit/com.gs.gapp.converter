/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public class FunctionInParameterConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.function.FunctionParameter>
    extends FunctionParameterConverter<S, T> {

	public FunctionInParameterConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T functionParameter) {
		super.onConvert(elementMember, functionParameter);

		functionParameter.setParameterType(ParameterType.IN);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
			if (originalModelElement instanceof ElementMember) {
				ElementMember elementMember = (ElementMember) originalModelElement;
				if (!elementMember.isTypeof(FunctionMemberEnum.IN.getName())) {
					result = false;
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.function.FunctionParameter(elementMember.getName());
		return result;
	}

	@Override
	public IMetatype getMetatype() {
		return FunctionMemberEnum.IN;
	}
}
