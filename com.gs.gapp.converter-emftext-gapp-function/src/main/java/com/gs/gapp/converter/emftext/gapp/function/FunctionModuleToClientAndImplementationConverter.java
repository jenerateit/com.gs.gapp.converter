/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;

/**
 * This converter automatically creates service interface, service client and service implementation
 * for the given function module in order to simplify the modeling and at the same time have
 * a universal way of further converting service models.
 * 
 * @author mmt
 *
 */
public class FunctionModuleToClientAndImplementationConverter<S extends FunctionModule, T extends ServiceClient>
    extends AbstractM2MModelElementConverter<S, T> {

	public FunctionModuleToClientAndImplementationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T serviceClient) {
		super.onConvert(element, serviceClient);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S functionModule, ModelElementI previouslyResultingElement) {
		
		ServiceInterface serviceInterface = new ServiceInterface(functionModule.getName());
		functionModule.addElement(serviceInterface);
		serviceInterface.addFunctionModules(functionModule);
		ServiceImplementation serviceImplementation = new ServiceImplementation(serviceInterface.getName() + "Impl", serviceInterface);
		serviceImplementation.setGenerated(false);  // we will never generate a server implementation for an automatically created service set (mmt 23-May-2018)
		serviceInterface.addServiceImplementations(serviceImplementation);
		
		@SuppressWarnings("unchecked")
		T result = (T) new ServiceClient(serviceInterface.getName() + "Client", serviceInterface);
		
		result.addConsumedServiceImplementations(serviceImplementation);
		
		// --- add created elements to module and the module's namespace in order to have clean model element relationships
		functionModule.addElement(serviceInterface);
		functionModule.addElement(serviceImplementation);
		functionModule.addElement(result);
		functionModule.getNamespace().addElement(serviceInterface);
		functionModule.getNamespace().addElement(serviceImplementation);
		functionModule.getNamespace().addElement(result);
		
		return result;
	}
}
