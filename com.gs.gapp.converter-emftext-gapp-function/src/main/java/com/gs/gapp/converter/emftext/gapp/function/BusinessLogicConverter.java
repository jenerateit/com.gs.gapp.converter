/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.BusinessLogic;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public class BusinessLogicConverter<S extends Element, T extends BusinessLogic>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public BusinessLogicConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T businessLogic) {
		super.onConvert(element, businessLogic);

		// --- now handle persistence modules
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.PERSISTENCE_MODULES_FOR_BUSINESSLOGIC.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				PersistenceModule persistenceModule = convertWithOtherConverter(PersistenceModule.class, optionValueReference.getReferencedObject());
				if (persistenceModule != null) businessLogic.addPersistenceModules(persistenceModule);
			}
		}
		
		// --- now handle function modules
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.FUNCTION_MODULES_FOR_BUSINESSLOGIC.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, optionValueReference.getReferencedObject());
				if (functionModule != null) businessLogic.addFunctionModules(functionModule);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return FunctionElementEnum.BUSINESSLOGIC;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new BusinessLogic(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
