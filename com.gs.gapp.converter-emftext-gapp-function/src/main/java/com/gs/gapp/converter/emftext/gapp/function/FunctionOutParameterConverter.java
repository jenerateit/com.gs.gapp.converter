/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public class FunctionOutParameterConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.function.FunctionParameter>
    extends FunctionParameterConverter<S, T> {

	public FunctionOutParameterConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T functionParameter) {

		super.onConvert(elementMember, functionParameter);

		functionParameter.setParameterType(ParameterType.OUT);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
			if (originalModelElement instanceof ElementMember) {
				ElementMember elementMember = (ElementMember) originalModelElement;
				if (!elementMember.isTypeof(FunctionMemberEnum.OUT.getName())) {
					result = false;
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.function.FunctionParameter(elementMember.getName());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}
	
	@Override
	public IMetatype getMetatype() {
		return FunctionMemberEnum.OUT;
	}
}
