/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public abstract class FunctionParameterConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.function.FunctionParameter>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public FunctionParameterConverter(AbstractConverter modelConverter,
			ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T functionParameter) {

		super.onConvert(elementMember, functionParameter);

		// --- type conversion
		Type type = null;

		if (elementMember.getMemberType() != null) {
			type = this.convertWithOtherConverter(Type.class, elementMember.getMemberType());
			if (type != null) {
				functionParameter.setType(type);
			} else {
				throw new ModelConverterException("found a modeled type " +
				        elementMember.getMemberType() + " of parameter '" +
				    	elementMember + "' of function " + elementMember.eContainer().eContainer().toString() +
				    	" that could not be converted to another type");
			}
		} else {
			OptionValueReference optionValueReference =  
				    elementMember.getOptionValueReferencesReader().getOptionValueReference(FunctionOptionEnum.IN_PARAM_DATA.getName());
			if (optionValueReference != null) {
				com.gs.gapp.language.gapp.ModelElement referencedElement =
						optionValueReference.getReferencedObject();
				
				if (referencedElement instanceof ElementMember) {
					ElementMember potentialField = (ElementMember) referencedElement;
					Element element = (Element) potentialField.eContainer().eContainer();
					Type owner = this.convertWithOtherConverter(Type.class, element);
					if (owner != null) {
						Field field = this.convertWithOtherConverter(Field.class, potentialField, owner);
						functionParameter.setType(field.getType());
					}
				}
			} else {
				optionValueReference =  
					    elementMember.getOptionValueReferencesReader().getOptionValueReference(FunctionOptionEnum.OUT_PARAM_DATA.getName());
				if (optionValueReference != null) {
					com.gs.gapp.language.gapp.ModelElement referencedElement =
							optionValueReference.getReferencedObject();
					
					if (referencedElement instanceof ElementMember) {
						ElementMember potentialField = (ElementMember) referencedElement;
						Element element = (Element) potentialField.eContainer().eContainer();
						Type owner = this.convertWithOtherConverter(Type.class, element);
						if (owner != null) {
							Field field = this.convertWithOtherConverter(Field.class, potentialField, owner);
							functionParameter.setType(field.getType());
						}
					}
				}
			}
			
			if (functionParameter.getType() == null) {
				throw new ModelConverterException("found a model element that has no type modeled and where the 'Data' option did not result in a type");
			}
		}
		
		// --- collection type conversion
		String collectionType =
		    elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(FunctionOptionEnum.COLLECTION_TYPE.getName());

		if (collectionType == null || collectionType.length() == 0) {
			// no collection type set
		} else {
			functionParameter.setCollectionType(CollectionType.fromString(collectionType));
		}
		
		// --- Mandatory Parameter
        Boolean mandatoryOption = elementMember.getOptionValueSettingsReader().getBooleanOptionValue(FunctionOptionEnum.PARAM_MANDATORY.getName());
        if (mandatoryOption != null) {
        	functionParameter.setMandatory(mandatoryOption.booleanValue());
        }
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.function.FunctionParameter(elementMember.getName());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}
}
