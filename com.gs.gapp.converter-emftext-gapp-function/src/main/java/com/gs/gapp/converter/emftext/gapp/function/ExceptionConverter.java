/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;

/**
 * @author mmt
 *
 */
public class ExceptionConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.function.BusinessException>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public ExceptionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T businessException) {
		super.onConvert(elementMember, businessException);

		// Nothing to be done here so far
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
			if (originalModelElement instanceof ElementMember) {
				ElementMember elementMember = (ElementMember) originalModelElement;
				if (!elementMember.isTypeof(FunctionMemberEnum.EXCEPTION.getName())) {
					result = false;
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new com.gs.gapp.metamodel.function.BusinessException(elementMember.getName());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		
		if (previouslyResultingElement instanceof Function) {
			result.setModule(((Function)previouslyResultingElement).getModule());
		} else if (previouslyResultingElement instanceof Module) {
			result.setModule((Module) previouslyResultingElement);
		}
		
		return result;
	}

	@Override
	public IMetatype getMetatype() {
		return FunctionMemberEnum.EXCEPTION;
	}
}
