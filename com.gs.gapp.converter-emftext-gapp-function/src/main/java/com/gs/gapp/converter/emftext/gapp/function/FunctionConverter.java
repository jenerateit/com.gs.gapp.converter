/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.DataCollectionTypeEnum;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition.OptionValue;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

/**
 * @author mmt
 *
 */
public class FunctionConverter<S extends Element, T extends com.gs.gapp.metamodel.function.Function>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public FunctionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	/**
	 * @param dataFieldReferences
	 * @return
	 */
	private Set<com.gs.gapp.metamodel.function.FunctionParameter> createSyntheticFunctionParameters(EList<OptionValueReference> dataFieldReferences) {
		Set<com.gs.gapp.metamodel.function.FunctionParameter> result =
		    new LinkedHashSet<>();
		
		if (dataFieldReferences != null) {
			for (OptionValueReference dataFieldReference : dataFieldReferences) {
				ModelElement referencedObject = dataFieldReference.getReferencedObject();
				if (referencedObject instanceof ElementMember) {
					ElementMember fieldMember = (ElementMember) referencedObject;
					Element fieldMemberOwner = (Element) fieldMember.eContainer().eContainer();
					// here we first need to convert the entity in order to be able to pass it on as previouslyCreatedElement
					Entity entity = this.convertWithOtherConverter(Entity.class, fieldMemberOwner);
					if (entity != null) {
						Field field = this.convertWithOtherConverter(Field.class, fieldMember, entity);
						
						// -- now create the parameter
						com.gs.gapp.metamodel.function.FunctionParameter functionParameter =
								new com.gs.gapp.metamodel.function.FunctionParameter(field.getName());
						functionParameter.setType(field.getType());
		
						result.add(functionParameter);
						OptionEnumerationEntry collectionType = dataFieldReference.getReferenceEnumerationValue();
						if (collectionType != null) {
							DataCollectionTypeEnum collectionTypeEnumEntry = DataCollectionTypeEnum.getFromName(collectionType.getEntry());
							if (collectionTypeEnumEntry != null) {
								switch (collectionTypeEnumEntry) {
								case ARRAY:
									functionParameter.setCollectionType(CollectionType.ARRAY);
									break;
								case LIST:
									functionParameter.setCollectionType(CollectionType.LIST);
									break;
								case SET:
									functionParameter.setCollectionType(CollectionType.SET);
									break;
								default:
									throw new ModelConverterException("unhandled data collection type enum entry '" + collectionTypeEnumEntry + "' found", fieldMember);
								}
							}
						}
					}
				}
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T function) {
		super.onConvert(element, function);

		// --- first handle directly modeled references to (entity) fields
		if (getModelConverter().getConverterOptions().isAgnosticEnhancement()) {
			Set<FunctionParameter> syntheticFunctionParameters =
					createSyntheticFunctionParameters(element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.DATA_IN.getName()));
			for (FunctionParameter syntheticFunctionParameter : syntheticFunctionParameters) {
				function.addFunctionInParameter(syntheticFunctionParameter);
			}
			
			syntheticFunctionParameters =
					createSyntheticFunctionParameters(element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.DATA_OUT.getName()));
			for (FunctionParameter syntheticFunctionParameter : syntheticFunctionParameters) {
				function.addFunctionOutParameter(syntheticFunctionParameter);
			}
		}
			
		// --- now handle regular element members
		for (ElementMember member : element.getElementMembers()) {

			com.gs.gapp.metamodel.function.FunctionParameter functionParameter =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.function.FunctionParameter.class, member, function);

			if (functionParameter != null) {
				// --- function parameters
				if (functionParameter.getParameterType() == ParameterType.IN) {
				    function.addFunctionInParameter(functionParameter);
				} else {
					function.addFunctionOutParameter(functionParameter);
				}
			} else {
				// --- exceptions
				com.gs.gapp.metamodel.function.BusinessException businessException =
						this.convertWithOtherConverter(com.gs.gapp.metamodel.function.BusinessException.class, member, function);
				if (businessException != null) {
					function.addBusinessException(businessException);
				} else {
				    throw new ModelConverterException("was not able to convert a member to a function parameter or a business exception, resulting element is null", new GappModelElementWrapper(member));
				}
			}
		}
		
		// --- handle the function's function, in case that's modeled
		String functionOption = element.getOptionValueSettingsReader().getEnumeratedOptionValue(FunctionOptionEnum.FUNCTION.getName());
		if (functionOption != null && functionOption.length() > 0) {
			function.setStorageFunction(StorageFunction.fromString(functionOption));
		}
		
		// --- handle referenced exception types and create business exceptions for them
		// --- Please note that when the model comes from the web modeler, such a conversion logic is NOT needed
		// --- since the DSLs therein allow for an explicit modeling of BusinessExceptions. (mmt 12-Aug-2022)
		EList<OptionValueReference> optionValueReferences = element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.EXCEPTIONS.getName());
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				ModelElement referencedObject = optionValueReference.getReferencedObject();
				ExceptionType exceptionType = this.convertWithOtherConverter(ExceptionType.class, referencedObject);
				if (exceptionType != null) {
					final BusinessException businessException;
					if (exceptionType.getSingleExtensionElement(BusinessException.class) != null) {
						businessException = exceptionType.getSingleExtensionElement(BusinessException.class);
					} else {
						businessException = new BusinessException(exceptionType.getName());
						businessException.setOriginatingElement(exceptionType);
						for (@SuppressWarnings("rawtypes") OptionValue optionValue : exceptionType.getOptions()) {
							businessException.addOptions(optionValue);  // with this we later know the HTTP status code to be used in a writer
						}
						businessException.setBody(exceptionType.getBody());
						businessException.setModule(function.getModule());
						addModelElement(businessException);
						exceptionType.getFields().stream()
							.forEach(field -> {
								Field newField = new Field(field.getName(), businessException);
								newField.setOriginatingElement(field);
								newField.setType(field.getType());
								for (@SuppressWarnings("rawtypes") OptionValue optionValue : field.getOptions()) {
									newField.addOptions(optionValue);
								}
							});
					}
					
					function.addBusinessException(businessException);
					// Since there now is a business exception, we have to avoid duplicate exception types. (mmt 22-May-2020)
					exceptionType.setGenerated(false); 
				}
			}
		}
		
		// --- related entity
		OptionValueReference valueReference = element.getOptionValueReferencesReader().getOptionValueReference(FunctionOptionEnum.ENTITY.getName());
		if (valueReference != null && valueReference.getReferencedObject() != null) {
			Entity relatedEntity = convertWithOtherConverter(Entity.class, valueReference.getReferencedObject());
			if (relatedEntity != null) {
				function.setEntity(relatedEntity);
			}
		}
		
		Long statusCode = element.getOptionValueSettingsReader().getNumericOptionValue(RestOptionEnum.STATUS_CODE.getName());
		if (statusCode != null) {
			// Nothing has to be done here, since there is no specific property in the Function class that could be set for this option.
			// Subsequent model converters and generation groups have to read the option value from the function themselves to be able to make use of it.
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return FunctionElementEnum.FUNCTION;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		
		FunctionModule owner = null;
		if (previouslyResultingElement instanceof FunctionModule) {
			owner = (FunctionModule) previouslyResultingElement;
		} else {
			throw new ModelConverterException("cannot create new Function instance since the previously resulting element is not of type 'FunctionModule'", previouslyResultingElement);
		}
		
		com.gs.gapp.metamodel.function.Function result =
				new com.gs.gapp.metamodel.function.Function(element.getName(), owner);
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return (T) result;
	}
}
