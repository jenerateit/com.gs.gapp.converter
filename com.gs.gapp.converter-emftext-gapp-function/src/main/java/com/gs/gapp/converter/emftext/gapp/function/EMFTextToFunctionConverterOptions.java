package com.gs.gapp.converter.emftext.gapp.function;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;

public class EMFTextToFunctionConverterOptions extends AbstractAnalyticsConverterOptions {
	
	public static final String PARAMETER_TYPIFY_IN_AND_OUT_PARAMETERS = "typify-in-and-out-parameters";

	public EMFTextToFunctionConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public boolean areInAndOutParametersTypified() {
		String typify = (String) getOptions().get(PARAMETER_TYPIFY_IN_AND_OUT_PARAMETERS);
		validateBooleanOption(typify, PARAMETER_TYPIFY_IN_AND_OUT_PARAMETERS);
		return Boolean.parseBoolean(typify);
	}
}
