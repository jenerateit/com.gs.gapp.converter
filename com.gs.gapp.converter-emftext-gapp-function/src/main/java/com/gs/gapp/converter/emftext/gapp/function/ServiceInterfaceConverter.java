/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public class ServiceInterfaceConverter<S extends Element, T extends ServiceInterface>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public ServiceInterfaceConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T serviceInterface) {
		super.onConvert(element, serviceInterface);

		// --- now handle function module references
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.FUNCTION_MODULES.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, optionValueReference.getReferencedObject());
				if (functionModule != null) serviceInterface.addFunctionModules(functionModule);
			}
		}
		
		// --- now handle persistence modules
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.PERSISTENCE_MODULES.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				PersistenceModule persistenceModule = convertWithOtherConverter(PersistenceModule.class, optionValueReference.getReferencedObject());
				if (persistenceModule != null) serviceInterface.addPersistenceModules(persistenceModule);
			}
		}
		
		// --- now handle used entities (and what's getting generated for them)
		optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.ENTITIES.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				Entity entity = convertWithOtherConverter(Entity.class, optionValueReference.getReferencedObject());
				if (entity != null) serviceInterface.addEntities(entity);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return FunctionElementEnum.INTERFACE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")
		T result = (T) new ServiceInterface(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}

}
