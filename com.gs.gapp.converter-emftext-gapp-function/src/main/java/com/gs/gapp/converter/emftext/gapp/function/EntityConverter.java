/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.dsl.function.FunctionOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.function.Storage;
import com.gs.gapp.metamodel.persistence.Entity;

/**
 * @author mmt
 *
 */
public class EntityConverter<S extends Element, T extends Entity>
    extends com.gs.gapp.converter.emftext.gapp.persistence.EntityConverter<S, T> {

	public EntityConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T entity) {
		super.onConvert(element, entity);

		// --- now handle consumed model elements
		EList<OptionValueReference> optionValueReferences =
				element.getOptionValueReferencesReader().getOptionValueReferences(FunctionOptionEnum.CONSUMED_SERVICES.getName());
		
		if (optionValueReferences != null) {
			for (OptionValueReference optionValueReference : optionValueReferences) {
				FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, optionValueReference.getReferencedObject());
				if (functionModule != null) {
					entity.addConsumedModelElement(functionModule);
				} else {
					ServiceInterface serviceInterface = convertWithOtherConverter(ServiceInterface.class, optionValueReference.getReferencedObject());
					if (serviceInterface != null) {
						entity.addConsumedModelElement(serviceInterface);
					} else {
						Storage storage = convertWithOtherConverter(Storage.class, optionValueReference.getReferencedObject());
						if (storage != null) {
							entity.addConsumedModelElement(storage);
						}
					}
				}
			}
		}
	}
}
