/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.function;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.emftext.gapp.persistence.EMFTextToPersistenceConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.function.FunctionMetamodel;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceModelProcessing;

/**
 * @author mmt
 *
 */
public class EMFTextToFunctionConverter extends EMFTextToPersistenceConverter {

	private EMFTextToFunctionConverterOptions converterOptions;
	
	/**
	 * 
	 */
	public EMFTextToFunctionConverter() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.basic.java.BasicToJavaConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new EMFTextToFunctionConverterOptions(getOptions());
	}
	
	@Override
	public EMFTextToFunctionConverterOptions getConverterOptions() {
		return converterOptions;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		result.add( new FunctionModuleConverter<>(this) );
		result.add( new ServiceInterfaceConverter<>(this) );
		result.add( new ServiceImplementationConverter<>(this) );
		result.add( new ServiceClientConverter<>(this) );
		result.add( new StorageConverter<>(this) );
		result.add( new BusinessLogicConverter<>(this) );
		result.add( new EntityConverter<>(this) );  // this is a special entity converter that extends the "normal" entity converter in order to be able to handle function metatypes (mmt 13-Sep-2018)
		
		// --- slave element converters
		result.add( new FunctionConverter<>(this) );
		result.add( new FunctionInParameterConverter<>(this) );
		result.add( new FunctionOutParameterConverter<>(this) );
		result.add( new ExceptionConverter<>(this) );
		result.add( new FunctionModuleToClientAndImplementationConverter<>(this) );
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		result.addAll(super.onPerformModelConsolidation(normalizedElements));
		
		if (getConverterOptions().areInAndOutParametersTypified()) {
			FunctionMetamodel.INSTANCE.createTypesForInAndOutParameters(result);
		}
		
		// --- Here we complete potentially missing modeling options to get a meaningful output of the code generation.
		// --- Note that this procedure doesn't validate the input model. So, as a result, we at most get INFO or WRNING messages.
		ServiceModelProcessing.completeServiceOptions(result)
			.stream()
			.forEach(message -> addMessage(message));
		
		// --- validate the service clients to find out whether its API matches the service implementations that are related to the client
		result.stream()
		    .filter(ServiceClient.class::isInstance)
		    .map(ServiceClient.class::cast)
		    .forEach(serviceClient -> serviceClient.validateServiceRelations());
		
		return FunctionMetamodel.INSTANCE.createExceptionParent(result);
	}
}
