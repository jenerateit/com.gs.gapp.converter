package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.openapi.HttpStatusCode;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.kaizen.oasparser.model3.Response;

public class ResponseToExceptionType<S extends Response, T extends ExceptionType> extends AbstractOpenApi3ToFunction<S, T> {

	public ResponseToExceptionType(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S response, T exceptionType) {
		String httpCode = Overlay.of(response).getPathInParent();
		HttpStatusCode httpStatusCode = HttpStatusCode.get(httpCode);
		StringBuilder exceptionDoc = new StringBuilder();
		if (response.getDescription() != null) {
			exceptionDoc.append(response.getDescription()).append(System.lineSeparator());
			exceptionType.setDefaultExceptionMessage(response.getDescription());  // CHQDG-161 (mmt 23-Feb-2021)
		}
		if (httpStatusCode != null) {
			exceptionDoc.append("exception type to be used for the HTTP stauts code ")
					.append(httpStatusCode.getCode()).append("(")
					.append(httpStatusCode.getDescription()).append(")");
		} else {
			exceptionDoc.append("exception type to be used as default, for cases where there is not an explicit exception type available");
		}
		exceptionType.setBody(exceptionDoc.toString());
		
		
		MediaTypes mediaTypes = getMediaTypes(response.getContentMediaTypes());
		if (mediaTypes != null) {
			// -- we can not do anything with the media type enums
			ConvertedSchema convertedSchema = convertSchema(mediaTypes.getMediaType().getSchema(), exceptionType);
			Field field = new Field("responseCode" + StringTools.firstUpperCase(httpCode), exceptionType);
			field.setType(convertedSchema.getType());
			field.setKeyType(convertedSchema.getKeyType());
			field.setCollectionType(convertedSchema.getCollectionType());
			field.setDimension(convertedSchema.getDimension());
			if (convertedSchema.getAdditionalModelInfo().length() > 0) {
				field.addToBody(convertedSchema.getAdditionalModelInfo().toString());
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S response, ModelElementI previousResultingModelElement) {
		if (!Function.class.isInstance(previousResultingModelElement)) {
			Message errorMessage = OpenApi3Messages.ERROR_UNEXPECTED_PREVIOUS_MODEL_ELEMENT.getMessageBuilder()
				.parameters(previousResultingModelElement.getClass().getName(),
						"response: " + Overlay.of(response).toString())
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		Function function = Function.class.cast(previousResultingModelElement);
		FunctionModule functionModule = function.getOwner();
		
		String httpCode = Overlay.of(response).getPathInParent();
		HttpStatusCode httpStatusCode = HttpStatusCode.get(httpCode);
		OptionDefinition<Long>.OptionValue statusCodeOptionValue = null;
		if (httpStatusCode != null) {
			statusCodeOptionValue = RestOptionEnum.OPTION_DEFINITION_STATUS_CODE. new OptionValue(httpStatusCode.getCode().longValue());
		}
		T exceptionType = null;
		String name = StringTools.firstUpperCase(function.getName()) + StringTools.firstUpperCase(httpCode);
		if (httpStatusCode == null) {
			// There are two ways to come to this place:
			// - the default exception - it has no status code defined
			// - a status code has been modeled, that is not contained in the enumeration HttpStatusCode
			TechnicalException technicalException = new TechnicalException("TechnicalException" + name);
			technicalException.setOriginatingElement(new GappOpenApi3ModelElementWrapper(response));
			function.addTechnicalException(technicalException);
			exceptionType = (T) technicalException;
		} else if (httpStatusCode.isServerError() || httpStatusCode.isRedirection()) {
			TechnicalException technicalException = new TechnicalException("TechnicalException" + name);
			technicalException.setOriginatingElement(new GappOpenApi3ModelElementWrapper(response));
			function.addTechnicalException(technicalException);
			technicalException.addOptions(statusCodeOptionValue);  // with this we later know the HTTP status code to be used in a writer
			exceptionType = (T) technicalException;
		} else if (httpStatusCode.isClientError()) {
			BusinessException businessException = new BusinessException("BusinessException" + name);
			businessException.setOriginatingElement(new GappOpenApi3ModelElementWrapper(response));
			function.addBusinessException(businessException);
			businessException.addOptions(statusCodeOptionValue);  // with this we later know the HTTP status code to be used in a writer
			exceptionType = (T) businessException;
		} else {
			Message errorMessage = OpenApi3Messages.ERROR_UNHANDLED_EXCEPTION_STATUS_CODE.getMessageBuilder()
				.parameters(httpCode, function.getName())
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		exceptionType.setModule(functionModule);
		return exceptionType;
	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean responsibleFor = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (responsibleFor) {
			HttpStatusCode httpStatusCode = HttpStatusCode.get(Overlay.of(Response.class.cast(originalModelElement)).getPathInParent());
			if (httpStatusCode == null || httpStatusCode.isServerError() || httpStatusCode.isRedirection() || httpStatusCode.isClientError()) {
				return true;
			} else {
				return false;
			}
		}
		
		return responsibleFor;
	}

}
