package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;

public class ServiceInterfaceToServiceImplementation<S extends ServiceInterface, T extends ServiceImplementation> extends AbstractOpenApi3ToFunction<S, T> {

	public ServiceInterfaceToServiceImplementation(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S serviceInterface, T serviceImplementation) {
		ServiceClient serviceClient = convertWithOtherConverter(ServiceClient.class, serviceImplementation);
		
		serviceInterface.addServiceClients(serviceClient);
	}

	@Override
	protected T onCreateModelElement(S serviceInterface, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T serviceImplementation = (T) new ServiceImplementation(serviceInterface.getName().replace("Interface", "Implementation") , serviceInterface);
		serviceInterface.addServiceImplementations(serviceImplementation);
		return serviceImplementation;
	}

}
