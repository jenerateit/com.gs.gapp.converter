/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.openapi.v3.gapp.function;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;


public class OpenApi3ToFunctionConverterProvider implements ModelConverterProviderI {

	/**
	 * 
	 */
	public OpenApi3ToFunctionConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new OpenApi3ToFunctionConverter();
	}
}
