package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3ToFunctionConverter;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.SchemaToType.OpenApiDataFormat;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.SchemaToType.TYPE;
import com.gs.gapp.dsl.java.JavaOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionLong;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionSerializable;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.reprezen.jsonoverlay.IJsonOverlay;
import com.reprezen.jsonoverlay.JsonOverlay;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.jsonoverlay.Reference;
import com.reprezen.jsonoverlay.ResolutionException;
import com.reprezen.kaizen.oasparser.model3.Header;
import com.reprezen.kaizen.oasparser.model3.MediaType;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;
import com.reprezen.kaizen.oasparser.model3.Operation;
import com.reprezen.kaizen.oasparser.model3.Parameter;
import com.reprezen.kaizen.oasparser.model3.Path;
import com.reprezen.kaizen.oasparser.model3.RequestBody;
import com.reprezen.kaizen.oasparser.model3.Response;
import com.reprezen.kaizen.oasparser.model3.Schema;
import com.reprezen.kaizen.oasparser.model3.Server;

public abstract class AbstractOpenApi3ToFunction<S extends Object, T extends ModelElement> extends AbstractModelElementConverter<S, T> {

	public AbstractOpenApi3ToFunction(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	@Override
	protected OpenApi3ToFunctionConverter getModelConverter() {
		return OpenApi3ToFunctionConverter.class.cast(super.getModelConverter());
	}
	
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement,
			ModelElementI previousResultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement, previousResultingModelElement);
	}
	
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		return null;
	}
	
	/**
	 * @param operation
	 * @param modelElement
	 */
	protected final void convertExtensions(final Operation operation, final ModelElement modelElement) {
		operation.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
	}
	
	/**
	 * @param openApi
	 * @param modelElement
	 */
	protected final void convertExtensions(final OpenApi3 openApi, final ModelElement modelElement) {
		openApi.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
		if (openApi.getInfo() != null) {
		    openApi.getInfo().getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
		}
	}
	
	/**
	 * @param path
	 * @param modelElement
	 */
	protected final void convertExtensions(final Path path, final ModelElement modelElement) {
		path.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
	}
	
	/**
	 * @param parameter
	 * @param modelElement
	 */
	protected final void convertExtensions(final Parameter parameter, final ModelElement modelElement) {
		parameter.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
	}
	
	/**
	 * @param header
	 * @param modelElement
	 */
	protected final void convertExtensions(final Header header, final ModelElement modelElement) {
		header.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
	}
	
	/**
	 * @param response
	 * @param modelElement
	 */
	protected final void convertExtensions(final Response response, final ModelElement modelElement) {
		response.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
	}
	
	/**
	 * @param requestBody
	 * @param modelElement
	 */
	protected final void convertExtensions(final RequestBody requestBody, final ModelElement modelElement) {
		requestBody.getExtensions().entrySet().stream().forEach(entry -> addOptionForExtension(entry.getKey(), entry.getValue(), modelElement));
	}
	
	/**
	 * @param key
	 * @param obj
	 * @param modelElement
	 */
	protected final void addOptionForExtension(String key, Object obj, ModelElement modelElement) {
		if (modelElement.getOption(key, null) != null) {
			addMessage(OpenApi3Messages.WARNING_EXTENSION_ALREADY_DEFINED.getMessageBuilder()
				.parameters(key)
				.build());
		}
		
//		System.out.println("key:" + key + ", value class:" + obj.getClass().getName());
		if (obj instanceof String) {
			String string = (String) obj;
			OptionDefinitionString newOptionDefinition = new OptionDefinitionString(key);
			modelElement.addOptions(newOptionDefinition.new OptionValue(string));
		} else if (obj instanceof Integer) {
			Integer integer = (Integer) obj;
			OptionDefinitionLong newOptionDefinition = new OptionDefinitionLong(key);
			modelElement.addOptions(newOptionDefinition.new OptionValue(integer.longValue()));
		} else if (obj instanceof Boolean) {
			Boolean bool = (Boolean) obj;
			OptionDefinitionBoolean newOptionDefinition = new OptionDefinitionBoolean(key);
			modelElement.addOptions(newOptionDefinition.new OptionValue(bool));
		} else if (obj instanceof Serializable) {
			Serializable serializable = (Serializable) obj;
			OptionDefinitionSerializable newOptionDefinition = new OptionDefinitionSerializable(key);
			modelElement.addOptions(newOptionDefinition.new OptionValue(serializable));
		} else {
			Message error = OpenApi3Messages.ERROR_EXTENSION_CANNOT_HANDLE.getMessageBuilder()
					.parameters(obj.getClass().getName())
					.build();
			throw new ModelConverterException(error.getMessage());
		}
	}

	/**
	 * @param name
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("hiding")
	protected <T extends com.gs.gapp.metamodel.basic.Namespace> T getNamespace(String name, Class<T> clazz) {
		String namespaceAsString;
		if (getModelConverter().getConverterOptions().isIgnoreTitleForNamespace()) {
			namespaceAsString =  getModelConverter().getConverterOptions().getNamespace().toLowerCase();
		} else {
			namespaceAsString =  getModelConverter().getConverterOptions().getNamespace() + "." + name.replaceAll("[^a-zA-Z0-9\\.]", "").toLowerCase();	
		}
		
		@SuppressWarnings("unchecked")
		T namespace = (T) getModelConverter().getModelElementCache().findModelElement(namespaceAsString, clazz);
		
		if (namespace == null) {
			Constructor<T> constructor;
			try {
				constructor = clazz.getConstructor(String.class);
				namespace = constructor.newInstance(namespaceAsString);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				Message error = OpenApi3Messages.ERROR_UNKNOWN.getMessageBuilder()
						.throwable(e)
						.parameters("could not create an instance of '" + clazz.getName() + "'")
						.build();
				throw new ModelConverterException(error.getMessage(), e);
			}
			
			addModelElement(namespace);
		}
		return namespace;
	}
	

	protected PersistenceModule getPersistenceModule(IJsonOverlay<?> element) {
		OpenApi3 openApi3 = Overlay.of(element).getModel();
		String name;
		if (openApi3 != null) {
			name = getEffectiveServiceName(openApi3);
		} else { // This is probably a schema from another file and has no model -> use the file name as persistence module name
			name = Overlay.of(element).getJsonReference();
			
			if (name != null && !name.isEmpty()) {
				if (name.contains(".json") || name.contains(".yaml") || name.contains(".yml") || name.contains(".js")) {
					// it is a file reference => use the file name
					if (name.lastIndexOf("/") > 0 && name.lastIndexOf(".") > 0 &&
							name.lastIndexOf("/") < name.lastIndexOf(".")) {
						// the name contains a path with slashes => remove leading path and the file ending
					    name = name.substring(name.lastIndexOf("/") + 1, name.lastIndexOf("."));
					} else if (name.lastIndexOf(".") > 0) {
						// the name has a file ending => remove the file ending
						name = name.substring(0, name.lastIndexOf("."));
					}
				} else if (name.contains("#/")) {
					// it is a component reference within a referenced file => use the component name
					name = name.substring(name.lastIndexOf("/") + 1);
				}
				
				name = normalizeString(name);
			} else {
				name = "default";
			}
		}
		
		PersistenceModule persistenceModule = PersistenceModule.class.cast(getModelConverter().getModelElementCache().findModelElement(name, PersistenceModule.class));
		if (persistenceModule == null) {
			String namespace = getNamespace(name, com.gs.gapp.metamodel.function.Namespace.class).getName() + ".definition";
			persistenceModule = new PersistenceModule(name);
			persistenceModule.setNamespace(new com.gs.gapp.metamodel.persistence.Namespace(namespace));
			addModelElement(persistenceModule);
		}
		return persistenceModule;
	}
	
	/**
	 * @param element
	 * @return
	 */
	protected Module getBasicModule(IJsonOverlay<?> element) {
		Module basicModule = getModel().getElement(Module.class, true);

		if (basicModule == null) {
			PersistenceModule persistenceModule = getPersistenceModule(element);
			com.gs.gapp.metamodel.basic.Namespace basicNamespace = new com.gs.gapp.metamodel.basic.Namespace(persistenceModule.getNamespace().getName());
			basicModule = new Module(persistenceModule.getName() + "Basic");
			basicModule.setNamespace(basicNamespace);
			getModel().addElement(basicModule);
		}
		return basicModule;
	}
	
	protected String normalizeString(String string) {
		return normalizeString(string, false);
	}
	
	protected String normalizeString(final String originalString, boolean replaceCertainCharacters) {
		String string = originalString.trim();
		if (replaceCertainCharacters) {
			if (string.startsWith("<=")) {
				string = string.replace("<=", "lessOrEqualThan");
			} else if (string.startsWith(">=")) {
				string = string.replace(">=", "greaterOrEqualThan");
			} else if (string.startsWith("<")) {
				string = string.replace("<", "lessThan");
			} else if (string.startsWith(">")) {
				string = string.replace(">", "greaterThan");
			} else if (string.startsWith("=")) {
				string = string.replace("=", "equals");
			} else if (string.startsWith("+")) {
				string = string.replace("+", "plus");
			} else if (string.startsWith("-")) {
				string = string.replace("-", "minus");
			}
		}
		
		if (!string.equals(originalString)) {
			addMessage(OpenApi3Messages.WARNING_NAMING_REPLACED_CERTAIN_CHARACTERS.getMessageBuilder()
				.parameters(originalString, string)
				.build());
		}
		
		String result = string.replaceAll("[^a-zA-Z0-9]", "");
		if (!result.equals(string)) {
			addMessage(OpenApi3Messages.WARNING_NAMING_TRUNCATED_NON_ALPHANUMERIC_CHARACTERS.getMessageBuilder()
					.parameters(string, result)
					.build());
		}
		return result;
	}
	
	protected <C extends JsonOverlay<?>> C getParent(Class<C> clazz, IJsonOverlay<?> child) {
		JsonOverlay<?> parent = Overlay.of(child).getParent();
		if (!clazz.isInstance(parent)) {
			Message error = OpenApi3Messages.ERROR_UNKNOWN.getMessageBuilder()
					.parameters("could not get correct parent from " + child.getClass() + " parent is of type " + parent.getClass())
					.build();
			throw new ModelConverterException(error.getMessage());
		}
		return clazz.cast(parent);
	}

	@Deprecated
	protected boolean isReferenced(IJsonOverlay<?> element, IJsonOverlay<?> parentElement) {
		String parentRef = Overlay.of(parentElement).getJsonReference();
		String ref = Overlay.of(element).getJsonReference();
		return !ref.contains(parentRef);
	}
	
//	protected String deduceNameForSchema(Schema schema) {
//		String name = null;
//		Overlay<Schema> schemaOverlay = Overlay.of(schema);
//		if (Schema.class.isInstance(schemaOverlay.getParent())) { // -- this is a map
//			name = Overlay.of(schemaOverlay.getParent()).getPathInParent();
//			name = StringTools.firstUpperCase(name) + "Value";
//		} else if (schemaOverlay.getJsonReference().contains("#/components/schemas/")) { // -- this is a schema from an other file
//			String ref = schemaOverlay.getJsonReference();
//			name = ref.substring(ref.indexOf("#/components/schemas/") + 21);
//			name = StringTools.firstUpperCase(name);
//		}
//		return null;
//	}
	
	protected boolean isReferenced(Schema schema) {
//		return Overlay.of(schema).getJsonReference().endsWith("#/components/schemas/" + schema.getName());
//		return Overlay.of(schema).getJsonReference().contains("#/components/schemas/"); // if the schema is referenced in an other file the schema has no name
		return Overlay.of(schema).getJsonReference().matches(".*#/components/schemas/[^/]+"); // if the schema is referenced in an other file the schema has no name
											// should match #/components/schemas/Duty/ but not  #/components/schemas/Duty/foo/bar
	}
	
	protected Function getFunction(FunctionParameter functionParameter) {
		FunctionModule functionModule = FunctionModule.class.cast(functionParameter.getModule());
		return functionModule.getFunctions().stream()
				.filter(fn -> fn.getFunctionInParameters().contains(functionParameter) || fn.getFunctionOutParameters().contains(functionParameter))
				.findAny()
				.orElseThrow(() -> {
					Message error = OpenApi3Messages.ERROR_UNKNOWN.getMessageBuilder()
							.parameters("could not find function for function paramter " + functionParameter.getName())
							.build();
					return new ModelConverterException(error.getMessage());
				});
	}
	
	
	protected final class ConvertedSchema {
		private final Type type;
		private final CollectionType collectionType;
		private final int dimension;
		private final Type keyType;
		private final OptionDefinition<Boolean>.OptionValue option;
		private final Boolean expandable;
		private StringBuilder additionalModelInfo = new StringBuilder();
		
		private ConvertedSchema(Type type, CollectionType collectionType, Boolean expandable, int dimension) {
			this.type = type;
			this.collectionType = collectionType;
			this.keyType = collectionType == CollectionType.KEYVALUE ? PrimitiveTypeEnum.STRING.getPrimitiveType() : null;
			this.expandable = expandable;
			this.dimension = dimension;
			if (collectionType == null) { // -- normal data type, not binary nor a collection
				boolean useWrapper = getModelConverter().getConverterOptions().isJavaUsePrimitiveWrapper();
				if (useWrapper && type instanceof PrimitiveType) {
				    this.option = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(useWrapper);
				} else {
					this.option = null;
				}
			} else if (collectionType == CollectionType.ARRAY) { // -- this is a binary data type
				// This is not required since not using a primitive type wrapper is the default.
				// We have to distinguish here in order to prevent some options from being created through model import. (mmt 03-Aug-2023)
//				this.option = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(false);
				this.option = null;
			} else if (collectionType == CollectionType.LIST || collectionType == CollectionType.SET ||
					collectionType == CollectionType.SORTED_SET || collectionType == CollectionType.KEYVALUE) { // -- this is a collection or a map
				if (type instanceof PrimitiveType) {
					// Only when there is a primitive type handled in the collection or map, we need to wrap primitive types in Java.
					// And what is more, we even MUST use the primitive type wrapper since collections and maps cannot handle primitive types. (mmt 03-Aug-2023)
				    this.option = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
				} else {
					this.option = null;
				}
			} else {
				Message error = OpenApi3Messages.ERROR_UNKNOWN.getMessageBuilder()
						.parameters("unknown collection type " + collectionType)
						.build();
				throw new ModelConverterException(error.getMessage());
			}
		}
		
		public Type getType() {
			return type;
		}
		
		public CollectionType getCollectionType() {
			return collectionType;
		}
		
		public Boolean getExpandable() {
			return expandable;
		}
		
		public int getDimension() {
			return dimension;
		}

		public Type getKeyType() {
			return keyType;
		}
		
		public OptionDefinition<Boolean>.OptionValue getOption() {
			return option;
		}

		public StringBuilder getAdditionalModelInfo() {
			return additionalModelInfo;
		}

		public void addAdditionalModelInfo(String additionalModelInfoLine) {
			if (this.additionalModelInfo.length() > 0) {
				this.additionalModelInfo.append(System.lineSeparator());
			}
			this.additionalModelInfo.append(additionalModelInfoLine);
		}
	}
	
	@Deprecated
	protected ConvertedSchema convertSchema(Schema schema, IJsonOverlay<?> parentElement, ModelElementI previousResultingModelElement) {
		return convertSchema(schema, previousResultingModelElement);
	}
	
	protected ConvertedSchema convertSchema(final Schema schema, final ModelElementI previousResultingModelElement) {
		//OpenApi3 openApi3 = Overlay.of(schema).getModel();
		CollectionType collectionType = null;
		Boolean expandable = null;
		int dimension = 0;
		
		// In the subsequent logic the 'effectiveSchema' is going to be changed, 'schema' keeps the
		// original value and helps during debugging to understand what is going on. (mmt 24-Jun-2022)
		Schema effectiveSchema = schema;
		
		// In the subsequent logic the 'previousResultingModelElementForSubconverts' is going to be changed,
		// 'previousResultingModelElement' keeps the original value and helps during debugging
		// to understand what is going on. (mmt 24-Jun-2022)
		ModelElementI previousResultingModelElementForSubconverts = previousResultingModelElement;

		if (SchemaToType.TYPE.getType(effectiveSchema.getType()) == TYPE.ARRAY) {
    		collectionType = CollectionType.LIST;
    		
    		while (SchemaToType.TYPE.getType(effectiveSchema.getType()) == TYPE.ARRAY) {
    			Overlay<Schema> arraySchemaOverlay = Overlay.of(effectiveSchema);
    			
    			Reference creatingRef = arraySchemaOverlay.getOverlay()._getCreatingRef();
        		if (creatingRef != null) {
        			addErrorIfNeeded(creatingRef.getInvalidReason());
        		}
    			
    			dimension++;
    			effectiveSchema = effectiveSchema.getItemsSchema();
    		}
    		
    		Reference creatingRef = Overlay.of(effectiveSchema).getOverlay()._getCreatingRef();
    		if (creatingRef != null) {
    			addErrorIfNeeded(creatingRef.getInvalidReason());
    		}
    	} else if (SchemaToType.isBinary(effectiveSchema)) {
    		collectionType = CollectionType.ARRAY;
		} else if (Overlay.of(effectiveSchema.getAdditionalPropertiesSchema()).isPresent()) {
			BooleanNode booleanNode = null;
			JsonNode overlayAsParsedJson = Overlay.of(effectiveSchema.getAdditionalPropertiesSchema()).getParsedJson();
			if (overlayAsParsedJson instanceof BooleanNode) {
				booleanNode = (BooleanNode) overlayAsParsedJson;
			}
			
			// AJAXB-44 Only when the additionalProperties is not a boolean, we have the case of a modeled dictionary/hashmap/associative array.
			// See here for more information: https://swagger.io/docs/specification/data-models/dictionaries/
			if (booleanNode == null) {
				collectionType = CollectionType.KEYVALUE;
				if (Schema.class.isInstance(Overlay.of(effectiveSchema).getParent())) {
					System.out.println("!!!!" + effectiveSchema.getName());
				}
	    		while (Overlay.of(effectiveSchema.getAdditionalPropertiesSchema()).isPresent()) {
	    			dimension++;
	    			if (isReferenced(effectiveSchema)) { // -- if the schema is referenced, clear it so no strange name is calculated
	    				previousResultingModelElementForSubconverts = null;
	    			}
	    			effectiveSchema = effectiveSchema.getAdditionalPropertiesSchema();
	    		}
	    		
	    		TYPE typeOfAdditionalParameters = TYPE.getType(effectiveSchema.getType());
	    		if (typeOfAdditionalParameters == TYPE.ARRAY) {
	    			Message errorMessage = OpenApi3Messages.ERROR_ADDITIONAL_PROPERTIES_DOESNT_SUPPORT_SCHEMA_WITH_ARRAY_TYPE.getMessageBuilder()
	    			.parameters(Overlay.of(schema).toString())
	    			.build();
	    			throw new ModelConverterException(errorMessage.getMessage());
	    		}
			}
		}
		
    	/* if this is a reference the previous element is the persistence module */
		if (isReferenced(effectiveSchema) || previousResultingModelElementForSubconverts == null) {
			previousResultingModelElementForSubconverts = getPersistenceModule(effectiveSchema); // convertWithOtherConverter(PersistenceModule.class, openApi3);
			if (getModelConverter().getConverterOptions().isExpandable()) {
				expandable = true;
			}
		} else if (previousResultingModelElementForSubconverts.getClass() == ComplexType.class) {
			previousResultingModelElementForSubconverts = ComplexType.class.cast(previousResultingModelElementForSubconverts).getOriginatingElement(Entity.class);
    	}
		final Schema newSchema = effectiveSchema;
		List<Type> exitingTypesForSchema = getConversionCache().entrySet().stream()
				.filter(e -> Type.class.isInstance(e.getValue()))
				.filter(e -> {
					if (Schema.class.isInstance(e.getKey().getOriginalModelElement())) {
						Schema existingSchema = Schema.class.cast(e.getKey().getOriginalModelElement());
						if (Overlay.of(newSchema).getJsonReference().equals(Overlay.of(existingSchema).getJsonReference())) {
							// --- this is not realy needed -> json ref should be enough
							return newSchema.toString().equals(existingSchema.toString());
						}
					}
					return false;
				})
				.map(e -> Type.class.cast(e.getValue()))
				.collect(Collectors.toList());

		Type type;
		if (!exitingTypesForSchema.isEmpty()) {
			type = exitingTypesForSchema.get(0);
		} else {
			type = convertWithOtherConverter(Type.class, effectiveSchema, previousResultingModelElementForSubconverts);
		}

		if (Entity.class.isInstance(type)) {
			type = convertWithOtherConverter(ComplexType.class, type);
		}
		AbstractOpenApi3ToFunction<S, T>.ConvertedSchema convertedSchema = new ConvertedSchema(type, collectionType, expandable, dimension);
		TYPE openApiType = TYPE.getType(effectiveSchema.getType());
		if (openApiType != null && effectiveSchema.getFormat() != null &&
				!effectiveSchema.getFormat().isEmpty() &&
				!openApiType.isPredefindFormatSupported(OpenApiDataFormat.get(effectiveSchema.getFormat()))) {
			convertedSchema.addAdditionalModelInfo("custom format modeled: " + effectiveSchema.getFormat());
		}
		
		return convertedSchema;
	}
	
	protected static final class MediaTypes {
		private final MediaType mediaType;
		private final List<String> mediaTypeList;
		
		private MediaTypes(MediaType mediaType, List<String> mediaTypeList) {
			this.mediaType = mediaType;
			this.mediaTypeList = mediaTypeList;
		}
		
		public MediaType getMediaType() {
			return mediaType;
		}
		
		public List<String> getMediaTypeList() {
			return mediaTypeList;
		}
	}
	
	protected MediaTypes getMediaTypes(Map<String, MediaType> mediaTypes) {
		if (mediaTypes.isEmpty()) {
			return null;
		}
		Entry<String, MediaType> firstMediaType = mediaTypes.entrySet().iterator().next();
		/* check if the schema of the different media types is the same */
		Schema firstSchema = firstMediaType.getValue().getSchema();
		List<String> mediaTypeEnums = mediaTypes.entrySet().stream()
				.filter(entry -> entry.getValue().getSchema() != null && entry.getValue().getSchema().equals(firstSchema))
				.map(entry -> entry.getKey())
				.collect(Collectors.toList());
		
		if (mediaTypeEnums.size() != mediaTypes.size()) {
			String unhandledMediaTypes = mediaTypes.entrySet().stream()
					.filter(entry -> entry.getValue().getSchema() != firstSchema)
					.map(entry -> entry.getKey())
					.collect(Collectors.joining(", "));
			addMessage(OpenApi3Messages.WARNING_UNKNOWN_MEDIA_TYPE.getMessageBuilder()
					.parameters(unhandledMediaTypes)
					.build());
		}
		return new MediaTypes(firstMediaType.getValue(), mediaTypeEnums);
	}
	
	protected static final class Text {
		private final String key;
		private final String value;
		
		public Text(String value) {
			this(null, value);
		}
		
		public Text(String key, String value) {
			this.key = key;
			this.value = value;
		}
		
		public boolean isNotEmpty() {
			return value != null && !value.isEmpty();
		}
		
		public String getText() {
			if (key != null) {
				return key + ": " + value.trim();
			} else {
				return value.trim().replaceAll("\\s+", " ");//.replaceAll("\\r\\n|\\r|\\n", " ");
			}
		}
		
	}

	protected String getBody(Text...text) {
		return Arrays.stream(text)
				.filter(Text::isNotEmpty)
				.map(Text::getText)
				.collect(Collectors.joining(StringTools.NEWLINE + StringTools.NEWLINE));
				
	}
	
	protected String getBody(String...text) {
		return getBody(Arrays.stream(text)
				.map(Text::new)
				.toArray(Text[]::new));
	}
	
	protected String getServerPath(Collection<Server> servers) {
		String serverPath = "/";
		List<String> serverPaths = getServerPaths(servers);
		if (serverPaths.size() == 1) {
			serverPath = serverPaths.get(0);
		}
		return serverPath;
	}
	
	/**
	 * This got invented for AJAX-253.
	 * 
	 * @param servers
	 * @return
	 */
	protected List<String> getServerPaths(Collection<Server> servers) {
		List<String> serverPaths = servers.stream()
				.map(Server::getUrl)
				.map(u -> {
					try {
						return new URL(u);
					} catch (MalformedURLException e) {
						addMessage(OpenApi3Messages.WARNING_UNKNOWN.getMessageBuilder()
								.parameters("unknown url format: " + u)
								.build());
						return null;
					}
				})
				.filter(url -> url != null)
				.map(URL::getPath)
				.distinct()
				.collect(Collectors.toList());
		return serverPaths;
	}
	
	protected void addMessage(Message message) {
		getModelConverter().addMessage(message);
	}
	
	protected void addErrorIfNeeded(ResolutionException resolutionException) {
		if (resolutionException != null) {
			addError(OpenApi3Messages.ERROR_REFERENCE_NOT_RESOLVABLE.getMessageBuilder()
					.parameters(resolutionException.getReference().getRefString(),
							    resolutionException.getMessage())
				    .build()
				    .getMessage());
		}
	}
	
	protected String getEffectiveServiceName(OpenApi3 openApi) {
		String serviceNameFromGeneratorOption = getModelConverter().getConverterOptions().getServiceName();
		String serviceNameFromOpenApiModel = openApi.getInfo().getTitle();
		
		return normalizeString(serviceNameFromGeneratorOption != null && serviceNameFromGeneratorOption.length() > 0 ?
				serviceNameFromGeneratorOption : serviceNameFromOpenApiModel);
	}
	
	/**
	 * This got invented for AJAX-253.
	 * 
	 * @param functionModule
	 * @param openApi3
	 */
	protected void addServerUrlsToDocumentation(FunctionModule functionModule, OpenApi3 openApi3) {
		List<String> serverPaths = getServerPaths(openApi3.getServers());
		if (!serverPaths.isEmpty()) {
			StringBuilder serverPathsForDoc = new StringBuilder("server paths from model:");
			serverPaths.stream()
			    .forEach(serverPath -> {
			    	serverPathsForDoc.append(System.lineSeparator()).append("    ").append(serverPath);
			    });
			functionModule.addToBody(serverPathsForDoc.toString());
		}
	}
}
