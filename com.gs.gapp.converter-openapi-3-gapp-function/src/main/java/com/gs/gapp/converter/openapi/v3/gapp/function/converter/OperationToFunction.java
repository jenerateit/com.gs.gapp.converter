package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.dsl.rest.TransportTypeEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.function.ServiceModelProcessing;
import com.gs.gapp.metamodel.openapi.HttpStatusCode;
import com.reprezen.jsonoverlay.MapOverlay;
import com.reprezen.kaizen.oasparser.model3.Header;
import com.reprezen.kaizen.oasparser.model3.Operation;
import com.reprezen.kaizen.oasparser.model3.Parameter;
import com.reprezen.kaizen.oasparser.model3.Path;
import com.reprezen.kaizen.oasparser.model3.Response;
import com.reprezen.kaizen.oasparser.ovl3.PathImpl;

public class OperationToFunction<S extends Operation, T extends Function> extends AbstractOpenApi3ToFunction<S, T> {

	public OperationToFunction(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	
	@Override
	protected void onConvert(S operation, T function) {
		function.setBody(getBody(operation.getSummary(), operation.getDescription()));
		
		@SuppressWarnings("unused")
		Optional<String> modeledHttpVerb = ServiceModelProcessing.getModeledHttpVerb(function);
		List<Parameter> combinedInParameters = new ArrayList<>();
		combinedInParameters.addAll(getPath(operation).getParameters());
		combinedInParameters.addAll(operation.getParameters());
		
		for (Parameter parameter : combinedInParameters) {
			FunctionParameter functionParameter = convertWithOtherConverter(FunctionParameter.class, parameter, function);
			functionParameter.setBody(getBody(parameter.getDescription()));
			function.addFunctionInParameter(functionParameter);
		}
		

		MediaTypes requestBodyMediaTypes = getMediaTypes(operation.getRequestBody().getContentMediaTypes());
		if (requestBodyMediaTypes != null) {
			FunctionParameter functionParameter = convertWithOtherConverter(FunctionParameter.class, requestBodyMediaTypes.getMediaType(), function);
			functionParameter.setBody(getBody(operation.getRequestBody().getDescription()));
			function.addFunctionInParameter(functionParameter);

			List<String> consumes = requestBodyMediaTypes.getMediaTypeList();
			if (consumes != null && !consumes.isEmpty()) {
			    OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(consumes);
			    function.addOptions(consumesOptionValue);
			}
		}
		
		Response responseForSuccess = null;
		HttpStatusCode httpStatusCodeForSuccess = null;
		Map<String, Header> successResponseHeaders = new HashMap<>();
		for (Entry<String, Response> entry : operation.getResponses().entrySet()) {
			if ("default".equalsIgnoreCase(entry.getKey())) {
				continue;  // this has no effect on the determination of the responses entry that relates to success
			}
			Response response = entry.getValue();
			HttpStatusCode httpStatusCode = HttpStatusCode.get(entry.getKey());
			if (httpStatusCode == null) {
				addMessage(OpenApi3Messages.WARNING_UNKNOWN_HTTP_STATUS_CODE.getMessageBuilder()
						.parameters(entry.getKey()).build());
			}
			if (httpStatusCodeForSuccess == null) httpStatusCodeForSuccess = httpStatusCode;  // make sure that there is a value set, which is needed in case there is only one defined (which is sufficient, according to OpenAPI spec)
			if (httpStatusCode != null && httpStatusCode.isSuccess()) {
				if (response.getHeaders() != null) {
					for (String headerName : response.getHeaders().keySet()) {
						successResponseHeaders.put(headerName, response.getHeaders().get(headerName));
					}
				}
				if (responseForSuccess == null) {
					responseForSuccess = response;
					httpStatusCodeForSuccess = httpStatusCode;
				} else {
					if (httpStatusCodeForSuccess.isSuccess() == false || httpStatusCode == HttpStatusCode.SC_OK) {
						// in case the currently set status code is not for success or in case several 2xx codes are specified, use the 200 code in case there is one, otherwise use the first one that has been found
						responseForSuccess = response;
						httpStatusCodeForSuccess = httpStatusCode;
					}
				}
			}
		}
		
		if (successResponseHeaders.size() > 0) {
			// --- add additional out parameters that are marked as response header (a REST option)
			for (Entry<String, Header> entry : successResponseHeaders.entrySet()) {
				Header header = entry.getValue();
				FunctionParameter functionParameter = new FunctionParameter(normalizeString(entry.getKey()));
				convertExtensions(header, functionParameter);
				functionParameter.setModule(function.getModule());
				function.addFunctionOutParameter(functionParameter);
				functionParameter.setParameterType(ParameterType.OUT);
				OptionDefinition<String>.OptionValue optionValueTransportType = RestOptionEnum.OPTION_DEFINITION_TRANSPORT_TYPE. new OptionValue(TransportTypeEnum.HEADER.getName());
				functionParameter.addOptions(optionValueTransportType);
				functionParameter.setBody(getBody(header.getDescription()));
				ConvertedSchema convertedSchema = convertSchema(header.getSchema(), functionParameter);
				functionParameter.setCollectionType(convertedSchema.getCollectionType());
				functionParameter.setDimension(convertedSchema.getDimension());
				functionParameter.setType(PrimitiveTypeEnum.STRING.getPrimitiveType()); // fra 11-Oct 2019 -- the type of a header can only be a string
			}
		}

		if (httpStatusCodeForSuccess == null) {
			httpStatusCodeForSuccess = HttpStatusCode.SC_OK;
			addMessage(OpenApi3Messages.WARNING_COULD_NOT_IDENTITY_SUCCESS_CASE.getMessageBuilder()
					.parameters(function.getName())
					.build());
		}
		function.addOptions(RestOptionEnum.OPTION_DEFINITION_STATUS_CODE. new OptionValue(httpStatusCodeForSuccess.getCode().longValue()));
		
		for (Entry<String, Response> entry : operation.getResponses().entrySet()) {
			if ("default".equalsIgnoreCase(entry.getKey())) {
				continue;  // this has no effect on the determination of the responses entry that relates to success
			}
			Response response = entry.getValue();
			HttpStatusCode httpStatusCode = HttpStatusCode.get(entry.getKey());
			
			@SuppressWarnings("unused")
			ExceptionType exceptionType = convertWithOtherConverter(ExceptionType.class, response, function);
			

			if (httpStatusCodeForSuccess == httpStatusCode) {
				MediaTypes mediaTypes = getMediaTypes(response.getContentMediaTypes());
				if (mediaTypes != null) {

					FunctionParameter functionParameter = convertWithOtherConverter(FunctionParameter.class, mediaTypes.getMediaType(), function);
					functionParameter.setBody(getBody(response.getDescription()));
					function.addFunctionOutParameter(functionParameter);

					List<String> produces = mediaTypes.getMediaTypeList();
					if (produces != null && !produces.isEmpty()) {
					    OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(produces);
					    function.addOptions(producesOptionValue);
					}
				}
			}
		}
	}
	


	@Override
	protected T onCreateModelElement(S operation, ModelElementI previousResultingModelElement) {
		if (!FunctionModule.class.isInstance(previousResultingModelElement)) {
			Message errorMessage = OpenApi3Messages.ERROR_UNEXPECTED_PREVIOUS_MODEL_ELEMENT.getMessageBuilder()
					.parameters(previousResultingModelElement.getClass().getName(),
							"operation: " + operation.getOperationId())
					.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		FunctionModule functionModule = FunctionModule.class.cast(previousResultingModelElement);
		
		
		String name = operation.getOperationId();
		if (name == null || name.isEmpty()) {
			
		 	// TODO WARNING for no operation id available
			@SuppressWarnings("rawtypes")
			MapOverlay parent = getParent(MapOverlay.class, operation);
			
			String verb = null;
			for (Object key : parent.keySet()) {
				if (parent.get(key.toString()) == operation) {
					verb = key.toString();
					break;
				}
			}
			
			if (verb == null) {
				Message errorMessage = OpenApi3Messages.ERROR_MORE_THAN_ONE_HTTP_VERB_AND_NO_OPERATIONID.getMessageBuilder()
					.parameters(getPath(operation).getPathString())
					.build();
		        throw new ModelConverterException(errorMessage.getMessage());
			}
			
//			if (parent.keySet().size() != 1) {
//				Message errorMessage = OpenApi3Messages.ERROR_MORE_THAN_ONE_HTTP_VERB_AND_NO_OPERATIONID.getMessageBuilder()
//						.parameters(getPath(operation).getPathString())
//						.build();
//				throw new ModelConverterException(errorMessage.getMessage());
//			}
//			String verb = parent.keySet().iterator().next().toString();
			
			String path = getPath(operation).getPathString();
			name = verb.toLowerCase() + StringTools.firstUpperCase(normalizeString(path));
			addMessage(OpenApi3Messages.WARNING_OPERATIONID_NOT_SET.getMessageBuilder()
					.parameters(path, verb).build());
		}
		@SuppressWarnings("unchecked")
		T function = (T) new Function(normalizeString(name), functionModule);
		GappOpenApi3ModelElementWrapper wrapper = new GappOpenApi3ModelElementWrapper(operation);
		function.setOriginatingElement(wrapper);
		
		convertExtensions(operation, function);
		
		return function;
	}
	
	
	private Path getPath(S operation) {
		@SuppressWarnings("rawtypes")
		MapOverlay parent = getParent(MapOverlay.class, operation);
		PathImpl path = getParent(PathImpl.class, parent);
		return path;
	}
	

}
