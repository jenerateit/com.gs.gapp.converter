package com.gs.gapp.converter.openapi.v3.gapp.function;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum OpenApi3Messages implements MessageI {
	ERROR_UNKNOWN(MessageStatus.ERROR,
			"0001",
			"An unknown error has occurred: %s",
			null),
	
	ERROR_UNEXPECTED_PREVIOUS_MODEL_ELEMENT(MessageStatus.ERROR,
			"0002",
			"unexpected previous model element: %s. This problem has occurred while trying to convert the model element '%s'.",
			"This probably is a bug in the generator. Please contact the producer of the generator and file a bug report."),
	
	ERROR_MORE_THAN_ONE_HTTP_VERB_AND_NO_OPERATIONID(MessageStatus.ERROR,
			"0003",
			"More than one HTTP verb defined for %s, but no operation id is set.",
			"Set an operation id"),
	
	ERROR_NO_OPENAPI_MODEL_FOUND_IN_INPUT(MessageStatus.ERROR,
			"0004",
			"The input for the generator doesn't include any OpenAPI model that can be processed.",
			"You need at least one yaml/yml/json file as being the input, where that file has the fields 'openapi' and 'info' set." + System.lineSeparator() +
			    "Please note that you might have model files that don't have these fields set and that contain model elements that are referenced from other model file." + System.lineSeparator() +
				"Those don't count as OpenAPI input model."),
	
	ERROR_UNHANDLED_EXCEPTION_STATUS_CODE(MessageStatus.ERROR,
			"0005",
			"Unhandled status code %s to convert to exception type in operation %s",
			"Check the status code of the operation"),
	
	ERROR_UNHANDLED_SCHEMA_TYPE(MessageStatus.ERROR,
			"0006",
			"Unhandled type of schema %s",
			"Please note that it is possible that you use a reference to a component that doesn't exist. Check your model an correct the reference if needed."),
	
	ERROR_EXTENSION_CANNOT_HANDLE(MessageStatus.ERROR,
			"0007",
			"Cannot handle OpenApi3 extension object of type '%s' (unknown type)",
			null),
	
	ERROR_SCHEMA_WITH_ARRAY_TYPE_NOT_SUPPORTED(MessageStatus.ERROR,
			"0008",
			"The generator doesn't support the conversion of schemas with an ARRAY type. The schema '%s' does have an ARRAY type.",
			"Please contact the producer of the generator when you need support for the ARRAY type."),
	
	ERROR_ADDITIONAL_PROPERTIES_DOESNT_SUPPORT_SCHEMA_WITH_ARRAY_TYPE(MessageStatus.ERROR,
			"0009",
			"The generator doesn't support the usage of 'additionalProperties' that have schema with an ARRAY type. The schema '%s' uses such additional properties.",
			"Please contact the producer of the generator when you need support for the ARRAY type."),
	
	ERROR_REFERENCE_NOT_RESOLVABLE(MessageStatus.ERROR,
			"0010",
			"The reference '%s' could not be resolved due to this reason: %s",
			"Please check your OpenAPI document(s) and correct the reference. Note that it could also be possible that you did not pass all required files to the generator. Check this, too."),
	
	// -----------------------------------------------------
	// -------- WARNINGS -----------------------------------
	
	WARNING_UNKNOWN(MessageStatus.WARNING,
			"1001",
			"An unknown warning has occurred: %s",
			null),
	
	WARNING_OPERATIONID_NOT_SET(MessageStatus.WARNING,
			"1002",
			"Operation id was not set for %s with operation %s",
			"Set an operation id to link the generated code more explicitly with the model"),
	
	WARNING_UNKNOWN_HTTP_STATUS_CODE(MessageStatus.WARNING,
			"1003",
			"Unknown http status code:  %s",
			null),
	
	WARNING_ALL_OF_EXPAND(MessageStatus.WARNING,
			"1004",
			"Could not determine the possible parent of %s from the allOf properties, try to expand all properties",
			"Change the model if possible to have only one parent"), 
	
	WARNING_EXTENSION_ALREADY_DEFINED(MessageStatus.WARNING,
			"1005",
			"Extension %s already defined",
			null),
	
	WARNING_UNKNOWN_MEDIA_TYPE(MessageStatus.WARNING,
			"1006",
			"Could not handle content media type %s",
			"Check it the media type is correct"),
	
	WARNING_EMPTY_VALUE_FOUND_IN_ENUM(MessageStatus.WARNING,
			"1007",
			"One of the values of the enum %s is empty. The empty value is simply ignored.",
			"If this happened unintentionally, you can simply delete the emtpty value from the enum."),
	
	WARNING_NAMING_REPLACED_CERTAIN_CHARACTERS(MessageStatus.WARNING,
			"1008",
			"A name/identifier contained special characters like for instance '+' or '-'. That got replaced with a speaking name: '%s' ==> '%s'.",
			null),
	
	WARNING_NAMING_TRUNCATED_NON_ALPHANUMERIC_CHARACTERS(MessageStatus.WARNING,
			"1009",
			"A name/identifier contained non-alphanumeric characters. Those have been removed: '%s' ==> '%s'.",
			null),
	
	WARNING_COULD_NOT_IDENTITY_SUCCESS_CASE(MessageStatus.WARNING,
			"1010",
			"Could not identify a response that represents the SUCCESS case in operation %s.",
			"Please add a success response if you want to explicitly control this."),
	
	WARNING_TYPE_HAS_NO_FILEDS(MessageStatus.WARNING,
			"1011",
			"A schema '%s' could successfully be converted to a type '%s', but the type doesn't have a single field.",
			"You might have forgotten to add properties to a schema or items to an array. However, it is perfectly fine to have types generated that have no fields."),
	
	// -----------------------------------------------------
	// -------- INFOS --------------------------------------
	
	INFO_MORE_THAN_ONE_TAG(MessageStatus.INFO,
			"2002",
			"Found more than one tag for %s %s using %s for grouping",
			null),
	
	INFO_UNKNOWN_STRING_FORMAT(MessageStatus.INFO,
			"2003",
			"Found unknown string format %s in schema '%s'. The generated code doesn't handle this format in a special way.",
			"You need to manually add code to validate this format"),
	
	INFO_UNKNOWN_NUMBER_FORMAT(MessageStatus.INFO,
			"2004",
			"Found unknown number format %s in schema '%s'. The generated code doesn't handle this format in a special way.",
			"You need to manually add code to validate this format"),
	
	INFO_UNKNOWN_INTEGER_FORMAT(MessageStatus.INFO,
			"2005",
			"Found unknown integer format %s in schema '%s'. The generated code doesn't handle this format in a special way.",
			"You need to manually add code to validate this format"),
	
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "OPENAPI3";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private OpenApi3Messages(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	

	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
