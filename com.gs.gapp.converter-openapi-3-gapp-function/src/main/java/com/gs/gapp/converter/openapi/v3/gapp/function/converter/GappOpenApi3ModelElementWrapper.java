package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.reprezen.jsonoverlay.IJsonOverlay;
import com.reprezen.jsonoverlay.JsonOverlay;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.kaizen.oasparser.model3.MediaType;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;
import com.reprezen.kaizen.oasparser.model3.Operation;
import com.reprezen.kaizen.oasparser.model3.Parameter;
import com.reprezen.kaizen.oasparser.model3.Response;
import com.reprezen.kaizen.oasparser.model3.Schema;
import com.reprezen.kaizen.oasparser.ovl3.RequestBodyImpl;
import com.reprezen.kaizen.oasparser.ovl3.ResponseImpl;

public class GappOpenApi3ModelElementWrapper extends ModelElementWrapper {

	private static final long serialVersionUID = -6695510545219422458L;
	
	private final OpenApi3 openApi3;
	
	private final Collection<OpenApi3> openApi3Collection = new LinkedHashSet<>();
	
	private final Response response;
	
	private final Schema schema;
	
	private final Parameter parameter;
	
	private final Operation operation;
	
	private final MediaType mediaType;

	public GappOpenApi3ModelElementWrapper(OpenApi3 openApiElement) {
		super(openApiElement);
		this.openApi3 = openApiElement;
		this.response = null;
		this.schema = null;
		this.parameter = null;
		this.operation = null;
		this.mediaType = null;
	}
	
	public GappOpenApi3ModelElementWrapper(Collection<OpenApi3> openApiElementCollection) {
		super(openApiElementCollection);
		this.openApi3 = null;
		this.response = null;
		this.openApi3Collection.addAll(openApiElementCollection);
		this.schema = null;
		this.parameter = null;
		this.operation = null;
		this.mediaType = null;
	}
	
	public GappOpenApi3ModelElementWrapper(Response response) {
		super(response);
		this.response = response;
		this.openApi3 = null;
		this.schema = null;
		this.parameter = null;
		this.operation = null;
		this.mediaType = null;
	}
	
	public GappOpenApi3ModelElementWrapper(Schema schema) {
		super(schema);
		this.response = null;
		this.openApi3 = null;
		this.schema = schema;
		this.parameter = null;
		this.operation = null;
		this.mediaType = null;
	}
	
	public GappOpenApi3ModelElementWrapper(Parameter parameter) {
		super(parameter);
		this.response = null;
		this.openApi3 = null;
		this.schema = null;
		this.parameter = parameter;
		this.operation = null;
		this.mediaType = null;
	}
	
	public GappOpenApi3ModelElementWrapper(Operation operation) {
		super(operation);
		this.response = null;
		this.openApi3 = null;
		this.schema = null;
		this.parameter = null;
		this.operation = operation;
		this.mediaType = null;
	}
	
	public GappOpenApi3ModelElementWrapper(MediaType mediaType) {
		super(mediaType);
		this.response = null;
		this.openApi3 = null;
		this.schema = null;
		this.parameter = null;
		this.operation = null;
		this.mediaType = mediaType;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getModuleName()
	 */
	@Override
	public String getModuleName() {
		String result = super.getModuleName();
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getId()
	 */
	@Override
	public String getId() {
		String result = super.getId();
		if (this.openApi3 != null) {
			result = getName().replace(" ", "_");
		} else if (this.response != null) {
			result = this.response.getName().replace(" ", "_");
		} else if (!this.openApi3Collection.isEmpty()) {
			result = getName().replace(" ", "_");
		} else if (this.schema != null) {
			result = getName().replace(" ", "_");
		} else if (this.parameter != null) {
			result = getName().replace(" ", "_");
		} else if (this.operation != null) {
			result = getName().replace(" ", "_");
		} else if (this.mediaType != null) {
			result = getName().replace(" ", "_");
		}
		
		return result;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#getName()
	 */
	@Override
	public String getName() {
		String result = super.getName();
		if (this.openApi3 != null) {
			result = openApi3.getInfo().getTitle();
		} else if (this.response != null) {
			result = this.response.getName();
		} else if (!this.openApi3Collection.isEmpty()) {
            if (this.openApi3Collection.size() == 1) {
				result = this.openApi3Collection.iterator().next().getInfo().getTitle();
			} else {
				StringBuilder names = new StringBuilder();
				this.openApi3Collection.stream()
				    .forEach(openApi3 -> {
				    	if (names.length() > 0) names.append(", ");
				    	names.append(openApi3.getInfo().getTitle());
				    });
				result = names.toString();
			}
		} else if (this.schema != null) {
			result = getNameOfLeadingElement(this.schema);
		} else if (this.parameter != null) {
			result = this.parameter.getName();
		} else if (this.operation != null) {
			result = this.operation.getOperationId() != null && this.operation.getOperationId().length() > 0 ?
					this.operation.getOperationId() : "implicit-operation-id";
		} else if (this.mediaType != null) {
			result = GappOpenApi3ModelElementWrapper.getNameOfResponse(this.mediaType);
			if (result == null) {
				// TODO In this case, the mediaType is for a request body.
				Overlay<?> overlay = Overlay.of(mediaType);
				JsonOverlay<?> parentOverlay = overlay.getParent();
			    if (parentOverlay instanceof RequestBodyImpl) {
					RequestBodyImpl requestBodyImpl = (RequestBodyImpl) parentOverlay;
					result = requestBodyImpl.getName();
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @param schema
	 * @return
	 */
	public static String getNameOfLeadingElement(Schema schema) {
		String result = schema.getName();
		JsonOverlay<?> currentJsonOverlay = (JsonOverlay<?>) schema;
		while (result == null && currentJsonOverlay != null) {
			if (Schema.class.isAssignableFrom(currentJsonOverlay.getClass())) {
				result = ((Schema)currentJsonOverlay).getName();
			}
			
			Overlay<?> overlay = Overlay.of(currentJsonOverlay);
			currentJsonOverlay = overlay.getParent();
		};
		
		return result;
	}
	
	/**
	 * @param mediaType
	 * @return
	 */
	public static String getNameOfResponse(MediaType mediaType) {
		String result = null;
		Overlay<?> overlay = Overlay.of(mediaType);
		JsonOverlay<?> parentOverlay = overlay.getParent();
		overlay = Overlay.of(parentOverlay);
		parentOverlay = overlay.getParent();
		if (parentOverlay instanceof ResponseImpl) {
			ResponseImpl responseImpl = (ResponseImpl)parentOverlay;
			result = responseImpl.getName();
		}
		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getType()
	 */
	@Override
	public String getType() {
		String result = super.getType();
		
		if (this.openApi3 != null) {
			result = "OpenAPI3 model";
		} else if (this.response != null) {
			result = "Response";
		} else if (!this.openApi3Collection.isEmpty()) {
			result = "Collection of OpenAPI3 models";
		} else if (this.schema != null) {
			result ="OpenAPI Schema";
		} else if (this.parameter != null) {
			result ="Parameter";
		} else if (this.operation != null) {
			result ="Operation";
		} else if (this.mediaType != null) {
			result ="Media Type";
		}
		
		return result;
	}

	/**
	 * @return the openApi3Collection
	 */
	public Collection<OpenApi3> getOpenApi3Collection() {
		return openApi3Collection;
	}

	/**
	 * @return the openApi3
	 */
	public OpenApi3 getOpenApi3() {
		return openApi3;
	}

	/**
	 * @return the response
	 */
	public Response getResponse() {
		return response;
	}

	/**
	 * @return the schema
	 */
	public Schema getSchema() {
		return schema;
	}

	/**
	 * @return the parameter
	 */
	public Parameter getParameter() {
		return parameter;
	}

	/**
	 * @return the operation
	 */
	public Operation getOperation() {
		return operation;
	}

	/**
	 * @return the mediaType
	 */
	public MediaType getMediaType() {
		return mediaType;
	}
	
	@Override
	public String getLink() {
		String result = null;
		
		if (this.openApi3 != null) {
			result = getBeautifiedJsonReference(this.openApi3);
		} else if (this.response != null) {
			result = getBeautifiedJsonReference(this.response);
		} else if (!this.openApi3Collection.isEmpty()) {
			result = this.openApi3Collection.stream()
					.map(o -> getBeautifiedJsonReference(o))
					.collect(Collectors.joining(", "));
		} else if (this.schema != null) {
			result = getBeautifiedJsonReference(this.schema);
		} else if (this.parameter != null) {
			result = getBeautifiedJsonReference(this.parameter);
		} else if (this.operation != null) {
			result = getBeautifiedJsonReference(this.operation);
		} else if (this.mediaType != null) {
			result = getBeautifiedJsonReference(this.mediaType);
		}
		
		return result;
	}
	
	private String getBeautifiedJsonReference(IJsonOverlay<?> overlay) {
		String reference = Overlay.of(overlay).getJsonReference();
		int hashSplit = reference.indexOf("#");
		String link = Paths.get(hashSplit < 0 ? reference : reference.substring(0, hashSplit)).getFileName().toString();
		if (hashSplit != -1) {
			link += "#" + reference.substring(hashSplit + 1);
		}
		return link;
	}
}
