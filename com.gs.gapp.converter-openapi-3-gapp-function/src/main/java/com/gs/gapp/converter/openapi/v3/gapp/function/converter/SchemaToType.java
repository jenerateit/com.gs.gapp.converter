package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.NameableI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.reprezen.jsonoverlay.JsonOverlay;
import com.reprezen.jsonoverlay.MapOverlay;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.jsonoverlay.Reference;
import com.reprezen.kaizen.oasparser.model3.Schema;

public class SchemaToType<S extends Schema, T extends Type> extends AbstractOpenApi3ToFunction<S, T> {

	/**
	 * @author mmt
	 *
	 */
	public static enum OpenApiDataFormat {
		INT_32 ("int32"),
		INT_64 ("int64"),
		FLOAT ("float"),
		DOUBLE ("double"),
		BYTE ("byte"),
		BINARY ("binary"),
		DATE ("date"),
		DATE_TIME ("date-time"),
		PASSWORD ("password"),
		UUID ("uuid"),
		;
		
		private static final Map<String, OpenApiDataFormat> stringToEnumCaseInsensitive =
				new HashMap<>();

		static {
			for (OpenApiDataFormat enumEntry : values()) {
				stringToEnumCaseInsensitive.put(enumEntry.getName().toLowerCase(), enumEntry);
			}
		}
		
		private final String name;
		
		private OpenApiDataFormat(String name) {
			this.name= name;
		}

		public String getName() {
			return name;
		}
		
		public static OpenApiDataFormat get(String name) {
			return name == null ? null : stringToEnumCaseInsensitive.get(name.toLowerCase());
		}
	}

	
	public static enum TYPE {
		STRING("string", EnumSet.of(OpenApiDataFormat.BYTE,
				                    OpenApiDataFormat.BINARY,
				                    OpenApiDataFormat.DATE,
				                    OpenApiDataFormat.DATE_TIME,
				                    OpenApiDataFormat.PASSWORD,
				                    OpenApiDataFormat.UUID)),
		NUMBER("number", EnumSet.of(OpenApiDataFormat.FLOAT,
                                    OpenApiDataFormat.DOUBLE)),
		INTEGER("integer", EnumSet.of(OpenApiDataFormat.INT_32,
                                      OpenApiDataFormat.INT_64)),
		BOOLEAN("boolean"),
		ARRAY("array"),
		OBJECT("object"),
		;
		
		public static TYPE getType(String name) {
			return Arrays.stream(TYPE.values())
					.filter(e -> e.getName().equals(name))
					.findAny()
					.orElse(TYPE.OBJECT); //-- this is strange
//					.orElseThrow(() -> new ModelConverterException("unknown type for a schema: " + name));
		}
		
		private final String name;
		
		private final EnumSet<OpenApiDataFormat> predefinedFormats;

		private TYPE(String name) {
			this.name = name;
			this.predefinedFormats = null;
		}
		
		private TYPE(String name, EnumSet<OpenApiDataFormat> predefinedFormats) {
			this.name = name;
			this.predefinedFormats = predefinedFormats;
		}
		
		public String getName() {
			return name;
		}
		
		public boolean isPredefindFormatSupported(OpenApiDataFormat format) {
			if (format == null) {
				return false;
			}
			return this.predefinedFormats != null && this.predefinedFormats.contains(format);
		}
	}
	
	private static final class PropertyHolder implements NameableI {
		private final String name;
		private final Schema schema;
		private final Schema parentSchema;
		private final Type previousResultingModelElement;
		
		public PropertyHolder(String name, Schema schema, Schema parentSchema, Type previousResultingModelElement) {
			this.name = name;
			this.schema = schema;
			this.parentSchema = parentSchema;
			this.previousResultingModelElement =previousResultingModelElement;
		}
		
		@Override
		public String getName() {
			return name;
		}
		
		public Schema getSchema() {
			return schema;
		}
		
		public Schema getParentSchema() {
			return parentSchema;
		}
		
		public Type getPreviousResultingModelElement() {
			return previousResultingModelElement;
		}
	}
	
	public SchemaToType(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected void onConvert(S schema, T type) {
		type.setBody(schema.getDescription());
		if (Entity.class.isInstance(type) && schema.getProperties() != null) {
			
			// --- first check resolvability of properties with referenced schemas
			final Overlay<Map<String, Schema>> propertiesOverlay = Overlay.of(schema.getProperties());
			schema.getProperties().entrySet().stream()
				.forEach(entry -> {
					if (propertiesOverlay.isReference(entry.getKey())) {
						Reference propertyReference = propertiesOverlay.getReference(entry.getKey());
						if (propertyReference != null) {
							addErrorIfNeeded(propertyReference.getInvalidReason());
						}
					}
					
					Schema itemsSchema = entry.getValue().getItemsSchema();
					Overlay<Schema> schemaOverlay = Overlay.of(itemsSchema);
					Reference creatingRef = schemaOverlay.getOverlay()._getCreatingRef();
					if (creatingRef != null) {
		    			addErrorIfNeeded(creatingRef.getInvalidReason());
		    		}
				});
			
			Entity entity = Entity.class.cast(type);
			List<PropertyHolder> combinedProperties = schema.getProperties().entrySet().stream()
					.map(entry -> new PropertyHolder(entry.getKey(), entry.getValue(), schema, type))
					.collect(Collectors.toCollection(ArrayList::new));
			
			if (schema.getAllOfSchemas() != null && !schema.getAllOfSchemas().isEmpty()) {
				Overlay<List<Schema>> overlaysForListOfSchemas = Overlay.of(schema.getAllOfSchemas());
				for (int ii=0; ii<schema.getAllOfSchemas().size(); ii++) {
					if (overlaysForListOfSchemas.isReference(ii)) {
						Reference anAllOfSchemaReference = overlaysForListOfSchemas.getReference(ii);
						if (anAllOfSchemaReference != null) {
							addErrorIfNeeded(anAllOfSchemaReference.getInvalidReason());
						}
					}
				}
				
				
				/* if there is only one referenced allOf property we can set this as a parent and the others as property */
				if (schema.getAllOfSchemas().stream().filter(s -> isReferenced(s)).count() == 1) {
					for (Schema allOfSchema : schema.getAllOfSchemas()) {
						if (isReferenced(allOfSchema)) {
							ConvertedSchema convertedSchema = convertSchema(allOfSchema, type);
							Entity originatingElement = convertedSchema.getType().getOriginatingElement(Entity.class);
							entity.setParent(originatingElement);
						} else {
							allOfSchema.getProperties().entrySet().stream()
									.map(entry -> new PropertyHolder(entry.getKey(), entry.getValue(), allOfSchema, type))
									.forEach(p -> combinedProperties.add(p));
						}
					}
				} else { /* we need to expand the allOf properties */
					addMessage(OpenApi3Messages.WARNING_ALL_OF_EXPAND.getMessageBuilder()
						.parameters(type.getName())
						.build());
					for (Schema allOfSchema : schema.getAllOfSchemas()) {
						Type previousResultingType = isReferenced(allOfSchema) ? convertSchema(allOfSchema, type).getType() : type;
						allOfSchema.getProperties().entrySet().stream()
								.map(entry -> new PropertyHolder(entry.getKey(), entry.getValue(), allOfSchema, previousResultingType))
								.forEach(p -> combinedProperties.add(p));
					}
				}
			}
			
			for (PropertyHolder propertyHolder : combinedProperties) {
				ConvertedSchema convertedSchema = convertSchema(propertyHolder.getSchema(), propertyHolder.getPreviousResultingModelElement());
				String fieldName = propertyHolder.getName().replaceAll("@", "");  // AJAX-186 (mmt 04-Mar-2021)
				
				String normalizedFieldName = normalizeString(fieldName, true);
				
				// --- AJAX-238, check for duplicate property names. Note that the do-while-loop will always break since the counter can get very high.
				// --- The loop could only run forever when there were an infinite inheritance tree.
				EntityField potentialDuplicateField = null;
				int counter = 1;
				String fieldNameForDuplicateCheck = normalizedFieldName;
				do {
				    potentialDuplicateField = entity.getFieldInParents(fieldNameForDuplicateCheck);
				    if (potentialDuplicateField != null) {
				    	if (counter == 1) {
					    	fieldNameForDuplicateCheck = StringTools.firstLowerCase(entity.getName()) + StringTools.firstUpperCase(normalizedFieldName);
					    } else {
					    	fieldNameForDuplicateCheck = normalizedFieldName + counter;
					    }
				    	counter++;
				    } else {
				    	// Here, we finally set the entity field's name.
				    	normalizedFieldName = fieldNameForDuplicateCheck;				    	
				    }
				} while (potentialDuplicateField != null);
					
				// --- Now, everything is set to create an entity field that is related to the modeled property.
				EntityField entityField = new EntityField(normalizedFieldName, entity);
				entityField.setOriginatingElement(propertyHolder);
				entityField.setCollectionType(convertedSchema.getCollectionType());
				entityField.setDimension(convertedSchema.getDimension());
				entityField.setType(convertedSchema.getType());
				entityField.setKeyType(convertedSchema.getKeyType());
				entityField.setBody(propertyHolder.getSchema().getDescription());
				entityField.setModule(entity.getModule());
				if (convertedSchema.getOption() != null) {
					entityField.addOptions(convertedSchema.getOption());
				}
				if (convertedSchema.getExpandable() != null && convertedSchema.getExpandable()) {
					entityField.setExpandable(true);
				}
				if (propertyHolder.getParentSchema().getRequiredFields().contains(propertyHolder.getName())) {
					entityField.setNullable(false);
				}
				if (convertedSchema.getAdditionalModelInfo().length() > 0) {
					entityField.addToBody(convertedSchema.getAdditionalModelInfo().toString());
				}
			}
		}
		
		if (type instanceof ComplexType) {
			ComplexType complexType = (ComplexType) type;
			if (complexType.getFields().isEmpty() &&
					// toString() outputs a JSON string, but _not_ for response object schemas
					Overlay.of(schema).getOverlay().toString().length() > 0) {
				addWarning(OpenApi3Messages.WARNING_TYPE_HAS_NO_FILEDS.getMessageBuilder()
						.parameters(schema.toString(), complexType.getName())
					    .build()
					    .getMessage());
			}
		}
	}
	

	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S schema, ModelElementI previousResultingModelElement) {
		Module module = null;
		FunctionParameter functionParameter = null;
		PersistenceModule persistenceModule = null;
		String name = StringTools.firstUpperCase(schema.getName());
		if (FunctionParameter.class.isInstance(previousResultingModelElement)) {
			functionParameter = FunctionParameter.class.cast(previousResultingModelElement);
			if (name == null || name.isEmpty()) {
				name = StringTools.firstUpperCase(functionParameter.getName()) + "Type" + StringTools.firstUpperCase(getFunction(functionParameter).getName());
			}
			module = functionParameter.getModule();
		} else if (ExceptionType.class.isInstance(previousResultingModelElement)) {
			ExceptionType exceptionType = ExceptionType.class.cast(previousResultingModelElement);
			name = exceptionType.getName() + "ResponseType";
			module = exceptionType.getModule();
		} else if (Type.class.isInstance(previousResultingModelElement)) {
			Type type = Type.class.cast(previousResultingModelElement);
			// --  We want a more expressive name, this is defined in an other schema so combine the name from the field and the outer type
			// -- The check if the name is not already set is not needed. The previousResultingModelElemnt will always be the persistence model if it is referenced 
			Overlay<Schema> schemaOverlay = Overlay.of(schema);
			if (Schema.class.isInstance(schemaOverlay.getParent())) { // -- this is an array
				name = Overlay.of(schemaOverlay.getParent()).getPathInParent();
			} else if (MapOverlay.class.isInstance(schemaOverlay.getParent())) {
				name = schemaOverlay.getPathInParent();
			} else {
    			if (schemaOverlay.isReference("items")) {
    				// TODO The following check doesn't work. Most probably it is a bug or missing feature in the Kaizen parser. (mmt 25-Jul-2022)
    				Reference itemsReference = schemaOverlay.getReference("items");
    				if (itemsReference != null) {
    					addErrorIfNeeded(itemsReference.getInvalidReason());
					}
    			} else {
					Message errorMessage = OpenApi3Messages.ERROR_UNHANDLED_SCHEMA_TYPE.getMessageBuilder()
							.parameters(schemaOverlay)
							.build();
					throw new ModelConverterException(errorMessage.getMessage());
    			}
			}
			name = type.getName() + StringTools.firstUpperCase(name) + "Type";
			module = type.getModule();
		} else if (PersistenceModule.class.isInstance(previousResultingModelElement)) {
			if (name == null || name.isEmpty()) {
				// -- This is possible if we have additional properties and an inline definition of the additional properties
				// -- This is also possible if the schema is referenced in an other file -> schema.getName() returns null
				Overlay<Schema> schemaOverlay = Overlay.of(schema);
				if (Schema.class.isInstance(schemaOverlay.getParent())) { // -- this is a map
					name = Overlay.of(schemaOverlay.getParent()).getPathInParent();
					name = StringTools.firstUpperCase(name) + "Value";
				} else if (schemaOverlay.getJsonReference().contains("#/components/schemas/")) { // -- this is a schema from an other file
					String ref = schemaOverlay.getJsonReference();
					name = ref.substring(ref.indexOf("#/components/schemas/") + 21);
					name = StringTools.firstUpperCase(name);
				} else {
					Message errorMessage = OpenApi3Messages.ERROR_UNHANDLED_SCHEMA_TYPE.getMessageBuilder()
							.parameters(Overlay.of(schema))
							.build();
					throw new ModelConverterException(errorMessage.getMessage());
				}
			}
			persistenceModule = PersistenceModule.class.cast(previousResultingModelElement);
			module = persistenceModule;
		} else {
			Message errorMessage = OpenApi3Messages.ERROR_UNEXPECTED_PREVIOUS_MODEL_ELEMENT.getMessageBuilder()
					.parameters(previousResultingModelElement.getClass().getName(),
							"schema: " + Overlay.of(schema).toString())
					.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		
		// for types and formats, see here: https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.0.3.md#data-types
		
		T type = null;
		switch (TYPE.getType(schema.getType())) {
		case STRING:
			if (!schema.getEnums().isEmpty()) {
				Enumeration enumeration = createEnumeration(schema, name, functionParameter, persistenceModule);
				module = getBasicModule(schema);
				type = (T) enumeration;
			} else if (schema.getFormat() != null) {
				switch (schema.getFormat()) {
				case "date":
					type = (T) PrimitiveTypeEnum.DATE.getPrimitiveType();
					break;
				case "date-time":
					type = (T) PrimitiveTypeEnum.DATETIME.getPrimitiveType();
					break;
				case "byte":
				case "binary":
					/* in this case the field or parameter need to set collection type to array */
					type = (T) PrimitiveTypeEnum.SINT8.getPrimitiveType();
					break;
				case "uuid":  // AJAX-174
					type = (T) PrimitiveTypeEnum.UUID.getPrimitiveType();
					break;
				default:
					addMessage(OpenApi3Messages.INFO_UNKNOWN_STRING_FORMAT.getMessageBuilder()
							.parameters(schema.getFormat(), Overlay.of(schema).toString()).build());
					type = (T) PrimitiveTypeEnum.STRING.getPrimitiveType();
					break;
				}
				
			} else {
				type = (T) PrimitiveTypeEnum.STRING.getPrimitiveType();
			}
			break;
		case NUMBER:
			if (schema.getFormat() != null) {
				switch (schema.getFormat()) {
				case "float":
					type = (T) PrimitiveTypeEnum.FLOAT32.getPrimitiveType();
					break;
				case "double":
					type = (T) PrimitiveTypeEnum.FLOAT64.getPrimitiveType();
					break;
				default:
					addMessage(OpenApi3Messages.INFO_UNKNOWN_NUMBER_FORMAT.getMessageBuilder()
						    .parameters(schema.getFormat(), Overlay.of(schema).toString())
						    .build());
					type = (T) PrimitiveTypeEnum.FLOAT64.getPrimitiveType();
				}
			} else { // -- guess something without format
			    type = (T) PrimitiveTypeEnum.FLOAT64.getPrimitiveType();
			}
			break;
		case INTEGER:
			if (schema.getFormat() != null) {
				switch (schema.getFormat()) {
				case "int32":
					type = (T) PrimitiveTypeEnum.SINT32.getPrimitiveType();
					break;
				case "int64":
					type = (T) PrimitiveTypeEnum.SINT64.getPrimitiveType();
					break;
				default:
					addMessage(OpenApi3Messages.INFO_UNKNOWN_INTEGER_FORMAT.getMessageBuilder()
						    .parameters(schema.getFormat(), Overlay.of(schema).toString())
						    .build());
					type = (T) PrimitiveTypeEnum.SINT32.getPrimitiveType();
				}
			} else { // -- guess something without format
				if (TYPE.getType(schema.getType()) == TYPE.INTEGER) {
					type = (T) PrimitiveTypeEnum.SINT32.getPrimitiveType();
				} else {
					type = (T) PrimitiveTypeEnum.FLOAT64.getPrimitiveType();
				}
			}
			break;
		case BOOLEAN:
			type = (T) PrimitiveTypeEnum.UINT1.getPrimitiveType();
			break;
		case ARRAY:
			Message errorMessageArrayNotSupported = OpenApi3Messages.ERROR_SCHEMA_WITH_ARRAY_TYPE_NOT_SUPPORTED.getMessageBuilder()
				.parameters(Overlay.of(schema).toString())
				.build();
			throw new ModelConverterException(errorMessageArrayNotSupported.getMessage());
		case OBJECT:
			if (!schema.getEnums().isEmpty()) {
				Enumeration enumeration = createEnumeration(schema, name, functionParameter, persistenceModule);
				module = getBasicModule(schema);
				type = (T) enumeration;
			} else {
				if (persistenceModule != null) {
					// AJAX-186
					Entity leadingEntity = getLeadingEntity(persistenceModule, schema);
					if (leadingEntity != null) {
						String moreQualifiedName = leadingEntity.getName() + StringTools.firstUpperCase(name);
						
						Set<ModelElementI> existingModelElements = getModelConverter().getModelElementCache().findModelElements(moreQualifiedName, ComplexType.class);
						if (!existingModelElements.isEmpty()) {
							// make the name being even more qualified
							String nameOfLeadingElement = GappOpenApi3ModelElementWrapper.getNameOfLeadingElement(schema);
							if (nameOfLeadingElement != null && nameOfLeadingElement.length() > 0) {
								moreQualifiedName = leadingEntity.getName() + StringTools.firstUpperCase(nameOfLeadingElement) + StringTools.firstUpperCase(name);
							}
						}
						// finally, we are setting the variable for further use
						name = moreQualifiedName;
					}
				} else if (functionParameter != null) {
					// Even in this case we need to check whether the name is unique and produce more qualified names.
					Set<ModelElementI> existingModelElements = getModelConverter().getModelElementCache().findModelElements(name, ComplexType.class);
					if (!existingModelElements.isEmpty()) {
						GappOpenApi3ModelElementWrapper wrapper = functionParameter.getOriginatingElement(GappOpenApi3ModelElementWrapper.class);
						if (wrapper != null && wrapper.getMediaType() != null) {
							String responseName = GappOpenApi3ModelElementWrapper.getNameOfResponse(wrapper.getMediaType());
							if (responseName != null && responseName.length() > 0) {
								name = name + responseName;
							}
						}
					}
				}
				
				Entity entity = new Entity(name);
				type = (T) entity;
				if (FunctionModule.class.isInstance(module)) {
					module = getPersistenceModule(schema); // convertWithOtherConverter(PersistenceModule.class, Overlay.of(schema).getModel());
				}
			}
			break;

		default:
			Message errorMessage = OpenApi3Messages.ERROR_UNHANDLED_SCHEMA_TYPE.getMessageBuilder()
			    .parameters(schema.getType())
			    .build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		
		if (module != null) {
		    type.setModule(module);
		}
		if (type.getOriginatingElement() == null) {
			GappOpenApi3ModelElementWrapper wrapper = new GappOpenApi3ModelElementWrapper(schema);
		    type.setOriginatingElement(wrapper);
		}
		
		// --- We are going to add the created type here in order to be able to find out, whether two types with duplicate names would be created. (mmt 07-Mar-2021)
		if (type.isGenerated() && type instanceof ComplexType) {
		    addModelElement(type);
		}
		
		return type;
	}

	protected Enumeration createEnumeration(S schema, String name, FunctionParameter functionParameter,
			PersistenceModule persistenceModule) {
		if (name.endsWith("Type")) {
			// Note that if the given name is just "Type", then the resulting enum's name would simply be "Enum", which is not sufficient. (mmt 03-Mar-2021)
			if ("type".equalsIgnoreCase(name)) {
				name = name + "Enum";
			} else {
			    name = name.substring(0, name.lastIndexOf("Type")) + "Enum";
			}
		}
		
		if (persistenceModule != null) {
			// AJAX-186
			Entity leadingEntity = getLeadingEntity(persistenceModule, schema);
			if (leadingEntity != null) {
				String moreQualifiedName = leadingEntity.getName() + StringTools.firstUpperCase(name);
				
				Set<ModelElementI> existingModelElements = getModelConverter().getModelElementCache().findModelElements(moreQualifiedName, ComplexType.class);
				if (!existingModelElements.isEmpty()) {
					// make the name being even more qualified
					String nameOfLeadingElement = GappOpenApi3ModelElementWrapper.getNameOfLeadingElement(schema);
					if (nameOfLeadingElement != null && nameOfLeadingElement.length() > 0) {
						moreQualifiedName = leadingEntity.getName() + StringTools.firstUpperCase(nameOfLeadingElement) + StringTools.firstUpperCase(name);
					}
				}
				// finally, we are setting the variable for further use
				name = moreQualifiedName;
			}
		} else if (functionParameter != null) {
			// Even in this case we need to check whether the name is unique and produce more qualified names.
			Set<ModelElementI> existingModelElements = getModelConverter().getModelElementCache().findModelElements(name, ComplexType.class);
			if (!existingModelElements.isEmpty()) {
				GappOpenApi3ModelElementWrapper wrapper = functionParameter.getOriginatingElement(GappOpenApi3ModelElementWrapper.class);
				if (wrapper != null && wrapper.getMediaType() != null) {
					String responseName = GappOpenApi3ModelElementWrapper.getNameOfResponse(wrapper.getMediaType());
					if (responseName != null && responseName.length() > 0) {
						name = name + responseName;
					}
				}
			}
		}
		
		Enumeration enumeration = new Enumeration(name);
		for (Object e : schema.getEnums()) {
			if (e == null || e.toString().isEmpty()) {
				addWarning(OpenApi3Messages.WARNING_EMPTY_VALUE_FOUND_IN_ENUM.getMessageBuilder()
					.parameters(enumeration.getName())
					.build().getMessage());
				continue;
			}
			EnumerationEntry enumerationEntry = new EnumerationEntry(e.toString());
			enumeration.addEnumerationEntry(enumerationEntry);
		}
		// --- The field name "enumEntryValue" represents a convention here. In other converters
		// --- there can be checks for exactly this name in order to come up with that value being set
		// --- in enum constructors (e.g. for Java). For programming languages with simpler enums,
		// --- this naming convention is ignored. (mmt 05-Mar-2021)
		// TODO Document this naming convention in generator documentation.
		Field enumField = new Field(Enumeration.FIELD_NAME_FOR_ENUM_ENTRY_VALUE, enumeration);
		enumField.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		return enumeration;
	}
	
	/**
	 * This method identifies an enclosing schema that got converted to an entity.
	 * The identified entity then can be used to become the owner of another type that got
	 * created for schema that is passed into the method.
	 * 
	 * This method got added to solve the issue AJAX-186.
	 * 
	 * @param persistenceModule
	 * @param schema
	 * @return
	 */
	private Entity getLeadingEntity(final PersistenceModule persistenceModule, final Schema schema) {
		if (persistenceModule == null) return null;
		
		Optional<Entity> identifiedEntity = persistenceModule.getEntities().stream().filter(entity -> {
				Entity result = null;	
				JsonOverlay<?> currentJsonOverlay = (JsonOverlay<?>) schema;
				do {
					Schema originatingSchema = entity.getOriginatingElement(Schema.class);
					if (originatingSchema == null) {
						GappOpenApi3ModelElementWrapper wrapper = entity.getOriginatingElement(GappOpenApi3ModelElementWrapper.class);
						if (wrapper != null) {
							originatingSchema = wrapper.getSchema();
						}
					}
					
					if (originatingSchema == currentJsonOverlay) {
						result = entity;
						break;
					}
					
					Overlay<?> overlay = Overlay.of(currentJsonOverlay);
					currentJsonOverlay = overlay.getParent();
				} while (entity == null || currentJsonOverlay != null);
				
				return result != null;
		    }).findFirst();
		
		return identifiedEntity.isPresent() ? identifiedEntity.get() : null;
	}
	
	public static boolean isBinary(Schema schema) {
		if (TYPE.getType(schema.getType()) == TYPE.STRING && schema.getFormat() != null) {
			if (schema.getFormat().equals("binary") || schema.getFormat().equals("byte")) {
				return true;
			}
		}
		return false;
	}

}
