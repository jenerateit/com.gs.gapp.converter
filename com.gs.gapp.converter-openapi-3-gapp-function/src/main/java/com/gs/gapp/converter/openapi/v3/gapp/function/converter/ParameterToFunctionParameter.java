package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.Arrays;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.reprezen.kaizen.oasparser.model3.Parameter;

public class ParameterToFunctionParameter<S extends Parameter, T extends FunctionParameter> extends AbstractOpenApi3ToFunction<S, T> {

	private static enum IN {
		PATH("path"),
		QUERY("query"),
		HEADER("header"),
		COOKIE("cookie"),
		;
		
		public static IN getIn(String name) {
			return Arrays.stream(IN.values())
					.filter(e -> e.getName().equals(name))
					.findAny()
					.orElseThrow(() -> new ModelConverterException("unknown in parameter: " + name));
		}
		
		private String name;

		private IN(String name) {
			this.name = name;
		}
		
		public String getName() {
			return name;
		}
	}
	
	public ParameterToFunctionParameter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S parameter, T functionParameter) {
		OptionDefinition<String>.OptionValue paramTypeOptionValue = null;
		switch (IN.getIn(parameter.getIn())) {
		case PATH:
			paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.PATH.getName());
			break;
		case QUERY:
			paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName());
			break;
		case HEADER:
			paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.HEADER.getName());
			break;
		case COOKIE:
			paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.COOKIE.getName());
			break;
		default:
			throw new ModelConverterException("unhandeled in parameter: " + parameter.getIn());
		}
    	functionParameter.addOptions(paramTypeOptionValue);

    	ConvertedSchema convertedSchema = convertSchema(parameter.getSchema(), functionParameter);
    	functionParameter.setCollectionType(convertedSchema.getCollectionType());
		functionParameter.setDimension(convertedSchema.getDimension());
		functionParameter.setKeyType(convertedSchema.getKeyType());
    	functionParameter.setType(convertedSchema.getType());
    	if (convertedSchema.getOption() != null) {
    		functionParameter.addOptions(convertedSchema.getOption());
		}
		functionParameter.setParameterType(ParameterType.IN);
		if (convertedSchema.getAdditionalModelInfo().length() > 0) {
			functionParameter.addToBody(convertedSchema.getAdditionalModelInfo().toString());
		}
		
	}
	

	@Override
	protected T onCreateModelElement(S parameter, ModelElementI previousResultingModelElement) {
		if (!Function.class.isInstance(previousResultingModelElement)) {
			throw new ModelConverterException("unexpected previous model element: " + previousResultingModelElement.getClass().getName());
		}
		Function function = Function.class.cast(previousResultingModelElement);


		@SuppressWarnings("unchecked")
		T functionParameter = (T) new FunctionParameter(parameter.getName());
		GappOpenApi3ModelElementWrapper wrapper = new GappOpenApi3ModelElementWrapper(parameter);
		functionParameter.setOriginatingElement(wrapper);
		functionParameter.setModule(function.getModule());
		
		convertExtensions(parameter, functionParameter);

		return functionParameter;
	}
	
	

}
