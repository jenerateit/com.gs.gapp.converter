package com.gs.gapp.converter.openapi.v3.gapp.function;

import java.io.Serializable;
import java.util.Arrays;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverterOptions;

public class OpenApi3ToFunctionConverterOptions extends PersistenceToBasicConverterOptions  {
	
	public enum SplitResource {
		NONE,
		PATH,
		TAG,
	}
	
	public static final String OPTION_NAMESPACE_DEPRECATED = "namespace";
	public static final String OPTION_NAMESPACE = "openapi.namespace";
	public static final String OPTION_SERVICENAME = "service-name";
	public static final String JAVA_PRIMITIVE_WRAPPER = "Java-UsePrimitiveWrapper";
	public static final String SPLIT_RESOURCES = "split-resources";
	public static final String EXPANDABLE = "expandable";
	public static final String OPTION_IGNORE_TITLE_FOR_NAMESPACE = "openapi.ignore-title-for-namespace";
	
	
	public OpenApi3ToFunctionConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public String getNamespace() {
		Serializable postfix = getOptions().get(OPTION_NAMESPACE);
		if (postfix == null) {
			postfix = getOptions().get(OPTION_NAMESPACE_DEPRECATED);
		}
		String result = (postfix == null || postfix.toString().length() == 0 ? "com.gs.vd.api" : postfix.toString().toLowerCase());
		return result;
	}
	
	public String getServiceName() {
		Serializable postfix = getOptions().get(OPTION_SERVICENAME);
		String result = (postfix == null || postfix.toString().length() == 0 ? "Service" : postfix.toString());
		return result;
	}

	public boolean isJavaUsePrimitiveWrapper() {
		String listBeanGenerationSupported = (String) getOptions().get(JAVA_PRIMITIVE_WRAPPER);
		validateBooleanOption(listBeanGenerationSupported, JAVA_PRIMITIVE_WRAPPER);
		return (listBeanGenerationSupported != null) && listBeanGenerationSupported.equalsIgnoreCase("true");
	}
	
	public SplitResource getSplitResources() {
		String splitResources = (String) getOptions().get(SPLIT_RESOURCES);
		
		if (splitResources != null) {
			// -- for backwards compatibility
			if (splitResources.equalsIgnoreCase("true")) {
				return SplitResource.PATH;
			} else if (splitResources.equalsIgnoreCase("false")) {
				return SplitResource.NONE;
			}
			
			if (splitResources.equalsIgnoreCase(SplitResource.PATH.name())) {
				return SplitResource.PATH;
			}
			if (splitResources.equalsIgnoreCase(SplitResource.TAG.name())) {
				return SplitResource.TAG;
			}
			
			throwIllegalEnumEntryException(splitResources, SPLIT_RESOURCES, Arrays.asList(new String[] {"true", "false", SplitResource.PATH.name(), SplitResource.TAG.name()}));
		}
		return SplitResource.NONE;
	}
	
	public boolean isExpandable() {
		String splitResources = (String) getOptions().get(EXPANDABLE);
		validateBooleanOption(splitResources, EXPANDABLE);
		return (splitResources != null) && splitResources.equalsIgnoreCase("true");
	}
	
	/**
	 * If this method returns false, the OpenAPI document's title in its info section is going to be appended to the namespace that is provided through a generator option.
	 * 
	 * @return
	 */
	public boolean isIgnoreTitleForNamespace() {
		String ignoreTitleForNamespace = (String) getOptions().get(OPTION_IGNORE_TITLE_FOR_NAMESPACE);
		validateBooleanOption(ignoreTitleForNamespace, OPTION_IGNORE_TITLE_FOR_NAMESPACE);
		return (ignoreTitleForNamespace != null) && ignoreTitleForNamespace.equalsIgnoreCase("true");
	}
}
