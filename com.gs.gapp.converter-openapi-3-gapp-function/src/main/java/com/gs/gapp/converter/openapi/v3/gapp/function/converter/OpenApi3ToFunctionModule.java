package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.Map.Entry;

import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.reprezen.kaizen.oasparser.model3.Info;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;
import com.reprezen.kaizen.oasparser.model3.Operation;
import com.reprezen.kaizen.oasparser.model3.Path;
import com.reprezen.kaizen.oasparser.model3.Schema;

public class OpenApi3ToFunctionModule<S extends OpenApi3, T extends FunctionModule> extends AbstractOpenApi3ToFunction<S, T> {

	public OpenApi3ToFunctionModule(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}
	
	@Override
	protected void onConvert(S openApi3, T functionModule) {
		Info info = openApi3.getInfo();
		functionModule.setBody(getBody(
				new Text(info.getTitle()),
				new Text("Version", info.getVersion()),
				new Text(info.getDescription())));
		
		// AJAX-253 commented this (mmt 20-Jul-2023)
//		functionModule.addOptions(RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(getServerPath(openApi3.getServers())));
		addServerUrlsToDocumentation(functionModule, openApi3);
				
		for (Entry<String, Path> entry : openApi3.getPaths().entrySet()) {
			String pathValue = entry.getKey();
			Path path = entry.getValue();

			OptionDefinition<String>.OptionValue pathOptionValue = RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(pathValue);
			for (Entry<String, Operation> operation : path.getOperations().entrySet()) {
				OptionDefinition<String>.OptionValue pathOptionOperation = RestOptionEnum.OPTION_DEFINITION_OPERATION. new OptionValue(operation.getKey().toUpperCase());
				Function function = convertWithOtherConverter(Function.class, operation.getValue(), functionModule);
				function.addOptions(pathOptionValue);
				function.addOptions(pathOptionOperation);
				convertExtensions(path, function);
			}
		}
		
		for (Entry<String, Schema> entry : openApi3.getSchemas().entrySet()) {
			@SuppressWarnings("unused")
			ConvertedSchema convertedSchema = convertSchema(entry.getValue(), null);
		}
	}

	@Override
	protected T onCreateModelElement(S openApi3, ModelElementI previousResultingModelElement) {
		String name = getEffectiveServiceName(openApi3);
		
		@SuppressWarnings("unchecked")
		T functionModule = (T) new FunctionModule(name);
		
		functionModule.setNamespace(getNamespace(name, com.gs.gapp.metamodel.function.Namespace.class));
		GappOpenApi3ModelElementWrapper openApi3ModelElementWrapper = new GappOpenApi3ModelElementWrapper(openApi3);
		functionModule.setOriginatingElement(openApi3ModelElementWrapper);
		
		convertExtensions(openApi3, functionModule);
		
		return functionModule;
	}

	

}
