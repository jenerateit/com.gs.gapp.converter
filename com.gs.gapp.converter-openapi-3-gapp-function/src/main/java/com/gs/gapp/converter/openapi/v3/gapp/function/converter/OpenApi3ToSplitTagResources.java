package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.kaizen.oasparser.model3.Info;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;
import com.reprezen.kaizen.oasparser.model3.Operation;
import com.reprezen.kaizen.oasparser.model3.Path;
import com.reprezen.kaizen.oasparser.model3.Schema;

public class OpenApi3ToSplitTagResources<S extends OpenApi3, T extends ModelElementWrapper> extends AbstractOpenApi3ToFunction<S, T> {

	private static final String NO_TAG = "ungrouped";
	
	private static final class TagPathOperationModelElement extends ModelElement {

		private static final long serialVersionUID = 264262532223755044L;

		public static final class Element {
			private final Path path;
			private final String verb;
			private final Operation operation;
			
			public Element(Path path, String verb, Operation operation) {
				this.path = path;
				this.verb = verb;
				this.operation = operation;
			}
			
			public Path getPath() {
				return path;
			}
			
			public String getVerb() {
				return verb;
			}
			
			public Operation getOperation() {
				return operation;
			}
			
			@Override
			public boolean equals(Object obj) {
				if (obj.getClass() != this.getClass()) {
					return false;
				}
				Element other = Element.class.cast(obj);
				if (this.path.getPathString().equals(other.path.getPathString()) && this.verb.equals(other.verb)) {
					return Overlay.of(this.operation).getJsonReference().equals(Overlay.of(other.operation).getJsonReference());
				}
				return false;
			}

			@Override
			public int hashCode() {
				return Objects.hash(this.path.getPathString(), this.verb);
			}
		}
		
		Set<Element> elements = new LinkedHashSet<>();
		
		public TagPathOperationModelElement(String name) {
			super(name);
		}

		public void add(Path path, String verb, Operation operation) {
			elements.add(new Element(path, verb, operation));
		}
		
		public Set<Element> elements() {
			return elements;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this.getClass() != obj.getClass()) {
				return false;
			}
			TagPathOperationModelElement other = TagPathOperationModelElement.class.cast(obj);
			return this.getName().equals(other.getName()) && this.elements.equals(other.elements);
		}
		
		@Override
		public int hashCode() {
			return Objects.hash(this.getName());
		}
	}
	
	public OpenApi3ToSplitTagResources(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected void onConvert(S openApi3, T wrapper) {
		Map<String, TagPathOperationModelElement> tagElements = new HashMap<>();
		for (Entry<String, Path> entry : openApi3.getPaths().entrySet()) {
			Path path = entry.getValue();
			for (Entry<String, Operation> operation : path.getOperations().entrySet()) {
				List<String> tags = operation.getValue().getTags();
				String tag;
				if (tags.isEmpty()) {
					tag = NO_TAG;
				} else {
					tag = tags.get(0);
					if (tags.size() > 1) {
						addMessage(OpenApi3Messages.INFO_MORE_THAN_ONE_TAG.getMessageBuilder()
								.parameters(entry.getKey(), operation.getKey(), tags.get(0))
								.build());
					}
				}
				TagPathOperationModelElement element = tagElements.computeIfAbsent(tag, k -> new TagPathOperationModelElement(k));
				element.add(path, operation.getKey(), operation.getValue());
			}

		}
		
		String name = getEffectiveServiceName(openApi3);
		for (TagPathOperationModelElement tagElement : tagElements.values()) {
			FunctionModule functionModule = new FunctionModule(name + StringTools.firstUpperCase(normalizeString(tagElement.getName())));

			// AJAX-188 Since tag names are unique, it is simpler and shorter to use the tag name only. It used to be the title + the tag name. (mmt 08-Mar-2021)
			functionModule.addOptions(RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(normalizeString(tagElement.getName())));
			
			Info info = openApi3.getInfo();
			functionModule.setBody(getBody(
					new Text(info.getTitle()),
					new Text("Version", info.getVersion()),
					new Text(info.getDescription()),
					new Text("Tag", tagElement.getName())));
			
			functionModule.setNamespace(getNamespace(name, com.gs.gapp.metamodel.function.Namespace.class));
			GappOpenApi3ModelElementWrapper openApi3Wrapper = new GappOpenApi3ModelElementWrapper(openApi3);
			functionModule.setOriginatingElement(openApi3Wrapper);

			convertExtensions(openApi3, functionModule);
			
			for (TagPathOperationModelElement.Element element : tagElement.elements()) {
				// AJAX-253 commented this (mmt 20-Jul-2023)
//				String serverPath = getServerPath(openApi3.getServers());
//				if (serverPath.endsWith("/")) {
//					serverPath = serverPath.substring(0, serverPath.length() - 1);
//				}
//				serverPath = serverPath + element.getPath().getPathString();
				addServerUrlsToDocumentation(functionModule, openApi3);
				
				OptionDefinition<String>.OptionValue pathOptionOperation = RestOptionEnum.OPTION_DEFINITION_OPERATION. new OptionValue(element.getVerb().toUpperCase());
				Function function = convertWithOtherConverter(Function.class, element.getOperation(), functionModule);
				function.addOptions(pathOptionOperation);
				function.addOptions(RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(element.getPath().getPathString()));
				
				convertExtensions(element.getPath(), function);
				
			}
//			functionModule.setOriginatingElement(openApi3);
//			getModel().addElement(functionModule);
			addModelElement(functionModule);
		}

		for (Entry<String, Schema> entry : openApi3.getSchemas().entrySet()) {
			@SuppressWarnings("unused")
			ConvertedSchema convertedSchema = convertSchema(entry.getValue(), null);
		}
	}

	@Override
	protected T onCreateModelElement(S openApi3, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T wrapper = (T) new GappOpenApi3ModelElementWrapper(openApi3);
		
		return wrapper;
	}

	
}
