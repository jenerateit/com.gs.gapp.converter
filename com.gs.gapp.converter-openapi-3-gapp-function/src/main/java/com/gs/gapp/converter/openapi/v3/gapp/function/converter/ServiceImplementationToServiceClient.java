package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;

public class ServiceImplementationToServiceClient<S extends ServiceImplementation, T extends ServiceClient> extends AbstractOpenApi3ToFunction<S, T> {

	public ServiceImplementationToServiceClient(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S serviceImplementation, T serviceClient) {
	}

	@Override
	protected T onCreateModelElement(S serviceImplementation, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T serviceClient = (T) new ServiceClient(serviceImplementation.getName().replace("Implementation", "Client"), serviceImplementation.getServiceInterface());
		serviceClient.addConsumedServiceImplementations(serviceImplementation);
		return serviceClient;
	}

}
