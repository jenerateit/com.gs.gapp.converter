package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;

/**
 * This converter reacts on a collection of OpenApi3 model instances.
 * 
 * <p>The OpenAPI model access takes care of such a collection being present in the input model.
 *
 * @param <S>
 * @param <T>
 */
public class CollectionToServiceInterface<S extends Collection<OpenApi3>, T extends ServiceInterface> extends AbstractOpenApi3ToFunction<S, T> {

	public CollectionToServiceInterface(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	@Override
	protected void onConvert(S model, T serviceInterface) {
		List<FunctionModule> functionModules = new ArrayList<>();
		switch (getModelConverter().getConverterOptions().getSplitResources()) {
		case NONE:
			functionModules.addAll(model.stream()
					.map(openApi3 -> convertWithOtherConverter(FunctionModule.class, openApi3))
					.collect(Collectors.toList()));
			break;
		case PATH:
			for (OpenApi3 openApi3 : model) {
				ModelElementWrapper wrapper = convertWithOtherConverter(ModelElementWrapper.class, openApi3);
				functionModules.addAll(openApi3.getPaths().values().stream()
						.map(path -> convertWithOtherConverter(FunctionModule.class, path, wrapper))
						.collect(Collectors.toList()));
			}
			break;
		case TAG:
			for (OpenApi3 openApi3 : model) {
				@SuppressWarnings("unused")
				ModelElementWrapper wrapper = convertWithOtherConverter(ModelElementWrapper.class, openApi3);
				
				functionModules.addAll(getModelConverter().getModelElementCache().getAllModelElements().stream()
						.filter(FunctionModule.class::isInstance)
						.map(FunctionModule.class::cast)
						.collect(Collectors.toList()));
			}
			break;
		default:
			break;
		
		}
		
		
		PersistenceModule[] persistenceModules = model.stream()
				.map(openApi3 -> getPersistenceModule(openApi3)) // convertWithOtherConverter(PersistenceModule.class, openApi3))
				.toArray(PersistenceModule[]::new);
		
		// --- entities are added in onPerformModelConsolidation, to be sure that all nested entities are created
//		Entity[] entities = Arrays.stream(persistenceModules)
//				.flatMap(p -> p.getEntities().stream())
//				.toArray(Entity[]::new);
		
		@SuppressWarnings("unused")
		ServiceImplementation serviceImplementation = convertWithOtherConverter(ServiceImplementation.class, serviceInterface);
		
//		serviceInterface.addEntities( entities); //entities.toArray(new Entity[0]));
		serviceInterface.addFunctionModules(functionModules.toArray(new FunctionModule[0]));
		serviceInterface.addPersistenceModules(persistenceModules);

	}

	@Override
	protected T onCreateModelElement(S collectionOfOpenApi3Models, ModelElementI previousResultingModelElement) {
		String name;
		Namespace namespace;
		if (collectionOfOpenApi3Models.size() == 1) {
			name = getEffectiveServiceName(collectionOfOpenApi3Models.iterator().next());
		} else {
			name = normalizeString(getModelConverter().getConverterOptions().getServiceName());
		}
		namespace = getNamespace(name, com.gs.gapp.metamodel.function.Namespace.class);
		
		@SuppressWarnings("unchecked")
		final T serviceInterface = (T) new ServiceInterface(name + "Interface");
		serviceInterface.setOriginatingElement(new GappOpenApi3ModelElementWrapper(collectionOfOpenApi3Models));
		Module module = new Module("ServiceModule");
		addModelElement(module);
		module.setNamespace(namespace);
		serviceInterface.setModule(module);
		
		collectionOfOpenApi3Models.forEach(openApi3 -> convertExtensions(openApi3, serviceInterface));
		
		return serviceInterface;
	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean responsible = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (responsible) {
			Collection<?> collection = Collection.class.cast(originalModelElement);
			responsible = collection.stream().filter(o -> !OpenApi3.class.isInstance(o)).count() == 0;
		}
		return responsible;
	}

}
