/**
 *
 */
package com.gs.gapp.converter.openapi.v3.gapp.function;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.openapi.v3.gapp.function.converter.CollectionToServiceInterface;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.MediaTypeToFunctionParameter;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.OpenApi3ToFunctionModule;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.OpenApi3ToSplitPathResources;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.OpenApi3ToSplitTagResources;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.OperationToFunction;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.ParameterToFunctionParameter;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.PathToFunctionModule;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.ResponseToExceptionType;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.SchemaToType;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.ServiceImplementationToServiceClient;
import com.gs.gapp.converter.openapi.v3.gapp.function.converter.ServiceInterfaceToServiceImplementation;
import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.ValidatorDuplicateFields;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.function.FunctionMetamodel;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.function.ServiceModelProcessing;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.ValidatorDuplicateEntityFields;
import com.gs.gapp.metamodel.product.ServiceApplication;
import com.gs.gapp.metamodel.product.ValidatorUniqueResourcePaths;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;


public class OpenApi3ToFunctionConverter extends PersistenceToBasicConverter {
	
	private OpenApi3ToFunctionConverterOptions converterOptions;
	
	private ValidatorUniqueResourcePaths validatorUniqueResourcePaths = new ValidatorUniqueResourcePaths();

	public OpenApi3ToFunctionConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		switch (getConverterOptions().getSplitResources()) {
		case NONE:
			result.add(new OpenApi3ToFunctionModule<>(this));
			break;
		case PATH:
			result.add(new OpenApi3ToSplitPathResources<>(this));
			result.add(new PathToFunctionModule<>(this));
			break;
		case TAG:
			result.add(new OpenApi3ToSplitTagResources<>(this));
			break;
		default:
			Message error = OpenApi3Messages.ERROR_UNKNOWN.getMessageBuilder()
			    .parameters("Unknown split resources option: " + getConverterOptions().getSplitResources())
			    .build();
			throw new ModelConverterException(error.getMessage());
		}
		
		result.add(new CollectionToServiceInterface<>(this));
		
		// --- slave element converters
		result.add(new OperationToFunction<>(this));
		result.add(new ParameterToFunctionParameter<>(this));
		result.add(new MediaTypeToFunctionParameter<>(this));
		result.add(new SchemaToType<>(this));
		result.add(new ResponseToExceptionType<>(this));
		
		result.add(new ServiceInterfaceToServiceImplementation<>(this));
		result.add(new ServiceImplementationToServiceClient<>(this));
		
		return result;
	}

	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		Set<Object> modelElements = super.onPerformModelConsolidation(normalizedElements);
		
		ServiceInterface serviceInterface = modelElements.stream()
				.filter(ServiceInterface.class::isInstance)
				.map(ServiceInterface.class::cast)
				.findAny().orElseThrow(() -> {
					Message error = OpenApi3Messages.ERROR_UNKNOWN.getMessageBuilder()
							.parameters("Could not find ServiceInterface")
							.build();
					return new ModelConverterException(error.getMessage());
				});
		
		// Add entities that are not yet part of the service interface in any form.
		serviceInterface.addEntities(modelElements.stream()
				.filter(Entity.class::isInstance)
				.map(Entity.class::cast)
				.filter(entity -> !serviceInterface.contains(entity))
				.toArray(Entity[]::new));
		
		LinkedHashSet<ServiceImplementation> serviceImplementations = modelElements.stream()
		        .filter(ServiceImplementation.class::isInstance)
			    .map(ServiceImplementation.class::cast)
			    .collect( Collectors.toCollection( LinkedHashSet::new ) );
		
		// --- Create a ServiceApplication to make the conversion result consistent with the gApp model converter.
		// --- As a consequence of this we can use ServiceApplicationToApplicationConverter instead of ModelToApplicationConverter
		// --- when it comes to create platform-specific artifacts.
		ServiceApplication serviceApplication = new ServiceApplication("RestApplication");
		getModel().addElement(serviceApplication);
		addModelElement(serviceApplication);
		serviceApplication.getServices().addAll(serviceImplementations);
		modelElements.add(serviceApplication);
		
		// --- Here we complete potentially missing modeling options to get a meaningful output of the code generation.
		// --- Note that this procedure doesn't validate the input model. So, as a result, we at most get INFO or WRNING messages.
		ServiceModelProcessing.completeServiceOptions(modelElements)
			.stream()
			.forEach(message ->	addMessage(message));
		
		// --- validate the service clients to find out whether its API matches the service implementations that are related to the client
		modelElements.stream()
		    .filter(ServiceClient.class::isInstance)
		    .map(ServiceClient.class::cast)
		    .forEach(serviceClient -> serviceClient.validateServiceRelations());
		
		
		if (getConverterOptions().isAgnosticEnhancement()) {
			modelElements = FunctionMetamodel.INSTANCE.createExceptionParent(modelElements);
		}
		
		return modelElements;
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelValidation(java.util.Set)
	 */
	@Override
	protected void onPerformModelValidation(Set<Object> result) {
		super.onPerformModelValidation(result);
		
		Collection<Message> messages = validatorUniqueResourcePaths.validate(result);
		this.addMessages(messages);
		
		// AJAX-187
		this.addMessages( new ValidatorDuplicateFields(Enumeration.class).validate(result) );
        this.addMessages( new ValidatorDuplicateEntityFields().validate(result) );
	}

	/* (non-Javadoc)
     * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#getConverterOptions()
     */
    @Override
	public OpenApi3ToFunctionConverterOptions getConverterOptions() {
    	if (this.converterOptions == null) {
    		this.converterOptions = new OpenApi3ToFunctionConverterOptions(getOptions());
    	}
		return converterOptions;
	}

	@Override
	protected Set<Class<?>> getFilteredMetaTypes() {
		return Stream.of(Field.class,
				FunctionParameter.class)
				.collect(Collectors.toCollection(LinkedHashSet::new));
	}

	@Override
	protected void onPerformValidationBeforeConversion(Set<Object> normalizedElements) {
		super.onPerformValidationBeforeConversion(normalizedElements);
		
		Set<OpenApi3> openApi3Models = normalizedElements.stream().filter(OpenApi3.class::isInstance)
			.map(OpenApi3.class::cast)
			.collect(Collectors.toSet());
		
		if (openApi3Models.isEmpty()) {
			addError(OpenApi3Messages.ERROR_NO_OPENAPI_MODEL_FOUND_IN_INPUT
					.getMessageBuilder()
			        .build().getMessage());
		}
	}
	
	
}
