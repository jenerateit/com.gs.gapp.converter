package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.Map.Entry;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.kaizen.oasparser.model3.Info;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;
import com.reprezen.kaizen.oasparser.model3.Operation;
import com.reprezen.kaizen.oasparser.model3.Path;

public class PathToFunctionModule<S extends Path, T extends FunctionModule> extends AbstractOpenApi3ToFunction<S, T> {

	public PathToFunctionModule(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected void onConvert(S path, T functionModule) {
		OpenApi3 openApi3 = Overlay.of(path).getModel();
		
		// AJAX-253 commented this (mmt 20-Jul-2023)
//		String serverPath = getServerPath(openApi3.getServers());
//		if (serverPath.endsWith("/")) {
//			serverPath = serverPath.substring(0, serverPath.length() - 1);
//		}
//		serverPath = serverPath + path.getPathString();
		addServerUrlsToDocumentation(functionModule, openApi3);

		OptionDefinition<String>.OptionValue pathOptionValue = RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(path.getPathString());
		functionModule.addOptions(pathOptionValue);
		for (Entry<String, Operation> operation : path.getOperations().entrySet()) {
			OptionDefinition<String>.OptionValue pathOptionOperation = RestOptionEnum.OPTION_DEFINITION_OPERATION. new OptionValue(operation.getKey().toUpperCase());
			Function function = convertWithOtherConverter(Function.class, operation.getValue(), functionModule);
			function.addOptions(pathOptionOperation);
			function.addOptions(RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue("/"));
		}
	}

	@Override
	protected T onCreateModelElement(S path, ModelElementI previousResultingModelElement) {
		if (!ModelElementWrapper.class.isInstance(previousResultingModelElement)) {
			Message errorMessage = OpenApi3Messages.ERROR_UNEXPECTED_PREVIOUS_MODEL_ELEMENT.getMessageBuilder()
				.parameters(previousResultingModelElement.getClass().getName(),
						"path: " + path.getPathString())
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		OpenApi3 openApi3 = OpenApi3.class.cast(ModelElementWrapper.class.cast(previousResultingModelElement).getWrappedElement());
		String name = getEffectiveServiceName(openApi3);
		@SuppressWarnings("unchecked")
		T functionModule = (T) new FunctionModule(name + StringTools.firstUpperCase(normalizeString(path.getPathString())));
		
		Info info = openApi3.getInfo();
		functionModule.setBody(getBody(
				new Text(info.getTitle()),
				new Text("Version", info.getVersion()),
				new Text(info.getDescription()),
				new Text("Path", path.getPathString())));
		
		functionModule.setNamespace(getNamespace(name, com.gs.gapp.metamodel.function.Namespace.class));
		GappOpenApi3ModelElementWrapper wrapper = new GappOpenApi3ModelElementWrapper(openApi3);
		functionModule.setOriginatingElement(wrapper);
		
		
		convertExtensions(path, functionModule);
		
		return functionModule;
	}

	

}
