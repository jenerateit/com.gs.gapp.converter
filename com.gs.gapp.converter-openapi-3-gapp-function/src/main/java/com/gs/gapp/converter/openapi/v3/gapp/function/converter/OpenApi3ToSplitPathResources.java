package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import java.util.Map.Entry;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;
import com.reprezen.kaizen.oasparser.model3.Path;
import com.reprezen.kaizen.oasparser.model3.Schema;

public class OpenApi3ToSplitPathResources<S extends OpenApi3, T extends ModelElementWrapper> extends AbstractOpenApi3ToFunction<S, T> {

	public OpenApi3ToSplitPathResources(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	@Override
	protected void onConvert(S openApi3, T wrapper) {
		for (Entry<String, Path> entry : openApi3.getPaths().entrySet()) {
			Path path = entry.getValue();

			@SuppressWarnings("unused")
			FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, path, wrapper);
		}

		for (Entry<String, Schema> entry : openApi3.getSchemas().entrySet()) {
			@SuppressWarnings("unused")
			ConvertedSchema convertedSchema = convertSchema(entry.getValue(), null);
		}
	}

	@Override
	protected T onCreateModelElement(S openApi3, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T wrapper = (T) new GappOpenApi3ModelElementWrapper(openApi3);
		
		return wrapper;
	}

	

}
