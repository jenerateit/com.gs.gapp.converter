package com.gs.gapp.converter.openapi.v3.gapp.function.converter;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.openapi.v3.gapp.function.OpenApi3Messages;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.reprezen.jsonoverlay.Overlay;
import com.reprezen.jsonoverlay.Reference;
import com.reprezen.kaizen.oasparser.model3.MediaType;

public class MediaTypeToFunctionParameter<S extends MediaType, T extends FunctionParameter> extends AbstractOpenApi3ToFunction<S, T> {

	
	public MediaTypeToFunctionParameter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected void onConvert(S mediaType, T functionParameter) {
		if (isRequestBody(mediaType)) {
			OptionDefinition<String>.OptionValue paramTypeOptionValue = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.BODY.getName());
			functionParameter.addOptions(paramTypeOptionValue);
			functionParameter.setParameterType(ParameterType.IN);
		} else {
			functionParameter.setParameterType(ParameterType.OUT);
		}
		
		Reference schemaReference = Overlay.of(mediaType).getReference("schema");
		if (schemaReference != null) {
		    addErrorIfNeeded(schemaReference.getInvalidReason());
		}
		
		if (mediaType.getSchema() == null) {
			// This can happen when an error has been added due to an incorrect/unresolvable reference ($ref).
			// The if-clause is here since we want to be able to collecto more of these kinds of error.
			// It is more useful to identify as many errors as possible at once instead of fail fast. (mmt 25-Jul-2022)
		} else {
			ConvertedSchema convertedSchema = convertSchema(mediaType.getSchema(), functionParameter);
			functionParameter.setCollectionType(convertedSchema.getCollectionType());
			functionParameter.setDimension(convertedSchema.getDimension());
			functionParameter.setType(convertedSchema.getType());
			functionParameter.setKeyType(convertedSchema.getKeyType());
			if (convertedSchema.getOption() != null) {
			    functionParameter.addOptions(convertedSchema.getOption());
			}
			if (convertedSchema.getAdditionalModelInfo().length() > 0) {
				functionParameter.addToBody(convertedSchema.getAdditionalModelInfo().toString());
			}
		}
	}

	@Override
	protected T onCreateModelElement(S mediaType, ModelElementI previousResultingModelElement) {
		if (!Function.class.isInstance(previousResultingModelElement)) {
			Message errorMessage = OpenApi3Messages.ERROR_UNEXPECTED_PREVIOUS_MODEL_ELEMENT.getMessageBuilder()
					.parameters(previousResultingModelElement.getClass().getName(), "media type:" + Overlay.of(mediaType).toString())
					.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		Function function = Function.class.cast(previousResultingModelElement);


		@SuppressWarnings("unchecked")
		T functionParameter = (T) new FunctionParameter(isRequestBody(mediaType) ? "request" : "response");
		GappOpenApi3ModelElementWrapper wrapper = new GappOpenApi3ModelElementWrapper(mediaType);
		functionParameter.setOriginatingElement(wrapper);
		functionParameter.setModule(function.getModule());
		
		return functionParameter;
	}
	
	private boolean isRequestBody(S mediaType) {
		return Overlay.of(mediaType).getJsonReference().contains("/responses/") ? false : true;
	}
	

}
