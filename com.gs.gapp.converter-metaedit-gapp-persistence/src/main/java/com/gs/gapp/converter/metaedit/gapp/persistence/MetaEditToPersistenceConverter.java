/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.persistence;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.metaedit.gapp.basic.MetaEditToBasicConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class MetaEditToPersistenceConverter extends MetaEditToBasicConverter {

	/**
	 * 
	 */
	public MetaEditToPersistenceConverter() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new PersistenceModuleConverter<>(this));
		result.add(new EntityConverter<>(this));
		result.add(new EntityFieldConverter<>(this));
		result.add(new EntityRelationEndConverter<>(this));

		return result;
	}

	@Override
	protected Set<Object> clientConvert(Collection<?> rawElements,
			ModelConverterOptions options) throws ModelConverterException {
		return super.clientConvert(rawElements, options);
	}
	
	

}
