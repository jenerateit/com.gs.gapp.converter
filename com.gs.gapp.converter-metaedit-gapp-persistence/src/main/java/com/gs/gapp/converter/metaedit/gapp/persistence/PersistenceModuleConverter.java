/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.persistence;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.gs.gapp.converter.metaedit.gapp.basic.BasicModuleConverter;
import com.gs.gapp.converter.metaedit.gapp.basic.GappMetaEditModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metaedit.dsl.persistence.GraphType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;
import com.vd.jenerateit.modelaccess.metaedit.model.Graph;

/**
 * @author mmt
 *
 */
public class PersistenceModuleConverter<S extends Graph, T extends com.gs.gapp.metamodel.persistence.PersistenceModule>
    extends BasicModuleConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{GraphType.PERSISTENCE_MODULE}) );

	public PersistenceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {
		
		super.onConvert(originalModelElement, resultingModelElement);

		Namespace namespace = null;
		try {
			namespace = new Namespace(originalModelElement.getPropertyByName("namespace").getValue().toString());
			resultingModelElement.addElement(namespace);
			resultingModelElement.setNamespace(namespace);
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		for (com.vd.jenerateit.modelaccess.metaedit.model.Object graphElement : originalModelElement.getObjects()) {

			com.gs.gapp.metamodel.persistence.Entity entity =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
						                       graphElement);
			if (entity != null) {
				resultingModelElement.addElement(entity);
				if (namespace != null) namespace.addEntity(entity);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.persistence.PersistenceModule result = null;
		try {
			result = new com.gs.gapp.metamodel.persistence.PersistenceModule(originalModelElement.getPropertyByName("module").getValue().toString());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getModel().addElement(result);  // adding the persistence module to the model in order to be able to process more than one persistence module per target project
		result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
		return (T) result;
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
