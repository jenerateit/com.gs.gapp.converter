/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.persistence;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.metaedit.gapp.basic.FieldConverter;
import com.gs.gapp.converter.metaedit.gapp.basic.GappMetaEditModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.java.JavaOptionEnum;
import com.gs.gapp.dsl.persistence.PersistenceOptionEnum;
import com.gs.gapp.metaedit.dsl.persistence.ObjectType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;

/**
 * @author mmt
 *
 */
public class EntityFieldConverter<S extends com.vd.jenerateit.modelaccess.metaedit.model.Object, T extends com.gs.gapp.metamodel.persistence.EntityField>
    extends FieldConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{ObjectType.ENTITY_FIELD}) );

	public EntityFieldConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- field type   ---> this is done in parent element converter already
//		ObjectProperty objectPropertyForType = null;
//		try {
//			objectPropertyForType = (ObjectProperty) originalModelElement.getPropertyByName("Type");
//			final com.gs.gapp.metamodel.basic.typesystem.Type type = convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Type.class, objectPropertyForType.getValue());
//			if (type != null) {
//			    resultingModelElement.setType(type);
//			}
//		} catch (ElementNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		

		// --- member options
		for (PersistenceOptionEnum option : PersistenceOptionEnum.getMemberOptions(BasicMemberEnum.FIELD)) {
			try {
					
				switch (option) {
				case BINARY:
					BaseProperty<Boolean> booleanPropertyForBinary = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setLob(booleanPropertyForBinary.getValue());
					break;
					
				case ENUMERATION_TYPE:
					// TODO
	//				String enumerationType =
	//				    originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(option.getName());
	//				if (enumerationType != null) {
	//                    if (PersistenceEnumerationType.LITERAL.getName().equals(enumerationType)) {
	//                    	resultingModelElement.setEnumType(EnumType.STRING);
	//                    } else if (PersistenceEnumerationType.LITERAL.getName().equals(enumerationType)) {
	//                    	resultingModelElement.setEnumType(EnumType.ORDINAL);
	//                    }
	//				}
					break;
				case ID:
					BaseProperty<Boolean> booleanPropertyForId = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setPartOfId(booleanPropertyForId.getValue());
					
					// this ensures that for Java for an id-field there is no primitive type being used
					OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
					resultingModelElement.addOptions(primitiveTypeWrapperOptionValue);
					
					break;
					
				case TRANSIENT:
					BaseProperty<Boolean> booleanPropertyForTransient = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setTransient(booleanPropertyForTransient.getValue());
					break;
					
				case UNIQUE:
					BaseProperty<Boolean> booleanPropertyForUnique = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setUnique(booleanPropertyForUnique.getValue());
					break;
					
				case SEARCHABLE:
					BaseProperty<Boolean> booleanPropertyForSearchable = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setSearchable(booleanPropertyForSearchable.getValue());
					break;

				case ORDER_BY:
				case CASCADE_TYPES:
				case LAZY_LOADING:
				case RELATES_TO:
				case PRIVATELY_OWNED:
				case KEEP_ORDERING:
					// is for entity relation end -> ignore these
					break;
				case STORABLE:
				case UTILITY_FIELDS:
					// not related to field but to entity -> ignore these
					break;
				default:
					break;
	
				}
			} catch (ElementNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previousResultingElement) {
		
		com.gs.gapp.metamodel.persistence.Entity fieldOwner = null;
		if (previousResultingElement != null && previousResultingElement instanceof com.gs.gapp.metamodel.persistence.Entity) {
			fieldOwner = (com.gs.gapp.metamodel.persistence.Entity) previousResultingElement;
		}

		com.gs.gapp.metamodel.persistence.EntityField result = null;

		if (fieldOwner != null) {
			try {
				result = new com.gs.gapp.metamodel.persistence.EntityField(originalModelElement.getPropertyByName("Name", String.class).getValue(), fieldOwner);
				result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
			} catch (ElementNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new ModelConverterException("owning entity element not successfully converted for original element", new GappMetaEditModelElementWrapper(originalModelElement));
		}
		return (T) result;
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
