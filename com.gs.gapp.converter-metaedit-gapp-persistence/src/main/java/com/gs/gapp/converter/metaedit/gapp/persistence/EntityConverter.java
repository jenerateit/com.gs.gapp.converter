/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.persistence;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.metaedit.gapp.basic.GappMetaEditModelElementWrapper;
import com.gs.gapp.converter.metaedit.gapp.basic.MetaEditToBasicModelElementConverter;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metaedit.dsl.persistence.ObjectType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.CollectionProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;
import com.vd.jenerateit.modelaccess.metaedit.model.Role;

/**
 * @author mmt
 *
 */
public class EntityConverter<S extends com.vd.jenerateit.modelaccess.metaedit.model.Object, T extends com.gs.gapp.metamodel.persistence.Entity>
    extends MetaEditToBasicModelElementConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{ObjectType.ENTITY}) );

	public EntityConverter(AbstractConverter modelConverter) {
		super(modelConverter, false, false, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- parent entity
		Role parentRole = originalModelElement.getRoleByTypeName("Child");
		if (parentRole != null) {
			Role childRole = parentRole.getRelationship().getRoleByTypeName("Parent");
			com.gs.gapp.metamodel.persistence.Entity parentEntity =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
							                       childRole.getObjects().iterator().next());
			if (parentEntity != null) {
				resultingModelElement.setParent(parentEntity);
			} else {
				throw new ModelConverterException("was not able to convert a parent entity: '" + parentRole + "'");
			}
		}
		
		try {
			BaseProperty<Boolean> booleanPropertyForAbstract = originalModelElement.getPropertyByName("Abstract", Boolean.class);
			resultingModelElement.setMappedSuperclass(booleanPropertyForAbstract.getValue());
		} catch (ElementNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
			
		try {
			BaseProperty<Boolean> booleanPropertyForUtilityFields = originalModelElement.getPropertyByName("UtilityFields", Boolean.class);
			resultingModelElement.setUtilityFields(new Boolean(booleanPropertyForUtilityFields.getValue()));
		} catch (ElementNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		CollectionProperty collectionPropertyForFields = null;
		try {
			collectionPropertyForFields = (CollectionProperty) originalModelElement.getPropertyByName("EntityFields");
			for (Object o : collectionPropertyForFields.getValue()) {
				if (o instanceof com.vd.jenerateit.modelaccess.metaedit.model.Object) {
					com.vd.jenerateit.modelaccess.metaedit.model.Object fieldObject = (com.vd.jenerateit.modelaccess.metaedit.model.Object) o;
					@SuppressWarnings("unused")
					com.gs.gapp.metamodel.persistence.EntityField entityField =
							this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.EntityField.class, fieldObject, resultingModelElement);
					
				}
			}
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		// --- relations to other entities
		List<Role> relationEndRoles = originalModelElement.getRolesByTypeName("RelationEnd");
		if (relationEndRoles != null) {
			for (Role relationEndRole : relationEndRoles) {
				
				Role roleOnOtherSide = relationEndRole.getRoleThroughRelationship("RelationEnd");
				@SuppressWarnings("unused")
				com.gs.gapp.metamodel.persistence.EntityField entityField =
						this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.EntityField.class,
								                       roleOnOtherSide,
								                       resultingModelElement);
			}
		}
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		T result = null;
		try {
			result = (T) new com.gs.gapp.metamodel.persistence.Entity(originalModelElement.getPropertyByName("Name").getValue().toString());
			result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (T) result;
	}

}
