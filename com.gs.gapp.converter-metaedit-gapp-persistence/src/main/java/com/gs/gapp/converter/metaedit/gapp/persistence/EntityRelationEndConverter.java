/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.persistence;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.metaedit.gapp.basic.GappMetaEditModelElementWrapper;
import com.gs.gapp.converter.metaedit.gapp.basic.MetaEditToBasicModelElementConverter;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.dsl.persistence.PersistenceOptionEnum;
import com.gs.gapp.metaedit.dsl.persistence.RoleType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.enums.CascadeType;
import com.gs.gapp.metamodel.persistence.enums.FetchType;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.BooleanProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;
import com.vd.jenerateit.modelaccess.metaedit.model.Role;


/**
 * @author mmt
 *
 */
public class EntityRelationEndConverter<S extends Role, T extends com.gs.gapp.metamodel.persistence.EntityRelationEnd>
    extends MetaEditToBasicModelElementConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{RoleType.RELATION_END}) );

	public EntityRelationEndConverter(AbstractConverter modelConverter) {
		super(modelConverter, true, true, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);
		
		// --- field type
		com.vd.jenerateit.modelaccess.metaedit.model.Object entityObjectForType = originalModelElement.getObjects().iterator().next();
		if (entityObjectForType != null) {
			final com.gs.gapp.metamodel.persistence.Entity entityType = convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class, entityObjectForType);
			if (entityType != null) {
			    resultingModelElement.setType(entityType);
			}
		}

		// --- setting some values that might have been set in super.onConvert() but have to have a different value since we are creating an EntityRelationEnd
		try {
		    BaseProperty<Boolean> booleanPropertyForEqualityRelevance = originalModelElement.getPropertyByName(BasicOptionEnum.EQUALITY_RELEVANCE.getName(), Boolean.class);
		    resultingModelElement.setRelevantForEquality(booleanPropertyForEqualityRelevance.getValue());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			
		    BaseProperty<String> stringPropertyForCollectionType = originalModelElement.getPropertyByName(BasicOptionEnum.COLLECTION_TYPE.getName(), String.class);
		    if (stringPropertyForCollectionType.getValue() != null && stringPropertyForCollectionType.getValue().length() > 0) {
		        resultingModelElement.setCollectionType(CollectionType.fromString(stringPropertyForCollectionType.getValue()));
		        resultingModelElement.setNullable(false); // a collection-typed entity relation end must never be null (it might be an empty collection, though)
		    }
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// --- member options
		for (PersistenceOptionEnum option : PersistenceOptionEnum.getMemberOptions(BasicMemberEnum.FIELD)) {
			try {
				switch (option) {
				case BINARY:
				case ENUMERATION_TYPE:
				case ID:
				case TRANSIENT:
				case UNIQUE:
					// these options got handled by the EntityFieldConverter already
					break;
	
				case ORDER_BY:
					// TODO this is not yet implemented in the MetaEdit modeling language
					break;
				case CASCADE_TYPES:
					BaseProperty<String> stringPropertyForCascadeType = originalModelElement.getPropertyByName(option.getName(), String.class);
					
	                if (stringPropertyForCascadeType != null && stringPropertyForCascadeType.getValue() != null && stringPropertyForCascadeType.getValue().length() > 0) {
	                	String[] cascadeTypes = stringPropertyForCascadeType.getValue().split(",");
	                	for (String cascadeType : cascadeTypes) {
	                		CascadeType cascadeTypeEnum = CascadeType.fromString(cascadeType.trim());
	                		if (cascadeTypeEnum == null) throw new ModelConverterException("no CascadeType enumeration entry found for String '" + cascadeType.trim() + "'");
	                		resultingModelElement.addCascadeOption(cascadeTypeEnum);
	                	}
	                }
					break;
					
				case LAZY_LOADING:
					resultingModelElement.setFetchType(FetchType.LAZY);  // this is the default value
					
					BaseProperty<Boolean> stringPropertyForLazyLoading = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					
					if (stringPropertyForLazyLoading.getValue()) {
						resultingModelElement.setFetchType(FetchType.LAZY);
					} else {
						resultingModelElement.setFetchType(FetchType.EAGER);
					}
					
					break;
				case RELATES_TO:  // means a bidirectional relationship, TODO this needs to be implemented (use Multiplicity property in MetaEdit modeling language)
	//				OptionValueReference optionValueReference = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(option.getName());
	//				if (optionValueReference != null) {
	//					final com.gs.gapp.metamodel.persistence.Entity otherEntity =
	//							this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
	//									                       optionValueReference.getReferencedObject().eContainer().eContainer());
	//					if (otherEntity == null) {
	//						throw new ModelConverterException("entity relation end conversion: other end's entity is null", new GappMetaEditModelElementWrapper(optionValueReference.getReferencedObject().eContainer().eContainer()));
	//
	//					}
	//					final com.gs.gapp.metamodel.persistence.EntityRelationEnd otherEnd =
	//							this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.EntityRelationEnd.class,
	//									                       optionValueReference.getReferencedObject(), otherEntity);
	//					if (otherEnd == null) {
	//						throw new ModelConverterException("Can not find other End of a relatesTo in field " +
	//							resultingModelElement.getName() + " in entity " + resultingModelElement.getOwner().getName());
	//					} else if (!otherEnd.getOwner().equals(otherEntity)) {
	//						throw new ModelConverterException("Found an other end with a different Entity than expected");
	//					}
	//
	////					resultingModelElement.setType(otherEntity);
	//					otherEnd.setType(resultingModelElement.getOwner());
	//					
	//
	//					final Relation relation = new Relation("relation-" + resultingModelElement.getName() + "<--->" + optionValueReference.getReferencedObject().getName());
	//					resultingModelElement.setRelation(relation);
	//					otherEnd.setRelation(relation);
	//					relation.setRoleA(otherEnd);
	//					relation.setRoleB(resultingModelElement);
	//					
	//					if (resultingModelElement.getCollectionType() != null && otherEnd.getCollectionType() != null) {
	//						// m-to-n => ownership is as defined
	//						resultingModelElement.setOwnerOfBidirectionalRelationship(true);
	//						otherEnd.setReferencedBy(resultingModelElement.getName());
	//					} else if (resultingModelElement.getCollectionType() != null && otherEnd.getCollectionType() == null) {
	//						// 1-to-n => ownership is always on the 1-side, which means the other side needs the 'referencedBy'
	//						otherEnd.setOwnerOfBidirectionalRelationship(true);
	//						resultingModelElement.setReferencedBy(otherEnd.getName());
	//					} else if (resultingModelElement.getCollectionType() == null && otherEnd.getCollectionType() != null) {
	//						// n-to-1 => ownership is always on the 1-side, which means that this side needs the 'referencedBy'
	//						resultingModelElement.setOwnerOfBidirectionalRelationship(true);
	//						otherEnd.setReferencedBy(resultingModelElement.getName());
	//					} else {
	//						// 1-to-1, the ownership is possible on both sides => leave it the way it is modeled
	//						resultingModelElement.setOwnerOfBidirectionalRelationship(true);
	//						otherEnd.setReferencedBy(resultingModelElement.getName());
	//					}
	//					
	//				}
					break;
					
				case PRIVATELY_OWNED:
					BaseProperty<Boolean> booleanPropertyForPrivatelyOwned = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setPrivatelyOwned(booleanPropertyForPrivatelyOwned.getValue());
					break;
					
				case KEEP_ORDERING:
					BaseProperty<Boolean> booleanPropertyForKeepOrdering = originalModelElement.getPropertyByName(option.getName(), Boolean.class);
					resultingModelElement.setKeepOrdering(booleanPropertyForKeepOrdering.getValue());
					break;
					
				case STORABLE:
				case SEARCHABLE:
				case UTILITY_FIELDS:
					// not related to field but to entity -> ignore these
					break;
					
				default:
					break;
				}
			} catch (ElementNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		
		if (result) {
			Role role = (Role) originalModelElement;
			try {
				BooleanProperty booleanProperty = (BooleanProperty) role.getPropertyByName("Navigable");
				if (booleanProperty != null && booleanProperty.getValue() == false) result = false;
			} catch (ElementNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previousResultingElement) {

		com.gs.gapp.metamodel.persistence.Entity relationEndOwner = null;
		if (previousResultingElement != null && previousResultingElement instanceof com.gs.gapp.metamodel.persistence.Entity) {
			relationEndOwner = (com.gs.gapp.metamodel.persistence.Entity) previousResultingElement;
		}

		if (relationEndOwner != null) {
			T result = null;
			try {
				String name = null;
				BaseProperty<String> stringPropertyForOptionalName = originalModelElement.getPropertyByName("NameOptional", String.class);
				if (stringPropertyForOptionalName.getValue() != null && stringPropertyForOptionalName.getValue().length() > 0) {
					name = stringPropertyForOptionalName.getValue();
				} else {
					name = originalModelElement.getObjects().iterator().next().getPropertyByName("Name", String.class).getValue();
				}
				
				result = (T) new com.gs.gapp.metamodel.persistence.EntityRelationEnd(name, relationEndOwner);
			} catch (ElementNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return result;
		} else {
			throw new ModelConverterException("owning entity element not successfully provided as previous resulting element", new GappMetaEditModelElementWrapper(originalModelElement));
		}
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
