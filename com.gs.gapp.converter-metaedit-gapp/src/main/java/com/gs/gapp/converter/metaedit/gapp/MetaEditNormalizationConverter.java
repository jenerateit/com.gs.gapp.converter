/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * As of now, this converter does nothing. In future this converter will be the place
 * where the input model can be modified and extended before passing the model to
 * a "real" model converter.
 *
 */
public class MetaEditNormalizationConverter extends AbstractConverter {


	public MetaEditNormalizationConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelElementConversion(java.util.Set)
	 */
	@Override
	protected void onPerformModelElementConversion(Set<?> modelElements) {
		// super-call is not going to be executed since we do not use any model element converter here so far
		super.onPerformModelElementConversion(modelElements);

		for (Object modelElement : modelElements) {
			System.out.println("MetaEdit element:" + modelElement);
		}
	}


	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(
			Set<?> normalizedElements) {
		Set<Object> result = super.onPerformModelConsolidation(normalizedElements);

		result.addAll(normalizedElements);

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
				new ArrayList<>();
		return result;
	}
}
