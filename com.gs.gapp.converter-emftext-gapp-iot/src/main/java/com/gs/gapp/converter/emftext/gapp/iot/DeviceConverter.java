/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.device.Device;

/**
 * @author mmt
 *
 */
public class DeviceConverter<S extends Element, T extends Device> extends FunctionblockConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public DeviceConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T device) {
		super.onConvert(element, device);

		// --- device implementation
		OptionValueReference deviceImplementationElementRef = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.DEVICE_IMPLEMENTATION.getName());
		if (deviceImplementationElementRef != null) {
			ModelElement deviceImplementationElement = deviceImplementationElementRef.getReferencedObject();
			Device deviceImplementation = convertWithOtherConverter(Device.class, deviceImplementationElement);
			device.setImplementation(deviceImplementation);
			deviceImplementation.setApplication(device.getApplication());
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.DEVICE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		
		T result = (T) new Device(element.getName());
		
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
