/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.iot.device.Hardware;

/**
 * @author mmt
 *
 */
public abstract class HardwareConverter<S extends Element, T extends Hardware> extends NodeConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public HardwareConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public HardwareConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T hardware) {
		super.onConvert(element, hardware);

		// --- data structure for hardware status
		OptionValueReference optionValueReferenceForStatus = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.STATUS.getName());
		if (optionValueReferenceForStatus != null) {
			ModelElement optionValueReferenceObjectForStatus = optionValueReferenceForStatus.getReferencedObject();
			if (optionValueReferenceObjectForStatus != null) {
				ComplexType statusType = convertWithOtherConverter(ComplexType.class, optionValueReferenceObjectForStatus);
				if (statusType != null) {
				    hardware.setStatusDataStructure(statusType);
				} else {
					PrimitiveType primitiveStatusType = convertWithOtherConverter(PrimitiveType.class, optionValueReferenceObjectForStatus);
					if (primitiveStatusType != null) {
						// in case of a modeled primitive type, we nonetheless add a complex type in order to provide a means to manually extend the generated code artifact
						statusType = new ComplexType(hardware.getName() + "StatusType");
//						hardware.getModule().addElement(statusType);
						statusType.setModule(hardware.getModule());
						Field statusField = new Field("status", statusType);
						statusField.setReadOnly(true);
						statusField.setType(primitiveStatusType);
						hardware.setStatusDataStructure(statusType);
					}
				}
			}
		}
		
		// --- data structure for hardware configuration
		OptionValueReference optionValueReferenceForConfiguration = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.CONFIGURATION.getName());
		if (optionValueReferenceForConfiguration != null) {
			ModelElement optionValueReferenceObjectForConfiguration = optionValueReferenceForConfiguration.getReferencedObject();
			if (optionValueReferenceObjectForConfiguration != null) {
				ComplexType configurationType = convertWithOtherConverter(ComplexType.class, optionValueReferenceObjectForConfiguration);
				hardware.setConfigurationDataStructure(configurationType);
			}
		}
		
		// --- data structure for hardware connection
		OptionValueReference optionValueReferenceForConnection = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.CONNECTION.getName());
		if (optionValueReferenceForConnection != null) {
			ModelElement optionValueReferenceObjectForConnection = optionValueReferenceForConnection.getReferencedObject();
			if (optionValueReferenceObjectForConnection != null) {
				ComplexType connectionType = convertWithOtherConverter(ComplexType.class, optionValueReferenceObjectForConnection);
				hardware.setConnectionDataStructure(connectionType);
			}
		}
		
		// --- data structure for hardware fault
		OptionValueReference optionValueReferenceForFault = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.FAULT.getName());
		if (optionValueReferenceForFault != null) {
			ModelElement optionValueReferenceObjectForFault = optionValueReferenceForFault.getReferencedObject();
			if (optionValueReferenceObjectForFault != null) {
				ExceptionType exceptionType = convertWithOtherConverter(ExceptionType.class, optionValueReferenceObjectForFault);
				hardware.setFaultDataStructure(exceptionType);
			}
		}

		// --- data structure for operations
		OptionValueReference optionValueReferenceForOperations = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.OPERATIONS.getName());
		if (optionValueReferenceForOperations != null) {
			ModelElement optionValueReferenceObjectForOperations = optionValueReferenceForOperations.getReferencedObject();
			if (optionValueReferenceObjectForOperations != null) {
				FunctionModule functionModule = convertWithOtherConverter(FunctionModule.class, optionValueReferenceObjectForOperations);
				hardware.setOperations(functionModule);
			}
		}

		// so far nothing to be converter here
	}
}
