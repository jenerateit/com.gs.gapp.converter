/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.Producer;

/**
 * @author mmt
 *
 */
public class ProducerConverter<S extends Element, T extends Producer>
    extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ProducerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- organisation name
		String organisationName = originalModelElement.getOptionValueSettingsReader().getTextOptionValue(IotOptionEnum.ORGANISATION_NAME.getName());
		resultingModelElement.setOrganisationName(organisationName);
		
		// --- homepage
		String homepage = originalModelElement.getOptionValueSettingsReader().getTextOptionValue(IotOptionEnum.HOMEPAGE.getName());
		resultingModelElement.setHomepage(homepage);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.PRODUCER;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")	
		T result = (T) new Producer(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
