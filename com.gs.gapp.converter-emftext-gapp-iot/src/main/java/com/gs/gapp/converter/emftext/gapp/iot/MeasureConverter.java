/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.Measure;
import com.gs.gapp.metamodel.iot.Unit;

/**
 * @author mmt
 *
 */
public class MeasureConverter<S extends Element, T extends Measure>
    extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public MeasureConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- units
		EList<OptionValueReference> unitReferences = originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(IotOptionEnum.UNITS.getName());
		for (OptionValueReference unitReference : unitReferences) {
			ModelElement referencedObject = unitReference.getReferencedObject();
			Unit unit = convertWithOtherConverter(Unit.class, referencedObject);
			if (unit != null) resultingModelElement.addUnit(unit);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.MEASURE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")	
		T result = (T) new Measure(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
