/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.businesslogic.BusinessLogic;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.SensorUsage;

/**
 * @author mmt
 *
 */
public class BusinessLogicConverter<S extends ElementMember, T extends BusinessLogic>
    extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public BusinessLogicConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T businessLogic) {
		super.onConvert(elementMember, businessLogic);

		// --- triggers
		EList<OptionValueReference> triggerReferences = elementMember.getOptionValueReferencesReader().getOptionValueReferences(IotOptionEnum.TRIGGERS.getName());
		for (OptionValueReference triggerReference : triggerReferences) {
			ModelElement referencedObject = triggerReference.getReferencedObject();
			SensorUsage sensorUsage = null;
			ActuatorUsage actuatorUsage = null;
			Topic topic = null;
			
			if (referencedObject instanceof ElementMember) {
				ElementMember referencedElementMember = (ElementMember) referencedObject;
				if (referencedElementMember.isTypeof(IotMemberEnum.ACTUATOR_USAGE.getName())) {
					actuatorUsage = businessLogic.getOwner().getActuatorUsage(referencedElementMember.getName());
					if (actuatorUsage != null) businessLogic.addTriggerActuatorUsage(actuatorUsage);
				} else if (referencedElementMember.isTypeof(IotMemberEnum.SENSOR_USAGE.getName())) {
					sensorUsage = businessLogic.getOwner().getSensorUsage(referencedElementMember.getName());
					if (sensorUsage != null) businessLogic.addTriggerSensorUsage(sensorUsage);
				}
			} else {
				topic = convertWithOtherConverter(Topic.class, referencedObject);
				if (topic != null) businessLogic.addTriggerTopic(topic);
			}
		}
		
		// --- function
		Element memberType = elementMember.getMemberType();
		if (memberType != null) {
			Function function = convertWithOtherConverter(Function.class, memberType);
			businessLogic.setFunction(function);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotMemberEnum.BUSINESS_LOGIC;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		
		// --- get application and construct the business logic object with it
		Application application = (Application) previouslyResultingElement;
		
		@SuppressWarnings("unchecked")	
		T result = (T) new BusinessLogic(elementMember.getName(), application);
		result.setModule(application.getModule());
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}
}
