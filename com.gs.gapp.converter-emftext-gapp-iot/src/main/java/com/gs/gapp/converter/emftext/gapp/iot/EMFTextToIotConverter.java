/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;

import java.util.List;

import com.gs.gapp.converter.emftext.gapp.ui.EMFTextToUIConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class EMFTextToIotConverter extends EMFTextToUIConverter {

	/**
	 * 
	 */
	public EMFTextToIotConverter() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		result.add(new DeviceModuleConverter<>(this));
		result.add(new TopologyModuleConverter<>(this));
		
		result.add(new AppConverter<>(this));
		result.add(new BrokerConverter<>(this));
		result.add(new TopicConverter<>(this));
		result.add(new ProducerConverter<>(this));
		result.add(new MeasureConverter<>(this));
		result.add(new UnitConverter<>(this));
		result.add(new DeviceConverter<>(this));
		
		// --- slave element converters
		result.add(new SensorConverter<>(this));
		result.add(new SensorUsageConverter<>(this));
		result.add(new ActuatorConverter<>(this));
		result.add(new ActuatorUsageConverter<>(this));
		result.add(new FunctionblockConverter<>(this));
		result.add(new BlockConverter<>(this));
		result.add(new ConnectionConverter<>(this));
		result.add(new BridgeConverter<>(this));
		result.add(new BusinessLogicConverter<>(this));
		
		return result;
	}

}
