/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotModuleTypeEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Namespace;
import com.gs.gapp.metamodel.iot.device.Actuator;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.device.DeviceModule;
import com.gs.gapp.metamodel.iot.device.Functionblock;
import com.gs.gapp.metamodel.iot.device.Sensor;

/**
 * @author mmt
 *
 */
public class DeviceModuleConverter<S extends Module, T extends DeviceModule> extends ModuleConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public DeviceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.CREATE_AND_CONVERT_IN_ONE_GO);  // to make sure that all devices are processed before topology is going to be processed
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T deviceModule) {

		super.onConvert(module, deviceModule);

		// --- namespace
		Namespace namespace = new Namespace(module.getNamespace().getName());
		deviceModule.setNamespace(namespace);

		// --- devices
		for (Element moduleElement : module.getElements()) {
			Device device = convertWithOtherConverter(Device.class, moduleElement);
			if (device != null) {
				deviceModule.addDevice(device);
			}
			
			Functionblock functionblock = convertWithOtherConverter(Functionblock.class, moduleElement);
			if (functionblock != null) deviceModule.addFunctionblock(functionblock);
			
			Actuator actuator = convertWithOtherConverter(Actuator.class, moduleElement);
			if (actuator != null) deviceModule.addActuator(actuator);
			
			Sensor sensor = convertWithOtherConverter(Sensor.class, moduleElement);
			if (sensor != null) deviceModule.addSensor(sensor);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Module) {
				Module theModule = (Module) originalModelElement;

				if (theModule.getModuleTypes().size() == 0 || theModule.getModuleTypesReader().containsModuleType(IotModuleTypeEnum.DEVICE.getName())) {
					if (theModule.hasElementsOfTypes( new BasicEList<>(IotElementEnum.getElementTypeNames()) ) == false) {
						// only if the module has at least one element for kind Device or Topology, it is recognized as a device module
						generalResponsibility = false;
					}
				} else {
					generalResponsibility = false;
				}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new DeviceModule(module.getName());
		result.setOriginatingElement(new GappModelElementWrapper(module));
		return result;
	}
}
