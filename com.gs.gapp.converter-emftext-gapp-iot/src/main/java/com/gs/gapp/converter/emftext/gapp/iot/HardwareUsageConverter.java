/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.device.HardwareUsage;

/**
 * @author mmt
 *
 */
public abstract class HardwareUsageConverter<S extends ElementMember, T extends HardwareUsage> extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public HardwareUsageConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- topic levels
		OptionValueReference topicLevelsElementRef = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.TOPIC_LEVELS.getName());
		if (topicLevelsElementRef != null) {
			ModelElement producerElement = topicLevelsElementRef.getReferencedObject();
			Topic topic = convertWithOtherConverter(Topic.class, producerElement);
			resultingModelElement.setTopic(topic);
		}
	}
}
