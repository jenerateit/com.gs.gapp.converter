/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import java.net.MalformedURLException;
import java.net.URL;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.topology.Bridge;
import com.gs.gapp.metamodel.iot.topology.Broker;

/**
 * @author mmt
 *
 */
public class BridgeConverter<S extends ElementMember, T extends Bridge> extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public BridgeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- bridge address
		String url = originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(IotOptionEnum.URL.getName());
		if (url != null && url.length() > 0) {
			try {
				resultingModelElement.setAddress(new URL(url));
			} catch (MalformedURLException ex) {
				throw new ModelConverterException("malformed url modeled on bridge '" + originalModelElement.getName() + "'", ex, url);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotMemberEnum.BRIDGE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		// --- get broker and remote broker (convert them) and construct the bridge object with them
		Broker broker = (Broker) previouslyResultingElement;
		Broker remoteBroker = convertWithOtherConverter(Broker.class, originalModelElement.getMemberType());
		
		@SuppressWarnings("unchecked")	
		T result = (T) new Bridge(originalModelElement.getName(), broker, remoteBroker);
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
