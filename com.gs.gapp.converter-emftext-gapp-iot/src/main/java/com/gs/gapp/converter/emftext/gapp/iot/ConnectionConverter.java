/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.SensorUsage;
import com.gs.gapp.metamodel.iot.topology.Broker;
import com.gs.gapp.metamodel.iot.topology.BrokerConnection;
import com.gs.gapp.metamodel.iot.topology.Publication;
import com.gs.gapp.metamodel.iot.topology.Subscription;

/**
 * @author mmt
 *
 */
public class ConnectionConverter<S extends ElementMember, T extends BrokerConnection>
    extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ConnectionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- publications
		EList<OptionValueReference> publicationReferences = originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(IotOptionEnum.PUBLICATIONS.getName());
		for (OptionValueReference publicationReference : publicationReferences) {
			SensorUsage sensorUsage = null;
			ActuatorUsage actuatorUsage = null;
			Topic topic = null;
			ModelElement referencedObject = publicationReference.getReferencedObject();
			Publication pub = new Publication(originalModelElement.getName() + "#" + referencedObject.getName());
			
			if (referencedObject instanceof ElementMember) {
				ElementMember referencedElementMember = (ElementMember) referencedObject;
				if (referencedElementMember.isTypeof(IotMemberEnum.ACTUATOR_USAGE.getName())) {
					actuatorUsage = resultingModelElement.getApplication().getActuatorUsage(referencedElementMember.getName());
					if (actuatorUsage != null) {
						pub.addActuatorUsage(actuatorUsage);
						resultingModelElement.addPublication(pub);
					}
				} else if (referencedElementMember.isTypeof(IotMemberEnum.SENSOR_USAGE.getName())) {
					sensorUsage = resultingModelElement.getApplication().getSensorUsage(referencedElementMember.getName());
					if (sensorUsage != null) {
						pub.addSensorUsage(sensorUsage);
						resultingModelElement.addPublication(pub);
					}
				}
			} else {
				topic = convertWithOtherConverter(Topic.class, referencedObject);
				if (topic != null) {
					pub.addTopic(topic);
					resultingModelElement.addPublication(pub);
				}
			}
		}
		
		// --- subscriptions
		EList<OptionValueReference> subscriptionReferences = originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(IotOptionEnum.SUBSCRIPTIONS.getName());
		for (OptionValueReference subscriptionReference : subscriptionReferences) {
			SensorUsage sensorUsage = null;
			ActuatorUsage actuatorUsage = null;
			Topic topic = null;
			ModelElement referencedObject = subscriptionReference.getReferencedObject();
			Subscription sub = new Subscription(originalModelElement.getName() + "#" + referencedObject.getName());
			
			if (referencedObject instanceof ElementMember) {
				ElementMember referencedElementMember = (ElementMember) referencedObject;
				if (referencedElementMember.isTypeof(IotMemberEnum.ACTUATOR_USAGE.getName())) {
					actuatorUsage = resultingModelElement.getApplication().getActuatorUsage(referencedElementMember.getName());
					if (actuatorUsage != null) {
						sub.addActuatorUsage(actuatorUsage);
						resultingModelElement.addSubscription(sub);
					}
				} else if (referencedElementMember.isTypeof(IotMemberEnum.SENSOR_USAGE.getName())) {
					sensorUsage = resultingModelElement.getApplication().getSensorUsage(referencedElementMember.getName());
					if (sensorUsage != null) {
						sub.addSensorUsage(sensorUsage);
						resultingModelElement.addSubscription(sub);
					}
				}
			} else {
				topic = convertWithOtherConverter(Topic.class, referencedObject);
				if (topic != null) {
					sub.addTopic(topic);
					resultingModelElement.addSubscription(sub);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotMemberEnum.CONNECTION;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		// --- get application and broker (convert them) and construct the broker connection object with them
		Application application = (Application) previouslyResultingElement;
		Broker broker = convertWithOtherConverter(Broker.class, originalModelElement.getMemberType());
		
		@SuppressWarnings("unchecked")	
		T result = (T) new BrokerConnection(originalModelElement.getName(), application, broker);
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
