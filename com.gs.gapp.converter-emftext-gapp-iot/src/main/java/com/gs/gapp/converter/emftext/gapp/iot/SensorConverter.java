/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Measure;
import com.gs.gapp.metamodel.iot.device.Sensor;

/**
 * @author mmt
 *
 */
public class SensorConverter<S extends Element, T extends Sensor> extends HardwareConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public SensorConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- measures
		EList<OptionValueReference> measuresReferences = originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(IotOptionEnum.MEASURES.getName());
		for (OptionValueReference measuresReference : measuresReferences) {
			ModelElement referencedObject = measuresReference.getReferencedObject();
			Measure measure = convertWithOtherConverter(Measure.class, referencedObject);
			if (measure != null) resultingModelElement.addMeasure(measure);
		}
		
		// --- sensor implementation
		OptionValueReference sensorImplementationElementRef = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.SENSOR_IMPLEMENTATION.getName());
		if (sensorImplementationElementRef != null) {
			ModelElement sensorElement = sensorImplementationElementRef.getReferencedObject();
			Sensor sensor = convertWithOtherConverter(Sensor.class, sensorElement);
			resultingModelElement.setImplementation(sensor);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.SENSOR;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
			
		@SuppressWarnings("unchecked")	
		T result = (T) new Sensor(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
