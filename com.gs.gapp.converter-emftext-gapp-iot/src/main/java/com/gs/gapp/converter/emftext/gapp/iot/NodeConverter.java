/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Node;
import com.gs.gapp.metamodel.iot.Producer;

/**
 * @author mmt
 *
 */
public abstract class NodeConverter<S extends Element, T extends Node>
    extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public NodeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public NodeConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T node) {
		super.onConvert(element, node);
		
		// --- producer
		OptionValueReference producerElementRef = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.PRODUCER.getName());
		if (producerElementRef != null) {
			ModelElement producerElement = producerElementRef.getReferencedObject();
			Producer producer = convertWithOtherConverter(Producer.class, producerElement);
			node.setProducer(producer);
		}
	}
}
