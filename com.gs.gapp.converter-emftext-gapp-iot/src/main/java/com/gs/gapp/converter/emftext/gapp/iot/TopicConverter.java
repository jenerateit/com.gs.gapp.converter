/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.Topic;

/**
 * @author mmt
 *
 */
public class TopicConverter<S extends Element, T extends Topic>
    extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public TopicConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.TOPIC;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		// --- actuator implementation
		String qualifier = originalModelElement.getOptionValueSettingsReader().getTextOptionValue(IotOptionEnum.QUALIFIER.getName());
		
		@SuppressWarnings("unchecked")	
		T result = (T) new Topic(originalModelElement.getName(), qualifier);
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
