/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.EList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.ApplicationTypeEnum;
import com.gs.gapp.metamodel.iot.businesslogic.BusinessLogic;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.topology.BrokerConnection;

/**
 * @author mmt
 *
 */
public class AppConverter<S extends Element, T extends Application> extends NodeConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AppConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T app) {
		super.onConvert(originalModelElement, app);
		
		// --- application type
		String appTypeString = originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(IotOptionEnum.APP_TYPE.getName());
		ApplicationTypeEnum applicationType = ApplicationTypeEnum.getFromName(appTypeString);
		app.setApplicationType(applicationType);
		
		// --- app implementation
		OptionValueReference appImplementationElementRef = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.APPLICATION_IMPLEMENTATION.getName());
		if (appImplementationElementRef != null) {
			ModelElement appImplementationElement = appImplementationElementRef.getReferencedObject();
			Application applicationImplementation = convertWithOtherConverter(Application.class, appImplementationElement);
			app.setImplementation(applicationImplementation);
		}
		
		// --- devices being used by application
		EList<OptionValueReference> devicesElementRef = originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(IotOptionEnum.DEVICES.getName());
		if (devicesElementRef != null) {
			for (OptionValueReference deviceElementRef : devicesElementRef) {
				ModelElement deviceElement = deviceElementRef.getReferencedObject();
				Device device = convertWithOtherConverter(Device.class, deviceElement);
				if (device != null) {
					app.addDevice(device);
					device.setApplication(app);
				}
			}
		}

		// --- handle element members
		for (ElementMember elementMember : originalModelElement.getElementMembers()) {
			if (elementMember.isTypeof(IotMemberEnum.CONNECTION.getName())) {
				@SuppressWarnings("unused")
				BrokerConnection brokerConnection = this.convertWithOtherConverter(BrokerConnection.class, elementMember, app);
			} else if (elementMember.isTypeof(IotMemberEnum.BUSINESS_LOGIC.getName())) {
				@SuppressWarnings("unused")
				BusinessLogic businessLogic = this.convertWithOtherConverter(BusinessLogic.class, elementMember, app);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.APP;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")	
		T result = (T) new Application(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
