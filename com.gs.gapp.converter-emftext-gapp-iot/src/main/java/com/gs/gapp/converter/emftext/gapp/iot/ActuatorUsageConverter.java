/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.device.Actuator;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.Functionblock;

/**
 * @author mmt
 *
 */
public class ActuatorUsageConverter<S extends ElementMember, T extends ActuatorUsage> extends SensorUsageConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ActuatorUsageConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember, T actuatorUsage) {
		super.onConvert(elementMember, actuatorUsage);

		// --- actuator comes from the actuator usage's type
		Actuator actuator = convertWithOtherConverter(Actuator.class, elementMember.getMemberType());
		actuatorUsage.setImplementation(actuator);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotMemberEnum.ACTUATOR_USAGE;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S elementMember, ModelElementI previouslyResultingElement) {
		Functionblock functionblock = null;
		if (previouslyResultingElement instanceof Functionblock) {
			functionblock = (Functionblock) previouslyResultingElement;
		}
			
		@SuppressWarnings("unchecked")	
		T result = (T) new ActuatorUsage(elementMember.getName(), functionblock);
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return result;
	}
}
