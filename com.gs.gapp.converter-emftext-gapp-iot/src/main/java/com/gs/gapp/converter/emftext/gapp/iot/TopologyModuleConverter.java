/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotModuleTypeEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Namespace;
import com.gs.gapp.metamodel.iot.device.DeviceModule;
import com.gs.gapp.metamodel.iot.topology.Broker;
import com.gs.gapp.metamodel.iot.topology.TopologyModule;

/**
 * @author mmt
 *
 */
public class TopologyModuleConverter<S extends Module, T extends TopologyModule> extends ModuleConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public TopologyModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T topologyModule) {

		super.onConvert(module, topologyModule);
		
		// --- first make sure that all imported device modules have been successfully converted in order to be able to access the results
		for (Module importedModule : module.getImports().getModules()) {
			@SuppressWarnings("unused")
			DeviceModule deviceModule = convertWithOtherConverter(DeviceModule.class, importedModule);
		}

		// --- namespace
		Namespace namespace = new Namespace(module.getNamespace().getName());
		topologyModule.setNamespace(namespace);

		// --- module elements
		for (Element moduleElement : module.getElements()) {
			Application application = convertWithOtherConverter(Application.class, moduleElement, topologyModule);
			if (application != null) {
				topologyModule.addApplication(application);
			}
			
			Broker broker = convertWithOtherConverter(Broker.class, moduleElement, topologyModule);
			if (broker!= null) topologyModule.addBroker(broker);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.getModuleTypes().size() == 0 || module.getModuleTypesReader().containsModuleType(IotModuleTypeEnum.TOPOLOGY.getName())) {
					if (module.hasElementsOfTypes( new BasicEList<>(IotElementEnum.getElementTypeNames()) ) == false) {
						// only if the module has at least one element for kind Device or Topology, it is recognized as a topology module
						generalResponsibility = false;
					}
				} else {
					generalResponsibility = false;
				}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new TopologyModule(module.getName());
		result.setOriginatingElement(new GappModelElementWrapper(module));
		return result;
	}
}
