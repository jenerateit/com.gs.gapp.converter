/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.device.Functionblock;

/**
 * @author mmt
 *
 */
public class BlockConverter<S extends ElementMember, T extends Functionblock> extends AbstractIotModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public BlockConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
		
		// --- functionblock implementation
		OptionValueReference functionblockImplementationElementRef = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.FUNCTIONBLOCK_IMPLEMENTATION.getName());
		if (functionblockImplementationElementRef != null) {
			ModelElement functionblockImplementationElement = functionblockImplementationElementRef.getReferencedObject();
			Functionblock functionblock = convertWithOtherConverter(Functionblock.class, functionblockImplementationElement);
			resultingModelElement.setImplementation(functionblock);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotMemberEnum.BLOCK;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		Functionblock functionblock = null;
		if (previouslyResultingElement instanceof Functionblock) {
			functionblock = (Functionblock) previouslyResultingElement;
		}
			
		@SuppressWarnings("unchecked")	
		T result = (T) new Functionblock(originalModelElement.getName(), functionblock);
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
