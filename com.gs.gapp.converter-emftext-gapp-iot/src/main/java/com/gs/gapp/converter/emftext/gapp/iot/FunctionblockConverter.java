/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.Functionblock;
import com.gs.gapp.metamodel.iot.device.SensorUsage;

/**
 * @author mmt
 *
 */
public class FunctionblockConverter<S extends Element, T extends Functionblock> extends HardwareConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public FunctionblockConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public FunctionblockConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T functionblock) {
		super.onConvert(element, functionblock);
		
		// --- topic levels
		OptionValueReference topicLevelsElementRef = element.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.TOPIC_LEVELS.getName());
		if (topicLevelsElementRef != null) {
			ModelElement producerElement = topicLevelsElementRef.getReferencedObject();
			Topic topic = convertWithOtherConverter(Topic.class, producerElement);
			functionblock.setTopic(topic);
		}
		
		// --- handle element members
		for (ElementMember elementMember : element.getElementMembers()) {
			if (elementMember.isTypeof(IotMemberEnum.BLOCK.getName())) {
				@SuppressWarnings("unused")
				Functionblock nestedFunctionblock = this.convertWithOtherConverter(Functionblock.class, elementMember, functionblock);
			} else if (elementMember.isTypeof(IotMemberEnum.SENSOR_USAGE.getName())) {
				@SuppressWarnings("unused")
				SensorUsage sensorUsage = this.convertWithOtherConverter(SensorUsage.class, elementMember, functionblock);
			} else if (elementMember.isTypeof(IotMemberEnum.ACTUATOR_USAGE.getName())) {
				@SuppressWarnings("unused")
				ActuatorUsage actuatorUsage = this.convertWithOtherConverter(ActuatorUsage.class, elementMember, functionblock);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.FUNCTIONBLOCK;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		Functionblock functionblock = null;
		if (previouslyResultingElement instanceof Functionblock) {
			functionblock = (Functionblock) previouslyResultingElement;
		}
			
		@SuppressWarnings("unchecked")	
		T result = (T) new Functionblock(element.getName(), functionblock);
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return result;
	}
}
