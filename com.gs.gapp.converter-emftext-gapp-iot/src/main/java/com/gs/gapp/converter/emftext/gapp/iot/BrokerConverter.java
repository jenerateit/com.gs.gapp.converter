/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.iot;


import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.iot.IotElementEnum;
import com.gs.gapp.dsl.iot.IotMemberEnum;
import com.gs.gapp.dsl.iot.IotOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.iot.topology.Bridge;
import com.gs.gapp.metamodel.iot.topology.Broker;

/**
 * @author mmt
 *
 */
public class BrokerConverter<S extends Element, T extends Broker> extends NodeConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public BrokerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- broker implementation
		OptionValueReference brokerImplementationElementRef = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(IotOptionEnum.BROKER_IMPLEMENTATION.getName());
		if (brokerImplementationElementRef != null) {
			ModelElement brokerImplementationElement = brokerImplementationElementRef.getReferencedObject();
			Broker broker = convertWithOtherConverter(Broker.class, brokerImplementationElement);
			resultingModelElement.setImplementation(broker);
		}
		
		// --- bridges
		for (ElementMember elementMember : originalModelElement.getElementMembers()) {
			if (elementMember.isTypeof(IotMemberEnum.BRIDGE.getName())) {
				@SuppressWarnings("unused")
				Bridge bridge = this.convertWithOtherConverter(Bridge.class, elementMember, resultingModelElement);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return IotElementEnum.BROKER;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		
		@SuppressWarnings("unchecked")	
		T result = (T) new Broker(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return result;
	}
}
