/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.CollectionProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;

/**
 * @author mmt
 *
 */
public class BasicTypeConverter<S extends com.vd.jenerateit.modelaccess.metaedit.model.Object, T extends com.gs.gapp.metamodel.basic.typesystem.ComplexType>
    extends MetaEditToBasicModelElementConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{com.gs.gapp.metaedit.dsl.basic.ObjectType.BASIC_TYPE}) );

	public BasicTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, false, false, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- TODO parent modeling for basic types is not yet implemented
//		if (originalModelElement.getParent() != null && originalModelElement.getParent().getElementDefinition().hasParentDefinition(BasicElementEnum.TYPE.getName())) {
//			com.gs.gapp.metamodel.basic.typesystem.ComplexType parent =
//					convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.ComplexType.class,
//							                  originalModelElement.getParent());
//			if (parent != null) {
//				resultingModelElement.setParent(parent);
//			} else {
//				// TODO A type with no members will be converted to a PrimitiveType, which cannot be a parent of a complex type. Should we really throw an exception in this case? (mmt 02-May-2013)
//				throw new ModelConverterException("could not successfully convert a basic DSL's complex type '" + originalModelElement + "' to a technology-agnostic complex type model element");
//			}
//		}

		// --- abstractness
		try {
			BaseProperty<Boolean> booleanPropertyForAbstract = originalModelElement.getPropertyByName("Abstract", Boolean.class);
			if (booleanPropertyForAbstract != null) {
				resultingModelElement.setAbstractType(new Boolean(booleanPropertyForAbstract.getValue()));
			}
		} catch (ElementNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		CollectionProperty collectionPropertyForFields = null;
		try {
			collectionPropertyForFields = (CollectionProperty) originalModelElement.getPropertyByName("Fields");
			for (Object o : collectionPropertyForFields.getValue()) {
				if (o instanceof com.vd.jenerateit.modelaccess.metaedit.model.Object) {
					com.vd.jenerateit.modelaccess.metaedit.model.Object fieldObject = (com.vd.jenerateit.modelaccess.metaedit.model.Object) o;
					@SuppressWarnings("unused")
					com.gs.gapp.metamodel.basic.typesystem.Field field =
							this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Field.class, fieldObject, resultingModelElement);
					
				}
			}
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result) {
			@SuppressWarnings("unchecked")
			S metaEditObject = (S) originalModelElement;
			result = hasFields(metaEditObject);
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.typesystem.ComplexType result = null;
		try {
			result = new com.gs.gapp.metamodel.basic.typesystem.ComplexType(originalModelElement.getPropertyByName("Name", String.class).getValue());
			result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (T) result;
	}
	
	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
