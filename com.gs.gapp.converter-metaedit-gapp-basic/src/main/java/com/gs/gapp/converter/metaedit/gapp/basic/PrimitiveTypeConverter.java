/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metaedit.dsl.basic.ObjectType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;

/**
 * @author mmt
 *
 */
public class PrimitiveTypeConverter<S extends com.vd.jenerateit.modelaccess.metaedit.model.Object, T extends com.gs.gapp.metamodel.basic.typesystem.PrimitiveType>
    extends MetaEditToBasicModelElementConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{ObjectType.BASIC_TYPE}) );

	public PrimitiveTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, false, true, true);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);
		// nothing to be converted for a primitive type, only the type's name is relevant
	}

	

	@Override
	public boolean isResponsibleFor(Object originalModelElement,
			ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		
		if (result) {
			// --- only if the original object does not have any fields defined, it is interpreted as a primitive type
			@SuppressWarnings("unchecked")
			S metaEditObject = (S) originalModelElement;
			result = hasFields(metaEditObject) == false;
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		T result = null;
		
		try {
			BaseProperty<String> stringPropertyForName = originalModelElement.getPropertyByName("Name", String.class);
				
			BaseProperty<String> stringPropertyForMappedSimpleType = originalModelElement.getPropertyByName("MappedSimpleType", String.class);
			
			PrimitiveTypeEnum primitiveTypeEnum = PrimitiveTypeEnum.fromStringCaseInsensitive(stringPropertyForMappedSimpleType != null ? stringPropertyForMappedSimpleType.getValue() : stringPropertyForName.getValue());
			if (primitiveTypeEnum != null) {
				if (primitiveTypeEnum.getPrimitiveType() == null) {
					throw new ModelConverterException("primitive type enum '" + primitiveTypeEnum + "' does not return a primitive type instance");
				}
			    result = (T) primitiveTypeEnum.getPrimitiveType();
			} else {
				// primitive type from incoming model is not known, at least create a new technology-agnostic model element for it
				result = (T) getModelConverter().getModelElementCache().findModelElement(stringPropertyForName.getValue(),
						                                                                 com.gs.gapp.metamodel.basic.typesystem.PrimitiveType.class);
				if (result == null) {
					result = (T) new com.gs.gapp.metamodel.basic.typesystem.PrimitiveType(stringPropertyForName.getValue());
					getModelConverter().getModelElementCache().add(result);
				}
			}
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (T) result;
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
