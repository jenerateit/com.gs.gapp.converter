package com.gs.gapp.converter.metaedit.gapp.basic;

import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.vd.jenerateit.modelaccess.metaedit.model.TypeElement;

public class GappMetaEditModelElementWrapper extends ModelElementWrapper {

	private static final long serialVersionUID = -6695510545219422458L;

	public GappMetaEditModelElementWrapper(TypeElement metaEditElement) {
		super(metaEditElement);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getWrappedElement()
	 */
	@Override
	public TypeElement getWrappedElement() {
		return (TypeElement) super.getWrappedElement();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getId()
	 */
	@Override
	public String getId() {
		return getWrappedElement().getId();
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#getName()
	 */
	@Override
	public String getName() {
		if (getWrappedElement() == null) {
			return null;
		} else {
			return getWrappedElement().getId();
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getType()
	 */
	@Override
	public String getType() {
		return getWrappedElement().getTypeName();
	}
}
