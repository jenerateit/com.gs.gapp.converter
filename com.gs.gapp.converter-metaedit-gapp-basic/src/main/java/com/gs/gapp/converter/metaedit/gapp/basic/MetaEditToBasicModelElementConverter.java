/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import java.util.Collections;
import java.util.Set;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.CollectionProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;
import com.vd.jenerateit.modelaccess.metaedit.model.TypeElement;

/**
 * @author mmt
 *
 */
public abstract class MetaEditToBasicModelElementConverter<S extends TypeElement, T extends com.gs.gapp.metamodel.basic.ModelElement>
    extends AbstractModelElementConverter<S, T> {

	public MetaEditToBasicModelElementConverter(
			AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		// --- comment
//		if (originalModelElement.getComment() != null) {
//			String comment = originalModelElement.getComment().getComment();
//		    resultingModelElement.setBody(comment.substring(2, comment.length()-2).replaceAll("\\*[ ]?", ""));
//		}
//
//		if (originalModelElement instanceof Module) {
//			Module module = (Module) originalModelElement;
//			createAndAddOptionValues(module.getOptionValueSettingsReader(), resultingModelElement);
//		} else if (originalModelElement instanceof Element) {
//			Element element = (Element) originalModelElement;
//			if (element.getElementBody() instanceof ElementBody) {
//				ElementBody elementBody = (ElementBody) element.getElementBody();
//				createAndAddOptionValues(elementBody.getOptionValueSettingsReader(), resultingModelElement);
//			}
//		} else if (originalModelElement instanceof ElementMember) {
//			ElementMember elementMember = (ElementMember) originalModelElement;
//			if (elementMember.getMemberBody() instanceof ElementMemberBody) {
//				ElementMemberBody memberBody = (ElementMemberBody) elementMember.getMemberBody();
//				createAndAddOptionValues(memberBody.getOptionValueSettingsReader(), resultingModelElement);
//			}
//
//			Module module = (Module) elementMember.eContainer().eContainer().eContainer();
//			if (module != null) {
//			    com.gs.gapp.metamodel.basic.Module basicModule = this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.Module.class, module);
//			    resultingModelElement.setModule(basicModule);
//			} else {
//				throw new ModelConverterException("could not find module for element member '" + elementMember + "'");
//			}
//
//		}
	}
	
	protected final boolean hasFields(
			com.vd.jenerateit.modelaccess.metaedit.model.Object baseType) {
		boolean result = false;
		CollectionProperty collectionProperty = null;
		try {
			collectionProperty = (CollectionProperty) baseType.getPropertyByName("Fields");
			if (collectionProperty != null
					&& collectionProperty.getValue() != null
					&& collectionProperty.getValue().size() > 0) {
				result = true;
			}
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	protected final boolean generalIsResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result && getMetatypes() != null) {
			if (originalModelElement instanceof TypeElement) {
				TypeElement typeElement = (TypeElement) originalModelElement;
				result = false;
				for (IMetatype metatype : getMetatypes()) {
	            	if (typeElement.getTypeName().equalsIgnoreCase(metatype.getName())) {
	                    // yes, it is an element of at least one of the given meta types, we are going to convert it
	            		result = true;
	            		break;
	            	}
				}
			}
		}

		return result;

	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
	}
	
	/**
	 * @return
	 */
	protected Set<IMetatype> getMetatypes() {
		return Collections.emptySet();
	}
}
