/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.metaedit.dsl.basic.ObjectType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;
import com.vd.jenerateit.modelaccess.metaedit.model.ObjectProperty;

/**
 * @author mmt
 *
 */
public class FieldConverter<S extends com.vd.jenerateit.modelaccess.metaedit.model.Object, T extends com.gs.gapp.metamodel.basic.typesystem.Field>
    extends MetaEditToBasicModelElementConverter<S, T> {

	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{ObjectType.BASIC_FIELD}) );
	
	public FieldConverter(AbstractConverter modelConverter) {
		super(modelConverter, true, true, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		// --- field type
		ObjectProperty objectPropertyForType = null;
		try {
			objectPropertyForType = (ObjectProperty) originalModelElement.getPropertyByName("Type");
			final com.gs.gapp.metamodel.basic.typesystem.Type type = convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Type.class, objectPropertyForType.getValue());
			if (type != null) {
			    resultingModelElement.setType(type);
			}
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// --- field options
		try {
		    BaseProperty<String> stringPropertyForCollectionType = originalModelElement.getPropertyByName(BasicOptionEnum.COLLECTION_TYPE.getName(), String.class);
		    if (stringPropertyForCollectionType != null && stringPropertyForCollectionType.getValue() != null && stringPropertyForCollectionType.getValue().length() > 0) {
		        resultingModelElement.setCollectionType(CollectionType.fromString(stringPropertyForCollectionType.getValue()));
		    }
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
		    BaseProperty<Boolean> booleanPropertyForEqualityRelevance = originalModelElement.getPropertyByName(BasicOptionEnum.EQUALITY_RELEVANCE.getName(), Boolean.class);
		    resultingModelElement.setRelevantForEquality(booleanPropertyForEqualityRelevance.getValue());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
		    BaseProperty<Boolean> booleanPropertyForReadOnly = originalModelElement.getPropertyByName(BasicOptionEnum.READ_ONLY.getName(), Boolean.class);
		    resultingModelElement.setReadOnly(booleanPropertyForReadOnly.getValue());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		// --- default value
		try {
		    @SuppressWarnings("unused")
			BaseProperty<String> stringPropertyForDefaultValue = originalModelElement.getPropertyByName(BasicOptionEnum.DEFAULT_VALUE.getName(), String.class);
		    //resultingModelElement.set(stringPropertyForDefaultValue.getValue()); TODO there is no default value supported anymore
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// --- length
		try {
			BaseProperty<Integer> integerPropertyForLength = originalModelElement.getPropertyByName(BasicOptionEnum.LENGTH.getName(), Integer.class);
			resultingModelElement.setLength(integerPropertyForLength.getValue());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// --- mandatory
		try {
			BaseProperty<Boolean> booleanPropertyForMandatory = originalModelElement.getPropertyByName(BasicOptionEnum.MANDATORY.getName(), Boolean.class);
			resultingModelElement.setNullable(!booleanPropertyForMandatory.getValue());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// --- minimum length
		try {
			BaseProperty<Integer> integerPropertyForMinLength = originalModelElement.getPropertyByName(BasicOptionEnum.MINIMUM_LENGTH.getName(), Integer.class);
			resultingModelElement.setMinLength(integerPropertyForMinLength.getValue());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// --- TODO multiplicity type not yet implemented for MetaEdit+
//		String multiplicityType = originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValue(BasicOptionEnum.MULTIPLICITY_TYPE.getName());
//				
//		if (multiplicityType != null) {
//			MultiplicityType multiplicityTypeEnumEntry = MultiplicityType.getFromName(multiplicityType);
//			if (multiplicityTypeEnumEntry != null) {
//				switch (multiplicityTypeEnumEntry) {
//				case M_TO_N:
//					resultingModelElement.setBidirectionalRelationshipType(BidirectionalRelationshipType.MANY_TO_MANY);
//					break;
//				case N_TO_ONE:
//					resultingModelElement.setBidirectionalRelationshipType(BidirectionalRelationshipType.MANY_TO_ONE);
//					break;
//				case ONE_TO_N:
//					resultingModelElement.setBidirectionalRelationshipType(BidirectionalRelationshipType.ONE_TO_MANY);
//					break;
//				case ONE_TO_ONE:
//					resultingModelElement.setBidirectionalRelationshipType(BidirectionalRelationshipType.ONE_TO_ONE);
//					break;
//				default:
//					throw new ModelConverterException("unhandled enumeration entry '" + multiplicityTypeEnumEntry + "' found", originalModelElement);
//				}
//			}
//		}

		// --- TODO cardinality not yet implemented
//		Long cardinality = originalModelElement.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.CARDINALITY.getName());
//		if (cardinality != null) {
//		    resultingModelElement.setCardinality(cardinality);
//		}
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previousResultingModelElement) {
		ComplexType owner = null;

		if (previousResultingModelElement != null && previousResultingModelElement instanceof ComplexType) {
			owner = (ComplexType) previousResultingModelElement;
		}
		
		com.gs.gapp.metamodel.basic.typesystem.Field result = null;
		
		if (owner != null) {
			
			try {
				result = new com.gs.gapp.metamodel.basic.typesystem.Field(originalModelElement.getPropertyByName("Name", String.class).getValue(), owner);
				result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
			} catch (ElementNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new ModelConverterException("owning entity element not successfully converted for original element", new GappMetaEditModelElementWrapper(originalModelElement));
		}

		
		return (T) result;
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
