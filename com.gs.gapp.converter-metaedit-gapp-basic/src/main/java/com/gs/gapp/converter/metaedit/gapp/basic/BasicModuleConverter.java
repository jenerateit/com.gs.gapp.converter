/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;


import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.gs.gapp.converter.metaedit.gapp.basic.GappMetaEditModelElementWrapper;
import com.gs.gapp.converter.metaedit.gapp.basic.MetaEditToBasicModelElementConverter;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metaedit.dsl.basic.GraphType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;
import com.vd.jenerateit.modelaccess.metaedit.model.Graph;

/**
 * @author mmt
 *
 */
public class BasicModuleConverter<S extends Graph, T extends com.gs.gapp.metamodel.basic.Module>
    extends MetaEditToBasicModelElementConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{GraphType.BASIC_MODULE}) );

	public BasicModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, false, false, false);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		Namespace namespace = null;
		try {
			namespace = new Namespace(originalModelElement.getPropertyByName("namespace").getValue().toString());
			resultingModelElement.addElement(namespace);
			resultingModelElement.setNamespace(namespace);
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		for (com.vd.jenerateit.modelaccess.metaedit.model.Object graphElement : originalModelElement.getObjects()) {

			com.gs.gapp.metamodel.basic.typesystem.Enumeration enumeration =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Enumeration.class,
						                       graphElement);
			if (enumeration != null) {
				resultingModelElement.addElement(enumeration);
				if (namespace != null) namespace.addElement(enumeration);
			}
			
			com.gs.gapp.metamodel.basic.typesystem.ComplexType complexType =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.ComplexType.class,
							                       graphElement);
			if (complexType != null) {
				resultingModelElement.addElement(complexType);
				if (namespace != null) namespace.addElement(complexType);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.Module result = null;
		try {
			result = new com.gs.gapp.metamodel.basic.Module(originalModelElement.getPropertyByName("module").getValue().toString());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		getModel().addElement(result);  // adding the persistence module to the model in order to be able to process more than one persistence module per target project
		result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
		return (T) result;
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
