/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.metaedit.dsl.basic.ObjectType;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.vd.jenerateit.modelaccess.metaedit.model.BaseProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.CollectionProperty;
import com.vd.jenerateit.modelaccess.metaedit.model.ElementNotFoundException;

/**
 * @author mmt
 *
 */
public class EnumerationConverter<S extends com.vd.jenerateit.modelaccess.metaedit.model.Object, T extends com.gs.gapp.metamodel.basic.typesystem.Enumeration>
    extends MetaEditToBasicModelElementConverter<S, T> {
	
	private final Set<IMetatype> metatypes = new HashSet<>( Arrays.asList(new IMetatype[]{ObjectType.ENUMERATION}) );

	public EnumerationConverter(AbstractConverter modelConverter) {
		super(modelConverter, false, false, true);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);

		try {
			for (BaseProperty<?> baseProperty : originalModelElement.getPropertiesByName("Items")) {
				if (baseProperty instanceof CollectionProperty) {
					CollectionProperty enumItemsCollectionProperty = (CollectionProperty) baseProperty;
					Collection<?> enumItems = enumItemsCollectionProperty.getValue();
					for (Object enumItem : enumItems) {
						if (enumItem instanceof com.vd.jenerateit.modelaccess.metaedit.model.Object) {
							com.vd.jenerateit.modelaccess.metaedit.model.Object enumItemObject = (com.vd.jenerateit.modelaccess.metaedit.model.Object) enumItem;
							BaseProperty<?> enumItemName = enumItemObject.getPropertyByName("Name");
							EnumerationEntry enumEntry = new EnumerationEntry((String) enumItemName.getValue());
							enumEntry.setBody(enumItemName.getDescription());
							resultingModelElement.addEnumerationEntry(enumEntry);
						}
					}
				}
			}
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		
		T result = null;
		try {
			result = (T) new com.gs.gapp.metamodel.basic.typesystem.Enumeration(originalModelElement.getPropertyByName("Name").getValue().toString());
		} catch (ElementNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		result.setOriginatingElement(new GappMetaEditModelElementWrapper(originalModelElement));
		return (T) result;
	}

	@Override
	protected Set<IMetatype> getMetatypes() {
		return metatypes;
	}
}
