/**
 *
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class MetaEditToBasicConverter extends AbstractAnalyticsConverter {

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#findMandatoryElements(java.util.Collection)
	 */
	@Override
	protected Set<Object> findMandatoryElements(Collection<Object> rawElements) {
		Set<Object> result = super.findMandatoryElements(rawElements);
		return result;
	}

	/**
	 * 
	 */
	public MetaEditToBasicConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new BasicModuleConverter<>(this));
		result.add(new EnumerationConverter<>(this));
		result.add(new FieldConverter<>(this));
		result.add(new PrimitiveTypeConverter<>(this));
		result.add(new BasicTypeConverter<>(this));

		return result;
	}

}
