/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.metaedit.gapp.basic;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author hrr
 *
 */
public class MetaEditToBasicConverterProvider implements ModelConverterProviderI {

	/**
	 * 
	 */
	public MetaEditToBasicConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new MetaEditToBasicConverter();
	}

}
