/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.persistence;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.emftext.gapp.basic.ComplexTypeConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.java.JavaOptionEnum;
import com.gs.gapp.dsl.persistence.PersistenceElementEnum;
import com.gs.gapp.dsl.persistence.PersistenceOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.enums.Nature;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

/**
 * @author mmt
 *
 */
public class EntityConverter<S extends Element, T extends com.gs.gapp.metamodel.persistence.Entity>
    extends ComplexTypeConverter<S, T> {

	public EntityConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T entity) {

		super.onConvert(element, entity);
		
		// --- parent entity
		if (element.getParent() != null) {
			com.gs.gapp.metamodel.persistence.Entity parentEntity =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
							                       element.getParent(), new Class<?>[0]);
			if (parentEntity != null) {
				entity.setParent(parentEntity);
			} else {
				throw new ModelConverterException("was not able to convert a parent entity: '" + element.getParent() + "'");
			}
		}

		// --- storable
		Boolean isStorable =
		    element.getOptionValueSettingsReader().getBooleanOptionValue(PersistenceOptionEnum.STORABLE.getName());
		if (isStorable != null) {
			entity.setMappedSuperclass(!isStorable);
		}

		// --- utility fields
		Boolean utilityFields =
		    element.getOptionValueSettingsReader().getBooleanOptionValue(PersistenceOptionEnum.UTILITY_FIELDS.getName());
		if (utilityFields != null) {
			entity.setUtilityFields(utilityFields);
		}
		
		// --- nature
		String natureOptionValue = element.getOptionValueSettingsReader().getEnumeratedOptionValue(PersistenceOptionEnum.NATURE.getName());
		if (natureOptionValue != null && natureOptionValue.length() > 0) entity.setNature(Nature.fromString(natureOptionValue));
		
		// --- exceptions related to storage functions
		EList<OptionValueReference> exceptionReferences = element.getOptionValueReferencesReader().getOptionValueReferences(PersistenceOptionEnum.EXCEPTIONS.getName());
		if (exceptionReferences != null) {
			for (OptionValueReference exceptionReference : exceptionReferences) {
				ModelElement referencedObject = exceptionReference.getReferencedObject();
                OptionEnumerationEntry storageFunctionOption = exceptionReference.getReferenceEnumerationValue();
                if (storageFunctionOption != null) {
	                StorageFunction storageFunction = StorageFunction.fromString(storageFunctionOption.getEntry());
	                if (storageFunction == null) storageFunction = StorageFunction.CREATE;  // that's the default, if nothing got modeled
	                
	                ExceptionType exceptionType = convertWithOtherConverter(ExceptionType.class, referencedObject);
	                if (exceptionType != null) {
	                	entity.addException(storageFunction, exceptionType);
	                }
                }
			}
		}
		
		// --- storage functions
		List<String> storageFunctionOptionValues = element.getOptionValueSettingsReader().getEnumeratedOptionValues(PersistenceOptionEnum.FUNCTIONS.getName());
		for (String storageFunctionName : storageFunctionOptionValues) {
			StorageFunction storageFunction = StorageFunction.fromString(storageFunctionName);
			entity.addStorageFunction(storageFunction);
		}
		
		// --- cacheable
		Boolean cacheable = element.getOptionValueSettingsReader().getBooleanOptionValue(PersistenceOptionEnum.CACHEABLE_ENTITY.getName());
	    if (cacheable != null) {
		    entity.setCacheable(cacheable);
	    }
		
		// --- convert all entity fields
		for (ElementMember elementMember : element.getElementMembers()) {
			//System.out.println("***" + originalModelElement.getName() + ":" + languageEntityField.getName());
//			@SuppressWarnings("unused")
//			String id = org.eclipse.emf.ecore.util.EcoreUtil.getID(elementMember);

			@SuppressWarnings("unused")
			com.gs.gapp.metamodel.persistence.EntityField entityField =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.EntityField.class, elementMember, entity);
		}
		
		// once all fields are converter, we can deduce certain properties to be set automatically
		for (EntityField entityField : entity.getEntityFields()) {
			if (entityField.isPartOfBusinessId()) {
				entityField.setRelevantForEquality(true);  // any business id field is relevant for equality checks (e.g. equals() and hashCode() in Java) (mmt 24-Apr-2018)
			} else if (entityField.isPartOfId()) {
				entityField.setRelevantForEquality(true);  // we also want id fields being part of the equality checks (mmt 20-Aug-2018)
			}
		}
		
		// --- set the number of test data entries
		Long numberOfTestEntries =
			    element.getOptionValueSettingsReader().getNumericOptionValue(PersistenceOptionEnum.NUMBER_OF_TEST_ENTRIES.getName());

		if (numberOfTestEntries != null) {
		    entity.setNumberOfTestEntries(numberOfTestEntries.intValue());
		}
		
		
		// --- multi tenancy support
		final OptionValueReference optionValueReferenceTenantEntity = element.getOptionValueReferencesReader().getOptionValueReference(PersistenceOptionEnum.TENANT_ENTITY.getName());
		if (optionValueReferenceTenantEntity != null) {
			final com.gs.gapp.metamodel.persistence.Entity tenantEntity =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
							optionValueReferenceTenantEntity.getReferencedObject());
			if (tenantEntity != null) {
				entity.setTenantEntity(tenantEntity);
			}
		}
		
		final OptionValueReference optionValueReferenceUserAccountEntity = element.getOptionValueReferencesReader().getOptionValueReference(PersistenceOptionEnum.USER_ACCOUNT_ENTITY.getName());
		if (optionValueReferenceUserAccountEntity != null) {
			final com.gs.gapp.metamodel.persistence.Entity userAccountEntity =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
							optionValueReferenceUserAccountEntity.getReferencedObject());
			if (userAccountEntity != null) {
				entity.setUserAccountEntity(userAccountEntity);
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicModelElementConverter#getMetatype()
	 */
	@Override
	public IMetatype getMetatype() {
		return PersistenceElementEnum.ENTITY;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.isTypeof(PersistenceElementEnum.ENTITY.getName())) {
                    // yes, it is an entity, we are going to convert it
            	} else {
            		generalResponsibility = false;
            	}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T entity = (T) new com.gs.gapp.metamodel.persistence.Entity(element.getName());
		entity.setOriginatingElement(new GappModelElementWrapper(element));
		return entity;
	}

}
