/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.persistence;


import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.converter.emftext.gapp.basic.ModuleConverter;
import com.gs.gapp.dsl.persistence.PersistenceElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Namespace;

/**
 * @author mmt
 *
 */
public class PersistenceModuleConverter<S extends Module, T extends com.gs.gapp.metamodel.persistence.PersistenceModule>
    extends ModuleConverter<S, T> {

	public PersistenceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T persistenceModule) {

		super.onConvert(module, persistenceModule);

		Namespace namespace = new Namespace(module.getNamespace().getName());
		persistenceModule.addElement(namespace);
		persistenceModule.setNamespace(namespace);

		for (Element moduleElement : module.getElements()) {

			com.gs.gapp.metamodel.persistence.Entity entity =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
						                       moduleElement);
			if (entity != null) {
				persistenceModule.addElement(entity);
				namespace.addEntity(entity);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.hasElementsOfTypes( new BasicEList<>(PersistenceElementEnum.getElementTypeNames()) ) == false) {
					// only if the module has at least one persistence element type, it is recognized as a persistence module
					generalResponsibility = false;
				}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.persistence.PersistenceModule result =
				new com.gs.gapp.metamodel.persistence.PersistenceModule(module.getName());
		getModel().addElement(result);  // adding the persistence module to the model in order to be able to process more than one persistence module per target project
		result.setOriginatingElement(new GappModelElementWrapper(module));
		return (T) result;
	}

}
