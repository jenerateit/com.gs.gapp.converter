/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.persistence;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.FieldConverter;
import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.java.JavaOptionEnum;
import com.gs.gapp.dsl.persistence.PersistenceElementEnum;
import com.gs.gapp.dsl.persistence.PersistenceEnumerationType;
import com.gs.gapp.dsl.persistence.PersistenceOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.persistence.enums.EnumType;

/**
 * @author mmt
 *
 */
public class EntityFieldConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.persistence.EntityField>
    extends FieldConverter<S, T> {

	public EntityFieldConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S elementMember,
			T entityField) {

		super.onConvert(elementMember, entityField);

		// --- field type
		final Element memberType = elementMember.getMemberType();
		final com.gs.gapp.metamodel.basic.typesystem.Type type = convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Type.class, memberType);
		if (type != null) {
		    entityField.setType(type);
		}

		// --- member options
		for (PersistenceOptionEnum option : PersistenceOptionEnum.getMemberOptions(BasicMemberEnum.FIELD)) {
			switch (option) {
			case BINARY:
				Boolean binary =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (binary != null) {
					entityField.setLob(binary);
				}
				break;

			case ENUMERATION_TYPE:
				String enumerationType =
				    elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(option.getName());
				if (enumerationType != null) {
					
					try {
						PersistenceEnumerationType persEnumType = PersistenceEnumerationType.valueOf(enumerationType.toUpperCase());
	
						if (persEnumType == PersistenceEnumerationType.LITERAL) {
							entityField.setEnumType(EnumType.STRING);
						} else if (persEnumType == PersistenceEnumerationType.ORDINAL) {
							entityField.setEnumType(EnumType.ORDINAL);
						}
					} catch (IllegalArgumentException ex) {
						throw new ModelConverterException("cannot handle enumeration type '" + enumerationType + "' when converting to the agnostic Persistence model", ex);
					}
				}
				break;
			case ID:
				Boolean id =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (id != null) {
					entityField.setPartOfId(id);
					if (id) {
						// this ensures that for Java for an id-field there is no primitive type being used
						OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
						entityField.addOptions(primitiveTypeWrapperOptionValue);
					}
				}
				break;
			case BUSINESS_ID:
				Boolean businessId =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (businessId != null) {
					entityField.setPartOfBusinessId(businessId);
				}
				break;
			case TRANSIENT:
				Boolean isTransient =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (isTransient != null) {
					entityField.setTransient(isTransient);
				}
				break;
			case UNIQUE:
				Boolean unique =
				    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (unique != null) {
					entityField.setUnique(unique);
				}
				break;
			case SEARCHABLE:
				Boolean searchable = 
					elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if(searchable != null) {
					entityField.setSearchable(searchable);
				} else { //if searchable is not set false for businessid then it means trues AJPA-168
					if(entityField.isPartOfBusinessId()) {
						entityField.setSearchable(true);
					}
				}
				break;
				
			case ORDER_BY:
			case CASCADE_TYPES:
			case LAZY_LOADING:
			case RELATES_TO:
			case PRIVATELY_OWNED:
			case KEEP_ORDERING:
				// is for entity relation end -> ignore these
				break;
			case STORABLE:
			case UTILITY_FIELDS:
			case NUMBER_OF_TEST_ENTRIES:
			case CACHEABLE_ENTITY:
				// not related to field but to entity -> ignore these
				break;
			case CACHEABLE_FIELD:
				Boolean cacheable = elementMember.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
			    if (cacheable != null) {
				    entityField.setCacheable(cacheable);
			    }
				break;
			case CONSUMED_ENTITIES:
				break;
			case EXCEPTIONS:
				break;
			case EXTERNALIZE:
				break;
			case FUNCTIONS:
				break;
			case NATURE:
				break;
			default:
				break;

			}
		}
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.gapp.basic.FieldConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingElement);

		if (previousResultingElement == null) {
			result = false;
		} else {
			if (result) {
				if (originalModelElement instanceof ElementMember) {
	            	ElementMember elementMember = (ElementMember) originalModelElement;
	            	Element memberOwner = null;
	            	if (elementMember.eContainer().eContainer() instanceof Element) {
						memberOwner = (Element) elementMember.eContainer().eContainer();

	            	}
					if (memberOwner.isTypeof(PersistenceElementEnum.ENTITY.getName()) && elementMember.isTypeof(BasicMemberEnum.FIELD.getName())) {
	                    // yes, it is a field in an entity, we are going to convert it
	            	} else {
	            		result = false;
	            	}
	            }
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S elementMember, ModelElementI previousResultingElement) {
		com.gs.gapp.metamodel.persistence.Entity fieldOwner = null;
		if (previousResultingElement != null && previousResultingElement instanceof com.gs.gapp.metamodel.persistence.Entity) {
			fieldOwner = (com.gs.gapp.metamodel.persistence.Entity) previousResultingElement;
		}

		com.gs.gapp.metamodel.persistence.EntityField result = null;

		if (fieldOwner != null) {
			result = new com.gs.gapp.metamodel.persistence.EntityField(elementMember.getName(), fieldOwner);
			result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		} else {
			throw new ModelConverterException("owning entity element not successfully converted for original element", new GappModelElementWrapper(elementMember.eContainer().eContainer()));
		}
		return (T) result;
	}

}
