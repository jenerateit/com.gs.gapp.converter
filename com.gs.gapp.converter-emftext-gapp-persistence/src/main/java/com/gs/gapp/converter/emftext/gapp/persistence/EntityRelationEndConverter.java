/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.persistence;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.emftext.gapp.basic.GappModelElementWrapper;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.persistence.PersistenceElementEnum;
import com.gs.gapp.dsl.persistence.PersistenceOptionEnum;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Relation;
import com.gs.gapp.metamodel.persistence.enums.CascadeType;
import com.gs.gapp.metamodel.persistence.enums.FetchType;


/**
 * @author mmt
 *
 */
public class EntityRelationEndConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.persistence.EntityRelationEnd>
    extends EntityFieldConverter<S, T> {

	
	public EntityRelationEndConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);
		
		// --- setting some values that might have been set in super.onConvert() but have to have a different value since we are creating an EntityRelationEnd
		if (resultingModelElement.getCollectionType() != null) {
		    resultingModelElement.setNullable(false); // a collection-typed entity relation end must never be null (it might be an empty collection, though) 
		}

		// --- member options
		for (PersistenceOptionEnum option : PersistenceOptionEnum.getMemberOptions(BasicMemberEnum.FIELD)) {
			switch (option) {
			case BINARY:
			case ENUMERATION_TYPE:
			case ID:
			case TRANSIENT:
			case UNIQUE:
				// these options got handled by the EntityFieldConverter already
				break;

			case ORDER_BY:
				EList<OptionValueReference> optionValueReferences = originalModelElement.getOptionValueReferencesReader().getOptionValueReferences(option.getName());
				if (optionValueReferences != null) {
					String comma = "";
					StringBuilder orderByClause = new StringBuilder();
					for (OptionValueReference optionValueReference : optionValueReferences) {
						ModelElement referencedObject = optionValueReference.getReferencedObject();
                        OptionEnumerationEntry orderByDirection = optionValueReference.getReferenceEnumerationValue();
                        orderByClause.append(comma)
				             .append(referencedObject.getName())
				             .append(orderByDirection != null && orderByDirection.getEntry() != null ? (orderByDirection.getEntry().equalsIgnoreCase("ASC") ? " ASC" : " DESC") : "");
                        comma = ",";
					}
					resultingModelElement.setOrderByClause(orderByClause.toString());
				}

				break;
			case CASCADE_TYPES:
				List<String> cascadeOptions =
		            originalModelElement.getOptionValueSettingsReader().getEnumeratedOptionValues(option.getName());

                if (cascadeOptions != null) {
                	for (String cascadeOption : cascadeOptions) {
                		CascadeType cascadeType = CascadeType.fromString(cascadeOption);
                		if (cascadeType == null) throw new ModelConverterException("no CascadeType enumeration entry found for String '" + cascadeOption + "'", originalModelElement);
                		resultingModelElement.addCascadeOption(cascadeType);
                	}
                }
				break;
			case LAZY_LOADING:
				Boolean lazyLoading =
				    originalModelElement.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (lazyLoading != null) {
					resultingModelElement.setFetchType(lazyLoading ? FetchType.LAZY : FetchType.EAGER);
				}
				break;
			case RELATES_TO:  // means a bidirectional relationship
				OptionValueReference optionValueReference = originalModelElement.getOptionValueReferencesReader().getOptionValueReference(option.getName());
				if (optionValueReference != null) {
					final com.gs.gapp.metamodel.persistence.Entity otherEntity =
							this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.Entity.class,
									                       optionValueReference.getReferencedObject().eContainer().eContainer());
					if (otherEntity == null) {
						throw new ModelConverterException("entity relation end conversion: other end's entity is null", new GappModelElementWrapper(optionValueReference.getReferencedObject().eContainer().eContainer()));

					}
					final com.gs.gapp.metamodel.persistence.EntityRelationEnd otherEnd =
							this.convertWithOtherConverter(com.gs.gapp.metamodel.persistence.EntityRelationEnd.class,
									                       optionValueReference.getReferencedObject(), otherEntity);
					if (otherEnd == null) {
						throw new ModelConverterException("Can not find other End of a relatesTo in field " +
							resultingModelElement.getName() + " in entity " + resultingModelElement.getOwner().getName());
					} else if (!otherEnd.getOwner().equals(otherEntity)) {
						throw new ModelConverterException("Found an other end with a different Entity than expected");
					}

//					resultingModelElement.setType(otherEntity);
					otherEnd.setType(resultingModelElement.getOwner());
					

					final Relation relation = new Relation("relation-" + resultingModelElement.getName() + "<--->" + optionValueReference.getReferencedObject().getName());
					resultingModelElement.setRelation(relation);
					otherEnd.setRelation(relation);
					relation.setRoleA(otherEnd);
					relation.setRoleB(resultingModelElement);
					
					if (resultingModelElement.getCollectionType() != null && otherEnd.getCollectionType() != null) {
						// m-to-n => ownership is as defined
						resultingModelElement.setOwnerOfBidirectionalRelationship(true);
						otherEnd.setReferencedBy(resultingModelElement.getName());
					} else if (resultingModelElement.getCollectionType() != null && otherEnd.getCollectionType() == null) {
						// 1-to-n => ownership is always on the 1-side, which means the other side needs the 'referencedBy'
						otherEnd.setOwnerOfBidirectionalRelationship(true);
						resultingModelElement.setReferencedBy(otherEnd.getName());
					} else if (resultingModelElement.getCollectionType() == null && otherEnd.getCollectionType() != null) {
						// n-to-1 => ownership is always on the 1-side, which means that this side needs the 'referencedBy'
						resultingModelElement.setOwnerOfBidirectionalRelationship(true);
						otherEnd.setReferencedBy(resultingModelElement.getName());
					} else {
						// 1-to-1, the ownership is possible on both sides => leave it the way it is modeled
						resultingModelElement.setOwnerOfBidirectionalRelationship(true);
						otherEnd.setReferencedBy(resultingModelElement.getName());
					}
					
				}
				break;
			case PRIVATELY_OWNED:
				Boolean privatelyOwned =
			        originalModelElement.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (privatelyOwned != null) {
					resultingModelElement.setPrivatelyOwned(privatelyOwned);
				}
				break;
			case KEEP_ORDERING:
				Boolean keepOrdering =
		        originalModelElement.getOptionValueSettingsReader().getBooleanOptionValue(option.getName());
				if (keepOrdering != null) {
					resultingModelElement.setKeepOrdering(keepOrdering);
				}
				break;
			case STORABLE:
			case SEARCHABLE:
			case UTILITY_FIELDS:
				// not related to field but to entity -> ignore these
				break;
			default:
				break;

			}
		}
	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingElement);

		if (previousResultingElement == null) {
			result = false;
		} else {
			if (result) {
				if (originalModelElement instanceof ElementMember) {
	            	ElementMember elementMember = (ElementMember) originalModelElement;
	            	if (elementMember.getMemberType() != null && elementMember.getMemberType().isTypeof(PersistenceElementEnum.ENTITY.getName())) {
						  // in this case it is an entity relation end, since the field has an entity type
					} else {
						result = false;
					}
	            }
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previousResultingElement) {

		com.gs.gapp.metamodel.persistence.Entity relationEndOwner = null;
		if (previousResultingElement != null && previousResultingElement instanceof com.gs.gapp.metamodel.persistence.Entity) {
			relationEndOwner = (com.gs.gapp.metamodel.persistence.Entity) previousResultingElement;
		}

		if (relationEndOwner != null) {
			@SuppressWarnings("unchecked")
			T result = (T) new com.gs.gapp.metamodel.persistence.EntityRelationEnd(originalModelElement.getName(), relationEndOwner);
			result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
			return result;
		} else {
			throw new ModelConverterException("owning entity element not successfully provided as previous resulting element", new GappModelElementWrapper(originalModelElement.eContainer().eContainer()));
		}
	}
}
