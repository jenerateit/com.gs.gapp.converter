/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.persistence;

import java.util.List;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.converter.emftext.gapp.basic.EMFTextToBasicConverter;
import com.gs.gapp.dsl.java.JavaOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;

/**
 * @author mmt
 *
 */
public class EMFTextToPersistenceConverter extends EMFTextToBasicConverter {

	/**
	 * 
	 */
	public EMFTextToPersistenceConverter() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new PersistenceModuleConverter<>(this));
		result.add(new EntityConverter<>(this));
		result.add(new EntityFieldConverter<>(this));
		result.add(new EntityRelationEndConverter<>(this));

		return result;
	}

	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		Set<Object> resultingElements = super.onPerformModelConsolidation(normalizedElements);
		
		if (getConverterOptions().isAgnosticEnhancement()) {
			for (Object element : resultingElements) {
				if (element instanceof Entity) {
					Entity entity = (Entity) element;
					if (entity.getTenantEntity() != null) {
						EntityField tenantPkField = new EntityField("pkOfOwning" + StringTools.firstUpperCase(entity.getTenantEntity().getName()), entity);
						tenantPkField.setNullable(false);
						tenantPkField.setType(entity.getTenantEntity().getAParentsIdFields().iterator().next().getType());
						entity.setTenantEntityField(tenantPkField);
						setDefaultValueForPk(tenantPkField);
						
						// this ensures that for Java for a field that holds an ID value there is no primitive type being used
						OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
						tenantPkField.addOptions(primitiveTypeWrapperOptionValue);
					}
					
					if (entity.getUserAccountEntity() != null) {
						EntityField userAccountPkField = new EntityField("pkOfOwning" + StringTools.firstUpperCase(entity.getUserAccountEntity().getName()), entity);
						userAccountPkField.setNullable(false);
						userAccountPkField.setType(entity.getUserAccountEntity().getAParentsIdFields().iterator().next().getType());
						entity.setUserAccountEntityField(userAccountPkField);
						setDefaultValueForPk(userAccountPkField);
						
						// this ensures that for Java for a field that holds an ID value there is no primitive type being used
						OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValue = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
						userAccountPkField.addOptions(primitiveTypeWrapperOptionValue);
						
						EntityField userAccountPkFieldForModifiedBy = new EntityField("pkOfModifying" + StringTools.firstUpperCase(entity.getUserAccountEntity().getName()), entity);
						userAccountPkFieldForModifiedBy.setNullable(false);
						userAccountPkFieldForModifiedBy.setType(userAccountPkField.getType());
						entity.setUserAccountEntityFieldForModifiedBy(userAccountPkFieldForModifiedBy);
						setDefaultValueForPk(userAccountPkFieldForModifiedBy);
						
						// this ensures that for Java for a field that holds an ID value there is no primitive type being used
						OptionDefinition<Boolean>.OptionValue primitiveTypeWrapperOptionValueForModifiedBy = JavaOptionEnum.OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER. new OptionValue(true);
						userAccountPkFieldForModifiedBy.addOptions(primitiveTypeWrapperOptionValueForModifiedBy);
					}
				}
			}
		}
		
		return resultingElements;
	}
	
	private void setDefaultValueForPk(Field field) {
		if (field.getType() instanceof PrimitiveType) {
			PrimitiveType primitiveType = (PrimitiveType) field.getType();
			if (primitiveType.getNumeric() != null && primitiveType.getNumeric()) {
				field.setDefaultValue("-1");
			} else if (PrimitiveTypeEnum.STRING == PrimitiveTypeEnum.fromStringCaseInsensitive(primitiveType.getName())) {
				field.setDefaultValue("-1");
			}
		}
	}
}
