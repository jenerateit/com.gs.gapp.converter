/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.OptionEnum;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementBody;
import com.gs.gapp.language.gapp.options.GappOptionValueReferencesReader;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeConstraints;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class PrimitiveTypeConverter<S extends Element, T extends com.gs.gapp.metamodel.basic.typesystem.PrimitiveType>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public PrimitiveTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T primitiveType) {
		super.onConvert(element, primitiveType);
		
		if (primitiveType.isBuiltInPrimitiveType()) {
			// built-in primitive types must not have any constraints
		} else {
			// CHQDG-153
			GappOptionValueReferencesReader referencesReader = element.getOptionValueReferencesReader();
			OptionValueReference constrainedTypeReference = referencesReader.getOptionValueReference(BasicOptionEnum.CONSTRAINED_TYPE.getName());
			if (constrainedTypeReference != null) {
				PrimitiveType constrainedPrimitiveType = convertWithOtherConverter(PrimitiveType.class, constrainedTypeReference.getReferencedObject());
				if (constrainedPrimitiveType != null) {
					PrimitiveTypeConstraints primitiveTypeConstraints = new PrimitiveTypeConstraints(primitiveType.getName() + "Constraints", constrainedPrimitiveType);
					primitiveType.setConstraints(primitiveTypeConstraints);
					// TODO add the constraints that are defined in the model (mmt 06-Nov-2019)
				}
			} else {
				// This is a fallback to the older mechanism for constrained types, which has the sole purpose of defining aliases.
				constrainedTypeReference = referencesReader.getOptionValueReference(OptionEnum.TYPE.getName());
				if (constrainedTypeReference != null) {
					PrimitiveType constrainedPrimitiveType = convertWithOtherConverter(PrimitiveType.class, constrainedTypeReference.getReferencedObject());
					if (constrainedPrimitiveType != null) {
						PrimitiveTypeConstraints primitiveTypeConstraints = new PrimitiveTypeConstraints(primitiveType.getName() + "Constraints", constrainedPrimitiveType);
						primitiveType.setConstraints(primitiveTypeConstraints);
						// TODO add the constraints that are defined in the model (mmt 03-Aug-2023)
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result) {
            if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.getElementDefinition().getName().equals(BasicElementEnum.TYPE.getName())) {
            		Boolean optionAbstract = element.getOptionValueSettingsReader().getBooleanOptionValue(OptionEnum.ABSTRACT.getName());
                    if (element.getElementBody() instanceof ElementBody) {
						ElementBody elementBody = (ElementBody) element.getElementBody();
                    	if (elementBody.getElementMembers() != null && elementBody.getTypedMembers().size() > 0 || optionAbstract != null && optionAbstract == true) {
                            // the type has members defined, in this case, we see the type as a complex type
                    		result = false;
                    	}
                    } else {
                    	// element has no body, so it does not have any members => a primitive type
                    }
            	} else {
            		result = false;
            	}
            }
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.typesystem.PrimitiveType result = null;

		PrimitiveTypeEnum primitiveTypeEnum = PrimitiveTypeEnum.fromStringCaseInsensitive(element.getName());
		if (primitiveTypeEnum != null) {
			if (primitiveTypeEnum.getPrimitiveType() == null) {
				throw new ModelConverterException("primitive type enum '" + primitiveTypeEnum + "' does not return a primitive type instance");
			}
		    result = primitiveTypeEnum.getPrimitiveType();
		} else {
			// primitive type from incoming model is not known, at least create a new technology-agnostic model element for it
			result = (com.gs.gapp.metamodel.basic.typesystem.PrimitiveType) getModelConverter().
					    getModelElementCache().findModelElement(element.getName(),
					                                            com.gs.gapp.metamodel.basic.typesystem.PrimitiveType.class);
			if (result == null) {
//				System.out.println("creating primitive type for '" + originalModelElement.getName() + "'");
				result = new com.gs.gapp.metamodel.basic.typesystem.PrimitiveType(element.getName());
				getModelConverter().getModelElementCache().add(result);
			}
		}

		return (T) result;
	}

	@Override
	public IMetatype getMetatype() {
		return BasicElementEnum.TYPE;
	}
}
