/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import org.eclipse.emf.common.util.BasicEList;

import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public class BasicModuleConverter<S extends Module, T extends com.gs.gapp.metamodel.basic.Module>
    extends ModuleConverter<S, T> {

	public BasicModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public BasicModuleConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T basicModule) {

		super.onConvert(module, basicModule);


		Namespace namespace = new Namespace(module.getNamespace().getName());
		basicModule.setNamespace(namespace);

		// --- and here convert primitive and complex types
		for (Element element : module.getElements()) {
			PrimitiveType primitiveType =
					this.convertWithOtherConverter(PrimitiveType.class, element);

			if (primitiveType != null) {
				basicModule.addElement(primitiveType);
				namespace.addElement(primitiveType);
			} else {
				ComplexType complexType =
						this.convertWithOtherConverter(ComplexType.class, element);
				if (complexType != null) {
				    basicModule.addElement(complexType);
				    namespace.addElement(complexType);
				} else {
					// try to convert to an enumeration
					Enumeration enumeration =
							this.convertWithOtherConverter(Enumeration.class, element);
					if (enumeration != null) {
					    basicModule.addElement(enumeration);
					    namespace.addElement(enumeration);
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Module) {
				Module module = (Module) originalModelElement;

				if (module.hasElementsOfTypes( new BasicEList<>(BasicElementEnum.getElementTypeNames()) ) == false) {
					// only if the module has at least one basic element type, it is recognized as a basic module
					generalResponsibility = false;
				}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S module, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.Module result =
				new com.gs.gapp.metamodel.basic.Module(module.getName());
		result.setOriginatingElement(new GappModelElementWrapper(module));
		if (module.getModuleTypesReader().containsModuleType("Asset")) {
			result.setGenerated(false);
		}
		return (T) result;
	}
}
