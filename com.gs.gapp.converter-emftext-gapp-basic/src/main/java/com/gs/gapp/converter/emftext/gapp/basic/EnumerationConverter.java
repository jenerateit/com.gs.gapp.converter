/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class EnumerationConverter<S extends Element, T extends com.gs.gapp.metamodel.basic.typesystem.Enumeration>
    extends ComplexTypeConverter<S, T> {

	public EnumerationConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T enumeration) {
		super.onConvert(element, enumeration);

		for (ElementMember member : element.getElementMembers()) {
			if (member.getMemberDefinition().getName().equalsIgnoreCase(BasicMemberEnum.ENTRY.getName())) {
				EnumerationEntry enumEntry = new EnumerationEntry(member.getName());
				if (member.getComment() != null) {
				    String comment = member.getComment().getComment();
				    if (comment != null) enumEntry.setBody(comment.substring(2, comment.length()-2).replaceAll("\\*[ ]?", ""));
				}
			    enumeration.addEnumerationEntry(enumEntry);
			}
		}
		
		// finally, we have to make sure that an enumeration's fields are read-only
		enumeration.getFields().stream().forEach(field -> field.setReadOnly(Boolean.TRUE));
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.isTypeof(BasicElementEnum.ENUMERATION.getName())) {
                    // yes, it is an enumeration, we are going to convert it
            	} else {
            		generalResponsibility = false;
            	}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S element, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.typesystem.Enumeration result =
				new com.gs.gapp.metamodel.basic.typesystem.Enumeration(element.getName());
		result.setOriginatingElement(new GappModelElementWrapper(element));
		return (T) result;
	}

	@Override
	public IMetatype getMetatype() {
		return BasicElementEnum.ENUMERATION;
	}
}
