/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.language.gapp.options.GappOptionValueReference;
import com.gs.gapp.language.gapp.options.GappOptionValueReferencesReader;
import com.gs.gapp.language.gapp.options.OptionDefinition;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionReference;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
@Deprecated
public class OptionReferenceConverter<S extends GappOptionValueReference, T extends com.gs.gapp.metamodel.basic.options.OptionDefinition<?>.OptionValue>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public OptionReferenceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {
        super.onConvert(originalModelElement, resultingModelElement);

        // nothing to be done here so far
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.basic.EMFTextToBasicModelElementConverter#onCreateModelElement(com.gs.gapp.language.basic.ModelElement)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		T result = null;

        OptionDefinition optionDefinition = originalModelElement.getOptionDefinition();
        GappOptionValueReferencesReader reader = new GappOptionValueReferencesReader( (List<GappOptionValueReference>) Collections.singletonList(originalModelElement) );

		switch (optionDefinition.getOptionType()) {
		case REFERENCE:
			com.gs.gapp.metamodel.basic.options.OptionDefinition<OptionReference> optionDefinitionReference =
		        this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.options.OptionDefinition.class, optionDefinition);

			EList<OptionValueReference> optionValueReferences = reader.getOptionValueReferences(optionDefinition.getName());
			if (optionValueReferences != null) {
				List<OptionReference> optionReferences = new ArrayList<>();
				int counter = 0;
				for (OptionValueReference optionValueReference : optionValueReferences) {
					ModelElement referencedObject = optionValueReference.getReferencedObject();
					Collection<com.gs.gapp.metamodel.basic.ModelElement> modelElements =
							this.convertWithOtherConverters(com.gs.gapp.metamodel.basic.ModelElement.class, referencedObject);
					if (modelElements != null) {
						com.gs.gapp.metamodel.basic.ModelElement modelElement = null;
						if (modelElements.size() == 0) {
							throw new ModelConverterException("was not able to convert referenced model element", new GappModelElementWrapper(referencedObject));
					    } else if (modelElements.size() == 1) {
							modelElement = modelElements.iterator().next();
						} else {
							if (referencedObject instanceof Module) {
								@SuppressWarnings("unused")
								Module referencedModule = (Module) referencedObject;

								for (com.gs.gapp.metamodel.basic.ModelElement potentialMatchingModelElement : modelElements) {
									if (potentialMatchingModelElement.getClass().getSimpleName().toLowerCase().contains(optionDefinition.getTypeOfReferenceTypeFilter().getPattern().toLowerCase())) {
										modelElement = potentialMatchingModelElement;
										break;
									}
								}
							} else {
								// we cannot handle this situation at the moment (mmt 15-May-2013)
							}

							if (modelElement == null) {
								throw new ModelConverterException("was not able to convert a referenced model element to a single model element", new GappModelElementWrapper(referencedObject));
							}
						}

						Collection<String> referencedElementSettings = new ArrayList<>();
						if (optionValueReference.getReferenceEnumerationValues() != null) {
							for (OptionEnumerationEntry setting : optionValueReference.getReferenceEnumerationValues()) {
								referencedElementSettings.add(setting.getEntry());
							}
						}
					    OptionReference optionReference = new OptionReference("option-reference" + counter, modelElement, referencedElementSettings);
					    optionReferences.add(optionReference);
					    counter++;
					}
				}

				result = (T) optionDefinitionReference.new OptionValue(optionReferences);
			}


			break;

		case ENUMERATED:
		case LOGICAL_VALUE:
		case NUMERIC:
		case TEXT:
		case QUOTED_TEXT:
			// these are handled in the OptionValueConverter
			break;
		default:
			throw new ModelConverterException("unknown option type '" + originalModelElement.getOptionDefinition().getOptionType() + "' in model element converter");
		}


		return result;
	}
}
