package com.gs.gapp.converter.emftext.gapp.basic;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum GappBasicConverterMessage implements MessageI {

	UNKNOWN_ERROR (MessageStatus.ERROR,
			"0001",
			"An unknown error has occurred: %s",
			null),
	KEY_TYPE_NOT_CONVERTABLE (MessageStatus.ERROR,
			"0002",
			"Could not successfully convert %s to a type for the key-value type mapping for the field %s.",
			"Change the type that has been set for the option 'KeyType'."),
	
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "GAPP-BASIC-CONVERTER";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private GappBasicConverterMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 