/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class ExceptionTypeConverter<S extends Element, T extends com.gs.gapp.metamodel.basic.typesystem.ExceptionType>
    extends ComplexTypeConverter<S, T> {

	public ExceptionTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {

		super.onConvert(originalModelElement, resultingModelElement);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.isTypeof(BasicElementEnum.EXCEPTION.getName())) {
                    // yes, it is an exception, we are going to convert it
            	} else {
            		generalResponsibility = false;
            	}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.typesystem.ExceptionType result =
				new com.gs.gapp.metamodel.basic.typesystem.ExceptionType(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return (T) result;
	}

	@Override
	public IMetatype getMetatype() {
		return BasicElementEnum.EXCEPTION;
	}
}
