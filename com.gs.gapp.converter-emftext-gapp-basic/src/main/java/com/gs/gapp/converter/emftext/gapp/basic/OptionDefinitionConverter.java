/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.language.gapp.options.OptionDefinition;
import com.gs.gapp.language.gapp.options.OptionEnumerationEntry;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class OptionDefinitionConverter<S extends OptionDefinition, T extends com.gs.gapp.metamodel.basic.options.OptionDefinition<? extends Serializable>>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public OptionDefinitionConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// nothing to be done here so far

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.basic.EMFTextToBasicModelElementConverter#onCreateModelElement(com.gs.gapp.language.basic.ModelElement)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		T result = null;

		switch (originalModelElement.getOptionType()) {
		case ENUMERATED:
			List<String> values = new ArrayList<>();
			for (OptionEnumerationEntry enumEntry : originalModelElement.getEnumeratedValues()) {
				values.add(enumEntry.getEntry());
			}
			result = (T) new com.gs.gapp.metamodel.basic.options.OptionDefinitionString(originalModelElement.getName(),
					                                                                      originalModelElement.getDocumentation() != null ? originalModelElement.getDocumentation().getText() : "not documented",
																		                  originalModelElement.getMandatory() == null ? false : originalModelElement.getMandatory(),
																		                  originalModelElement.getDefaultValue(),
																		                  values,
																		                  originalModelElement.getMultivalued() == null ? false : originalModelElement.getMultivalued());
			break;
		case LOGICAL_VALUE:
			result = (T) new com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean(originalModelElement.getName(),
					                                                                    originalModelElement.getDocumentation() != null ? originalModelElement.getDocumentation().getText() : "not documented",
					                                                                    originalModelElement.getMandatory() == null ? false : originalModelElement.getMandatory(),
																	                    originalModelElement.getDefaultValue() == null ? null : Boolean.parseBoolean(originalModelElement.getDefaultValue()),
															                    		null,
															                    		originalModelElement.getMultivalued() == null ? false : originalModelElement.getMultivalued());
			break;
		case NUMERIC:
			result = (T) new com.gs.gapp.metamodel.basic.options.OptionDefinitionLong(originalModelElement.getName(),
					                                                                    originalModelElement.getDocumentation() != null ? originalModelElement.getDocumentation().getText() : "not documented",
					                                                                    originalModelElement.getMandatory() == null ? false : originalModelElement.getMandatory(),
					                                                                    originalModelElement.getDefaultValue() == null ? null : Long.parseLong(originalModelElement.getDefaultValue()),
			                                                                    		null,
			                                                                    		originalModelElement.getMultivalued() == null ? false : originalModelElement.getMultivalued());
			break;
		case TEXT:
		case QUOTED_TEXT:
			result = (T) new com.gs.gapp.metamodel.basic.options.OptionDefinitionString(originalModelElement.getName(),
					                                                                      originalModelElement.getDocumentation() != null ? originalModelElement.getDocumentation().getText() : "not documented",
					                                                                      originalModelElement.getMandatory() == null ? false : originalModelElement.getMandatory(),
					                                                                      originalModelElement.getDefaultValue(),
					                                                                      null,
					                                                                      originalModelElement.getMultivalued() == null ? false : originalModelElement.getMultivalued());
			break;
		case REFERENCE:
			result = (T) new com.gs.gapp.metamodel.basic.options.OptionDefinitionString(originalModelElement.getName(),
					                                                                    originalModelElement.getDocumentation() != null ? originalModelElement.getDocumentation().getText() : "not documented",
																	                    originalModelElement.getMandatory() == null ? false : originalModelElement.getMandatory(),
																	                    originalModelElement.getDefaultValue(),
																	                    null,
																	                    originalModelElement.getMultivalued() == null ? false : originalModelElement.getMultivalued());
			break;
		default:
			throw new ModelConverterException("unknown option type '" + originalModelElement.getOptionType() + "' in model element converter");
		}


		return result;
	}
}
