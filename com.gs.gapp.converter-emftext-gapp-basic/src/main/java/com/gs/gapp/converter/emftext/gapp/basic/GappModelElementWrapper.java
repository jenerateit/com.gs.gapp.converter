package com.gs.gapp.converter.emftext.gapp.basic;

import org.eclipse.emf.ecore.EObject;

import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;

public class GappModelElementWrapper extends ModelElementWrapper {

	private static final long serialVersionUID = -6695510545219422458L;

	public GappModelElementWrapper(EObject eObject) {
		super(eObject);
//		super(new Object());  // an attempt while chasing the memory leak TODO remove later
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getWrappedElement()
	 */
	@Override
	public ModelElement getWrappedElement() {
		if (super.getWrappedElement() instanceof ModelElement) {
		    return (ModelElement) super.getWrappedElement();
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getId()
	 */
	@Override
	public String getId() {
		String prefix = "no-prefix";
		String qualifiedName = getWrappedElement().getName();
		if (getWrappedElement() instanceof Element) {
			Element element = (Element) getWrappedElement();
            prefix = element.getElementDefinition().getName();
            qualifiedName = getWrappedElement().getNameWithPrefix(2);
		} else if (getWrappedElement() instanceof ElementMember) {
			ElementMember elementMember = (ElementMember) getWrappedElement();
			prefix = elementMember.getMemberDefinition().getName();
			qualifiedName = getWrappedElement().getNameWithPrefix(2);
		} else if (getWrappedElement() instanceof Module) {
			@SuppressWarnings("unused")
			Module module = (Module) getWrappedElement();
            prefix = "module";
            qualifiedName = getWrappedElement().getNameWithPrefix(2);
		}

		return new StringBuilder(prefix.replace(".", "_").replace("$", "_").replace("-", "_")).append("_").append(qualifiedName.replace(".", "_").replace("-", "_").replace("$", "_")).toString();
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#getName()
	 */
	@Override
	public String getName() {
		if (getWrappedElement() == null) {
			return null;
		} else {
			if (getWrappedElement() instanceof com.gs.gapp.language.gapp.ModelElement) {
				com.gs.gapp.language.gapp.ModelElement modelElement = getWrappedElement();
				return modelElement.getName();
			} else {
				return  super.getName();
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementWrapper#getType()
	 */
	@Override
	public String getType() {
		String typeName = super.getType();
		if (getWrappedElement() instanceof Element) {
			Element element = (Element) getWrappedElement();
            typeName = element.getElementDefinition().getName();
		} else if (getWrappedElement() instanceof ElementMember) {
			ElementMember elementMember = (ElementMember) getWrappedElement();
			typeName = elementMember.getMemberDefinition().getName();
		} else if (getWrappedElement() instanceof Module) {
			@SuppressWarnings("unused")
			Module module = (Module) getWrappedElement();
			typeName = "module";
		}

		return typeName;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#getModule()
	 */
	@Override
	public String getModuleName() {
		String moduleName = null;
		if (getWrappedElement() instanceof Element) {
			Element element = (Element) getWrappedElement();
            moduleName = ((Module)element.eContainer()).getName();
		} else if (getWrappedElement() instanceof ElementMember) {
			ElementMember elementMember = (ElementMember) getWrappedElement();
			moduleName = ((Module)elementMember.getMemberBody().eContainer().eContainer().eContainer().eContainer()).getName();
		} else if (getWrappedElement() instanceof Module) {
			Module module = (Module) getWrappedElement();
			moduleName = module.getName();
		}

		return moduleName;
	}
}
