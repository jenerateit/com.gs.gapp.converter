/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import java.math.BigDecimal;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.dsl.basic.MultiplicityType;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.typesystem.BidirectionalRelationshipType;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ConverterMessage;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public class FieldConverter<S extends ElementMember, T extends com.gs.gapp.metamodel.basic.typesystem.Field>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public FieldConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	
	

	public FieldConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S elementMember,
			T field) {

		super.onConvert(elementMember, field);

		// --- field type
		final Element memberType = elementMember.getMemberType();
		if (memberType != null) {
			final com.gs.gapp.metamodel.basic.typesystem.Type type = convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Type.class, memberType);
			if (type != null) {
			    field.setType(type);
			} else {
				throw new ModelConverterException("was not able to convert the type '" + memberType + "'", elementMember);
			}
		} else {
			// there was no member-type modeled, which could have been made on purpose (e.g. for automated derivation of the type according to option settings)
			// TODO what should we do in this case?
		}

		// --- field options
		String collectionType =
		    elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(BasicOptionEnum.COLLECTION_TYPE.getName());

		if (collectionType == null || collectionType.length() == 0) {
			// no collection type set
		} else {
			field.setCollectionType(CollectionType.fromString(collectionType));
		}

		Boolean relevantForEquality =
		    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.EQUALITY_RELEVANCE.getName());
		if (relevantForEquality != null) {
			field.setRelevantForEquality(relevantForEquality);
		}

		Boolean readOnly =
			    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.READ_ONLY.getName());
		if (readOnly != null) {
			field.setReadOnly(readOnly);
		}

		// --- default value
		String defaultValue =
		    elementMember.getOptionValueSettingsReader().getTextOptionValue(BasicOptionEnum.DEFAULT_VALUE.getName());
		if (defaultValue != null) {
			field.setDefaultValue(defaultValue);
		}
		
		// --- length
		Long length =
	        elementMember.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.LENGTH.getName());
		if (length != null) {
			field.setLength((int)length.longValue());
		}
		
		// --- mandatory
		Boolean mandatory =
		    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.MANDATORY.getName());
		if (mandatory != null) {
			field.setNullable(mandatory ? false : true);
		}
		
		// --- minimum length
		Long minLength =
	        elementMember.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.MINIMUM_LENGTH.getName());
		if (minLength != null) {
			field.setMinLength((int)minLength.longValue());
		}
		
		// --- multiplicity type
		String multiplicityType = elementMember.getOptionValueSettingsReader().getEnumeratedOptionValue(BasicOptionEnum.MULTIPLICITY_TYPE.getName());
				
		if (multiplicityType != null) {
			MultiplicityType multiplicityTypeEnumEntry = MultiplicityType.getFromName(multiplicityType);
			if (multiplicityTypeEnumEntry != null) {
				switch (multiplicityTypeEnumEntry) {
				case M_TO_N:
					field.setBidirectionalRelationshipType(BidirectionalRelationshipType.MANY_TO_MANY);
					break;
				case N_TO_ONE:
					field.setBidirectionalRelationshipType(BidirectionalRelationshipType.MANY_TO_ONE);
					break;
				case ONE_TO_N:
					field.setBidirectionalRelationshipType(BidirectionalRelationshipType.ONE_TO_MANY);
					break;
				case ONE_TO_ONE:
					field.setBidirectionalRelationshipType(BidirectionalRelationshipType.ONE_TO_ONE);
					break;
				default:
					throw new ModelConverterException("unhandled enumeration entry '" + multiplicityTypeEnumEntry + "' found", elementMember);
				}
			}
		}

		// --- cardinality
		Long cardinality = elementMember.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.CARDINALITY.getName());
		if (cardinality != null) {
		    field.setCardinality(cardinality);
		}
		
		// --- maximum value
		Long maximumValue =
	        elementMember.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.MAXIMUM_VALUE.getName());
		if (maximumValue != null) {
			field.setMaximumValue((int)maximumValue.longValue());
		}
		
		// --- minimum value
		Long minimumValue =
	        elementMember.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.MINIMUM_VALUE.getName());
		if (minimumValue != null) {
			field.setMinimumValue((int)minimumValue.longValue());
		}
		
		// --- maximum decimal value
		String maximumDecimalValue =
	        elementMember.getOptionValueSettingsReader().getTextOptionValue(BasicOptionEnum.MAXIMUM_DECIMAL_VALUE.getName());
		if (maximumDecimalValue != null && maximumDecimalValue.length() > 0) {
			field.setMaximumDecimalValue(new BigDecimal(maximumDecimalValue));
		}

		// --- minimum decimal value
		String minimumDecimalValue =
	        elementMember.getOptionValueSettingsReader().getTextOptionValue(BasicOptionEnum.MINIMUM_DECIMAL_VALUE.getName());
		if (minimumDecimalValue != null && minimumDecimalValue.length() > 0) {
			field.setMinimumDecimalValue(new BigDecimal(minimumDecimalValue));
		}
		
		// --- mandatory
		Boolean expandable =
		    elementMember.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.EXPANDABLE.getName());
		if (expandable != null) {
			field.setExpandable(expandable);
		}
		
		// --- externalize
		Boolean externalize = elementMember.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.EXTERNALIZABLE.getName());
		if (externalize != null) {
			field.setExternalizable(externalize);
		}
		
		// --- number of bytes
		Long numberOfBytes =
	        elementMember.getOptionValueSettingsReader().getNumericOptionValue(BasicOptionEnum.NUMBER_OF_BYTES.getName());
		if (numberOfBytes != null) {
			field.setNumberOfBytes(numberOfBytes);
		}
		
		// --- key type
		if (field.getCollectionType() == CollectionType.KEYVALUE) {
		    OptionValueReference optionValueReference =
		    		elementMember.getOptionValueReferencesReader().getOptionValueReference(BasicOptionEnum.KEYTYPE.getName());
			if (optionValueReference != null && optionValueReference.getReferencedObject() != null) {
				Type keyType = convertWithOtherConverter(Type.class, optionValueReference.getReferencedObject());
				if (keyType != null) {
				    field.setKeyType(keyType);
				} else {
					Message message = GappBasicConverterMessage.KEY_TYPE_NOT_CONVERTABLE.getMessage(optionValueReference.getReferencedObject(),
							                                                                        field.getQualifiedName());
					throw new ModelConverterException(message.getMessage(), elementMember);
				}
			} else {
				field.setKeyType(PrimitiveTypeEnum.STRING.getPrimitiveType());
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (previousResultingModelElement == null) {
			return false;
		} else {
			if (result) {
				if (originalModelElement instanceof ElementMember) {
					ElementMember elementMember = (ElementMember) originalModelElement;
	            	if (elementMember.isTypeof(BasicMemberEnum.FIELD.getName())) {
	                    // yes, it is a field, we are going to convert it
	            	} else {
	            		result = false;
	            	}
	            }
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S elementMember, ModelElementI previousResultingModelElement) {
		ComplexType owner = null;

		if (previousResultingModelElement != null && previousResultingModelElement instanceof ComplexType) {
			owner = (ComplexType) previousResultingModelElement;
		}

		com.gs.gapp.metamodel.basic.typesystem.Field result =
				new com.gs.gapp.metamodel.basic.typesystem.Field(elementMember.getName(), owner);
		result.setOriginatingElement(new GappModelElementWrapper(elementMember));
		return (T) result;
	}



	@Override
	public IMetatype getMetatype() {
		return BasicMemberEnum.FIELD;
	}
}
