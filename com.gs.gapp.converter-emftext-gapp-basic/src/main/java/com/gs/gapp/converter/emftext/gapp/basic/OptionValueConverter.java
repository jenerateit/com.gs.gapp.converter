/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import java.util.Collections;
import java.util.List;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.language.gapp.options.GappOptionValueSetting;
import com.gs.gapp.language.gapp.options.GappOptionValueSettingsReader;
import com.gs.gapp.language.gapp.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class OptionValueConverter<S extends GappOptionValueSetting, T extends com.gs.gapp.metamodel.basic.options.OptionDefinition<?>.OptionValue>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public OptionValueConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {
        super.onConvert(originalModelElement, resultingModelElement);

        // nothing to be done here so far
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.emftext.basic.EMFTextToBasicModelElementConverter#onCreateModelElement(com.gs.gapp.language.basic.ModelElement)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		T result = null;

        OptionDefinition optionDefinition = originalModelElement.getOptionDefinition();
        GappOptionValueSettingsReader reader = new GappOptionValueSettingsReader( (List<GappOptionValueSetting>) Collections.singletonList(originalModelElement) );

		switch (optionDefinition.getOptionType()) {
		case ENUMERATED:
			com.gs.gapp.metamodel.basic.options.OptionDefinition<String> optionDefinitionEnumerated =
		        this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.options.OptionDefinition.class, optionDefinition);

			result = (T) optionDefinitionEnumerated.new OptionValue(reader.getEnumeratedOptionValues(optionDefinition.getName()));
			break;
		case LOGICAL_VALUE:
			com.gs.gapp.metamodel.basic.options.OptionDefinition<Boolean> optionDefinitionLogical =
			    this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.options.OptionDefinition.class, optionDefinition);

			result = (T) optionDefinitionLogical.new OptionValue(reader.getBooleanOptionValues(optionDefinition.getName()));
			break;
		case NUMERIC:
			com.gs.gapp.metamodel.basic.options.OptionDefinition<Long> optionDefinitionNumeric =
		        this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.options.OptionDefinition.class, optionDefinition);

		    result = (T) optionDefinitionNumeric.new OptionValue(reader.getNumericOptionValues(optionDefinition.getName()));
			break;
		case TEXT:
		case QUOTED_TEXT:
			com.gs.gapp.metamodel.basic.options.OptionDefinition<String> optionDefinitionText =
    		    this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.options.OptionDefinition.class, optionDefinition);

			result = (T) optionDefinitionText.new OptionValue(reader.getTextOptionValues(optionDefinition.getName()));
			break;
		default:
			throw new ModelConverterException("unknown option type '" + originalModelElement.getOptionDefinition().getOptionType() + "' in model element converter");
		}


		return result;
	}
}
