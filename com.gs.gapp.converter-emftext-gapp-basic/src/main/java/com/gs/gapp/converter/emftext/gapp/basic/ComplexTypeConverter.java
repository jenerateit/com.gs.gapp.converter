/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.OptionEnum;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.dsl.basic.BinaryCodingEndiannessEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementBody;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Endianness;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public class ComplexTypeConverter<S extends Element, T extends com.gs.gapp.metamodel.basic.typesystem.ComplexType>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public ComplexTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S element,
			T complexType) {

		super.onConvert(element, complexType);

		// the following id is just a test to find a way to always return the same id here
		@SuppressWarnings("unused")
		long id = element.getCounterId();

		// --- parent
		if (element.getParent() != null && element.getParent().getElementDefinition().hasParentDefinition(BasicElementEnum.TYPE.getName())) {
			com.gs.gapp.metamodel.basic.typesystem.ComplexType parent =
					convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.ComplexType.class,
							                  element.getParent());
			if (parent != null) {
				complexType.setParent(parent);
			} else {
				// TODO A type with no members will be converted to a PrimitiveType, which cannot be a parent of a complex type. Should we really throw an exception in this case? (mmt 02-May-2013)
				throw new ModelConverterException("could not successfully convert a basic DSL's complex type '" + element + "' to a technology-agnostic complex type model element");
			}
		}

		// --- abstractness
		Boolean optionAbstract = element.getOptionValueSettingsReader().getBooleanOptionValue(OptionEnum.ABSTRACT.getName());
		if (optionAbstract != null && optionAbstract == true) {
			complexType.setAbstractType(true);
		}

		// --- finally, convert all element members (here we handle fields only)
		for (ElementMember member : element.getElementMembers()) {
			@SuppressWarnings("unused")
			com.gs.gapp.metamodel.basic.typesystem.Field field =
					this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.typesystem.Field.class,
							                       member, complexType);
		}
		
		// --- externalize
		Boolean externalize = element.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.EXTERNALIZABLE.getName());
		if (externalize != null) {
			complexType.setExternalizable(externalize);
		}
		
		// --- binary coding
		Boolean binaryCoding = element.getOptionValueSettingsReader().getBooleanOptionValue(BasicOptionEnum.BINARY_CODING.getName());
		if (binaryCoding != null) {
			complexType.setBinaryCoding(binaryCoding);
		}
		
		// --- binary coding endianness
		String endiannessAsString = element.getOptionValueSettingsReader().getEnumeratedOptionValue(BasicOptionEnum.BINARY_CODING_ENDIANNESS.getName());
		if ( endiannessAsString != null) {
			BinaryCodingEndiannessEnum enumInModel = BinaryCodingEndiannessEnum.forName(endiannessAsString);
			if (enumInModel != null) {
			    complexType.setBinaryCodingEndianness(Endianness.valueOf(enumInModel.name()));
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generallyResponsible = super.generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generallyResponsible) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
				Boolean optionAbstract = element.getOptionValueSettingsReader().getBooleanOptionValue(OptionEnum.ABSTRACT.getName());
				if (element.getElementDefinition().getName().equalsIgnoreCase(BasicElementEnum.TYPE.getName())) {
                    if (element.getElementBody() instanceof ElementBody) {
						ElementBody elementBody = (ElementBody) element.getElementBody();
                    	if ((elementBody.getElementMembers() == null || elementBody.getTypedMembers().size() == 0) && (optionAbstract == null || optionAbstract == false)) {
                            // the type has no members defined, in this case, we see the type as a primitive type, not a complex type
                    		generallyResponsible = false;
                    	}
                    } else {
                    	// element has no body, so it does not have any members => a primitive type, not a complex type
                    	generallyResponsible = false;
                    }
            	} else {
            		generallyResponsible = false;
            	}
            }
		}

		return generallyResponsible;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyResultingElement) {
		com.gs.gapp.metamodel.basic.typesystem.ComplexType result =
				new com.gs.gapp.metamodel.basic.typesystem.ComplexType(originalModelElement.getName());
		result.setOriginatingElement(new GappModelElementWrapper(originalModelElement));
		return (T) result;
	}

	@Override
	public IMetatype getMetatype() {
		return BasicElementEnum.TYPE;
	}
}
