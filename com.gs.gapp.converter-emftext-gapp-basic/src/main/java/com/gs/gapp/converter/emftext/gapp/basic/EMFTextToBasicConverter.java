/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.language.gapp.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class EMFTextToBasicConverter extends AbstractAnalyticsConverter {

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#findMandatoryElements(java.util.Collection)
	 */
	@Override
	protected Set<Object> findMandatoryElements(Collection<Object> rawElements) {
		Set<Object> result = super.findMandatoryElements(rawElements);
		
		boolean isModelFound = false;
		for (Object element : rawElements) {
			if (element instanceof Model) {
				isModelFound = true;
				break;
			}
		}
		
		if (!isModelFound) {
			// Belatedly creating an instance of type Model since the previous transformation step
			// did not provide such an instance in its resulting collection of model elements.
			// This is required to be able to trigger certain model element converters (ModelTo...Converter).
			Model initialModel = new Model("InitialModel");
			result.add(initialModel);
		}
		return result;
	}

	/**
	 * 
	 */
	public EMFTextToBasicConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new BasicModuleConverter<>(this));
		result.add(new EnumerationConverter<>(this));
		result.add(new ExceptionTypeConverter<>(this));
		result.add(new ComplexTypeConverter<>(this));
		result.add(new PrimitiveTypeConverter<>(this));

		result.add(new OptionDefinitionConverter<OptionDefinition, com.gs.gapp.metamodel.basic.options.OptionDefinition<?>>(this));
		result.add(new OptionValueConverter<>(this));
		// Note that option value references do not need to be converted with its own element converter. Instead, the objects that references point to
		// are going to be converted to new model elements. (mmt 19-Sep-2013)
//		result.add(new OptionReferenceConverter<GappOptionValueReference, com.gs.gapp.metamodel.basic.options.OptionDefinition<?>.OptionValue>(this));

		result.add(new FieldConverter<>(this));
		
		result.add(new VersionConverter<>(this));


		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		Set<Object> intermediateResult = super.onPerformModelConsolidation(normalizedElements);
		
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		for (Object obj : intermediateResult) {
			if (obj instanceof com.gs.gapp.metamodel.basic.options.OptionDefinition ||
					obj instanceof com.gs.gapp.metamodel.basic.options.OptionDefinition.OptionValue ||
					obj instanceof com.gs.gapp.metamodel.basic.typesystem.PrimitiveType) {
				// the idea is to never generate anything for these kind of model elements => remove them from the set of
				// model elements in order to improve performance and simplify the debugging process (mmt 04-Dec-2018)
			} else {
				result.add(obj);
			}
		}
		return result;
	}

	
}
