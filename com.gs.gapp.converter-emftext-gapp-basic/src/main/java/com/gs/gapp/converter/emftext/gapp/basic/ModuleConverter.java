/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public abstract class ModuleConverter<S extends Module, T extends com.gs.gapp.metamodel.basic.Module>
    extends EMFTextToBasicModelElementConverter<S, T> {

	public ModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public ModuleConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S module, T basicModule) {
		super.onConvert(module, basicModule);
	}
}
