/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import java.util.Collection;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IMetatyped;
import com.gs.gapp.dsl.OptionEnum;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementBody;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ElementMemberBody;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.language.gapp.definitions.ElementDefinition;
import com.gs.gapp.language.gapp.definitions.ModuleTypeDefinition;
import com.gs.gapp.language.gapp.options.GappOptionValueReferencesReader;
import com.gs.gapp.language.gapp.options.GappOptionValueSetting;
import com.gs.gapp.language.gapp.options.GappOptionValueSettingsReader;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Version;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public abstract class EMFTextToBasicModelElementConverter<S extends ModelElement, T extends com.gs.gapp.metamodel.basic.ModelElement>
    extends AbstractModelElementConverter<S, T> implements IMetatyped {

	public EMFTextToBasicModelElementConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	public EMFTextToBasicModelElementConverter(
			AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}

	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public EMFTextToBasicModelElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		
		// --- comment
		if (originalModelElement.getComment() != null) {
			String comment = originalModelElement.getComment().getComment();
		    if (comment != null && comment.length() > 0) {
		    	String strippedComment = comment.substring(2, comment.length()-2).replaceAll("\\*[ ]?", "");
		    	resultingModelElement.setBody(strippedComment);
		    	// TODO Activate the following for a minor version increment since it leads to many generated files being minimally modified. (mmt 12-Mar-2021)
		    	//resultingModelElement.setBody(removeTrailingSpaces(strippedComment));
		    }
		}

		if (originalModelElement instanceof Module) {
			Module module = (Module) originalModelElement;
			createAndAddOptionValues(module.getOptionValueSettingsReader(), resultingModelElement);
			createAndAddOptionReferences(module.getOptionValueReferencesReader(), resultingModelElement);
		} else if (originalModelElement instanceof Element) {
			Element element = (Element) originalModelElement;
			if (element.getElementBody() instanceof ElementBody) {
				ElementBody elementBody = (ElementBody) element.getElementBody();
				createAndAddOptionValues(elementBody.getOptionValueSettingsReader(), resultingModelElement);
				createAndAddOptionReferences(elementBody.getOptionValueReferencesReader(), resultingModelElement);
			}
		} else if (originalModelElement instanceof ElementMember) {
			ElementMember elementMember = (ElementMember) originalModelElement;
			if (elementMember.getMemberBody() instanceof ElementMemberBody) {
				ElementMemberBody memberBody = (ElementMemberBody) elementMember.getMemberBody();
				createAndAddOptionValues(memberBody.getOptionValueSettingsReader(), resultingModelElement);
				createAndAddOptionReferences(memberBody.getOptionValueReferencesReader(), resultingModelElement);
			}

			Module module = (Module) elementMember.eContainer().eContainer().eContainer();
			if (module != null) {
			    Collection<com.gs.gapp.metamodel.basic.Module> basicModules = this.convertWithOtherConverters(com.gs.gapp.metamodel.basic.Module.class, module);
			    if (basicModules.size() == 0) {
			    	// do nothing here
			    } else if (basicModules.size() == 1) {
			    	resultingModelElement.setModule(basicModules.iterator().next());
			    } else {
			    	for (ElementDefinition elementDefinition : elementMember.getMemberDefinition().getOwnerDefinitions()) {
			    		for (ModuleTypeDefinition moduleTypeDefinition : module.getModuleTypes()) {
			    			if (elementDefinition.getModuleTypesReader().containsModuleType(moduleTypeDefinition.getName())) {
				    			// -- TODO found a matching module type => find the right module to be set (mmt 23-Dec-2015)
				    		}
			    		}
			    	}
			    }
			} else {
				throw new ModelConverterException("could not find module for element member '" + elementMember + "'");
			}

		}
	}

	/**
	 * 
	 * @param referencesReader
	 * @param modelElement
	 */
	private void createAndAddOptionReferences(GappOptionValueReferencesReader referencesReader, T modelElement) {
		if (modelElement instanceof Version) return;  // don't version the version itself
		
		OptionValueReference versionReference = referencesReader.getOptionValueReference(BasicOptionEnum.VERSION.getName());
		if (versionReference != null) {
			Version version = convertWithOtherConverter(Version.class, versionReference.getReferencedObject());
			if (version != null) modelElement.setVersion(version);
		}
		
		versionReference = referencesReader.getOptionValueReference(BasicOptionEnum.SINCE_VERSION.getName());
		if (versionReference != null) {
			Version sinceVersion = convertWithOtherConverter(Version.class, versionReference.getReferencedObject());
			if (sinceVersion != null) modelElement.setSinceVersion(sinceVersion);
		}
		
		versionReference = referencesReader.getOptionValueReference(BasicOptionEnum.DEPRECATED_SINCE_VERSION.getName());
		if (versionReference != null) {
			Version deprecatedSinceVersion = convertWithOtherConverter(Version.class, versionReference.getReferencedObject());
			if (deprecatedSinceVersion != null) modelElement.setDeprecatedSinceVersion(deprecatedSinceVersion);
		}
	}
	
	/**
	 * @param settingsReader
	 * @param resultingModelElement
	 */
	private void createAndAddOptionValues(GappOptionValueSettingsReader settingsReader, T resultingModelElement) {
		for (GappOptionValueSetting setting : settingsReader.getSettings()) {
			com.gs.gapp.metamodel.basic.options.OptionDefinition<?>.OptionValue convertedOptionValue =
                this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.options.OptionDefinition.OptionValue.class, setting);
			if (convertedOptionValue != null) {
				resultingModelElement.addOptions(convertedOptionValue);
			} else {
				throw new ModelConverterException("Was not able to convert the option value setting element '" + setting + "'");
			}
		}
		
		// --- setting the sort value for any type of model element, in case there is one modeled (otherwise that value is ModelElement.DEFAULT_SORT_VALUE)
		Long sortValue = settingsReader.getNumericOptionValue(OptionEnum.SORT_VALUE.getName());
		if (sortValue != null) {
		    if (sortValue <= Integer.MAX_VALUE) {
			    resultingModelElement.setSortValue(sortValue.intValue());
		    } else {
		    	throw new ModelConverterException("value for option '" + OptionEnum.SORT_VALUE.getName() + "' must not be bigger than " + Integer.MAX_VALUE, resultingModelElement.getOriginatingElement());
		    }
		}
	}

	protected final boolean generalIsResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result && getMetatype() != null) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.isTypeof(getMetatype().getName())) {
                    // yes, it is an element of the given type, we are going to convert it
            	} else {
            		result = false;
            	}
            } else if (originalModelElement instanceof ElementMember) {
				ElementMember elementMember = (ElementMember) originalModelElement;
				if (elementMember.isTypeof(getMetatype().getName())) {
                    // yes, it is an element member of the given type, we are going to convert it
            	} else {
            		result = false;
            	}
            }
		}

		return result;

	}

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
	}


	/**
	 * @return
	 */
	@Override
	public IMetatype getMetatype() {
		return null;
	}
}
