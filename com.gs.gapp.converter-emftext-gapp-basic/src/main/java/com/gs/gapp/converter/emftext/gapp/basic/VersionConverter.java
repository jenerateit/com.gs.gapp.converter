/**
 *
 */
package com.gs.gapp.converter.emftext.gapp.basic;

import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Version;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * @author mmt
 *
 */
public class VersionConverter<S extends Element, T extends Version> extends EMFTextToBasicModelElementConverter<S, T> {

	public VersionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S element, T version) {
		super.onConvert(element, version);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean generalResponsibility = generalIsResponsibleFor(originalModelElement, previousResultingModelElement);
		@SuppressWarnings("unused")
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (generalResponsibility) {
			if (originalModelElement instanceof Element) {
				Element element = (Element) originalModelElement;
            	if (element.isTypeof(BasicElementEnum.VERSION.getName())) {
                    // yes, it is a version, we are going to convert it
            	} else {
            		generalResponsibility = false;
            	}
            }
		}

		return generalResponsibility;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S element, ModelElementI previouslyResultingElement) {
		String textOptionValue = element.getOptionValueSettingsReader().getTextOptionValue(BasicOptionEnum.VERSION_NUMBER.getName());
		T version = (T) new Version(element.getName(), textOptionValue == null || textOptionValue.length() == 0 ? "0" : textOptionValue);
		version.setOriginatingElement(new GappModelElementWrapper(element));
		return version;
	}

	@Override
	public IMetatype getMetatype() {
		return BasicElementEnum.VERSION;
	}
}
