/**
 *
 */
package com.gs.gapp.converter.emftext.gapp;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.dsl.OptionEnum;
import com.gs.gapp.language.gapp.Comment;
import com.gs.gapp.language.gapp.Element;
import com.gs.gapp.language.gapp.ElementMember;
import com.gs.gapp.language.gapp.ModelElement;
import com.gs.gapp.language.gapp.Module;
import com.gs.gapp.language.gapp.options.GappOptionValueReference;
import com.gs.gapp.language.gapp.options.OptionValueReference;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class EMFTextNormalizationConverter extends AbstractAnalyticsConverter {


	public EMFTextNormalizationConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelElementConversion(java.util.Set)
	 */
	@Override
	protected void onPerformModelElementConversion(Set<?> modelElements) {
		// super-call is not going to be executed since we do not use any model element converter here so far
		super.onPerformModelElementConversion(modelElements);

		for (Object modelElement : modelElements) {
			if (modelElement instanceof Module) {
				Module module = (Module) modelElement;
				TreeIterator<EObject> allContents = module.eAllContents();
				while (allContents.hasNext()) {
					EObject nextContent = allContents.next();
					if (nextContent instanceof Element) {
						Element element = (Element) nextContent;
						resolveAlias(element);
				    } else if (nextContent instanceof ElementMember) {
						ElementMember elementMember = (ElementMember) nextContent;
						resolveAlias(elementMember);
				    } else if (nextContent instanceof GappOptionValueReference) {
						GappOptionValueReference gappOptionValueReference = (GappOptionValueReference) nextContent;
						resolveAlias(gappOptionValueReference);
					}
				}
			}
		}
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(
			Set<?> normalizedElements) {
		Set<Object> result = super.onPerformModelConsolidation(normalizedElements);

		result.addAll(normalizedElements);
//		result.remove(getModel());
//		result.addAll(getModelElementCache().getAllModelElements());

		return result;
	}

	/**
	 * @param member
	 */
	private void resolveAlias(ElementMember member) {
		Element memberType = member.getMemberType();
		if (memberType != null) {
			AliasedElement aliasedElement = findAliasedType(memberType);
			Element actualMemberType = aliasedElement.getAliasedElement();
			member.setMemberType(actualMemberType);
			extendComment(member.getComment(), aliasedElement);
		} else {
			// this is not an error, a member may have not type set (e.g. enumeration)
		}
	}

	/**
	 * @param element
	 */
	private void resolveAlias(Element element) {
		Element parentType = element.getParent();
		if (parentType != null) {
			AliasedElement aliasedElement = findAliasedType(parentType);
			Element actualParentType = aliasedElement.getAliasedElement();
			element.setParent(actualParentType);
			extendComment(element.getComment(), aliasedElement);
		}
	}

	/**
	 * @param gappOptionValueReference
	 */
	private void resolveAlias(GappOptionValueReference gappOptionValueReference) {
		if (gappOptionValueReference == null) {
			return;
		}

		for (OptionValueReference optionValueReference : gappOptionValueReference.getOptionValueReference()) {
			ModelElement referencedObject = optionValueReference.getReferencedObject();
			if (referencedObject instanceof Element) {
				Element referencedType = (Element) referencedObject;
				AliasedElement aliasedElement = findAliasedType(referencedType);
				Element actualReferencedType = aliasedElement.getAliasedElement();
				
				if (gappOptionValueReference.getOptionDefinition().getName().equals(OptionEnum.TYPE.getName())) {
					// in this case we would replace the aliasing itself, which is not what we want here
				} else {
					optionValueReference.setReferencedObject(actualReferencedType);
					
					extendComment(gappOptionValueReference.getComment(), aliasedElement);
				}
			}
		}
	}
	
	/**
	 * @param comment
	 * @param aliasedElement
	 */
	private void extendComment(Comment comment, AliasedElement aliasedElement) {
		if (isAliasingInfoAttachedToComment() &&
				comment != null && aliasedElement.getAliasingChain() != null && aliasedElement.getAliasingChain().length() > 0) {
			if (comment.getComment() == null || comment.getComment().length() == 0) {
				comment.setComment("ALIAS: " + aliasedElement.getAliasingChain());
			} else {
				String newComment = comment.getComment().substring(0, comment.getComment().length()-2) + System.lineSeparator() + "ALIAS: " +  aliasedElement.getAliasingChain() + System.lineSeparator() + "*/";
				comment.setComment(newComment);
			}
		}
	}
	
	/**
	 * If there is a recurisvely modeled aliasing, this method resolves an alias
	 * until the final alias that refers to a type that itself isn't an alias.
	 * 
	 * @param type
	 * @return the aliased type
	 */
	private AliasedElement findAliasedType(Element type) {
		Element aliasedType = type;
		OptionValueReference linkedElementReference = null;
		Set<Element> aliases = new LinkedHashSet<>();  // this is used to identify infinite loops
		
		StringBuilder aliasingChain = new StringBuilder("'").append(type.getQualifiedName()).append("'");
		
		do {
			aliases.add(aliasedType);
			linkedElementReference = aliasedType.getOptionValueReferencesReader().getOptionValueReference(OptionEnum.TYPE.getName());
			if (linkedElementReference != null) {
				// there is an aliased type, check further
				Element actualType = (Element) linkedElementReference.getReferencedObject();
				aliasedType = actualType;
				aliasingChain.append(", alias for '").append(actualType.getQualifiedName()).append("'");
				
				if (aliases.contains(actualType)) {
					// detected an infinite loop (resursive modeling of type aliases)
					Message message = EMFTextNormalizationConverterMessage.INFINITE_RECURSION_IN_TYPE_ALIASING.getMessage(aliasingChain.toString());
					throw new ModelConverterException(message.getMessage(), type);
				}
				
			}
		} while (linkedElementReference != null);
		
		return new AliasedElement(aliasedType, aliasedType != type ? aliasingChain.toString() : null);
	}
	
	private class AliasedElement {
		private final Element aliasedElement;
		private final String aliasingChain;
		
		public AliasedElement(Element aliasedElement, String aliasingChain) {
			super();
			this.aliasedElement = aliasedElement;
			this.aliasingChain = aliasingChain;
		}
		/**
		 * @return the aliasedElement
		 */
		public Element getAliasedElement() {
			return aliasedElement;
		}
		/**
		 * @return the aliasingChain
		 */
		public String getAliasingChain() {
			return aliasingChain;
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();
		return result;
	}
	
	/**
	 * @return
	 */
	protected boolean isAliasingInfoAttachedToComment() {
		Serializable optionValue = this.getOptions().get(ConverterOption.ATTACH_ALIASING_INFO_TO_COMMENT.getName());
		return optionValue != null && optionValue.toString().equalsIgnoreCase("true");
	}
	
	public enum ConverterOption {
		
		ATTACH_ALIASING_INFO_TO_COMMENT ("emftext.normalization.attach-aliasing-info-to-comment"),
		;
		
		private final String name;
		
		private ConverterOption(String name) {
			this.name = name;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
	}
}
