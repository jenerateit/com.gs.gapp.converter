package com.gs.gapp.converter.emftext.gapp;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum EMFTextNormalizationConverterMessage implements MessageI {

	INFINITE_RECURSION_IN_TYPE_ALIASING (MessageStatus.ERROR,
			"0001",
			"An infinite recursion in type aliasing has been identified: %s",
			"Please remove the recursive type aliasing from your model."),
	
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "EMFTEXT_NORM_CONVERTER";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private EMFTextNormalizationConverterMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 